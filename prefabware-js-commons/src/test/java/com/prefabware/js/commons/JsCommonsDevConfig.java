package com.prefabware.js.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.prefabware.js.commons.intern.JsCommonsDojoConfig;
import com.prefabware.js.commons.intern.JsCommonsMavenConfig;
import com.prefabware.web.ResourceConfig;
import com.prefabware.web.ResourceMappings.ResourceMapping;

/**
 * use this config when running inside the IDE
 * to map the static resources of this project directly to the src/main/resources folder
 * the spring-boot-maven-plugin is configured with <addResources>true</addResources>
 * so this resources will be availalble on the classpath  
 * this together allows to make changes in static resources and to use them without build or restart of the server
 * just edit the resource, refresh your browser and changes will be active..
 * 
 * this config is meant to be used during development of this module only
 * it will only be activated, when the spring profile "dev" is active.
 *
 * @author stefan
 *
 */
@Import({JsCommonsDojoConfig.class,JsCommonsMavenConfig.class,ResourceConfig.class})
public class JsCommonsDevConfig {
	@Bean()
	ResourceMapping rmWebJarPrefabware() {
		return new ResourceMapping("/webjars/prefabware/**",
				"classpath:/prefabware/");
	}
	@Bean()
	ResourceMapping rmWebJarPrefabwareCommons() {
		return new ResourceMapping("/webjars/prefabware/commons/**",
				"classpath:/prefabware/commons/");
	}
	@Bean()
	ResourceMapping rmWebJarPrefabwareUtil() {
		return new ResourceMapping("/webjars/prefabware/util/**",
				"classpath:/prefabware/util/");
	}
}
