package com.prefabware.js.commons;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author stefan
 *
 */
@Configuration
@EnableAutoConfiguration
@Import({JsCommonsDevConfig.class})
public class SpringBootDevApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootDevApplication.class);
    }   

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDevApplication.class);
    }

}
