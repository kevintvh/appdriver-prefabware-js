package com.prefabware.js.commons.intern;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prefabware.commons.maven.Maven;

/**
 * this config should not be imported by other projects 
 * @author stefan
 *
 */
@Configuration
public class JsCommonsMavenConfig {
	@Bean
	Maven jsCommonsMaven() {
		//use the root common of this project + filtered		
		return new Maven("com.prefabware.js.commons.filtered");
	}	
}
