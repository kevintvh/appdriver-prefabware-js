package com.prefabware.js.commons.intern;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prefabware.commons.maven.Maven;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

/**
 * to configure the resources of the js commons as a webjar
 * @author stefan
 *
 */
@Configuration
public class JsCommonsWebJarConfig {
	@Bean
	WebJar jsCommonsWebJar(@Qualifier("jsCommonsMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	
	@Bean
	ResourceMapping rmWebJarPrefabwareCommons(@Qualifier("jsCommonsWebJar") WebJar wj) {
		String path = "prefabware/commons/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}

	@Bean
	ResourceMapping rmWebJarPrefabwareUtil(@Qualifier("jsCommonsWebJar") WebJar wj) {
		String path = "prefabware/util/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}

	@Bean
	ResourceMapping rmWebJarPrefabware(@Qualifier("jsCommonsWebJar") WebJar wj) {
		String path = "prefabware/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}	
}
