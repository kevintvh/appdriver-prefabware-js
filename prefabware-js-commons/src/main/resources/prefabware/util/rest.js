//functions for rest
define([ 'dojo', 'dojo/_base/lang'
         , 'dojo/_base/array'
         , 'dojox/collections/Dictionary'
         , 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo, lang, array) {
	var global = function() {

		this.selfLink = function(resource) {
			// returns the self link from the given REST resource
			// returns null if it does not exist
			// resource={
			// "name" : "Berlin",
			// "version" : 0,
			// "links" : [ {
			// "rel" : "self",
			// "href" : "http://localhost:8080/plugins-store/api/city/1"
			// }, {
			// "rel" : "country",
			// "href" : "http://localhost:8080/plugins-store/api/city/1/country"
			// } ]
			// };
			var links = resource.links;
			if (links == null) {
				return null;
			}
			var selfArray = dojo.filter(links, function(link) {
				return link.rel == 'self';
			});
			if (selfArray == null || selfArray.length == 0) {
				return null;
			}
			return selfArray[0];

		};
		this.linkMap = function(linkArray) {
			//returns a map with the links of the given array
			// key is link.rel
			var map=new dojox.collections.Dictionary();
			array.forEach(linkArray,function(link) {
				map.add(link.rel,link);
			});
			return map;
		};
		this.links = function(resource) {
			// returns an array with the link from the given REST resource
			// WITHOUT the self link
//			{
//				    "name" : "Frankfurt",
//				    "version" : 0,
//				    "size" : "L",
//				    "links" : [ {
//				      "rel" : "self",
//				      "href" : "http://localhost:8080/plugins-store/api/city/2"
//				    }, {
//				      "rel" : "country",
//				      "href" : "http://localhost:8080/plugins-store/api/city/2/country"
//				    } ]
//				  } 
//				}
			var links = resource.links;
			if (links == null) {
				return null;
			}
			return dojo.filter(links, function(link) {
				return link.rel != 'self';
			});
			
		};
		this.hrefId = function(href) {
			// returns the id from the given href
			// returns null if it does not exist
			// "href" == "http://localhost:8080/plugins-store/api/city/1"
			var segments = href.split('/');
			var id = segments[segments.length - 1];
			if (prefabware.lang.isNumber(id)) {
				return id;
			}
			return null;
		};

	};
	var instance = new global();
	lang.setObject("prefabware.util.rest", instance);
	return instance;
});