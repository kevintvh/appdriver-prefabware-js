//to build a Url
//or to parse a string with a url
//after parsing you use the build methods to change the parsed url
// like urlBuilder.fragment("fragment")
// you may use .clearQuery() to just reset the query part of the url
// you can receive the url as a string with urlBuilder.build()
// or urlBuilder.toString();

define('prefabware/util/UrlBuilder', [ 'dojo',  'dojo/_base/declare',"dojo/_base/url",'prefabware/lang' ], function(dojo) {
	dojo.declare("prefabware.util.UrlBuilder", null, {
		//	     scheme-specific-part →                        →                       → |
		//	     |
		//	http://hans:geheim@example.org:80/demo/example.cgi?land=de&stadt=aa#geschichte
		//	|      |    |      |           | |                 |                |
		//	|      |    |      host        | path              query            fragment
		//	|      |    password           port
		//	|      user
		//	scheme (hier gleich Netzwerkprotokoll)
		_scheme:"http",// http
		_user:"",
		_password:"",
		_host:"",
		_port:"",
		_path:"",// /a/b with leading '/', without trailing '/'
		_query:"",
		_fragment:"",
		constructor : function(options) {			
		},
		
		toString : function() {
			//the same as build
			return this.build();
		},
		build : function() {
			var url="";
			prefabware.lang.assert(this._notEmpty(this._scheme),"scheme=null is NOT allowed");			
			url=this._append(url,"",this._scheme,"_scheme");
			url=url+"://";
			url=this._append(url,"",this._user);
			
			prefabware.lang.assert(!(this._empty(this._user)&&this._notEmpty(this._password)),"a password is only allowed if the user is set");			
			url=this._append(url,":",this._password);
			if (this._notEmpty(this._user)) {
				url=url+"@";
			}
			//the path has its leading '/'
			//but if there is no path we have to add it
			url=this._append(url,"",this._host,"_host");
//			if (this._empty(this.path)) {
//				url=url+"/";
//			}
			
			url=this._append(url,":",this._port);
			
			url=this._append(url,"",this._path);
			url=this._append(url,"?",this._query);
			url=this._append(url,"#",this._fragment);	
			return url;
		},
		parse : function(url) {
			//parses the given url into this builder setting the values of all its parts
			//and returns this 
			var parsed=new dojo._Url(url);
			this.scheme(parsed.scheme);
			this.host(parsed.host);
			this.port(parsed.port);
			this.path(parsed.path);
			var user=parsed.user;
			var password=parsed.password;
			if (user==null&&password!=null) {
				//if there is only one of user and password, it should be parsed as the user
				//not the password
				this.user(password);
			}else{
			this.user(parsed.user);
			this.password(parsed.password);
			}
			
			var query=parsed.query;
			if (query!==null) {
				//the parsed query is a complete string like "land=de&stadt=berlin"
				//so do not add, but just set it as value
				this._query=query;
			}
			this.fragment(parsed.fragment);
			return this;
		},
		_notEmpty : function(value_p) {
			//return true if the string is!=null and is not empty
			if (value_p==null) {
				return false;
			}
			//the value may be a number, so convert to string
			var value=value_p.toString();
			return (value.length>0);
		},
		_empty : function(string) {
			//return true if the string is==null or is empty
			return !this._notEmpty(string);
		},
		_append : function(string,seperator,apendix,obligatoryProperty) {
			//if apendix contains a text :
			//1. _appends the seperator to the string
			//2. _appends the apendix to the string
			//if the apendix==null or "" returns the unchanged string
			//if the obligatoryProperty!=null the _append is obligatory, so when apendix does not contain a text, an exception is thrown
			if (apendix!=null&&apendix.length>0) {
				return string+seperator+apendix;
			}else{
				if (obligatoryProperty==null) {
					return string;
				}else{
					prefabware.lang.throwError(obligatoryProperty+" is obligatory but==null");
				}
			}
		},
		scheme : function(scheme) {
			prefabware.lang.assert(this._notEmpty(scheme),"scheme=null or empty is NOT allowed, scheme="+scheme);	
			if (prefabware.lang.endsWith(scheme,"://")) {
				this._scheme=scheme.substring(0,scheme.length-3);
			}else{
				this._scheme=scheme;
			}
			return this;
		},
		user : function(user) {
			this._user=user;
			return this;
		},
		password : function(password) {
			this._password=password;
			return this;
		},
		host : function(host) {
			this._host=host;
			return this;
		},
		port : function(port_p) { 
			if (this._empty(port_p)) {
				return this;
			}
			var port=port_p.toString();//port may be numeric, so convert it to string
			this._port=port;
			return this;
		},
		path : function(path_p) {
			if (this._empty(path_p)) {
				return this;
			}
			var path=path_p.toString();//path may be numeric,e.g. the id of a rest resource, so convert it to string
			//pasth must have a leading /
			//prepend one if has none
			if (path.substring(0,1)=='/') {
				this._path=path;
			}else{
				//prepend one if has none
				this._path='/'+path;
			}
			return this;
		},
		appendPath : function(path_p) {
			if (this._empty(this._path)) {
				//if this path is still empty, set it the provided as this path
				return this.path(path_p);
			}
			var path=path_p.toString();//path may be numeric,e.g. the id of a rest resource, so convert it to string
			//pasth must have a leading '/'
			//prepend one if has none
			if (path.substring(0,1)=='/') {
				this._path=this._path+path;
			}else{
				//prepend one if has none
				this._path=this._path+'/'+path;
			}
			return this;
		},
		clearQuery : function() {
			//clears the query part of this url
			this._query="";
			return this;
		},
		query : function(key,value) {
			//_appends the query for the given key and value
			//uri encodes the value
			var keyValue=key+"="+encodeURIComponent(value);
			if (this._query.length==0) {
				//the first do not seperate by &
				this._query=this._append(this._query,"",keyValue);
			}else{
				//
				this._query=this._append(this._query,"&",keyValue);
			}
			return this;
		},
		fragment : function(fragment) {
			this._fragment=fragment;
			return this;
		},
	});
});
