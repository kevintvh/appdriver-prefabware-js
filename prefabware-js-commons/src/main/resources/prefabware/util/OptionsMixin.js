//to mix the options into this object
define('prefabware/util/OptionsMixin', [ 'dojo',  'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.util.OptionsMixin", null, {
		mixinOptions : function(options) {
			//loops over all options, 
			for ( var opt in options) {
				if (this[opt]!==undefined) {
					//if a property with the same name exists, it is set to the options value
					this[opt]=options[opt];
				}
			};
		},
	});
});
