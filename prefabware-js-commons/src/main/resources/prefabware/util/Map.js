//a generic registry
define('prefabware/util/Map', [ 'dojo',  'dojo/_base/declare' ], function(
		dojo) {
	dojo.declare("prefabware.util.Map", null, {
		__map : new Object(),
		constructor : function() {
		},
//		key : function(value) {
//			throw 'subclass of Map must overwrite method key()' ;
//		},
		__propertyName : function(key) {
			//returns the propertyName touse in the internal __map object
			//to avoid invalid propertyNames, '.'are replaced by'_'
			return key.replace(/\./g, '_');
		},
		put : function(key,value) {
			this.__map[this.__propertyName(key)]=value;
		},
//		registerValue : function(value) {
//			this.register(this.key(value),value);
//		},
		find : function(key) {
			return this.__map[this.__propertyName(key)];
		},
		remove : function(key) {
			delete this.__map[this.__propertyName(key)];
		},
		values : function() {
			values=new Array();
			for ( var prop in this.__map) {
				var value=this.__map[prop];
				values.push(value);
			}
			return values;
		},
		
	});
});