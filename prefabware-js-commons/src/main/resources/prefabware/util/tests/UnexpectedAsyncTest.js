// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.UnexpectedAsyncTest");
//Import in the code being tested.
dojo.require("prefabware.util.Parameters");
dojo.require("dojo._base.lang");
dojo.require("dojo.Deferred");
doh.register("prefabware.util.tests.UnexpectedAsyncTest", [
 
  {
	  name: "Parameters",
	  setUp: function(){
	  },
	  expectedException: function(name,caught){
		  if (caught==null) {
			  throw(name+' expected but no excepetion was raised');
		  }
		  if (caught==null||caught.name!=name) {
				throw(name+' expected but raised '+caught.name+' message='+caught.message);
			}
	  },
	  runTest: function(){
		  var isResolved=false;
		  var deferred=null;
		  var a=null,b=null,c=null;
		  a=function(a1,a2){
			  var oriArguments=arguments;
			  try {
				  b(a1+'b',a2+'b');  
			} catch (e) {
				if (e.deferred!=undefined) {
					deferred.then(function(){
						a.apply(this,oriArguments);
					});
				}
			}
		  };
		  b=function(b1,b2){
			var result= c(b1+'c',b2+'c');
			if (result.then) {
				//its an unexpected async
				throw {deferred:result};
			}
		  };
		  c=function(c1,c2){
			  if (isResolved==false) {
				  isResolved=true;
			  deferred = new dojo.Deferred(function(reason){
				    // do something when the Deferred is cancelled
				  });
			  setTimeout(function(){
			      deferred.resolve("success");
			    }, 1000);
			  return deferred;
			  }else{
				  return {msg:'c was called',c1:c1,c2:c2};
			  };
		  };
		  
		  
//		  var deferred = new doh.Deferred();
//		    setTimeout(deferred.getTestCallback(function(){
//		    	doh.assertEqual('success', a('a','b'));
//		    }), 100);
//		    return deferred;
	  },
	  tearDown: function(){
	  },
  },
 
  // ...
]);