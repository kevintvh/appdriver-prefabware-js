// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.UrlBuilderTest");
//Import in the code being tested.
dojo.require("prefabware.util.UrlBuilder");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.UrlBuilderTest", [
 
  {   
	  name: "location.href",
	  setUp: function(){
	  },
	  runTest: function(){
		  var b1,url;
		  
		  b1 = new prefabware.util.UrlBuilder();
		  url=location.href;
		  b1.parse(url);
		  doh.assertEqual(url, b1.build());
		  b1.fragment("fragment1");
		  doh.assertEqual(url+"#fragment1", b1.build());
		  b1.fragment("fragment2");
		  doh.assertEqual(url+"#fragment2", b1.build());  
		  doh.assertEqual(b1.build(), b1.build(b1.parse(b1.build())));
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "parser",
	  setUp: function(){
	  },
	  runTest: function(){
		  var b1,url;
		  
		  b1 = new prefabware.util.UrlBuilder();
		  url="http://localhost?land=de&stadt=berlin#geschichte";
		  b1.parse(url);
		  doh.assertEqual(url, b1.build());
		  b1.appendPath("path1");
		  doh.assertEqual("http://localhost/path1?land=de&stadt=berlin#geschichte", b1.build());

		  
		  b1 = new prefabware.util.UrlBuilder();
		  url="http://hans@example.org:80/demo/example.cgi#geschichte";
		  b1.parse(url);
		  doh.assertEqual(url, b1.build());
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "builder",
	  setUp: function(){
	  },
	  runTest: function(){
		  var b1;
		  b1 = new prefabware.util.UrlBuilder();
		  
		  b1.host("localhost");
		  b1.appendPath("invoice");
		  b1.appendPath(4);//like the id of a rest resource
		  doh.assertEqual("http://localhost/invoice/4", b1.build());

		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost");
		  doh.assertEqual("http://localhost", b1.build());
		  
		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost").port("8080");
		  doh.assertEqual("http://localhost:8080", b1.build());
		  
		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost").port("8080").path("app-crm/api/invoice/2");
		  doh.assertEqual("http://localhost:8080/app-crm/api/invoice/2", b1.build());
		  
		  //user no password
		  b1 = new prefabware.util.UrlBuilder();
		  b1.scheme("http://").user("hans").host("example.org").port("80").path("demo/example.cgi").fragment("geschichte");
		  doh.assertEqual("http://hans@example.org:80/demo/example.cgi#geschichte", b1.build());

		  b1 = new prefabware.util.UrlBuilder();
		  b1.scheme("http://").user("hans").password("geheim").host("example.org").port("80").path("demo/example.cgi").fragment("geschichte");
		  doh.assertEqual("http://hans:geheim@example.org:80/demo/example.cgi#geschichte", b1.build());
		  
		  //query
		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost").query("land","de");
		  doh.assertEqual("http://localhost?land=de", b1.build());

		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost").query("land","de").query("stadt","berlin");
		  doh.assertEqual("http://localhost?land=de&stadt=berlin", b1.build());

		  b1 = new prefabware.util.UrlBuilder();
		  b1.host("localhost").query("land","de").query("stadt","berlin").fragment("geschichte");
		  doh.assertEqual("http://localhost?land=de&stadt=berlin#geschichte", b1.build());
		 
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  // ...
]);