// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.LangTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("prefabware.test");
doh.register("prefabware.util.tests.LangTest", [
{
	name : "getObjectAsync test null value result",
	setUp : function() {
		
	},
	runTest : function() {
		var def = new doh.Deferred();
		var f=function(object,prop){
			return prefabware.lang.deferredValue(object[prop]);
		};
		//happy path
		var c1={d:null};
		var b1={c:c1};
		var a1={b:b1};
		var pd=prefabware.lang.getObjectAsync(a1,"b.c.d",f).then(function(d){
			doh.assertEqual(null,d);
			def.resolve(true);
		});
		return def;
	},
	tearDown : function() {
	}
},
{
	name : "getObjectAsync test null value in path",
	setUp : function() {
		
	},
	runTest : function() {
		var def = new doh.Deferred();
		var f=function(object,prop){
			return prefabware.lang.deferredValue(object[prop]);
		};
		//happy path
		var c1={d:'dValue'};
		var b1={c:null};
		var a1={b:b1};
		var pd=prefabware.lang.getObjectAsync(a1,"b.c.d",f).then(function(d){
			doh.assertEqual(null,d);
			def.resolve(true);
		});
		return def;
	},
	tearDown : function() {
	}
},
{
	name : "getObjectAsync test happy path",
	setUp : function() {

	},
	runTest : function() {
		var def = new doh.Deferred();
		var f=function(object,prop){
			return prefabware.lang.deferredValue(object[prop]);
		};
		//happy path
		var c1={d:'dValue'};
		var b1={c:c1};
		var a1={b:b1};
		var pd=prefabware.lang.getObjectAsync(a1,"b.c.d",f).then(function(d){
			doh.assertEqual("dValue",d);
			def.resolve(true);
		});
		return def;
	},
	tearDown : function() {
	}
},
{
	name : "lang test",
	setUp : function() {
		
	},
	runTest : function() {
		
		doh.assertEqual("ab2c",prefabware.lang.concat("a","b",2,"c"));
		doh.assertEqual("ab",prefabware.lang.concat(null,"a",null,"b",null));
		doh.assertEqual("ab",prefabware.lang.concat(undefined,"a",undefined,"b",undefined));
		doh.assertEqual("1ab",prefabware.lang.concat(1,undefined,"a",undefined,"b",undefined));
		doh.assertTrue(prefabware.lang.isHash({}));
		doh.assertFalse(prefabware.lang.isHash(new Date()));
		doh.assertFalse(prefabware.lang.isHash("x"));
		doh.assertFalse(prefabware.lang.isHash(true));
		doh.assertFalse(prefabware.lang.isHash(1));
		doh.assertFalse(prefabware.lang.isHash([]));
		doh.assertFalse(prefabware.lang.isHash(null));
		doh.assertFalse(prefabware.lang.isHash(function(){}));
		
		doh.assertEqual({"boolean":true},prefabware.lang.toHash(true,"boolean"));
		doh.assertEqual({"string":"my string"},prefabware.lang.toHash("my string","string"));
		var d=new Date();
		doh.assertEqual({"date":d},prefabware.lang.toHash(d,"date"));
		var h={n:"v"};
		doh.assertEqual(h,prefabware.lang.toHash(h));
		
		
		doh.assertTrue(prefabware.lang.equal(1,1));
		doh.assertTrue(prefabware.lang.equal("1","1"));
		doh.assertFalse(prefabware.lang.equal("1",1));
		doh.assertFalse(prefabware.lang.equal("1",""));
		doh.assertTrue(prefabware.lang.equal({a:'a',b:'b',c:'c'},{a:'a',b:'b',c:'c'}));
		doh.assertTrue(prefabware.lang.equal(null               ,null              ));
		doh.assertFalse(prefabware.lang.equal({a:'a',b:'b',c:'d'},{a:'a',b:'b',c:'c'})," different values");
		doh.assertFalse(prefabware.lang.equal({a:'a',b:'b',d:'d'},{a:'a',b:'b',c:'c'})," different properties");
		doh.assertFalse(prefabware.lang.equal({a:'a',b:'b'     },{a:'a',b:'b',c:'c'})," first has less arguments");
		doh.assertFalse(prefabware.lang.equal({a:'a',b:'b',c:'c'},{a:'a',b:'b'     })," second has less arguments");
		doh.assertFalse(prefabware.lang.equal(null               ,{a:'a',b:'b'     })," first is null");
		doh.assertFalse(prefabware.lang.equal({a:'a',b:'b',c:'c'},null              )," second is null");
		
		doh.assertTrue(prefabware.lang.isReservedWord('delete'));
		doh.assertFalse(prefabware.lang.isReservedWord('delete_'));
		doh.assertTrue(prefabware.lang.isNumber('1'));
		doh.assertTrue(prefabware.lang.isNumber(1));
		doh.assertTrue(prefabware.lang.isNumber(0));
		doh.assertTrue(prefabware.lang.isNumber('0'));
		doh.assertFalse(prefabware.lang.isNumber(''));
		doh.assertFalse(prefabware.lang.isNumber(' '));
		doh.assertFalse(prefabware.lang.isNumber('x'));
		doh.assertTrue(prefabware.lang.endsWith('Berlin*','*'));
		doh.assertFalse(prefabware.lang.endsWith('1','2'));
		doh.assertTrue(prefabware.lang.endsWith('1','1'));
		doh.assertTrue(prefabware.lang.endsWith('12345','345'));
		doh.assertEqual([1,2,3],prefabware.lang.selectNotNull([null,1,2,null,3]));
		doh.assertEqual('userName',prefabware.lang.firstLetterLowerCase('UserName'));
		doh.assertEqual('UserName',prefabware.lang.firstLetterUpperCase('userName'));
	},
	tearDown : function() {
	}
},
]);