// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.i18n.ResourceI18nTest");
//Import in the code being tested.
dojo.require("prefabware.util.ResourceI18n");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.i18n.ResourceI18nTest", [
 
  {   
	  name: "nls resource",
	  setUp: function(){
	  },
	  runTest: function(){
		  var def=new doh.Deferred();
		  var resource = new prefabware.util.ResourceI18n({namespace:"prefabware.util.tests.i18n.nls",name:"labels",locale:'es'});
		  doh.assertEqual("prefabware.util.tests.i18n.nls", resource.namespace);
		  resource.startup()
		  	.then(function(){
		  		doh.assertEqual('Hola mundo !',resource.getLocalization("greeting"));
		  		def.callback(true);
		  	});
		  return def;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "resource with qualifier",
	  comment: "to test that using other qualifiers than a locale does not work",
	  setUp: function(){
	  },
	  runTest: function(){
		  var def=new doh.Deferred();
		  var resource = new prefabware.util.ResourceI18n({namespace:"prefabware.util.tests.i18n.ui",name:"icons",locale:'whitemin'});
		  var exceptionOccured=false;
		  try {
			  resource.startup();
		} catch (e) {
			exceptionOccured=true;
		}
		doh.assertTrue(exceptionOccured);
		def.callback(true); 
		return def;
	  },
	  tearDown: function(){
	  }
  },
]);