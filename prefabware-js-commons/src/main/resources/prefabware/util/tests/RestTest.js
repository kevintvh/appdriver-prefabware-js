// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.RestTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("prefabware.test");
dojo.require("prefabware.util.rest");
doh.register("prefabware.util.tests.RestTest", [
{
	name : "rel",
	resource:null,
	setUp : function() {
		this.resource={
				  "links" : [ ],
				  "content" : [ {
				    "name" : "Frankfurt",
				    "version" : 0,
				    "size" : "L",
				    "links" : [ {
				      "rel" : "self",
				      "href" : "http://localhost:8080/plugins-store/api/city/2"
				    }, {
				      "rel" : "country",
				      "href" : "http://localhost:8080/plugins-store/api/city/2/country"
				    } ]
				  } ]
				}
			
	},
	runTest : function() {
		var links=prefabware.util.rest.links(this.resource.content[0]);
		doh.assertTrue(links!=null);
		doh.assertTrue(links.length=1);
		var map=prefabware.util.rest.linkMap(links);
		var country=map.item("country");
		doh.assertEqual('http://localhost:8080/plugins-store/api/city/2/country',country.href);
		doh.assertEqual(null,prefabware.util.rest.hrefId(country.href),'null expected because the link does not include the id of the resource');
		
		
	},
	tearDown : function() {
	}
},
{
	name : "selfLink",
	resource:null,
	setUp : function() {
		this.resource={
				"name" : "Berlin",
				"version" : 0,
				"links" : [ {
					"rel" : "self",
					"href" : "http://localhost:8080/plugins-store/api/city/1"
				}, {
					"rel" : "country",
					"href" : "http://localhost:8080/plugins-store/api/city/1/country"
				} ]
		};
		
	},
	runTest : function() {
		var selfLink=prefabware.util.rest.selfLink(this.resource);
		doh.assertTrue(selfLink!=null);
		var id=prefabware.util.rest.hrefId(selfLink.href);
		doh.assertTrue(id == 1);
	},
	tearDown : function() {
	}
},
]);