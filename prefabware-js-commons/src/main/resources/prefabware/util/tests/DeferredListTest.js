// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.DeferredListTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("doh");
dojo.require("prefabware.test");
dojo.require("prefabware.lang");
doh.register("prefabware.util.tests.DeferredListTest", [
{
	name : "deferred list",
	setUp : function() {

	},
	runTest : function() {
		var def = new doh.Deferred();
		var context={result:[]};
		
		var f=function(element){
			this.result.push(element);
			return prefabware.lang.deferredValue(null);
		};
		var arr=['a','b','c','d'];
		prefabware.lang.deferredList(arr,f,context)
			.then(function(){
				doh.assertEqual(arr,context.result);
				def.callback(true);
			});
	},
	tearDown : function() {
	}
},
]);