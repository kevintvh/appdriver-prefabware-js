// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.TemplateTest");
//Import in the code being tested.
dojo.require("prefabware.util.Template");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.TemplateTest", [
 
  {   
	  name: "TemplateTest",
	  setUp: function(){
	  },
	  runTest: function(){
		  var template = new prefabware.util.Template({template:"a:${a1}, b:${b1}, c:${c1} "});
		  var keys=template.findKeys();
		  doh.assertEqual(3, keys.length);
		  doh.assertEqual("a1", keys[0]);
		  doh.assertEqual("b1", keys[1]);
		  doh.assertEqual("c1", keys[2]);
		  
		  var replaced=template.replace({"a1":"A1","c1":"C1"});
		  // ${a1} is replaced with A1
		  // ${b1} is replaced with b1 because a value is not set. So only $ and brackets are removed
		  // ${c1} is replaced with C1
		  
		  doh.assertEqual("a:A1, b:b1, c:C1 ", replaced);
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "i18nTest",
	  setUp: function(){
	  },
	  runTest: function(){
		  //to test replacement of i18n placeholder
		  // only ${i18n.xxx} should be considered as key
		  // they keys used by the template are the part after 'i18n.'
		  // if a value for a key is not set when calling replace, the key should be used instead of the placeholder
		  var regEx=/\$\{i18n\.([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g;
		  var template = new prefabware.util.Template({template:"User:${i18n.user}, User2:${i18n.user2}, b:${b1}, c:${c1} ",regEx:regEx});
		  var keys=template.findKeys();
		  doh.assertEqual(2, keys.length);
		  doh.assertEqual("user", keys[0]);
		  doh.assertEqual("user2", keys[1]);
		  
		  var replaced=template.replace({"user":"Stefan Isele"});
		  // ${i18n.user} is replaced with "Stefan Isele"
		  // ${i18n.user2} is replaced with "user2", its akey, but no value was given
		  // ${b1} is unchanged because its not considered a key by the regex
		  // ${c1} is unchanged because its not considered a key by the regex
		  
		  doh.assertEqual("User:Stefan Isele, User2:user2, b:${b1}, c:${c1} ", replaced);
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
 
  // ...
]);