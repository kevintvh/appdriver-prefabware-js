//a file resource like a json file
define('prefabware/util/ResourceJsonFile', [ 'dojo',  'prefabware/util/ResourceFile','dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.util.ResourceJsonFile", [prefabware.util.ResourceFile], {
		asJson:null,		
		getContentAsJson : function() {
			if (this.asJson==null) {
				this.asJson= dojo.fromJson(this.getContentAsString());
			}
			return this.asJson;
		},
		
	});
});
