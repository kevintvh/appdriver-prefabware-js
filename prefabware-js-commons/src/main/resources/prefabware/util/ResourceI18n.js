// to load a localization resource for i18n
// because loading is async you have to call startup() once and wait for the returned promose to resolve
// 
// the namespace must be the namespace without nls
define('prefabware/util/ResourceI18n', [ 
        'dojo','dojo/i18n',
        'prefabware/lang',
        'prefabware/util/Resource','dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.util.ResourceI18n", [prefabware.util.Resource], {
		locale:null,//the locale to use, may be null to use the default locale
		resource:null,//the loaded resource e.g. {greeting: "Hola mundo !", $locale: "es"}
	// options.locale is the locale to load the localization for. if none is supplied, the default locale is used
	//                if options.locale is supplied, it must be a valid locale, otherwise doj.i18n will not
	//                be able handle it.
	// options.namespace is the path to the folder containing the resources relative to dojo, seperated by '.'
	// in the following example it would be the namespace of teh folder nls
	// 	e.g. com.prefabware.module.nls
	// nls
	//   labels.js	
	//   --de
	//      labels.js	
	//   --es
	//      labels.js	
		constructor : function(options) {
			if (options.namespace==null) {
				prefabware.lang.throwError("a namespace is required");
			}
			if (options.name==null) {
				prefabware.lang.throwError("a name is required");
			}
			if (options.name.indexOf('.')>=0) {
				prefabware.lang.throwError("the name must not have a suffix, it must be the name only, without e.g. '.js'");
			}
			if (options.qualifiedName!=null) {
				prefabware.lang.throwError("I18n resources can only be created with a namespace and a name, not with a qualifiedName");
			}
			if (options.locale!=null) {
				this.locale=options.locale;
			}
		},
		startup : function() {
			//async
			var deferred = new dojo.Deferred();
			var path=this.toPath();
			var id="dojo/i18n!"+path;
			if (this.locale!=null) {
				id=id+'/'+this.locale;
			}
			if (this.name!=null) {
				id=id+'/'+ this.name;
			}
			var that=this;
			require([id], function(resource){
				that.resource=resource;
				if (resource!=null) {
					deferred.resolve(resource);
				}else{
					deferred.reject('could not find resource '+id);
				};
				});
			return deferred.promise;
		},
		getLocalization : function(key) {
			//the key is used as a javascript property. change it if necessary
			//e.g. replace . by _ for typenames
			var adjusted=key.split('.').join('_');
			return this.resource[adjusted];
		},
	});
});
