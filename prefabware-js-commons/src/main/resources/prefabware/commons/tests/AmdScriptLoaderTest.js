// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.AmdScriptLoaderTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.commons.tests.AmdScriptLoaderTest", [
{
			name : "load a script dynamically",
			timeout : 4000,
			setUp : function() {

			},
			runTest : function() {
				// its an async test, so return a doh.Deferred to make test
				// execution wait for the reolve
				var def = new doh.Deferred();

				doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest == null,
						'sript should not be loaded yet');
				require(['prefabware/commons/script!prefabware/commons/tests/script1.js'],function(script){
					doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest != null,
					'sript should not be loaded now');
					def.callback(true);
				});
				return def;
			},
			tearDown : function() {
			}
}]);