// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.Proxy");
//Import in the code being tested.
dojo.require("prefabware.commons.Proxy");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
dojo.require("dojo.promise.Promise");
doh.register("prefabware.commons.tests.Proxy", [
 
  {
	  name: "async Proxy",
	  map:null,
	  setUp: function(){
		 
	  },
	  runTest: function(){
		  var def=new doh.Deferred();
		  var person={
				  firstName:"Stefan",
				  name:"Isele",
				  fullName:function(){return this.firstName+' '+this.name;}
		  };
		  var proxy = new prefabware.commons.Proxy();
		  proxy.doResolve=function(){
			  return prefabware.lang.deferredValue(person);
		  };
		  proxy.resolve().then(function(){
			  doh.assertEqual(undefined, proxy.firstName);
			  proxy.defineProperty("firstName");
			  doh.assertEqual("Stefan", proxy.firstName);
			  
			  doh.assertEqual(undefined, proxy.fullName);
			  proxy.defineMethod("fullName");
			  doh.assertEqual("Stefan Isele", proxy.fullName());
			  def.callback(true);
		  });
		  return def;
	  },
	  tearDown: function(){
	  }
  },
  {
	  name: "Proxy",
	  map:null,
	  setUp: function(){
		  
	  },
	  runTest: function(){
		  var person={
				  firstName:"Stefan",
				  name:"Isele",
				  fullName:function(){return this.firstName+' '+this.name;}
		  };
		  var proxy = new prefabware.commons.Proxy();
		  proxy.doResolve=function(){
			  return person;
		  };
		  doh.assertEqual(undefined, proxy.firstName);
		  proxy.defineProperty("firstName");
		  doh.assertEqual("Stefan", proxy.firstName);
		  
		  doh.assertEqual(undefined, proxy.fullName);
		  proxy.defineMethod("fullName");
		  doh.assertEqual("Stefan Isele", proxy.fullName());
		  return;
	  },
	  tearDown: function(){
	  }
  },
 
  // ...
]);