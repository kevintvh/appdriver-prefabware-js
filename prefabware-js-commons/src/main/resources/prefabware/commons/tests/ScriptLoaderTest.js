// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.ScriptLoaderTest");
//Import in the code being tested.
dojo.require("prefabware.commons.ScriptLoader");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.commons.tests.ScriptLoaderTest", [
{
			name : "load a script dynamically",
			timeout : 4000,
			setUp : function() {

			},
			runTest : function() {
				// its an async test, so return a doh.Deferred to make test
				// execution wait for the reolve
				var def = new doh.Deferred();

				var cl = new prefabware.commons.ScriptLoader();
				doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest == null,
						'sript should not be loaded yet');
				var deferred = cl.load('prefabware/commons/tests/script1.js');
				deferred.then(function(instance) {
					doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest != null,
							'sript should not be loaded yet');
					doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest.counter== 1,
					'sript should not be loaded yet');
					doh.assertTrue(cl.cache.count== 1,
					'should be cached');
				}, function(error) {
					// Do something on failure.
				}).then(function(){
					//load again
					return cl.load('prefabware/commons/tests/script1.js');
				}).then(function(){
					doh.assertTrue(window.prefabware_commons_tests_ScriptLoaderTest.counter== 1,
					'sript should not be loaded again');
					doh.assertTrue(cl.cache.count== 1,
					'should be cached');
					def.callback(true);
				});
				return def;
			},
			tearDown : function() {
			}
}]);