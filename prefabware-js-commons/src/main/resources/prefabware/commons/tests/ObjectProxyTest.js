// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.commons.tests.ObjectProxyTest");
// Import in the code being tested.
dojo.require("prefabware.commons.tests.TestObject");
dojo.require("prefabware.commons.ObjectProxy");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.commons.tests.ObjectProxyTest", [ {
	extensionPoint : null,
	extension : null,
	setUp : function() {
	},
	runTest : function() {
		var that=this;
		proxy=new prefabware.commons.ObjectProxy(prefabware.commons.tests.TestObject);
		proxy.doResolve=function(reference) {			
				return new prefabware.commons.tests.TestObject('target');
			};
			
	doh.assertFalse(proxy.__isResolved);
	doh.assertEqual(4, proxy.add(1,3));
	doh.assertTrue(proxy.__isResolved);
	doh.assertEqual('target', proxy.id);
	},
	
	tearDown : function() {
	}
},
]);