//to load css dynamic using the amd loader
//usage :
//require(['prefabware/commons/css!prefabware/commons/tests/script1.css']
define(["dojo","dojo/dom-construct"], function (dojo,dom) {
  
  return {
    load : function (id, require, callback) {
    	var head=dojo.query("head")[0];
    	var links=dojo.query("head link");
    	var ref;
    	var pos;
    	if (links=nul||links.length==0) {
			ref=head;
			pos='last';
		}else{
			var last=links[links.length-1];
			ref=last;
			pos='after';
		}
    	dom.place('<link rel="stylesheet" type="text/css" href="'+require.toUrl(id)+'">',ref,pos);
    	callback();
    }
  };
});


