//matches never
define('prefabware/commons/match/NeverMatcher', [ 'dojo',
		'prefabware/commons/match/Matcher', 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.commons.match.NeverMatcher",
			[ prefabware.commons.match.Matcher ], {
				constructor : function(options) {
				},
				matches : function(options) {
					return {
						matcher : this,
						degree : this.NO_MATCH
					};
				},
				accepts : function(options) {
					// accepts everything
					return true;
				},
			});
});
