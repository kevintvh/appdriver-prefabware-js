//to find the best matching resolver or to find all matching resolvers
//1. register matcher
//2. call register.startup();
//3. call register.bestMatch(..);
//or
//3. call register.allMatches(..);
define('prefabware/commons/match/MatchResultResolver', [ 'dojo',
		'dojo/_base/array',
		'dojox/collections/Dictionary',
		'prefabware/lang',
		'dojo/_base/declare' ], function(dojo, array) {
	dojo.declare("prefabware.commons.match.MatchResultResolver", null, {
		matchers : null, // the registered matchers
		matchersByOrder : null, // the registered matchers in the order they should be applied
		order:null,
		isStarted:false,//true=startup was called
		constructor : function() {
			this.matchers = new dojox.collections.Dictionary();
		},
		register : function(matcher) {
			this.matchers.add(matcher.id, matcher);
			if (this.isStarted) {
				//sort after every register if allready started
				this.sortMatcher();
			}
		},
		unregister : function(id) {
			this.sortMatcher;
		},
		sortMatcher : function() {
			this.matchersByOrder=this.matchers.getValueList().slice();
			this.matchersByOrder.sort(function(matcherA,matcherB) {
				return matcherA.order-matcherB.order;
			});
		},
		startup : function() {
			this.sortMatcher();
			this.isStarted=true;
		},
		__calcMatch : function(matcher, options) {
			var result = matcher.matches(options);
//			prefabware.lang.log("matcher " + matcher.id + " bestMatch value ="
//					+ options.value + " result=" + result.degree);
			return result;
		},
		allMatches : function(options) {
			//returns all matches
			var results = array.map(this.matchersByOrder, function(
					matcher) {
				return this.__calcMatch(matcher, options);
			}, this);
			return array.filter(results, function(result) {
				return result.degree > 0;
			}, this);
		},
		bestMatch : function(options) {
			// returns the best match
			var bestResult = {
				degree : 0
			};
			array.forEach(this.matchersByOrder, function(matcher) {
				var result = this.__calcMatch(matcher, options);
				if (result.degree > bestResult.degree) {
//					prefabware.lang.log("matcher " + matcher.id
//							+ " is best result now");
					bestResult = result;
				}
			}, this);
			if (bestResult.degree == 0) {
				return null;
			}
			return bestResult;
		},
	});
});
