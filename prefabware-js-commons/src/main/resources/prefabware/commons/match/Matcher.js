//to match against a velue
define('prefabware/commons/match/Matcher', [ 'dojo', 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.commons.match.Matcher", null, {
		id : null,
		onDoesNotApply:'error',//'error' or 'ignore'
		order : 100,// the order for the match resolver		
		degree : 1,// minimum match	
		evalMatcher:null,//will be evaled, has access to type or qattribute
		owner:null,
		EXACT_MATCH : {
			matcher : null,
			degree : 100
		},
		GENERIC_MATCH : {
			matcher : null,
			degree : 50
		},
		NO_MATCH : {
			matcher : null,
			degree : -1
		},
		setOwner : function(owner) {
			//strange errors ocurred when i tried to provide the owner (which had also a backreference to the matcher)
			//as part of the options.
			//so the owner now has to be set through this method, allowing subclasses to overwrite this
			this.owner=owner;
		},
		doesNotApply : function(message) {
			//subclasses may call this, if they recognize that this Mather does not apply to the given options
			if (this.onDoesNotApply=='ignore') {
				return;
			}else{
				prefabware.lang.throwError(message);
			}
		},
		optionsToString : function(options) {
			//returns the options converted to a string
			//subclasses may override this
			return dojo.toJson(options);
		},
		constructor : function(options) {
			this.id = options.id;
			this.owner = options.owner;
			this.EXACT_MATCH.matcher=this;
			this.GENERIC_MATCH.matcher=this;
			this.NO_MATCH.matcher=this;
			if (options.degree) {
				this.degree=options.degree;
			}
			if (options.order) {
			this.order = options.order;
			}
			if (options.onDoesNotApply) {
				this.onDoesNotApply=options.onDoesNotApply;
			}
			if (options.matches!=null) {
				prefabware.lang.assert(typeof options.matches=="string",'options.matches must be a string with valid javascript');
				this.evalMatcher=options.matches;//it must be a string pattern!
			}
		},		
		matches : function(options) {
			// subclases must implelemnt this method
		},
	});
});
