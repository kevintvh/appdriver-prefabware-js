//to load classes at runtime
//TODO move to util
define('prefabware/commons/ClassLoader', [ 'dojo', 'dojo/Deferred','prefabware/lang',
		'dojo/_base/declare' ], function(dojo, Deferred) {
	dojo.declare("prefabware.commons.ClassLoader", null, {

		constructor : function() {
		},
		load : function(className) {
			var that=this;
			//async
			//loads a class asynchronously if its not allready loaded
			var existing=that.getClassSilent(className);
			if (existing!=undefined) {
				return prefabware.lang.deferredValue(existing);
			}
			var classPath = className.split('.').join('/');
			var deferred = new Deferred();
			require([ classPath ], function() {
				//the argument provided to this method is an empty object
				//how ever the clazz should be loaded and exist now
				deferred.resolve(that.getClass(className));
			});
			return deferred;
		},
		getClass : function(className) {
			//returns an allready loaed class
			var clazz = dojo.getObject(className);			
			if (clazz == undefined) {
				throw ('could not load class ' + className);
			}
			return clazz;
		},
		getClassSilent : function(className) {
			//returns an allready loaed class
			var clazz = dojo.getObject(className);			
			return clazz;
		},
		createInstance : function(className,options) {
			//async
			//returns an instanceof the class, passes the options to the constructor
			return this.load(className).then(function(clazz){
				return new clazz(options);
			});			
		},
	});
});