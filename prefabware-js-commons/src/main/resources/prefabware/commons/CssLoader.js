//to load scripts at runtime
//takes care to load any script only once
//only for scripts loaded with the same instance of ScriptLoader
define('prefabware/commons/CssLoader', [ 'dojo'
                                            ,'dojo/_base/array'
                                            ,'dojo/dom-construct'
                                            ,'dojox/collections/Dictionary'
                                            ,'prefabware/commons/ResourceLoader'
                                            ,'prefabware/lang',
		'dojo/_base/declare' ], function(dojo, array,dom) {
	dojo.declare("prefabware.commons.CssLoader", [prefabware.commons.ResourceLoader], {
		constructor : function() {
		},
		_loadResource : function(url) {
			//loads the resource with the given url
			var head=dojo.query("head")[0];
			dom.place('<link rel="stylesheet" type="text/css" href="'+url+'">',head,'last');
			//the browser loads the resource when the link was added to the dom
			//we do have no information when he finished loading
			//so we return an allready resolved promise
			return prefabware.lang.deferredValue(head);
		},
	});
});