//to load scripts dynamic using the amd loader
//usage :
//require(['prefabware/commons/script!prefabware/commons/tests/script1.css']
define(["prefabware/commons/ScriptLoader"], function () {
  
  return {
    load : function (id, require, callback) {
    	//if more than one script are required, the amd loader start the second import 
    	//after the first script was requested, not after it has run
    	//this makes this ScriptLoader useless, when a class has imports, that have a dependency relation.
    	//because the second script can start before the first has run..
    	//in that cases you have to require by hand, using the 
    	dojo.io.script.get({ url:require.toUrl(id)})
    		.then(function(){
    			callback();    
    		});
    }
  };
});
