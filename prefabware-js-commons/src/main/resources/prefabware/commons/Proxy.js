//a dynamic proxy to allow e.g. lazy resolving of references
//can resolve everything if an according __resolver method is provided
//works for properties of type function as well as for simple properties !
//create an instance
//add all properties you want to set and get using addProperty(propertName)
//set a reference. this is object should contain everything you need to resolve the proxy
//set doResolve to a method that can resolve the proxy. the function will get the reference as argument
//and must return the resolved entity
//normally you will also set
define('prefabware/commons/Proxy', [ 'dojo'
                                     , "dojo/when"
                                     , 'prefabware/lang','dojo/_base/declare' ], function(dojo,when) {
	dojo.declare("prefabware.commons.Proxy", null, {
		//will be used to resolve the reference in doResolve()
		__reference:null,
		//if __isResolved==true, this contains the resolved entity of this proxy
		__target:null,
		//allwayy true, just to make it easy to check
		__isProxy:true,
		//==true means this proxy is currently resolving
		__isResolving:false,
		//==true means this proxy is allready resolved
		__isResolved:false,
		//==true means this proxy resolved to null
		__isResolvedToNull:false,
		__resolverResult:null,
		
		constructor : function() {
		},
		resolve : function() {
			//can handle sync and async resolver
			//allways returns a promise for async resolver
			//or allways returns the resolved value
			if (this.__target!=null) {
				return this.__resolverResult;
				//return this.__target;
			}
			if (this.__isResolving) {
				//when the proxy is in the process of async resolving
				//return the promise to all callers, so they can continue when resolved
				return this.__resolverResult;
			}
			var that=this;
			//must resolve to the target or null if not possible
			that.__isResolving=true;;
			that.__resolverResult= when(this.doResolve(this.__reference), function(value){
				that.__target=value;
				if (that.__target==null) {
					//does not need to be an error
					//throw('reference '+that.__reference+' was resolved to null');
					that.__isResolvedToNull=true;
				}
				//mark as resolved
				that.__isResolved=true;
				that.__isResolving=false;
				return that.__target;
			  });
			return that.__resolverResult;
		},
		doResolve : function(reference) {
			throw('cannot resolve reference '+__reference+', function doResolve was not set');
		},
		doGet : function(propertyName,value) {
			//propertyName is the name of the property requested
			// value is target[propertyName]
			//default implementation just returns the found value
			//subclasses may change the value before returning it
			return value;
		},
		get : function (propertyName){
    		if (!this.__isResolved) {
    			if (this.resolve().then) {
    				//returned a promise
    				prefabware.lang.throwError('get on an unresolved Proxy is not allowed');
				}
			}   
    		var value;
    		if (this.__target!=null) {
    			value=this.__target[propertyName];
			}else{
				//the proxy resolved to null, that may happen e.g. with REST url pointing
				//to an attribute with value==null like e.g. /invoice/6/customer
				value=null;
			}
    		return this.doGet(propertyName,value);
    	},
    	set : function (propertyName,value){
    		if (!this.__isResolved) {
    			this.resolve();
    			if (this.resolve().then) {
    				//returned a promise
    				prefabware.lang.throwError('set on an unresolved Proxy is not allowed');
				}
			}     		
    		this.__target[propertyName]=value;
    	},
    	defineMethod : function(methodName){
    			that=this;
    			that[methodName]=function(){
    				if (that.__target==null) {
    					that.resolve();
    				}     		
    			return that.__target[methodName].apply(that.__target,arguments);  };
    	},
    		//old name : createPropertyAccessors : function(propertyName){
    	defineProperty : function(propertyName){
    		//creates setter/getter for the property    		
    		//adds a property that will delegate to the set/get accessor method
			var getter=dojo.partial( this.get, propertyName);
			var setter=dojo.partial( this.set, propertyName);

    		Object.defineProperty(this, propertyName, {get:getter,
                set:setter,
                enumerable : true,
                configurable : true});
    	}
	});
});
