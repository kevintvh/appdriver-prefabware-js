//a proxy for instances of dojo classes
//call the constructor with a dojo class
//e.g. proxy=new prefabware.commons.ObjectProxy(prefabware.commons.tests.TestObject);
//the proxy will have all the properties and methods the class defines
//set a resolver function
//proxy.doResolve=function(reference) {			
//				return new prefabware.commons.tests.TestObject('target');
//			};
//the proxy will resolve to the provided object when a property or function is accessed the first time
define('prefabware/commons/ObjectProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/commons/Proxy',
		  'dojo/_base/declare' ],
		  function(dojo,lang,Proxy) {
	dojo.declare("prefabware.commons.ObjectProxy", [prefabware.commons.Proxy], {
		constructor : function(clazz) {
			this.__reference=clazz;
			for ( var prop in clazz.prototype) {
				//create delegates for all properties and methods defined for the dojo class
				if (typeof clazz.prototype[prop] === 'function') {
					this.defineMethod(prop);
				}else{
					this.defineProperty(prop);
				};
			};
			return;
		},
		doResolve: function(reference){
		var cls= this.__reference;
			return new cls();
		}		
	});
});
