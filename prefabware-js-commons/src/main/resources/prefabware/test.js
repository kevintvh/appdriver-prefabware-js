//a global object for tests
//to have a seperate namespace where global test objects can be created 
//namely the setup, to share one setup amon many testcases in doh
define([ 'dojo', 'prefabware/util/OptionsMixin'
         , 'dojo/_base/array'
         , 'dojo/_base/lang'
         , 'prefabware/lang'
         ,'dojo/_base/declare' ], function(dojo, optionsMixin,array,lang) {
	var global = function(){
		this.setup={};//the setup for the current test
		this.setupF=null;//the setup function to call
		this.createSetup = function(f) {
			this.setup={};
			this.setupF=f;//execute the setup function
			return this.setup;
		};
		this.resolveSetup = function() {
			return this.setup;
		};
		this.runSetup = function() {
			return this.setupF.call(this.setup);
		};
		};
		var instance=new global();
		lang.setObject("prefabware.test", instance);
		return instance;
});