//to inject,register and find beans
define('prefabware/injector/Injector', [ 'dojo',
                                       'dojo/_base/lang',
                                       'dojo/_base/array',
                                       'prefabware/lang',
		'dojox/collections/Dictionary', 'dojo/_base/declare' ], function(dojo,
		lang,array) {
	dojo.declare("prefabware.injector.Injector", null, {
		//the name of the property inject in the annotation
		INJECT : 'inject',
		beans : null,
		constructor : function() {
			this.beans = new dojox.collections.Dictionary();
			this.register({id:'injector',type:'injector'},this);
		},
		register : function(params, instance,override) {
			//override = true -> override existing beans, do not throw an exception 
			var key=this.__keyFromParams(params);
			if (!override&&this.beans.containsKey(key)) {
				throw 'a bean for the key ' + key + ' was allready registered, registered bean='+this.beans.item(key);
			}
			//prefabware.lang.log('registering bean with key ' + key );
			return this.beans.add(key, instance);
		},
		find : function(key) {
			if (!this.beans.containsKey(key)) {
				throw 'cannot find bean with key ' + key;
			}
			return this.beans.item(key);
		},
		startup : function() {
			//injects into all registered beans
			array.forEach(this.beans.getKeyList(),function(key){
				var bean=this.beans.item(key);
				this.injectInto(bean);
			},this);
		},
		injectInto : function(instance) {
			// injects beans into the instance
			// all fields that have a value starting with @inject:'bean' will be
			// set to the according bean
			for ( var prop in instance) {
				var value = instance[prop];
				var ann=this.getAnnotation(value);
				if (ann!=undefined) {
					var key = this.__keyFromAnnotation(ann);
					var bean=this.find(key);
					//prefabware.lang.log('injecting bean '+bean + ' for key '+key + ' into '+instance);
					instance[prop] = bean;
				}
			}
			return instance;
		},
		getAnnotation : function(value) {
			// returns the annotation found in the value
			// returns undefined if no valid annotation can be found
			if (value==undefined || value==null) {
				return undefined;
			}
			if (dojo.isObject()) {
				return !undefined;
			}
			if (dojo.isArray()) {
				return !undefined;
			}
			if (!value[this.INJECT]) {
				return undefined;
			}
			return value;
		},
		__keyFromAnnotation : function(annotation) {
			return annotation.type+'$'+annotation.inject;
		},
		__keyFromParams : function(params) {
			return params.type+'$'+params.id;
		}
	});
});