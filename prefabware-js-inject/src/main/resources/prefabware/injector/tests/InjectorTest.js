// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.injector.tests.InjectorTest");
// Import in the code being tested.
dojo.require("prefabware.injector.Injector");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.injector.tests.InjectorTest", [ {
	setUp : function() {
	},
	runTest : function() {
		var injector=new prefabware.injector.Injector();
		var uiPlugin={
				id:'prefabware.ui_dijit',
				text:'i am the plugin'
		};
		var uiPlugin2={
				id:'prefabware.ui_dijit2',
				text:'i am the plugin2'
		};
		var bean={
			uiPlugin:{inject:'prefabware.ui_dijit',type:'plugin'},	
			myInjector:{inject:'injector',type:'injector'},	
		};
		var bean2={
				uiPlugin:{inject:'prefabware.ui_dijit',type:'plugin'},	
				myInjector:{inject:'injector',type:'injector'},	
		};
		
	injector.register({id:'prefabware.ui_dijit',type:'plugin'},uiPlugin);
	//registering the same key again should result in an exception
	var exceptions=0;
	try {
		injector.register({id:'prefabware.ui_dijit',type:'plugin'},uiPlugin);
	} catch (e) {
		exceptions++;
	}
	doh.assertEqual(1,exceptions);
	injector.injectInto(bean);
	doh.assertEqual(uiPlugin,bean.uiPlugin);
	doh.assertEqual(injector,bean.myInjector);

	//registering the same key again with override=true should be ok
	injector.register({id:'prefabware.ui_dijit',type:'plugin'},uiPlugin2,true);
	injector.injectInto(bean2);
	doh.assertEqual(uiPlugin2,bean2.uiPlugin);
	doh.assertEqual(injector,bean.myInjector);
	
	},
	
	tearDown : function() {
	}
},
]);