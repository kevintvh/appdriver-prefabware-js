package com.prefabware.js.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.prefabware.web.PathBuilder;
import com.prefabware.web.test.FirefoxTestSupport;

/**
 * to run Dojo DOH tests from a Maven project
 * <ol>
 * <li>call the constructor {@link #DojoTestSupport(String)} with the base-relativeUrl of the web app as argument
 * <li>add tests using {@link #add(String)} or {@link #addAll(Collection)}, each must be the relative relativeUrl of a html-page with DOH tests
 * <li>call {@link #testAll()} to execute tests
 * 
 * </ol>
 * 
 * @author Stefan Isele
 *
 */
public class DojoTestSupport {
	public static class TestPage{
		public TestPage(String relativeUrl) {
			this.relativeUrl=relativeUrl;
		}
		public TestPage(String relativeUrl, long timeout) {
			this(relativeUrl);
			this.timeout=timeout;					
		}
		String relativeUrl;
		/**
		 * time to wait for the test result after navigating to the page
		 * in milliseconds
		 */
		long timeout;
	}
	protected String baseUrl;
	public WebDriver driver;
	protected FirefoxTestSupport support;
	protected Set<TestPage>testPages;
	protected StringBuffer verificationErrors = new StringBuffer();
	private boolean setUp;

	public DojoTestSupport(String baseUrl) {
		super();
		this.testPages=new LinkedHashSet<TestPage>();
		this.baseUrl = baseUrl;
	}

	/**
	 * adds the html page with the relative url to the test
	 * @param relativeUrl
	 * @return
	 */
	public TestPage add(String relativeUrl) {
		TestPage testPage = new TestPage(relativeUrl);
		testPages.add(testPage);
		return testPage;
	}
	/**
	 * adds the html page with the relative url to the test
	 * @param relativeUrl
	 * @param timeout, the time to wait for asynchronous scripts in millisecondss
	 * used for driver.manage().timeouts().setScriptTimeout
	 * @return
	 */
	public TestPage add(String relativeUrl,long timeout) {
		TestPage testPage = new TestPage(relativeUrl,timeout);
		testPages.add(testPage);
		return testPage;
	}

	/**
	 * @param testHtml
	 *            e.g. "prefabware/injector/tests/InjectorTest.html"
	 * @throws Exception
	 */
	protected String getTestUrl(TestPage testHtml) throws Exception {		
		return new PathBuilder(baseUrl).appendPath(testHtml.relativeUrl).toString(); 
	}

	protected void setUp() throws Exception {
		if (!setUp) {
			support=new FirefoxTestSupport();
			support.setUp();
			driver = support.driver;
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			setUp=true;
		}
	}

	public void tearDown() throws Exception {
		support.tearDown();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	/**
	 * executes {@link #setUp}
	 * runs all tests
	 * executes {@link #tearDown()}
	 * @throws Exception
	 */
	public void testAll() throws Exception {
		for (TestPage testHtml : testPages) {
			testOne(testHtml);
		}
		tearDown();
	}
enum State{
	inProgress,success,failure, noTestsFound;
}
	public  void testOne(TestPage testPage) throws Exception {
		setUp();
		//setting scripttimeout doesnt help to wait for results of asynchronous or slow tests
		//driver.manage().timeouts().setScriptTimeout(testPage.timeout, testPage.unit);
		//instead wait explicitely here in java after navigating to the page
		driver.navigate().to(getTestUrl(testPage));
		Thread.sleep(testPage.timeout);
		State state;
		do {
			state = checkTests();
			if (state==State.inProgress) {
				Thread.sleep(100);
			}
		} while (state==State.inProgress);
	}

	private State checkTests() throws InterruptedException {
		List<WebElement> elements = driver.findElements(By
				.className("testGroupName"));
		assertNotNull("found no list of test groups", elements);
		assertTrue("list of test groups is empty", elements.size() > 0);
		System.out.println("Found test groups: " + elements);
		for (WebElement div : elements) {
			System.out.println("checking test group: " + div.getText());
			WebElement td = div.findElement(By.xpath("parent::*"));
			assertNotNull("did not find outer <td>", td);
			WebElement tr = td.findElement(By.xpath("parent::*"));
			assertNotNull("did not find outer <tr>", tr);
			String attribute = tr.getAttribute("class");
			System.out.println("testGroup has class: " + attribute);
			if (attribute.contains("inProgress")) {
				//the test is still in progress, try again
				return State.inProgress;
			}
			assertTrue(attribute.contains("success"));
			return State.success;
		}
		return State.noTestsFound;
	}
	public static class Result{
		protected String message;

		public Result(String message) {
			super();
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		@Override
		public String toString() {
			return "Result [message=" + message + "]";
		}
		
	}
	public static class Success extends Result{

		public Success(String message) {
			super(message);
		}
		public String toString() {
			return "Success [message=" + message + "]";
		}
	}
	public static class Failure extends Result{
		
		public Failure(String message) {
			super(message);
		}
		public String toString() {
			return "Failure [message=" + message + "]";
		}
	}
}