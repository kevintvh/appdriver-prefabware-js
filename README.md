Created by Stefan Isele and his company [prefabware](http://prefabware.com)

### What is this repository for? ###
This repository contains the code for the client (browser) of [appdriver](http://appdriver.com) - the enterprise framework. Its organized into various maven projects, that are developed to be reused in other projects as well.

### Quick summary ###
Most of the code is Javascript in form of AMD modules based on [Dojo](http://http://dojotoolkit.org/).
We use Dojo as our primary Javacript framework, to load Javascript in the browser, write tests, to manipulate the DOM and to create a compressed build. We do NOT use any of the Dojo Widgets for the user interface. Its possible to use whatever library or CSS for the user interface, so far we have implementations using JQuery and Bootstrap.

### Overview of the most important modules ###
* **thirdparty-parent** - javascript libraries like dojo, jQuery, bootstrap, packed as maven artefacts so they can be used as maven dependencies.
* **prefabware-js-commons** javascript utilities 
* **prefabware-js-inject**  dependency injection
* **prefabware-js-plugin**  a plugin system with extensionpoints that allows to organize javascript into flexible, extensible modules
* **prefabware-plugins**  various plugins that make up the client for appdriver
* **prefabware-js-model**  a type system to define composites, entites, valueobjects with attributes and references


### How do I get set up? ###
 
* Summary of set up
The code is one big multimodule maven project. Check it out, and build it using 
 ```mvn package ```
* Configuration!
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact