//a extension that is assigned to a type or a generic type name
define('prefabware/plugin/TypeExtension', [ 'dojo',
                                        'dojo/_base/lang',
                                        'dojo/_base/array',
                                        'prefabware/model/TypeRegistry',
                                        'prefabware/commons/ClassLoader',
                                        'prefabware/commons/ObjectProxy',
                                        'prefabware/plugin/Extension',
                                        'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
                                        'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
                                        'dojo/_base/declare' ], function(
		dojo, lang,array) {
	dojo.declare('prefabware.plugin.TypeExtension', [prefabware.plugin.Extension, prefabware.plugins.prefabware.model.TypeAcceptorMixin, prefabware.plugins.prefabware.model.TypeRegistryAccessMixin], {
		constructor : function() {
		},
		getType : function() {
			//returns the type, this exttension is assigned to
			//returns null, if this extension is assigned to a generic type name
			var type=this.getTypeRegistry().findType(this.json.type);
			return type;
		},		
		
	});
});

