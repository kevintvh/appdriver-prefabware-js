//a extension that can be hooked into an extensionPoint
//the extension has the same id as its ExtensionDefinition
define('prefabware/plugin/Extension', [ 'dojo',
                                        'dojo/_base/lang',
                                        'dojo/_base/array',
                                        'dojo/aspect',
                                        'prefabware/commons/ClassLoader',
                                        'prefabware/commons/ObjectProxy',
                                        'dojo/_base/declare' ], function(
		dojo, lang,array,aspect) {
	dojo.declare("prefabware.plugin.Extension", null, {
		//the plugin that offers the extensionPoint
		extendedPlugin :null,
		//the plugin this extension is declared in
		plugin :null,
		extensionPoint :null,//the point extended by this
		extendedPlugin : null,//the plugin where the extensionPoint is declared
		id:null,
		json:null,//an object with the declaration of this Extension, as read from the plugin.json
		//the class to load for the instance
		className :null,
		_definition :null,
		matcher :null,
		injector:{inject:'injector',type:'injector'},//to inject into objects created by this extension	
		loader:{inject:'classLoader',type:'bean'},
		
		constructor : function(definition) {
			this._definition=definition;
			this.plugin=definition.plugin;
			this.extendedPlugin=definition.extendedPlugin;
			this.extensionPoint=definition.extensionPoint;
			this.id=definition.id;
			this.matcher=definition.matcher;
			this.json=definition.json;
		},
		__afterCreate:function(){
			//subclasses may override this
			//the extension is created, all properties are set
			//also extension.json is set and can be modified
			//e.g. to apply defaults for unset properties
		},
		startup:function(){
			//subclasses may override this to inizialize the extension
			//they may return a promise or nothing			
		},				
		__startup:function(){
			//internal method to inizialize the extension,subclasses MUST NOT override this
			var promise=this.startup();
			if (promise!=null) {
				return promise;}
			else{
				return prefabware.lang.deferredValue();
			}
		},				
	});
});