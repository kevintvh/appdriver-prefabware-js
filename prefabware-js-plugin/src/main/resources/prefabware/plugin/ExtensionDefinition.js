//the definition of a extension that can be hooked into an extensionPoint
//based on this definition a extension can be created
//the definition is an internal object and should not be used by clients directly
//the definition creates the extension async
//the idea is to create instances of extensions as late as possible
define('prefabware/plugin/ExtensionDefinition',
		[ 'dojo', 'dojo/_base/lang', 'dojo/_base/array', 'dojo/promise/all','dojo/when',
				'prefabware/commons/match/AllMatcher', 'prefabware/lang',
				'dojo/Deferred', 'prefabware/commons/ClassLoader',
				'dojo/_base/declare' ], function(dojo, lang, array, all) {
			dojo.declare("prefabware.plugin.ExtensionDefinition", null, {
				json : null,// an object with the declaration of this Extension,
							// as read
				// from the plugin.json
				// the plugin this extension is declared in
				plugin : null,
				// the plugin that offers the extensionPoint
				extensionPoint : null,// the point extended by this
				extendedPlugin : null,//the plugin where the extensionPoint is declared
				extension : null, // the extension created for this definition
				cache:true,//true=reuse extension once created
				matcher:null,
				override:false,//true = this may override a allready registered extensiondefinition with the same id
				id : null,
				injector:{inject:'injector',type:'injector'},
				__loader:{inject:'classLoader',type:'bean'},
				// instances
				constructor : function(options) {
					lang.mixin(this, options);
					this.id = this.json.id;
					if (this.id == undefined) {
						// give every extension a unique id
						this.id = this.extensionPoint.nextExtensionId();
					}
					this.extendedPlugin=this.extensionPoint.plugin;
					if (this.json.override) {
						this.override=this.json.override;
					}
				},
				createMatcher : function() {
					// returns a matcher if there is one defined in json
					// like { ... matcher
					// :{className:'prefabware.commons.match.EqualMatcher'
					if (this.matcher != null) {
						prefabware.lang.deferredValue(this.matcher);
					};
					var that = this;
					var json = this.json.matcher;
					if (json==null) {
						//if no matcher at all was declared in the json
						//this extension should never be found by a matcher, it will only be accessed
						//by its id so create a NeverMatcher
						json={};
						json.className="prefabware.commons.match.NeverMatcher";
						}
					
					
					var	matcherClass = json.className;
					//if no matcher class is configured take the 
					//matcherclass of the extensionpoint
					if (matcherClass==null) {
						matcherClass = this.extensionPoint.matcherClass;
					};

					if (json.id == null) {
						json.id = this.id + '_' + 'matcher';
					};
					//options.owner=that;cannot provide that as part of the options because of strange sideeffects
					//options.json=json;//this statement lead to overlapping memory structure and strange sideeffects
					return this.__loader.createInstance(matcherClass, json)
							.then(function(matcher) {
								that.matcher = matcher;
								matcher.setOwner(that);
								return matcher;
							});
				},
				createExtension : function() {
					//async
					var def = this;
					if (this.cache&&this.extension != null) {
						return prefabware.lang.deferredValue(this.extension);
					}
					// creates the extension for this definition
					// and returns it async
					var className = this.__calcExtensionClass();
					// prefabware.lang.log( 'create instance of '+className +'
					// for
					// extensionPoint '+point.id +' in plugin '+plugin.id);
					var dM = this.createMatcher();
					var dI = this.__loader.createInstance(className, def).then(
							function(extension) {
								//the extension needs dependencies to be injected !
								def.extendedPlugin.injector.injectInto(extension);
								def.extension = extension;
								return extension;
							}).then(function(extension){
								//TODO remove?? because will be started immedietly after creation
								var dA=extension.__afterCreate();
								if (dA!=null) {
									return dA.then(function(){
										//wait till the returned promise is resolved, then return the extension
										return extension;
									});
								}else{
									return extension;
								}
							}).then(function(extension){
								var dS=extension.__startup();
								def.extension=extension;//may be reused if cache==true
								if (dS!=null) {
									return dS.then(function(){
										//wait till the returned promise is resolved, then return the extension
										return extension;
									});
								}else{
									return extension;
								}
							});;
					
					return all([ dM, dI ]).then(function(results) {
						return results[1];
					});
				},
				__calcExtensionClass : function() {
					if (this.json.className) {
						// theres a special class for this extension configured
						// in json
						// so use that
						return this.json.className;
					} else if (this.extensionPoint.json.extensionClass) {
						// the extensionpoint wants a special extension class
						return this.extensionPoint.json.extensionClass;
					} else {
						// default
						return 'prefabware.plugin.Extension';
					}
				},
			});
		});
