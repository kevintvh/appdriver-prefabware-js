//to define a Plugin
//a PluginDefinition can be 
//read from a file using  fromFile
//and than created using create Plgugin
//or 
//a plugin can be created directly from an JSON object
//using createPlugin

//the JSON object will be left unchanged e.g. for debugging
//instead all properties will be copied into the PluginDefinition
//ExtensionPoints of the Plugin are instantiated
//Extensions of the Plugin are NOT instantiated
//Dependencies of the Plugin are NOT instantiated

define('prefabware/plugin/PluginDefinition', [ 'dojo',
                                               'dojo/_base/array',
                                               'dojo/promise/all',
                                               'dojo/Deferred',
                                               'dojo/text',
                                               'prefabware/plugin/Plugin',
                                               'prefabware/plugin/Extension',
                                               'prefabware/plugin/ExtensionPoint',
                                               'prefabware/commons/ClassLoader',
                                               'dojo/_base/declare' ],
        function(dojo,array,all,Deferred) {
	dojo.declare("prefabware.plugin.PluginDefinition", null, {
		jsonPlugin:null,
		package_:null,  
		loader:{inject:'classLoader',type:'bean'},
		id:null,//ifentifies the plugin, must be unique
		constructor : function(id) {
			this.id=id;
		},	
		fromFile : function(package_) {
			//async
			//loads the plugin definition from the given package_
			//per convention 
			// -every plugin has its own file named 'plugin.json'
			// the plugin.json is located inside a folder with a layout according the segments of the plugin id.
			//this folder is called the plugin folder
			// example : id='d.e.f' ,folder= /d/e/f ,file='/d/e/f/plugin.json' 
			//(the Java build tool Maven uses the same layout for its repositories)
			
			//package_ specifies the location where the plugin folder is searched
			//the package_ is just used to find the plugin.json, it is not used to identify a plugin in any way
			
			//with data-dojo-config (former dojoConfig) many package_s can be registered.
			//every entry in dojoConfig maps a dot-seperated namespace to a folder relative to the dojo folder
			//			data-dojo-config="'async':true,'parseOnLoad':false,
			//				'packages':[{'name':'prefabware','location':'../../prefabware'},
			//							{'name':'apps','location':'../../apps'},
			//							{'name':'shapes','location':'../../shapes'},
			//							{'name':'jquery-ui','location':'../../jquery-ui'},
			//							{'name':'yui','location':'../../yui'}]"
			
			//from this dojo 'knows' where to find e.g. the package prefabware and all its subpackages
			//this is used to load classes in dojo.
			//dojo.cache uses the same logic to load resources. 
			//dojo.cache is used here to load the plugin file
			
			//example : package=='prefabware.b.c', id='d.e', fileName='json.plugin'
			//1) the location that is registered in data-dojo-config is used to replace a given package name
			//   so 'prefabware is replaced by location'../../prefabware'
			//   the resulting path =='../../prefabware.b.c
			//2) the plugin id is attached 
			//   plugin id='d.e'
			//   the resulting path =='../../prefabware.b.c.d.e
			//3) '.' is replaced by '/'
			//   the resulting path =='../../prefabware/b/c/d/e
			//4) the file name is attached
			//   the resulting file name =='../../prefabware/b/c/d/e/json.plugin
			//5) finally the file is loaded from there
			//dojo.cache works a little different, so you do not see the above reflected in the code here 
			var that=this;
			this.package_=package_;//remember for debugging and logging
			var pluginPath=this.__path(this.id)+'/plugin.json';
			return this.loadJsonFromFile(package_,pluginPath).then(function(jsonPlugin){
				//set the namespace of the jsonPlugin, to use it as a default for dependencies
				jsonPlugin.namespace=package_;
				prefabware.lang.log('loaded plugin from '+pluginPath);
				if (jsonPlugin==undefined) {
					throw 'could not load '+pluginPath+' for plugin '+that.id;
				}
				return that.createPlugin(jsonPlugin);
			});
		},
		loadJsonFromFile : function(namespace,fullFileName) {
			if (namespace==undefined) {
				throw 'namespace must be set';
			}
			if (namespace==undefined) {
				throw 'fileName must be set';
			}
			var file=namespace.split('.').join('/')+'/'+fullFileName;
			//loads a json object from the given file
			//var string=dojo.cache(namespace, fullFileName);
			//TODO use ReourceJSonFile here
			var deferred = new Deferred();
			var that=this;
			require(["dojo/text!"+file], function(string){
				if (string==null) {
					throw 'could not read file from '+namespace+fullFileName;
				}
				prefabware.lang.log('-----------------------------------------');
				prefabware.lang.log('from '+file);
				prefabware.lang.log('read '+string);
				prefabware.lang.log('-----------------------------------------');
				var json=dojo.fromJson(string);
				json.source=file;
				deferred.resolve(json);
			});
			return deferred;
		},
		createPlugin : function(jsonPlugin) {
			var that=this;
			that.jsonPlugin=jsonPlugin;
			//creates a plugin based on its json configuration
			//creates the extension points, the plugin offers
			//does NOT create the extensions the plugin contributes
			//the extensions of the plugin definition (the json object) 
			//can be accessed using jsonPlugin.extensions
			//does NOT load dependencies
			//the dependencies of the plugin definition (the json object) 
			//can be accessed using jsonPlugin.extensions
			var plugin;
			//the plugin instance is created here at the moment
			//for load optimization this may be delayed until its really used
			var wait = new Deferred();			 
			if (jsonPlugin.className) {
				prefabware.lang.log( 'trying to create instance of '+jsonPlugin.className+' for plugin '+jsonPlugin.id);
				//loads the class async !!
				var deferred = that.loader.load(jsonPlugin.className);
				deferred.then(function(clazz){
					plugin=new clazz();
					wait.resolve(plugin);
				})
			}else{
				plugin = new prefabware.plugin.Plugin(jsonPlugin.id);
				wait.resolve(plugin);
			};
			return wait.then(function(plugin){
			if (plugin==undefined) {
				throw 'could not create instance of plugin '+jsonPlugin.id;
			};
			plugin.id=jsonPlugin.id;
			plugin.comment=jsonPlugin.comment;
			plugin.json=jsonPlugin;
			plugin.namespace=jsonPlugin.namespace;
			
			//Extensions and Dependencies will be created by the PluginRegistry
			plugin.dependencies=jsonPlugin.dependencies;
			array.forEach(plugin.dependencies,function(dependency){
				if (dependency.namespace==undefined) {
					//set the default namespace
					dependency.namespace=plugin.namespace;
				}
				if (dependency.id===plugin.id) {
					throw 'plugin '+plugin.id+' from namespace '+plugin.namespace+' declares a dependency on itself';
				}
			});
			
			prefabware.lang.debug("finished parsing Plugin "+ plugin.id);
			return plugin;
			}).then(function(plugin){
				//create and add the extensionPoints the plugin offers
				return that.__createExtensionPoints(plugin);
			});
		},	
		__path : function(id) {
			return id.replace(/\./g, '/');
		},		
		__createExtensionPoints : function(plugin) {
			var jsonPlugin=plugin.json;
			if (jsonPlugin.extensionPoints===undefined||jsonPlugin.extensionPoints===null||jsonPlugin.extensionPoints.length===0) {
				prefabware.lang.log( 'plugin '+this.id+' declares no extensionPoints');
			}
			//create extensionpoints async
			var promises=array.map(jsonPlugin.extensionPoints, function(jPoint){
				return this.__createExtensionPoint(jPoint,plugin);
			},this);
			//return a promise that resolves,when all extensions are resolved and registered
			return all(promises).then(function(results){
				array.forEach(results,function(extensionPoint){
					//should not be needed, its available as declaredClass
					//extensionPoint.className=extensionPoint.json.className;
					plugin.registerExtensionPoint(extensionPoint);
				});
				return plugin;
			  });
		},
		__createExtensionPoint : function(jPoint,plugin) {
			var that=this;
			//extensionpoints have to be loaded directly not lazy e.g. to check extensions against it
			var extensionPoint=null;
			if (jPoint.id.indexOf(this.id)!=0) {
				prefabware.lang.warn('extensionPoint id '+jPoint.id+' should start with the plugins id '+this.id);
			}
			var wait = new Deferred();
			var className=jPoint.className;
			var options={
					'id':jPoint.id,
					'plugin':plugin,
					'json':jPoint,
					'min':jPoint.min,
					'max':jPoint.max,
					'extensionClass':jPoint.extensionClass,
					'createDefault':jPoint.createDefault,
					'source':plugin.json.source
					};
			if (className) {
				prefabware.lang.log( 'trying to create instance of '+className+' for plugin '+plugin.id);
				that.loader.load(className).then(function(clazz){
					extensionPoint=new clazz(options);
					wait.resolve(extensionPoint);
				});
			}else{
				//TODO provide the whole json as parameter to the constructor ?
				extensionPoint=new prefabware.plugin.ExtensionPoint(options);
				wait.resolve(extensionPoint);
			};
			
			//this.injector.register({id:extensionPoint.id,type:'extensionpoint'},extensionPoint);
			return wait.promise;
		},
	});
});
