//a Registry for Plugins
define('prefabware/plugin/PluginRegistry', [ 'dojo',  'dojo/text',
                                             'dojo/_base/array',
                                             "dojo/promise/all",
                                             'dojo/_base/Deferred',
                                             'dojo/aspect',
                                             'prefabware/lang',
                                             'prefabware/injector/Injector',
                                             'prefabware/util/Map',
                                             'prefabware/plugin/Plugin',
                                             'prefabware/plugin/PluginDefinition',
                                             'prefabware/plugin/ExtensionDefinition',
                                             'prefabware/plugin/Extension',
                                             'prefabware/plugin/ExtensionPoint',
                                             'prefabware/commons/ObjectProxy',
                                             'dojox/collections/Dictionary',
                                             'dojox/collections/ArrayList',
                                             'dojo/_base/declare'],
    function(dojo,TEXT,array,promiseAll,Deferred,aspect) {
	dojo.declare("prefabware.plugin.PluginRegistry", null, {
		//the namespace of the plugins folder
		//the real path name depends on the path set in dojo config for the namespace
		//in this folder the registry.json is searched
		//inside this folder the plugin.json files are searched
		//e,g, plugin prefabware.ui_dijit is searched in prefabware.plugin.prefabware.ui_dijit
		registryNamespace:'prefabware.plugins',
		registryFileName:'registry.json',		
		plugins : null,
		pluginList : null,// a list of all plugins, in the order they are loaded
		loading : null,// all plugins currently loading, to detect cycles
		extensionPoints :null,
		extensions:null,
		injector:null,
		loader:null,
		counter:0,
		constructor : function(options) {
			this.loader=new prefabware.commons.ClassLoader();
			if (options===undefined) {
				options={};
			}
			if (options.namespace!=undefined) {
				this.registryNamespace=options.namespace;
			}
			if (options.fileName!=undefined) {
				this.registryFileName=options.fileName;
			}
			this.injector=new prefabware.injector.Injector();
			this.injector.register({id:'classLoader',type:'bean'},this.loader);
			var that=this;
			aspect.after(this.loader,"createInstance",function(promise){
				//let the classloader inject into all instances
				return promise.then(function(instance){
					that.injector.injectInto(instance);
					return instance;
				});
			});
			this.plugins=new dojox.collections.Dictionary();	
			this.pluginList=new dojox.collections.ArrayList();
			this.loading=new dojox.collections.Dictionary();	
			this.extensionPoints=new dojox.collections.Dictionary();	
			//add comfort methods to the map, it will be passed to the extensions
			var extensions=new prefabware.util.Map();
			    extensions.findExtension=function(id){
				//returns the extension with the given id
				return extensions.find(id);
			};
			    extensions.resolveReference=function(id){
				//returns the extension with the given @id
				//cut of the @ before looking for the extension
				return extensions.fetchExtension(id.substring(1));
			};
			this.extensions=extensions;
		},		
		starting:null,
		startup : function() {			
			//is async
			//returns a promise
			var that=this;
			return this.loadFromFile().then(function(){
			//inject into all registered beans
			that.injector.startup();
			
			var startupPromises=array.map(that.plugins.getValueList(), function(plugin){		
				return plugin.__startup();
			},that);
			
			return promiseAll(startupPromises).then(function(){
				var afterStartup=array.map(that.plugins.getValueList(), function(plugin){	
					prefabware.lang.log( 'calling plugin.afterStartup of '+plugin.id);
					return plugin.__afterStartup();
				},that);
				return promiseAll(afterStartup).then(function(){return that;});
			});
			});
		},
			
		__key : function(defId) {
			return defId.replace(/\./g, '_');;
		},		
		__path : function(id) {
			return id.replace(/\./g, '/');;
		},		
		findPlugin : function(id) {
			return this.plugins.item(id);
		},
		findExtensionPoint : function(pointId) {
			return this.extensionPoints.item(pointId);
		},
		loadFromFile : function() {
			//if no registry was set, do nothing
			if (this.registryFileName==null) {
				return;
			}
			//build a list of all registries, the root and the imported
			var registries= this.resolveRegistries(this.registryNamespace, this.registryFileName);
			
			//build a list with all plugins of all registries
			var jPlugins=new Array();
			array.forEach(registries, function(jRegistry){
				array.forEach(jRegistry.plugins, function(jPlugin){
					if (jPlugin.namespace==undefined) {
						//this===jRegistry here
						jPlugin.namespace=this.namespace;
						if (jPlugin.namespace==undefined) {
							//should not happen
							throw 'plugin '+jPlugin.id+' has no namespace';
						}
					}
					jPlugins.push(jPlugin);
				},jRegistry);
			},this);
			
			//sort the plugins in start level order
			jPlugins.sort(function(plugin1, plugin2) {
				  // Ascending
				  return plugin1.startLevel - plugin2.startLevel;
				});
			//now we have a list of all the plugins to load,
			//including those from imported registries
			//in the order of the startlevel
			//but still we dont have resolved the dependencies
			//var jPluginsWithDep=this.resolveDependencies(jPlugins);
			var that=this;
			return this.loadPlugins(jPlugins).then(function(plugins){
				prefabware.lang.log( 'loaded Plugins :');
				that.pluginList.forEach(function(plugin){
					prefabware.lang.log( plugin.id);
				});
			});			
		},
		resolveRegistries : function(namespace,fileName) {
			var registries=new Array();
			//loads a registry from the file with fileName in the namespace
			//returns the registry			
			
			prefabware.lang.log( 'trying to read PluginRegistry from namespace='+namespace+' name='+fileName);
			var registry=this.readJson(namespace,fileName);
			if (registry==undefined) {
				throw 'could not read PluginRegistry from namespace='+namespace+' name='+fileName;
			}
			//the namespace of a registry is the default namespace for its plugins
			//so store it it for later access
			registry.namespace=namespace;
			registries.push(registry);
			//imports
			 array.forEach(registry.imports,function(import_){
				 //caution 'import' is an reserved word of javascript !!
				 //import is the qualified name of the file like
				 //prefabware.business.commons.registry.json				 
				 var imports=this.resolveRegistries(import_.namespace, import_.fileName);
				 registries.push(imports);
			 },this);
			 
			 return registries;
			 
		},
		loadPlugins : function(jPlugins) {
			//loads the plugins
			var promises= array.map(jPlugins, function(jPlugin){	
				prefabware.lang.log( 'about to load Plugin '+jPlugin.id);
				 return this.loadPlugin(jPlugin);
				  },this);
			 return promiseAll(promises);
		},
		loadPlugin : function(jPlugin) {
			//async !!
			//this first step loads the jPlugin and all its dependencies (also transitive)
			//from their plugin.json files
			//nothing else is done in this first step
			var that=this;
			var plugin=this.findPlugin(jPlugin.id);
			if (plugin!=undefined) {
				prefabware.lang.log( 'Plugin '+plugin.id+" was allready registered, no need to load again");
				var wait = new Deferred();		
				//if allready registered, just return a allready resolved promise
				wait.resolve(plugin);
				return wait.promise;
			};			
			//are we in a cycle ?
			if (that.loading.item(jPlugin.id)!=null) {
				//because the plugins load asynchronous, in rare cases the current plugin was 
				//loaded before but not yet registered.
				//to avoid double loading just cancel loading here
				//just return a null, will be filtered out later
				return prefabware.lang.deferredValue(null);
			}else{
				that.loading.add(jPlugin.id,jPlugin);
			}
			var def=new prefabware.plugin.PluginDefinition(jPlugin.id);
			this.injector.injectInto(def);
			return def.fromFile(jPlugin.namespace).then(function(plugin){
			 //the plugin now is only partially loaded, 
			 //extensionPoints are loaded, extensions and dependencies are not loaded
			 //this has to be done now
			prefabware.lang.log( 'about to load dependencies of Plugin '+plugin.id);
			var promises= array.map(plugin.json.dependencies,function(dependency){				 
				//load dependencies BEFORE the plugin
				prefabware.lang.log( 'about to load dependency '+dependency.id+' of Plugin '+plugin.id);
				var promise=null;
				try {
					promise=this.loadPlugin(dependency);
				} catch (e) {
					if (dependency.optional==true) {
						//swollow the exception
						prefabware.lang.log("could not load optional dependency "+dependency.id);
						promise=prefabware.lang.deferredValue(null);
					}else{
						throw e;
					}
				}
				return promise;
				  },that);
			 return promiseAll(promises).then(function(){
				 prefabware.lang.log( 'loaded all dependencies of Plugin '+plugin.id);
				 //register the plugin and its extension points
				 that.registerPlugin(plugin);
				 //now that plugins are loaded, that this plugin dependes on
				 //and the own extensionpoints are also registered,
				 //we can create the extensions				 
				 that.__registerExtensions(plugin);
				 that.loading.remove(plugin.id);
			 });
			});
		},
		registerPlugin : function(plugin) {
			prefabware.lang.log( 'registering Plugin '+plugin.id);
			var id=plugin.id;
			//register early
			this.plugins.add(id,plugin);
			//also maintain a list of the plugins in the order they were registered
			this.pluginList.add(plugin);
			//set a backwards reference
			//that allows plugins to create and register extensions at runtime
			//TODO remove this, should be done by injection
			plugin.pluginRegistry=this;
			//register all the extension points that the plugin offers
			array.forEach(plugin.extensionPoints.getValueList(), function(extensionPoint){
				this.extensionPoints.add(extensionPoint.id,extensionPoint);
				prefabware.lang.log('registered extensionPoint '+extensionPoint.id+' for plugin '+id);
			},this);
			 this.injector.register({id:plugin.id,type:'plugin'},plugin);
			prefabware.lang.log('registered Plugin '+id);
		},
		registerExtensions : function(plugins) {
			array.forEach(plugins, function(plugin){
				prefabware.lang.log("2. pass "+ plugin.id);
				this.__registerExtensions(plugin);
				prefabware.lang.debug("finished parsing Plugin", plugin.id);
			},this);
		},
		__registerExtensions : function(plugin) {
			//before all Extensionpoints were registered through registerPlugin.
			//now we can register the extensions
			//register all the extensions that the plugin declares
			array.forEach(plugin.json.extensions, function(jextension, i){
				var point=this.findExtensionPoint(jextension.extensionPoint);
				if (!point) {
					throw('could not find extensionPoint '+jextension.extensionPoint+' extended by plugin '+plugin.id);
				}
				jextension.source=plugin.json.source;
				var owner=point.plugin;
				var extension=this.__createExtension(jextension,plugin,owner,point);
				this.__registerExtension(extension);
			},this);
			prefabware.lang.log('finished 2. pass for plugin '+plugin.id);
		},
		__registerExtension : function(extension) {
			var point=extension.extensionPoint;
			point.registerExtensionDef(extension);
			//register to the points plugin also
			point.plugin.registerExtensionDef(extension);
			this.extensions.put(extension.id,extension);
			
			extension.extensions=this.extensions;
			 //register with override=true, overriding conflicts are resolved where the extension is registered in the point
			this.injector.register({id:extension.id,type:'extension'},extension,true);
			prefabware.lang.log('registered extension '+extension.id+' to plugin '+extension.plugin.id);
		},
		readJson : function(namespace,fullFileName) {
			var string=dojo.cache(namespace, fullFileName);
			prefabware.lang.log('read '+string);
			prefabware.lang.log('from '+namespace+fullFileName);
			var json=dojo.fromJson(string);
			json.source=namespace+'/'+fullFileName;
			return json;
		},		
		__createExtension : function(jExtension,plugin,extendedPlugin,point) {
			var extension=new prefabware.plugin.ExtensionDefinition(
					{id:jExtension.id,
					json:jExtension,
					plugin:plugin,
					extendedPlugin:extendedPlugin,
					extensionPoint:point}
					);
			this.injector.injectInto(extension);
			return extension;
		},
	});
});
