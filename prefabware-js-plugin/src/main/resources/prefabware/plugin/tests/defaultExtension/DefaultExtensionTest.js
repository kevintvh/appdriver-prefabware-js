// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.DefaultExtensionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.DefaultExtensionTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.defaultExtension',fileName:'defaultExtensionTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var plugin=that.registry.findPlugin('defaultExtension.a');
			doh.assertTrue(plugin!=null);
			plugin.matchExtensions("defaultExtension.a.extension1",{value:"y"})
				.then(function(extensions1){
					doh.assertTrue(extensions1!=null);
					doh.assertTrue(extensions1.length==1,"default should be created");
					var extension1=extensions1[0];
					doh.assertEqual('prefabware.plugin.tests.defaultExtension.Extension1Default',extension1.declaredClass);
					return extension1;
				})
				.then(function(extension1){
					plugin.matchExtensions("defaultExtension.a.extension1",{value:"y"})
						.then(function(extensions2){
							doh.assertTrue(extensions2!=null);
							doh.assertTrue(extensions2.length==1,"extension must be returned");
							var extension2= extensions2[0];
							doh.assertTrue(extension2===extension1,"default extension should be reused");
							return extension2; 
						});
				})
				.then(function(extension2){
					plugin.matchExtensions("defaultExtension.a.extension1",{value:"z"})
					.then(function(extensions3){
						doh.assertTrue(extensions3!=null);
						doh.assertTrue(extensions3.length==1,"default should be created");
						var extension3= extensions3[0];
						doh.assertTrue(extension3.id=='withZ');
						testDeferred.callback(true);
				});
			});
		});
		return testDeferred;	
	},
	tearDown : function() {
	}
},
]);