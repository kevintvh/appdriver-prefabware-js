dojo.provide("prefabware.plugin.tests.extension.ExtensionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.ExtensionDefinition");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.extension.ExtensionTest", [ 

{
	name : "matchExtension",
	timeout : 3000,
	extensionPoint : null,
	extension : null,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
	createExtensionDef : function(plugin,value) {
		var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
		doh.assertTrue(eP!=undefined);

		var json={'extensionPoint':'a.extensionpoint1',
					'id':'extension'+'_'+value,
				'matcher'   : {
					'className' : 'prefabware.commons.match.EqualMatcher',
					'value'     : value			 
				}};
		var ext= new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
		this.registry.injector.injectInto(ext);
		return ext;
	},
	createExtensionDefWoMatcher : function(plugin) {
		//creates a plugindefinition wthout matcher, so a default matcher will be assigned
		var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
		doh.assertTrue(eP!=undefined);
		
		var json={'extensionPoint':'a.extensionpoint1',
					'id':'extension_withoutmatcher'
				};
		var ext= new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
		this.registry.injector.injectInto(ext);
		return ext;
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		var jsonPlugin={'id':'a.b',
					'comment'    : 'comment of plugin',   
					'extensionPoints' : [
					                     {'id':'a.b.extensionpoint1','min':1,'max':99999},
					                     ],
					                     'dependencies' : [ 
					                                       {'id':'dependency.c',
					                                       },
					                                       {'id':'dependency.d'
					                                       },
					                                       ]
			};
			
			var def=new prefabware.plugin.PluginDefinition();
			this.registry.injector.injectInto(def);
			def.createPlugin(jsonPlugin)
						.then(function(plugin){
							doh.assertTrue(plugin!=null);
							//this is normally done by the registry
							plugin.injector=new prefabware.injector.Injector(); 
							//var extd0=that.createExtensionDefWoMatcher(plugin);
							var extd1=that.createExtensionDef(plugin,'value1');
							var extd2=that.createExtensionDef(plugin,'value2');
							var extd3=that.createExtensionDef(plugin,'value3');
							var extd3_2=that.createExtensionDef(plugin,'value3');
							var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
							//eP.registerExtensionDef(extd0);
							eP.registerExtensionDef(extd1);
							eP.registerExtensionDef(extd2);
							eP.registerExtensionDef(extd3);
							extd3_2.id='extension_value3_2';
							eP.registerExtensionDef(extd3_2);
							eP.__startup();
							eP.matchExtension({value:'value2'}).then(function(extension){
								doh.assertFalse(extension==null);
								doh.assertEqual('extension_value2',extension.id,'should match the second extension');
								doh.assertEqual('prefabware.plugin.Extension',extension.declaredClass);
								doh.assertEqual('prefabware.commons.match.EqualMatcher',extension.matcher.declaredClass);
								return ;
							}).then(function(){
								eP.matchExtensions({value:'value3'}).then(function(extensions){
									doh.assertFalse(extensions==null);
									doh.assertEqual(2,extensions.length,'the two extensions with value3 should be found');
									return ;
								});
							}).then(function(){
									eP.matchExtensions({value:'value99'}).then(function(extensions){
										doh.assertFalse(extensions==null);
										doh.assertEqual(0,extensions.length,'no two extensions with value99 should be found');
										return ;
								}).then(function(){
									testDeferred.callback(true);
								});
							});;
						});
	
		return testDeferred;
	},
	tearDown : function() {
	}
},
{
	name : "create extension",
	timeout : 3000,
	extensionPoint : null,
	extension : null,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		var jsonPlugin={'id':'a.b',
				'comment'    : 'comment of plugin',   
				'extensionPoints' : [
				                     {'id':'a.b.extensionpoint1','min':1,'max':99999},
				                     ],
				                     'dependencies' : [ 
				                                       {'id':'dependency.c',
				                                       },
				                                       {'id':'dependency.d'
				                                       },
				                                       ]
		};
		
		var def=new prefabware.plugin.PluginDefinition();
		that.registry.injector.injectInto(def);
		def.createPlugin(jsonPlugin)
		.then(function(plugin){
			doh.assertTrue(plugin!=null);
			//this is normally done by the registry
			plugin.injector=new prefabware.injector.Injector(); 
			var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
			doh.assertTrue(eP!=undefined);
			
			var json={'extensionPoint':'a.extensionpoint1',
					'matcher'   : {
						'className' : 'prefabware.commons.match.EqualMatcher',
						'value'     : 'value1'			 
					}};
			var extd=new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
			that.registry.injector.injectInto(extd);
			extd.createExtension().then(function(extension){
				doh.assertFalse(extension==null);
				doh.assertEqual('a.b.extensionpoint1_1',extension.id,'should have a generated id');
				doh.assertEqual('prefabware.plugin.Extension',extension.declaredClass);
				doh.assertEqual('prefabware.commons.match.EqualMatcher',extension.matcher.declaredClass);
				testDeferred.callback(true);
			});
		});
		
		
		return testDeferred;
	},
	tearDown : function() {
	}
},
{
	name : "create matcher",
	registry : null,
	extensionPoint : null,
	extension : null,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
runTest : function() {
	var testDeferred=new doh.Deferred();
	var that=this;
		var jsonPlugin={'id':'a.b',
				'comment'    : 'comment of plugin',   
				'extensionPoints' : [
				                     {'id':'a.b.extensionpoint1','min':1,'max':99999},
				                     ],
				                     'dependencies' : [ 
				                                       {'id':'dependency.c',
				                                       },
				                                       {'id':'dependency.d'
				                                       },
				                                       ]
		};
		
		var def=new prefabware.plugin.PluginDefinition();
		that.registry.injector.injectInto(def);
		def.createPlugin(jsonPlugin)
		.then(function(plugin){
			doh.assertTrue(plugin!=null);
			var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
			doh.assertTrue(eP!=undefined);
			
			var json={'extensionPoint':'a.extensionpoint1',
					'matcher'   : {
						'className' : 'prefabware.commons.match.EqualMatcher',
						'value'     : 'value1'			 
					}};
			var extd=new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
			that.registry.injector.injectInto(extd);
			extd.createMatcher().then(function(matcher){
				doh.assertFalse(matcher==null);
				doh.assertEqual('prefabware.commons.match.EqualMatcher',matcher.declaredClass);
				doh.assertEqual('value1',matcher.value);
				testDeferred.callback(true);
			});
		});
		
		
		return testDeferred;
	},
	tearDown : function() {
	}
},

]);