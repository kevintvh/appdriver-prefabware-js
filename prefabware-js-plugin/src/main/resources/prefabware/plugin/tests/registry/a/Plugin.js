//a view that can be shown in the workspace
define('prefabware/plugin/tests/registry/a/Plugin', [ 
       'dojo',
       'prefabware/plugin/Plugin',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugin.tests.registry.a.Plugin", [prefabware.plugin.Plugin], {
		//modelPlugin:{inject:'prefabware.model',type:'plugin'},  	
		started:false,
		constructor : function() {
		},		
		doSomething : function() {
			return "doSomething";
		},
	});
});
