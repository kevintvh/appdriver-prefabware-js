// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.registry.PluginRegistryTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.Deferred");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.registry.PluginRegistryTest", [ 
{
	name : "load registry" +
			"",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugin.tests.registry',fileName:'pluginRegistryTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var plugin=that.registry.findPlugin('a');			
			doh.assertEqual(plugin.declaredClass,'prefabware.plugin.tests.registry.a.Plugin',"expected to find plugin a");
			doh.assertTrue(plugin.started,"plugin a should be started");
			//get all extensions of action
			var ep=plugin.findExtensionPoint('a.extension1');
			//there must be some defined
			doh.assertTrue(ep!=null);
			doh.assertEqual(ep.declaredClass,"prefabware.plugin.tests.registry.a.Extension1");
			
			plugin.fetchExtension('a.optionalextension1').then(function(extension){
				doh.assertTrue(extension==null,'null expected, none registered and is optional');
				testDeferred.callback(true);
			});
			
		});
		return testDeferred;
	},
	tearDown : function() {
	}
},
//{
//	name : "load",
//	registry : null,
//	extensionPoint : null,
//	extension : null,
//	setUp : function() {
//		this.registry = new prefabware.plugin.PluginRegistry(
//				{namespace:'prefabware.plugin.tests',fileName:'pluginRegistryTest-registry-types.json'});
//		
//	},
//	runTest : function() {
//		
//		this.registry.startup();
//		
//		var plugin=this.registry.findPlugin('prefabware.model');		
//		doh.assertFalse(plugin==null||plugin==undefined,"expected to find plugin");
//
//		//with the ModelServer we do not need to have an extension for model.types		
//		var extensionPoint=plugin.findExtensionPoint('prefabware.model.typeLoader');
//		doh.assertTrue(extensionPoint!=null);
//		var extensions=plugin.findExtensionsByPoint(extensionPoint.id);
//		doh.assertEqual(1,extensions.length);
//		
//		
//		var typeRegistry=plugin.getTypeRegistry();
//		doh.assertTrue(typeRegistry!=undefined);
//		//the types should be registered at the type registry
//		var cur=typeRegistry.findType('com.prefabware.business.commons.QuantityUnit');
//		doh.assertFalse(cur==null);
//		
//		plugin=this.registry.findPlugin('prefabware.ui_dijit');
//		doh.assertFalse(plugin==null||plugin==undefined);
//		
//	},
//	tearDown : function() {
//	}
//},
]);