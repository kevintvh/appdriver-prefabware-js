//the ui controller
//can openEditor,openView
define('prefabware/plugin/tests/extensionInject/Extension1', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,aspect) {
	dojo.declare("prefabware.plugin.tests.extensionInject.Extension1", [prefabware.plugin.Extension], {
		startup : function() {
		},
		constructor : function() {
		},
		createClassToLoad : function() {
			return this.loader.createInstance(this.json.classToLoad);
		},
	});
});
