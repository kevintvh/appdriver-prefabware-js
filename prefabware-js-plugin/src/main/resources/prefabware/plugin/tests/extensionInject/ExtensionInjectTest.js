// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.ExtensionInjectTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.ExtensionInjectTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.extensionInject',fileName:'plugin-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var plugin=that.registry.findPlugin('extensionInject.a');
			doh.assertTrue(plugin!=null);
			plugin.fetchExtension("extensionInject.a.extension1")
				.then(function(ext){
					doh.assertTrue(ext!=null);
					return ext.createClassToLoad();
				})
				.then(function(instance){
					doh.assertTrue(instance.injector!=null,"the injector should have been injected !");
					doh.assertEqual("prefabware.injector.Injector",instance.injector.declaredClass,"the injector must be an instance of Injector !");
					testDeferred.callback(true);
			});
		});
		return testDeferred;	
	},
	tearDown : function() {
	}
},
]);