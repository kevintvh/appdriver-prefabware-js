// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.cache.CacheTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.plugin.PluginRegistry");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.cache.CacheTest", [ 

{
	name : "cached extensions",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.cache',fileName:'plugin-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var plugin=that.registry.findPlugin('a.b');
			doh.assertTrue(plugin!=null);
			plugin.matchExtensions("a.b.cached",{value:"a1"})
				.then(function(extensions1){
					doh.assertTrue(extensions1!=null);
					doh.assertTrue(extensions1.length==1,"one extension should be found");
					var extension1=extensions1[0];
					doh.assertEqual("a1",extension1.json.matcher.value);
					return extension1;
				})
				.then(function(extension1){
					plugin.matchExtensions("a.b.cached",{value:"a1"})
						.then(function(extensions2){
							doh.assertTrue(extensions2!=null);
							doh.assertTrue(extensions2.length==1,"extension must be returned");
							var extension2= extensions2[0];
							doh.assertTrue(extension2===extension1,"the same extension should be returned because cached");
							testDeferred.callback(true);
							return extension2; 
						});
				});				
		});
		return testDeferred;	
	},
	tearDown : function() {
	}
},


]);