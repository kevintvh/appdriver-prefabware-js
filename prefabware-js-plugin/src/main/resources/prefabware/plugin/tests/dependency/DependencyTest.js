// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.DependencyTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.DependencyTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.dependency',fileName:'dependencyTest-registry.json'});
	},
	runTest : function() {
		
		this.registry.startup();
		var aPlugin=this.registry.findPlugin('dependency.a');
		doh.assertFalse(aPlugin==null);
		var bPlugin=this.registry.findPlugin('dependency.b');
		doh.assertFalse(bPlugin==null);
		
	},
	tearDown : function() {
	}
},
{
	name : "load one plugin from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.dependency',fileName:'registry-a-only.json'});
	},
	runTest : function() {
		this.registry.startup();
		
		var plugin=this.registry.findPlugin('dependency.a');		
		doh.assertFalse(plugin==null||plugin==undefined);
		
		//we loaded only plugin dependency.a from the registry
		//through dependencied dependency.b and dependency.c should also be loaded
		var plugin=this.registry.findPlugin('dependency.b');		
		doh.assertFalse(plugin==null||plugin==undefined);
		var plugin=this.registry.findPlugin('dependency.c');		
		doh.assertFalse(plugin==null||plugin==undefined);
		
	},
	tearDown : function() {
	}
},
]);