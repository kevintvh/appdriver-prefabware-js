dojo.provide("prefabware.plugin.tests.nonlazy.NonLazyExtensionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.ExtensionDefinition");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.nonlazy.NonLazyExtensionTest", [ 

{
	name : "NonLazyExtensionTest",
	timeout : 3000,
	registry : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugin.tests.nonlazy',fileName:'nonlazyTest-registry.json'});
	},
	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var pluginA=that.registry.findPlugin('nonlazy.nonlazy_a');//its a bad name for a plugin..			
			doh.assertEqual(pluginA.declaredClass,'prefabware.plugin.Plugin');
			doh.assertTrue(pluginA.started,"plugin a should be started");
			var point1=pluginA.findExtensionPoint('nonlazy.extensionpoint1');
			var point2=pluginA.findExtensionPoint('nonlazy.extensionpoint2');
			doh.assertEqual('prefabware.plugin.Extension',point1.extensionDefinitions.getIterator().get(0).value.extension.declaredClass,'should be allready resolved because extensionpoint is not lazy');
			doh.assertEqual(null,point2.extensionDefinitions.getIterator().get(0).value.extension,'should not be resolved because extensionpoint is lazy');
			testDeferred.callback(true);
		});
		return testDeferred;
	},
	tearDown : function() {
	}
},


]);