// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.definition.PluginDefinitionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.plugin.PluginRegistry");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.definition.PluginDefinitionTest", [ 

{
	name : "load plugin from object",
	timeout:1000,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		//test to load a plugin definition from an object
		//with a ExtensionPoint that will be loaded dynamically
		var jsonPlugin={'id':'a.b',
			'comment'    : 'comment of plugin',   
			 'extensionPoints' : [
			                      {'id':'a.b.extensionpoint1', 'className':'prefabware.plugin.tests.definition.MyExtensionPoint','min':1,'max':99999}
			                      ],
			'dependencies' : [ 
								{'id':'dependency.c',
			  					  },
			  					  {'id':'dependency.d'
			  					  },
							  ]
			};
		
		var def=new prefabware.plugin.PluginDefinition();
		this.registry.injector.injectInto(def);
		def.createPlugin(jsonPlugin)
					.then(function(plugin){
						doh.assertTrue(plugin!=null);
						doh.assertEqual('a.b', plugin.id);
						var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
						doh.assertTrue(eP!=undefined);
						doh.assertEqual(eP.declaredClass,"prefabware.plugin.tests.definition.MyExtensionPoint");
						
						doh.assertEqual('comment of plugin', plugin.comment);
						doh.assertEqual(2, plugin.dependencies.length);
						doh.assertEqual('dependency.c', plugin.dependencies[0].id);
						doh.assertEqual('dependency.d', plugin.dependencies[1].id);	
						testDeferred.callback(true);
					});
		
		return testDeferred;
	},
},
{
	name : "load plugin from file",
	timeout:1000,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
	runTest : function() {
		//test to load a plugin definition from a file
		var testDeferred=new doh.Deferred();
		var pluginId='a.b';
		var def=new prefabware.plugin.PluginDefinition(pluginId);
		this.registry.injector.injectInto(def);
		def.fromFile('prefabware.plugin.tests.definition').then(function(plugin){
			doh.assertTrue(plugin!=null);
			doh.assertEqual(plugin.declaredClass,"prefabware.plugin.Plugin");
			testDeferred.callback(true);
		});
		return testDeferred;
	},
	tearDown : function() {
	}
},


]);