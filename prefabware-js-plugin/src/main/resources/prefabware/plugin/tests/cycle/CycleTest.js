// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.CycleTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.CycleTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.cycle',fileName:'cycleTest-registry.json'});
	},
	runTest : function() {
//		var exceptionCatched=false;
//		try {
//			this.registry.startup();
//		} catch (e) {
//			doh.assertEqual("cycle in plugins detected when trying to load cycle.b ,stack:cycle.a,cycle.b",e);
//			exceptionCatched=true;
//		}
//		doh.assertTrue(exceptionCatched);
	},
	tearDown : function() {
	}
},
]);