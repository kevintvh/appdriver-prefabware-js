//describes a point, where a Extension can hook in
//ExtensionPoint can use predefined properties or create there own
// className :  the name of a class. An instance of this class is created during startup
//
define('prefabware/plugin/ExtensionPoint', [
          'dojo',
          'dojo/_base/array',
          'dojo/promise/all',
          'prefabware/lang',
          'prefabware/commons/Cache',
          'prefabware/commons/match/MatchResultResolver',
          'dojo/Deferred',
          'dojo/_base/declare' ], function(
		dojo,array,all) {
	dojo.declare("prefabware.plugin.ExtensionPoint", null, {
		id :null,
		min:0,//minimum number of extensions to this point
		max:1,//maximum number of extentions to this point
		lazy:true,//true=load extensions on first request, false=load in _afterStartup
		__nextExtensionId:1,
		json:null,//an object with the declaration of this ExtensionPoint, as read from the plugin.json
		plugin :null,
		className :null,//the class of the extension for this point
		cache:false,//true=this extensionPoint will only resolve extensions once and than use its cache 
		defaultExtension:null,//the plugin will create a default extension using this class if none matches
		hasDefault:false,
		extensionDefinitions : null,//the definition of extensions for this extensionPoint. by their id
		matcherClass:null,// the class of default matcher to use, set in setup
		resolver:null,
		matchOptions:null,//a string with the name of the type, the matcher must accept
		__loader :{inject:'classLoader',type:'bean'},
		constructor : function(options) {
			this.json=options.json;
			this.id=this.json.id;
			this.plugin=options.plugin;
			if (options.min!=undefined) {
				this.min=options.min;
			}
			if (options.max!=undefined) {
				this.max=options.max;
			}
			if (options.json.lazy==false) {
				this.lazy=false;
			}
			if (options.json.cache==true) {
				this.cache=true;
			}
			if (options.json.matcher!=undefined) {
				this.matcherClass = options.json.matcher.className;
				this.matchOptions = options.json.matcher.options;
			}
			if (this.matchOptions == null) {
				this.matchOptions = {type:'string'};
			}
			if (this.matcherClass == null) {
				if (this.max > 1) {
					// max>1 means many extensions, but does not necessary mean
					// we need a matcher !
					prefabware.lang.warn('extension point ' + this.id + ' has max='
							+ this.max + ' but does not declare a matcher');
				}
				// provide a default matcher anyway
				this.matcherClass = 'prefabware.commons.match.AllMatcher';
			}
			
			if (options.json.defaultExtension!=undefined) {
				this.defaultExtension=options.json.defaultExtension;
				this.hasDefault=true;
			}else{
				this.hasDefault=false;
				this.defaultExtension={};
			}
			if (this.defaultExtension.matcher==null) {
				//if no matcher is declared for the default extension give it the matcher of the point 
				this.defaultExtension.matcher=options.json.matcher;
			}
			this.resolver=new prefabware.commons.match.MatchResultResolver();
			
			this.extensionDefinitions = new dojox.collections.Dictionary();//the extension by extensionId
			if (this.cache) {
				this.__bestCache=prefabware.commons.Cache(this.resolver,['bestMatch'],this.matchOptionsToString);
				this.__allCache=prefabware.commons.Cache(this.resolver,['allMatches'],this.matchOptionsToString);
			}
		},
		matchOptionsToString : function(options) {
			//subclasses may override this method
			//TODO have to use the matchOptionsToString of the matcher, but of which instance ?
			//TODO cannot create an instance of the matcher, it may require parameter
			return dojo.toJson(options);
		},		
		postCreate : function(instance) {
			//the ExtensionPoint can initialize the extension here
		},		
		__cache : function(cache,options,extension) {
			//to cache an extension that was not found by a matcher
			//adds the given options and extension to the cache
			if (this.cache){
			cache.put(cache.key(options),extension);
			};
		},		
		cacheBest : function(options,extension) {
			//to cache an extension as result of matchExtension, even if it was not found by a matcher
				this.__cache(this.__bestCache, options, extension);
		},		
		cacheAll : function(options,extensions) {
			//to cache an extension as result of matchExtensions, even if it was not found by a matcher
				this.__cache(this.__allCache, options, extensions);
		},		
		nextExtensionId : function() {
			//calculates a unique id for a extension of this point
			var extId= this.id + '_' +this.__nextExtensionId; 
			this.__nextExtensionId++;
			return extId;
		},
		registerExtensionDef : function(extensionDef) {
			//async
			//registers the given extension definition
			//returns nothing
			var that=this;
			if (this.extensionDefinitions.containsKey(extensionDef.id)) {
				var existing=this.extensionDefinitions.item(extensionDef.id);
				prefabware.lang.info('trying to register extension '+extensionDef.json+'with same id as allready registered extension :'+existing.json);
				if (extensionDef.override) {
					prefabware.lang.info('overriding existing extension, because the new extension has overide=true');
					//remove the allready registered matcher
					if (existing.matcher) {
						that.resolver.unregister(existing.matcher.id);
					}
				}else{throw "a extension with id "+extensionDef.id+' is allready registered';}
			}
			extensionDef.createMatcher().then(function(matcher){
				that.resolver.register(matcher);
				that.extensionDefinitions.add(extensionDef.id,extensionDef);
			});
			
		},
		fetchExtensions:function(){
			//async
			//returns an array with all extensions of this point
			var promises = array.map(this.extensionDefinitions.getValueList(), function(extdef) {				
				return extdef.createExtension();
			});
			return all(promises);
		},
		fetchExtension:function(){
			//async
			//returns exactly one extension if exactly one is registered
			//returns null if min=0 and max=1 and none registered
			//else throws an exception
			var defs=this.extensionDefinitions;
			if (defs.count>1) {
				throw 'tried to fetch the only exception but '+defs.count+' are found';
			}
			if (this.min==0 && defs.count==0) {
				//its optional and none registered, thats ok,do not throw an exception just return null
				return prefabware.lang.deferredValue(null);
			} else	if (defs.count!=1) {
				throw 'tried to fetch the only exception but '+defs.count+' are registered';
			}
			//there is only one, return it
			var only=defs.getValueList()[0];
			return only.createExtension();			
		},
		matchExtensions:function(options){
			//async
			//returns allways a promise 
			//- with all matching extensions for the given options
			//- or an empty array
			var result=this.resolver.allMatches(options);
			if (result.length>0) {
			var exts=array.map(result,function(match){
				var def=match.matcher.owner;
				return def.createExtension();
			});
			return all(exts);
			}else{
				return prefabware.lang.deferredValue([]);
			}
		},
		matchExtension:function(options){
			//async
			//returns allways a promise 
			//- with the best matching extension for the given options
			//- or null
			if (this.extensionDefinitions.count==0) {
				//no extension registered, matching is useless
				//but it must not be an error
				prefabware.lang.log( "tried to matchExtension, but no extension registered for point "+this.id);
				return prefabware.lang.deferredValue(null);
			} 
			var result=this.resolver.bestMatch(options);
			if (result!=null) {
				var def=result.matcher.owner;
				return def.createExtension();
			}else{
				return prefabware.lang.deferredValue(null);
			}
		},
		registerExtension : function(extension){
			//registers the given extension
			if (this.extensions.containsKey(extension.id)) {
				prefabware.lang.throwError( "a extension with id "+extenson.id+' is allready registered');
			}
			this.extensions.add(extension.id,extension);
		},
		startup:function(){
			//subclasses may override this to inizialize the extension
		},		
		__startup : function() {
			//internal method ,subclasses MUST NOT override this
			var that=this;
			var point = this;
			var defs=this.extensionDefinitions;
			//check if the min/max constraints of this point are fulfilled
				if (defs.count<point.min) {
					prefabware.lang.throwError( 
							'not enough extensions for extension point '
							+point.id+' required minimum '+point.min+' found '+defs.count+' ids:'+defs.getKeyList().toString());
				}
				if (defs.count>point.max) {
					prefabware.lang.throwError(
							'too much extensions for extension point '
							+point.id+' allowed maximum '+point.max+' found '+defs.count+' ids:'+defs.getKeyList().toString());
				}
			this.resolver.startup();
			this.startup();
			return;				
		},
		afterStartup:function(){
			//subclasses may override this
		},		
		__afterStartup : function() {
			var that=this;
			//internal method to inizialize the extension,subclasses MUST NOT override this 
			//load all extensions now if not lazy
			if (this.lazy==false) {
				return this.fetchExtensions().then(function(){
					return that.__promiseOrNullPromise(that.afterStartup());
				});
			}else{
				return that.__promiseOrNullPromise(that.afterStartup());
			};
		},
		__promiseOrNullPromise : function(p) {
			//if p is a promise it is returned
			//if p is undefined a promise resolving to null is returned
			if (p!=undefined) {
				return p;
			}else{
				return prefabware.lang.deferredValue(null);
			}
		},
		
	});
});
