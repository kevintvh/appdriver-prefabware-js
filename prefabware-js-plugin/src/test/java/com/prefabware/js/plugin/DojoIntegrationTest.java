 package com.prefabware.js.plugin;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.test.DohTestUrlBuilder;
import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestApplication.class)
// port:0 = use a random, free port
@WebIntegrationTest("server.port:0")
public class DojoIntegrationTest {
	private DojoTestSupport testSupport;

	@Value("${local.server.port}") int port;

	@Autowired @Qualifier("jsCommonsWebJar") WebJar cwj;
	@Autowired @Qualifier("thirdpartyDojoWebJar") WebJar dwj;
	@Autowired @Qualifier("rmWebJarDojoUtil") ResourceMapping rmWebJarDojoUtil;
	@Autowired @Qualifier("rmWebJarPrefabware") ResourceMapping rmWebJarPrefabware;

	@Before
	public void setUp() throws Exception {
		String baseUrl = "http://localhost:" + Integer.toString(port);
		testSupport = new DojoTestSupport(baseUrl);

		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b
				.withDohUtilBaseUrl(rmWebJarDojoUtil.urlPath())
				.withModule("prefabware", rmWebJarPrefabware.urlPath());

		testSupport.add(b.withTestClass("prefabware.plugin.tests.extensionInject.ExtensionInjectTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.cache.CacheTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.cycleRepeated.CycleRepeatedTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.cycle.CycleTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.defaultExtension.DefaultExtensionTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.optional.OptionalTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.override.OverrideTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.attributes.AttributesTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.nonlazy.NonLazyExtensionTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.extension.FindExtensionTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.extension.ExtensionByIdTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.extension.ExtensionTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.definition.PluginDefinitionTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.dependency.DependencyTest").build());
    	testSupport.add(b.withTestClass("prefabware.plugin.tests.registry.PluginRegistryTest").build());
	}

	@Test
	public void testAll() throws Exception {
		testSupport.testAll();
		return;
	}
}