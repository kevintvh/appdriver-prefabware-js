//definition of an EntityType
define('prefabware/model/EntityType', [ 'dojo', 
		'prefabware/model/CompositeType',
		'prefabware/model/Entity', 'prefabware/model/ReferenceAttribute',
		'dojo/_base/declare' ], function(dojo, CompositeType,
		Entity) {
	dojo.declare("prefabware.model.EntityType",
			[ prefabware.model.CompositeType ], {
			isEntityType:true,
				newId:{next:-1},//next id for new instances, allways <0
				//minimal id for entities persisted localy in a memory store, its a constant !
				//ids higher than that are considered temporary, saved to a memory store TODO refactor
				__MIN_PERSISTED_LOCAL_ID:9000000000000000,
				__MIN_PERSISTED_REMOTE_ID:1,
				nextId:{next:9007199254740992},//next id for saved objects, allways>0,Number.MAX_VALUE is to big for that !!
				//the attribute, that is used as a human readable key for instances of this type
				//by default this is the attribute used to search for
				keyAttribute:null,
				constructor : function(name, labels) {
					//if no special class is provided, use Entity
					this.instanceClass=prefabware.model.Entity;
				},
				startup : function() {					
					this.inherited(arguments);
					//still no key found
					if (this.keyAttribute==null) {
						//that works only for entities
						this.keyAttribute=this.getAttribute('pfw_id');
					}
					if (!this.isAbstract) {
						//no error for abstract classes
						//still no key found
						if (this.keyAttribute==null) {
							throw 'cannot determine keyAttribute for type '+this.name;
						}
					}
					
					return;
				},
				addAttribute : function(options) {
					var attribute = this.inherited(arguments);	
					if (attribute.isKey) {
						this.keyAttribute=attribute;
					}
					return attribute;
				},
				create : function(data) {
					if (data != undefined && data != null) {
						return new this.instanceClass(this, data.pfw_id,
								data.version);
					} else {
						var entity=new this.instanceClass(this);
						entity.pfw_id = this.__newId();
						return entity;
					}
				},
				__newId : function() {
					// returns the next id  for a new instance of this type
					var id = this.newId.next;
					this.newId.next--;
					return id;
				},
				__nextId : function() {
					// returns the next id  for an instance of this type
					// local stores use this when saving entities the first time
						var id = this.nextId.next;
						this.nextId.next--;
						return id;
				},
			});
});
