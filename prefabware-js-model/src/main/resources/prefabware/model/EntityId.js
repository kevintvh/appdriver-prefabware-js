//definition of an action
define('prefabware/model/EntityId', [ 'dojo',  'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.model.EntityId", null, {
		type:null,
		id:null,
		constructor : function(type,id) {
			this.type=type;
			this.id=id;
		},
		name:function(){
			return this.type.name + '.' + this.id;
		},		
		sysName : function() {
			// dots '.' in the name are not allowed e.g. when used as a property
			// so replace all . by _
			return this.type.sysName()+'_'+this.id;
		}		
	});
});

