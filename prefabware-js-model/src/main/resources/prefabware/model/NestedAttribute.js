//a nested attribute, e.g. user.role.name
//it is composed of the name and the last attribute in the chain
//name= user.role.name
//attribute=the attribute 'name' of role
define('prefabware/model/NestedAttribute', [ 'dojo', 
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.model.NestedAttribute", null, {
		attribute : null,
		name : null,
		// TODO may be 'containment' has to be determined differently
		constructor : function(name, attribute) {
			// the
			this.attribute = attribute;
			this.name = name;
			this.names=name.split('.');
			this.rootName=this.names[0];
			// all other properties delegate to attribute
			for ( var prop in attribute) {
				if (prop != 'name'	&& attribute.hasOwnProperty(prop)) {
					this.__createPropertyAccessors(prop, attribute);
				}
			}
		},
		stringOf : function(value) {
			return this.attribute.stringOf(value);
		},
		__get : function (propertyName){
    		return this.attribute[propertyName];
    	},
    	__set : function (propertyName,value){
    		return this.attribute[propertyName]=value;
    	},
		__createPropertyAccessors : function(propertyName) {
			// adds a property that will delegate to the get accessor method
			var getter=dojo.partial( this.__get, propertyName);
			var setter=dojo.partial( this.__set, propertyName);

			Object.defineProperty(this, propertyName, {
				get : getter,
				set : setter,
				enumerable : true,
				configurable : true
			});
		}
	});
});