//the root all types
define('prefabware/model/Type', 
		[ 'dojo', 
		  'dojo/_base/lang',
		  'dojo/_base/array',
		  'dojox/collections/Dictionary',
		  'dojo/Evented',
		  'dojox/collections/ArrayList',
		  'prefabware/model/NestedAttribute',		  
		  'prefabware/model/Attribute',
		  'prefabware/model/QualifiedAttribute',
		  'dojo/_base/declare' ], function(dojo,lang,array,dictionary,Evented) {
	dojo.declare("prefabware.model.Type", [Evented], {
		//simple types have no attributes, only composite types have.
		//Attributes are declared here, to have a consistent API for all types
		attributes : null,
		attributesList:null,
		name : null,
		label : null,
		isStarted : false,//was startup() called ?
		typeRegistry:null,//
		superType:null,//the superclass or null
		isAbstract:false,//is this a abstract type
		constructor : function(options) {
			if (dojo.isString(options)) {
				this.name = options;
			}else{
				this.__mixinArgs(options);
			}
			this.attributes=new dojox.collections.Dictionary();
		},
		__mixinArgs : function(args) {
			//mixes the args into the according fields of this 
			//in contrast to dojo.lang.mixin only fields are mixed in, that are declared in this class
			for ( var arg in args) {
				if (this[arg]===undefined) {
					//cannot use this[arg]!=undefined cause thats true if this[arg]===null
				}else{
					this[arg]=args[arg];
				}
			}
		},
//		mixin : function(instance) {
//			//mixes the methods defined in _createMixin into the instance of this type
//			if (this.__createMixin) {
//				lang.mixin(instance,this.__createMixin);
//			}
//		},
		create : function(args) {
			//creates an instance of this type without defaults
			throw 'method create must be implemend by subclass';
		},
		createDefault : function() {
			var instance=this.create();
			this.applyDefaults(instance);
			return instance;
		},		
		applyChanges : function(instance) {
			//is called after changes have been applied to the instance
			//just calls lifecycle listeners
			this.emit("changesApplied", {sender:this,type:this,instance:instance});
			return instance;
		},
		applyDefaults : function(instance) {
			//applies the defaults to the given instance
			array.forEach(this.getAttributes(),function(attribute){
				var value=attribute.createDefault(instance);
				if (instance[attribute.name]==null&&value!==undefined) {
					//doe not override allready set values
					//like PfwType
					instance[attribute.name]=value;
				}
			});
			//call lifecycle listeners
			this.emit("defaultsApplied", {sender:this,type:this,instance:instance});
			return instance;
		},
		ondefaultsApplied : function(event) {},
		matches : function(name) {
			// returns true, if the name of this type matches the given name,
			// also generic '*'
			if (this.name==name) {
				return true;
			}else if (name.indexOf('*') == name.length - 1) {
				// ends with *
				if (type.name.indexOf(candidate.substring(0,
						candidate.length - 1)) == 0) {
					// starts with the given generic name without *
					return true;
				}
			}else{return false;}
		},
		addAttribute : function(options) {
			var attribute = this.__createAttribute(options);
			this.attributes.add(attribute.name,attribute);
			var prop=attribute.name.toUpperCase();
			if (this[prop]==undefined) {
				//declare a property with the name of the attribute for easy access
				this[prop]=attribute;
			}
			return attribute;
		},
		__createAttribute : function(options) {
			//subclasses can override this function to create subtypes of attribute
			var attribute = new prefabware.model.Attribute(options);			
			return attribute;
		},
		convertFrom : function(rawValue) {
			//TODO this method should be named fromString 
			//converts the rawValue from the internal not localized string representation 
			//into a value of this type and returns it
			//subclasses may override this to implement their own conversion
			return rawValue;
		},
		labelOf : function(instance) {
			//returns the label of the given instance of this type
			//may be overridden
			return this.stringOf(instance);
		},
		toString : function() {
			//is absolutely necessary for toJson !!
			return this.name;
		},
		__json__ : function() {
			//to allow dojo.toJson()
			return this.name;
		},
		stringOf : function(value) {
			if (value==undefined||value==null) {
				return '';
			}			
			//returns a string of the given value
			//the value must be of this type
			//the returned string is the internal string representation of value
			//and may be different from the localized value
			this.assertIsValue(value);
			return value.toString();
		},
		serializableOf : function(value) {
			if (value!=null&&typeof value.__serializable=='function') {
				return value.__serializable();
			}
			//may be overridden
			return value;
		},		
		__getAttribute : function(name) {
			//returns the attribute of this type with the given name.
			//name must NOT be a nested one like role.name
			var attribute=this.attributes.item(name);
			//Dictionary returns a value for name==='constructor'
			//because it simply uses name in this
			//that would be its own constructor
			//in those cases theres no attribute
			if (lang.isFunction(this.attributes.item(name))) {
				return undefined;
			}
			return attribute;
		},
		getQualifiedAttribute : function(name) {
			//returns an attribute with a reference to this type
			//TODO cache
			var attribute=this.getAttribute(name);
			return new prefabware.model.QualifiedAttribute({attribute:attribute,type:this});
		},
		getAttribute : function(property) {
			// gets the attribute of this type
			// the property can also be a nested property like user.role.name
			// resolve all entites found on its way, if necessary
			var nested = property.split(".");
			var currentType = this;
			var attribute;
			for ( var i = 0; i < nested.length; i++) {
				var prop = nested[i];
				attribute = currentType.__getAttribute(prop);
				if (attribute == undefined || attribute == null) {
					break;
				}
				currentType = attribute.type;
			}
			if (nested.length > 1) {
				//TODO cache this !
				return new prefabware.model.NestedAttribute(property,attribute);
			} else {
				return attribute;
			}
		},
		simpleName : function() {
			var elements=this.name.split(".");
			return elements[elements.length-1];
		},
		simpleNameFirstLetterLowerCase : function() {
			// returns the simpleName with the first letter lowercase
			// returns 'user' for 'prefabware.business.commons.User'
			var name=this.simpleName();
			return name.charAt(0).toLowerCase() + name.substring(1, name.length);
		},
		namespace : function() {
			return this.name.substring(0,this.name.length-this.simpleName().length-1);
		},
		sysName : function() {
			// dots '.' in the name are not allowed e.g. when used as a property
			// so replace all . by _
			return this.name.replace(/\./g, '_');
		},
		startup : function() {
			//keep the here defined attributes as own attributes
			this.__ownAttributes=this.attributes;
			var list=new Array();
			if (this.superType!=null) {
				var that=this;
				var attributes=new dojox.collections.Dictionary();
				//add own attributes  
				array.forEach(this.__ownAttributes.getValueList(),function(att){
					list.push(att);
					attributes.add(att.name,att);
				});
				//add attributes of all supertypes, not overriding own ones
				array.forEach(this.superType.getAttributes(),function(att){
					if (!that.__ownAttributes.containsKey(att.name)) {
						//do not override own attributes with those from superclasses
						list.push(att);
						attributes.add(att.name,att);
					}
				});
				this.attributes=attributes;
			};
			//sort the attributes
			list.sort(function(att1,att2){
				return att1.sort-att2.sort;
			});
			this.attributesList=list;
			//remove this method
			this.startup=undefined;
			this.isStarted=true;
			var that=this;
			this.addAttribute=function(){throw 'type '+that.name+' is allready initialized, it cannot be changed now';};
		},
		getQualifiedAttributes : function() {
			var that=this;
			return array.map(this.getAttributes(),function(att){
				return that.getQualifiedAttribute(att.name);
			});
		},
		getAttributes : function(options) {
			//options are used to filter			
			//{isLabel:true} will return only attributes, that have isLabel==true
			if (this.startup) {
				this.startup();				
			}
			if (options==null) {
				return this.attributes.getValueList();
			}
			return array.filter(this.attributes.getValueList(),function(attrs){
				for(var prop in options){
				    if (attrs[prop]!=options[prop]) {
				    	//the attributes property is different from the requested one
				    	//exclude the attribute
						return false;
					}
				}
				return true;//all properties are the same, select the attribute
			},this);
		},
		isSuperTypeOf : function(type) {
			//return true, if this type can be assigned from the given type,
			//meaning that this type is a super of the given type or the same as type
			return  this.superTypeDistance(type)>=0;
			
		},
		assertIsValue : function(value) {
			if (!this.isValue(value)) {
				throw 'value '+value+'is not a valid '+this.name;
			}
		},
		isValue : function(value) {
			if (value==null) {
				return true;
			}
			//return true, if the value is a legal value for this type
			//every value is an instance of Type
			//other types may implement their own logic
			return true;
			
		},
		toString : function() {return this.declaredClass + ' '+this.name;
		},
		superTypeDistance : function(type) {
			//returns the number of inheritance steps between this type and the given type
			//returns <0 if this is no superType of type
			//returns 0 if this==type
			//returns 1 if this is the superType of type
			//returns 2 if this is the superType of type.superType
			//and so on
			if (this==type) {
				//its the same type
				return 0;
			}else if (type.superType) {
				//this type may be one of the super types of the given type
				//so add 1 to the distance
				return 1 + this.superTypeDistance(type.superType);
			}else{
				//even if we iterated through many supertypes, if we end here the result
				//will be lower than 0
				return -99999;
			}
		}
		
	});
});
