//definition of an Entity
define('prefabware/model/Composite', [ 'dojo', 
		'prefabware/model/Type','dojo/_base/lang',
		'dojo/_base/array',
		'dojo/promise/all',
		 'dojo/when',
		'prefabware/commons/ClassLoader',
		'prefabware/model/ReferenceAttribute',
		'dojo/_base/declare' ], function(dojo, Type,lang,array,all,when) {
	dojo.declare("prefabware.model.Composite", null, {
		__static:{newCounter : 0,cloneId : 0},
		__classLoader:new prefabware.commons.ClassLoader(),
		__prefabware:null,//move all internal methods here
		pfw_type:null,
		__type:null,
		__isComposite:true,
		__isDirty:false,//is the composite changed after read
		__isEnumeration:false,
		constructor : function(type) {
			this.__prefabware={};
			this.__type=type;
			this.__isEnumeration=type.isEnumeration;
			this.pfw_type=type.name;
			//extend this composite by mixin the methods from the custom object
			//type.mixin(this);
		},
		__mixInto : function(data) {
		//some attributes may have the wrong type when coming from the server
		//dates my be of type String
		//convert them to the right type
		//do not touch proxied properies here to avoid resolving the proxy !
		//we have to modify data, not this !1
		for ( var prop in data) {
			var attribute=this.__type.getAttribute(prop);
			if (attribute!=undefined) {
				data[prop]=attribute.convertFrom(data[prop]);
			}
		}
		lang.mixin(data, this);
		return data;
		},
		
		__label : function() {
			var label="";
			array.forEach(this.__type.labelAttributes,function(attr){
				label=label+" "+this.__labelOf(attr.name);
			},this);
			return label;
		},
		__description : function() {
			var description="";
			array.forEach(this.__type.descriptionAttributes,function(attr){
				description=description+" "+this.__labelOf(attr.name);
			},this);
			return description;
		},
		__fullLabel : function() {
			//a string consisting of the types simpleName and the entites label
			//like Customer Schmidt			
			return this.__type.simpleName()+' '+this.__label();
		},
		__setDefaults : function() {
			if (this.__type.setDefaults) {
				//set defaults in this entity
				this.__type.setDefaults(this);
			}
		},
		__setValueOf : function(attributeName,newValue) {
			// sets the property to the given value 
			// the property can also be a nested property like user.role.name
			// lang.getObject does not resolve proxies, so we cannot use it !!!!			
			// resolve all entites found on its way, if necessary
			// Order.customer.owner.name
			var nested = attributeName.split(".");
			var currEnt = this;
			//for simple, not nested attributes the loop is never executed
			var value   = this;
			// loop over customer.owner
			for ( var i = 0; i < nested.length-1; i++) {
				var prop = nested[i];
				value = currEnt[prop];
				if (value==undefined || value == null) {
					var	propType=currEnt.__type.getAttribute(prop).type;
					var value=propType.createDefault();
					currEnt[prop]=value;
				}
				//the value is the next entity to use
				currEnt = value;
			}
			//value is the value of the last but one property
			//example : Order.customer.owner
			//the property to set is the last element in the array of properties
			//set its value to the given newValue
			value[nested[nested.length-1]]=newValue;
			return;
		},
		__equals : function(other) {
			//returns true, if this composite equals the other composite
			if (other==null||other==undefined) {
				return false;
			};
			if (this.__type!=other.__type) {
				return false;
			}
			//they are equal, if the value of the key attribute is equal
			return this.__valueOf(this.__type.keyAttribute.name)==other.__valueOf(other.__type.keyAttribute.name);
		},
		__valueOf : function(attributeName) {
			// gets the value of the property with the given name
			// the property can also be a nested property like user.role.name
			// lang.getObject does not resolve proxies, so we cannot use it !!!!			
			// resolve all entites found on its way, if necessary
			var nested = attributeName.split(".");
			var currEnt = this;
			var value;
			for ( var i = 0; i < nested.length; i++) {
				var prop = nested[i];
				//currEnt = this.resolve(currEnt);
				value = currEnt[prop];
				if (!value || value == null) {
					break;
				}
				currEnt = value;
			}
			return value;
		},
		
		__labelOf : function(attributeName) {
			var attr=this.__type.getAttribute(attributeName);
			if (attr==undefined) {
				throw 'cannot find attribute '+attributeName+' for type '+this.__type.name;
			}
			var value=this.__valueOf(attributeName);
			return attr.stringOf(value);
		},		
		__addProperty : function(propertyName){
			if (this[propertyName]!=undefined) {
				//throw 'property '+property+' allready exists in'+this;
			}
    		Object.defineProperty(this, propertyName, {
    			get:this.__getId,
                set:this.__setId,
                enumerable : true,
                configurable : true});
    	},
    	__toJson : function() {
			var clone=this.__clone();
			var serializable=clone.__serializable();
			var json = dojo.toJson(serializable);
			return json;
		},
		__serializable : function() {
			return this.__type.serializableOf(this);
		},
		__clone : function() {
			return this.__clone2(this);
		},
		__clone2 : function(entity) {
			if (entity==undefined||entity==null) {
				return entity;
			}
			if (entity.__isEnumeration) {
				//do not clone enumerations, they are singletons
				return entity;
			}
			if (entity.__isProxy) {
				//TODO 
				return entity.__clone();
			}
			
			// clones the given entity.
			// entity can also be an array of entities
			// returns a DEEP copy
			// __id,__parent etc. from the store will not be copied
			// - its not necessary
			// - causes problems beacuse --parent contains the entity and so its
			// a circular structure
			// that e.g. can not be converted to json
			var clone;
			if (dojo.isArray(entity)) {
				clone = new Array();
				// clone an array by cloning its elements
				for ( var index = 0; index < entity.length; index++) {
					var element = entity[index];
					clone[index] = this.__clone2(element);
				}
				return clone;
			} else if(entity.__isComposite){
				//clone entity
				clone=entity.__type.create(entity.__type);
				this.__static.cloneId++;
				clone.__cloneId=this.__static.cloneId;
				var attributes=entity.__type.getAttributes();
				for ( var int2 = 0; int2 < attributes.length; int2++) {
					var attr = attributes[int2];
					var attrName=attr.name;
					//value can be itself arry,entity or simple value
					clone[attrName]=this.__clone2(entity[attrName]);
				}
				//clone system fields
				clone.__type=entity.__type;		
				clone.__prefabware=entity.__prefabware;				
			} 
			else{
				//everything else clone by reference
				clone=entity;
			}
			return clone;
		},
		__resolveProxies : function(attributeNames) {
			//async
			//resolves the proxies of the attributes with the given names
			//names can also be names of nested properties like ["customer.type.name"]
			//if no names are given, all attributes will be resolved
			//resolving attributes may be necessary, even if this is not a proxy !
			//invoiceLine.price is not a proxy, because its a value object
			//but invoiceLine.price.currency IS a a proxy
			var that=this;
			if (attributeNames==null) {
				attributeNames=array.map(this.__type.getAttributes(),function(attr){
					return attr.name;
				});
			}
			
			var f=function(object,prop){
				var value=object.__valueOf(prop);
				if (value == null ) {					
					return prefabware.lang.deferredValue(value);
				}
					var p;
					if (value.__resolveThisProxy!=null) {
						//prefer this method, its a member of composite and can be overwritten in
						//call __resolveThisProxy even if value is not a proxy itself
						//may be he needs to resolve eager attributes
						p=value.__resolveThisProxy();
					}else if (value.__isProxy&&!value.__isResolved){
						p=value.resolve();
					}else{
						//its not a proxy nor has a __resolveThisProxy
						//TODO reorganize
						return prefabware.lang.deferredValue(value);
					}
					return  p.then(function(resolved){
						if (value.__isResolvedToNull) {
							//if the proxy points nowhere, remove the according property	
							//from the composite;
							delete that[prop];
						}
						return resolved;
					});
			};
			
			//1. start with resolving this !!			
		return that.__resolveThisProxy().then(function(){
			//2. resolve the proxies of the attributes
				var pa=array.map(attributeNames,function(attrName){
					return prefabware.lang.getObjectAsync(that,attrName,f);
					});
				return all(pa);
			});
		},
		__resolveThisProxy : function() {
			var that=this;
			//async
			//resolves this, if its a proxy
			if (this.__isProxy&&!this.__isResolved) {
				//if this is a proxy, it has a method resolve..., TODO avoid name collision
				//TODO the return value of resolve() is useless
				return this.resolve().then(function(){
					return prefabware.lang.deferredValue(that);
				});
			}else{
				return prefabware.lang.deferredValue(this);
			}
		},
	});
});
