//to match qualified attribute names like "com.prefabware.business.commons.Amount.value"
//is configured with a qualified attribute name
//matches this against a attribute name
//uses regex to match
//generic matches must use '.*' instead of '*'
//com.prefabware.commons.Customer.address matches     com.prefabware.commons.Customer.address
//com.prefabware.commons.Customer.a.*     matches     com.prefabware.commons.Customer.address 
//com.prefabware.commons.Customer.a.*     matches NOT com.prefabware.commons.Customer.zipCode 
//com.prefabware.commons..*.address       matches     com.prefabware.commons.Customer.address
//can also match to a type, meaning that it applies to all attributes of the given type
define('prefabware/model/match/AttributeNameMatcher', [ 'dojo',
		'prefabware/commons/match/Matcher',
		'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.model.match.AttributeNameMatcher",
			[ prefabware.commons.match.Matcher ], {
		pattern:null,//the pattern,its a string !	
		constructor : function(options) {
					prefabware.lang.assert(options!=null,'options must be passed');
					prefabware.lang.assert(options.pattern!=null,'options.pattern must be set');
					prefabware.lang.assert(typeof options.pattern=="string",'options.pattern must be a string with a pattern for a name of a qualified attribute');
					this.pattern=options.pattern;//it must be a string pattern!
					this.degree = this.GENERIC_MATCH.degree;// generic match
					this.regEx=new RegExp(this.pattern);
				},
				matches : function(options) {
					//this.attribute is a string, may be a pattern
					//options.attribute must be a attribute
					if (options.qattributeName==null) {
						throw 'options.qattributeName must be set';
					}
					if (! typeof options.qattributeName=='string') {
						throw 'options.qattributeName must be a string';
					}
					var oqattributeName=options.qattributeName;
					var result=null;
					if (this.pattern===oqattributeName) {
						var that=this;
						result=	{
								matcher:that,
								degree: this.EXACT_MATCH.degree
								};
						prefabware.lang.log('matcher with this.pattern='+this.pattern+' matches options.qattributeName='+oqattributeName+' result.degree='+result.degree);
						return result;
					}
					else if ( this.regEx.test(oqattributeName)) {
							var that=this;
							result=	{
									matcher:that,
									degree:that.degree
							};
							prefabware.lang.log('with this.pattern='+this.pattern+' matches options.qattributeName='+oqattributeName+' result.degree='+result.degree);
							return result;
					} else{
						prefabware.lang.log('with this.pattern='+this.attribute+' matches options.qattributeName='+oqattributeName+' result: NO_MATCH');
						return this.NO_MATCH;
					}
				},
			});
});
