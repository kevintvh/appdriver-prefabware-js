//type of pfw_type. this is the afftribute containing the name of the server (java) class of an entity
define('prefabware/model/PfwType', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",'prefabware/model/StringType','prefabware/model/Attribute',
		  'dojo/_base/declare' ], function(dojo,lang,locale,Type) {
	dojo.declare('prefabware.model.PfwType', [prefabware.model.StringType], {
		constructor : function(name) {
		},
		
	labelOf : function(value) {
		if (value==undefined||value==null) {
			return "";
		}
		var elements=value.split(".");
		//return the last element of the qualified name
		return elements[elements.length-1];
	},	
	__serializableOfSingle : function(value) {
		return value;
	},
	});
});
