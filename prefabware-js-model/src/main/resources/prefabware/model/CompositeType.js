//definition of an CompositeType
define('prefabware/model/CompositeType', [ 'dojo',  
		'prefabware/model/Type',
		'prefabware/model/Composite',
		'dojo/_base/array',
		'prefabware/model/ReferenceAttribute',
		'dojo/_base/declare' ], function(dojo, Type,Composite,array) {
	dojo.declare("prefabware.model.CompositeType", [ prefabware.model.Type ], {
		instanceClass : null,//the javascript class to use as instance of this type
		labelAttributes:null,//the attributes used for a label
		eagerAttributes:null,//attributes that must be loaded eager
		descriptionAttributes:null,//the attributes used for a description
		
		constructor : function(options) {
			//if no special class is provided, use Composite
			this.instanceClass=prefabware.model.Composite;
			this.eagerAttributes=[];
		},
		startup : function() {
			this.addAttribute({name:'self',type:this,min:1,max:1,isSystem:true});
			this.inherited(arguments);	
			//TODO do that in addAttribute
			this.labelAttributes=this.__findLabelAttributes();
			this.descriptionAttributes=this.__findDescriptionAttributes();
			return;
		},
		addAttribute : function(options) {
			var attr=this.inherited(arguments);
			if (attr.isEager) {
				this.eagerAttributes.push(attr);
			}
			return attr;
		},
		__createAttribute : function(options) {
			var attribute;
			if (options.type instanceof prefabware.model.CompositeType) {
				attribute = new prefabware.model.ReferenceAttribute(options);
			} else {
				attribute = this.inherited(arguments);
			}
			return attribute;
		},
		__findLabelAttributes : function() {
			var labels=[];
			array.forEach(this.getAttributes(),function(attr){
				if (attr.isLabel) {
					labels.push(attr);
				}
			});
			labels.sort(function(a,b){return a.labelOrder-b.labelOrder;});
			return labels;
		},		
		__findDescriptionAttributes : function() {
			var descriptions=[];
			array.forEach(this.getAttributes(),function(attr){
				if (attr.isDescription) {
					descriptions.push(attr);
				}
			});
			descriptions.sort(function(a,b){return a.descriptionOrder-b.descriptionOrder;});
			return descriptions;
		},		
		create : function(args) {
			//return new prefabware.model.Composite(this);
			var instance= new this.instanceClass(this,args);
			instance.__type=this;
			return instance;
		},
		serializableOf : function(composite) {
			if (composite==null) {
				return null;
			}
			if (composite.__isProxy) {
				//may be its a proxy
				return composite.__serializable();
			}
			//a composite is serialized by serializing its attribute values
			var serializable=null;
			serializable=new Object();
			array.forEach(composite.__type.getAttributes(),function(attribute){
				var attrName=attribute.name;
				var value=composite[attrName];
				var attSerializable=attribute.serializableOf(value);
				serializable[attrName]=attSerializable;
			});
			return serializable;
		},
		isValue : function(value) {	
			if (value==null) {
				//generally null is a vaqlid value for a composite
				return true;
			}
			//return true, if the value is an instance of this composite
			//the value must have a __type
			return value.__type!=undefined && this.isSuperTypeOf(value.__type);
		},	
	});
});
