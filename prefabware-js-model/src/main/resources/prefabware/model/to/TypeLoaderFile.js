//to load types from jsonTypes. the loader will only read the file and return a json object containing the type definition

//add type jsonTypes using .add
//than use it to create the TypeRegistry
//new prefabware.model.TypeRegistry({loader:yourLoader});
//the TypeRegistry will just call load to load a type
//load types using .load(typeName);
//to register jsonTypes, that on request of the typeRegistry will be imported into the typeregistry
define('prefabware/model/to/TypeLoaderFile', [
        'dojo',
        'dojo/cache',
        'dojox/collections/Dictionary',
        'dojo/_base/array',
        'prefabware/lang',
        'dojo/_base/declare' ], function(dojo,cache,dictionary,array) {
	dojo.declare("prefabware.model.to.TypeLoaderFile", null, {
		jsonTypes:null,//the jsonTypes registered here
		constructor : function() {			
			this.jsonTypes=new dojox.collections.Dictionary();
		},
		addJsonResource : function(jsonResource) {
			var json=jsonResource.getContentAsJson();
			//json can be a single type
			//or an array of types
			if (!dojo.isArrayLike(json)) {
				this.addJsonType(json,jsonResource);
			}else{
				array.forEach(json,function(jsonType){
					this.addJsonType(jsonType,jsonResource);
				},this);
			}
		},
		addJsonType : function(jsonType,source) {
			this.jsonTypes.add(jsonType.name,{jsonType:jsonType,source:source});
		},
		add : function(typeName,namespace,file) {
			//deprecated
			var string=dojo.cache(namespace, file);
			var jsonType=	 dojo.fromJson(string);
			this.addJsonType(jsonType,namespace+file);
		},
		load:function(typeName){
			//loads the type with the given name and returns its json transport object
			prefabware.lang.info("trying to load "+typeName);
			var item=this.jsonTypes.item(typeName);
			if (item!=null) {
				return item.jsonType;
			}else{
				return null;};
			
		}		
	});
});

