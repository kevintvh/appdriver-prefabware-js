// to test the typeimporter and type customization
dojo.provide("prefabware.model.to.tests.typeLoaderList.TypeLoaderListTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.to.TypeImporter");
dojo.require("prefabware.model.to.TypeLoaderFile");
dojo.require("prefabware.model.to.TypeLoaderList");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.util.ResourceJsonFile");
dojo.require("dojo._base.lang");
dojo.require("dojo.cache");
dojo.require("dojo.aspect");
dojo.require("prefabware.model.to.tests.Currency");
doh.register("prefabware.model.to.tests.typeLoaderList.TypeLoaderListTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	registry:null,
	loader:null,
	importer:null,
	setUp : function() {
		this.loader1 = new prefabware.model.to.TypeLoaderFile();
		this.loader2 = new prefabware.model.to.TypeLoaderFile();
		this.loader = new prefabware.model.to.TypeLoaderList();
		this.loader.register(this.loader1);
		this.loader.register(this.loader2);
		this.registry = new prefabware.model.TypeRegistry({loader:this.loader});
		
		this.importer = this.registry.importer;
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
		this.BOOLEAN = this.registry.BOOLEAN;
		this.TYPE = this.registry.TYPE;
		this.COMPOSITE = this.registry.COMPOSITE;
		this.ENTITY = this.registry.ENTITY;
	},
	runTest : function() {
		var jr1 = new prefabware.util.ResourceJsonFile({qualifiedName:"prefabware.model.to.tests.typeLoaderList.types1.json"});
		this.loader1.addJsonResource(jr1);
		var jr2 = new prefabware.util.ResourceJsonFile({qualifiedName:"prefabware.model.to.tests.typeLoaderList.types2.json"});
		this.loader2.addJsonResource(jr2);
		
		var a1=this.registry.findType("test.A1");
		doh.assertTrue(a1!=null);		
		var b1=this.registry.findType("test.B1");
		doh.assertTrue(b1!=null);		
	
		var a2=this.registry.findType("test.A2");
		doh.assertTrue(a2!=null);		
		var b2=this.registry.findType("test.B2");
		doh.assertTrue(b2!=null);		
		
		return;
	},
	tearDown : function() {
	}
} ]);