// to test the typeimporter and type customization
dojo.provide("prefabware.model.to.tests.LoadTypeFromFileTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.to.TypeImporter");
dojo.require("prefabware.model.to.TypeLoaderFile");
dojo.require("prefabware.model.EntityType");
dojo.require("dojo._base.lang");
dojo.require("dojo.cache");
dojo.require("dojo.aspect");
dojo.require("prefabware.model.to.tests.Currency");
doh.register("prefabware.model.to.tests.LoadTypeFromFileTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	registry:null,
	loader:null,
	importer:null,
	setUp : function() {
		this.loader = new prefabware.model.to.TypeLoaderFile();
		this.registry = new prefabware.model.TypeRegistry({loader:this.loader});
		
		this.importer = this.registry.importer;
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
		this.BOOLEAN = this.registry.BOOLEAN;
		this.TYPE = this.registry.TYPE;
		this.COMPOSITE = this.registry.COMPOSITE;
		this.ENTITY = this.registry.ENTITY;
	},
	runTest : function() {
		this.loader.add("com.prefabware.business.commons.QuantityUnit","prefabware.model.to.tests", "quantityUnit-type.json");
		this.loader.add("com.prefabware.business.commons.BaseQuantitiyUnit","prefabware.model.to.tests", "baseunit-type.json");
		this.loader.add("com.prefabware.business.commons.Currency","prefabware.model.to.tests", "currency-type.json");
		this.loader.add("com.prefabware.business.commons.Currency2","prefabware.model.to.tests", "currency2-type.json");
		this.loader.add("com.prefabware.business.commons.Amount","prefabware.model.to.tests", "amount-type.json");
		
		//customize the importer
		//so for type Currency there is a special instanceclass
		this.importer.getCustomTypeClass=function(type){
			//clients may hook in here to return custom classes for the given type
			return;
		};
		this.importer.getCustomInstanceClass=function(type){
			//clients may hook in here to return custom classes for the given type
			if (type.name=='com.prefabware.business.commons.Currency') {
				return prefabware.model.to.tests.Currency;
			}
		};
		
		var baseUnitType=this.registry.findType("com.prefabware.business.commons.BaseQuantitiyUnit");
		doh.assertTrue(baseUnitType!=null);
		doh.assertEqual('com.prefabware.business.commons.BaseQuantitiyUnit',baseUnitType.name);
		doh.assertEqual(6,baseUnitType.elements.count);
		doh.assertEqual("GRAM",baseUnitType.GRAM.name);
		
		var quantityUnitType=this.registry.findType("com.prefabware.business.commons.QuantityUnit");
		doh.assertTrue(quantityUnitType!=null);
		var qu=quantityUnitType.create();
		qu.baseUnit=baseUnitType.GRAM;
		doh.assertEqual("GRAM",qu.__labelOf("baseUnit"));
		doh.assertEqual("GRAM",qu.baseUnit.__serializable());
		
		doh.assertEqual("GRAM",qu.__serializable().baseUnit,"enums should be serialized as there name only");
		doh.assertEqual("GRAM",qu.__clone().__serializable().baseUnit,"enums should be serialized as there name only");
		
		//now test that Currency uses the custom classes, but Currency2 not
		var currency2Type=this.registry.findType("com.prefabware.business.commons.Currency2");
		doh.assertTrue(currency2Type!=null);
		doh.assertEqual('com.prefabware.business.commons.Currency2',currency2Type.name);
		
		var currency2=currency2Type.create();
		doh.assertTrue(currency2.myType==null,'should not have this special attribute of class Currency');

		//now the customized currency with a special __label method
		var currencyType=this.registry.findType("com.prefabware.business.commons.Currency");
		doh.assertTrue(currencyType!=null);
		doh.assertEqual('com.prefabware.business.commons.Currency',currencyType.name);
		
		var currency=currencyType.create();
		doh.assertEqual('label of Currency',currency.__label(),'expected the result of the custom instance');
		doh.assertEqual('prefabware.model.to.tests.Currency',currency.myType,'expected that attribute from the mixed in class');
		
		
		return;
	},
	tearDown : function() {
	}
} ]);