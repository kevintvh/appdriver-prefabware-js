//to load types from different TypeLoaders
//tries to load a type using all registered TypeLoaders in the order they were registered
//TypeLaoder should not throw exception, if thay cannot find a type, instead they just return null
//TypeLoader will be called only once for each type so can optimize themselfes 
define('prefabware/model/to/TypeLoaderList', [
        'dojo',
        'dojo/cache',
        'dojox/collections/Dictionary',
        'dojo/_base/array',
        'prefabware/lang',
        'dojo/_base/declare' ], function(dojo,cache,dictionary,array) {
	dojo.declare("prefabware.model.to.TypeLoaderList", null, {
		loaders:null,//the loaders registered here
		constructor : function() {			
			this.loaders=[];
		},		
		register : function(loader) {
			//registers the loader
			this.loaders.push(loader);
		},
		load:function(typeName){
			var jsonType=null;
			array.some(this.loaders,function(loader){
				jsonType =loader.load(typeName);
				return jsonType!=null;
			});			
			return jsonType;
		}		
	});
});

