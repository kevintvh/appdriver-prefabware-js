//to import types from json, and create the accoding type
//just creates the type, does not register it!
//this class is meant to be used by the typeRegistry only
define('prefabware/model/to/TypeImporter', [
        'dojo',
        'dojo/_base/lang',
        'dojo/_base/array',
        'dojo/aspect',
        "dojo/_base/declare",
        'prefabware/commons/ClassLoader',
        'prefabware/model/_EnumerationType',
        'dojo/_base/declare' ], function(dojo,lang,array,aspect,declare) {
	dojo.declare("prefabware.model.to.TypeImporter", null, {
		typeRegistry:null,
		cache:null,
		classLoader:new prefabware.commons.ClassLoader(),
		constructor : function(typeRegistry) {
			this.cache=new  dojox.collections.Dictionary();
			//typeRegistry is mandatory
			if (typeRegistry==undefined||typeRegistry==null) {
				throw('the parameter typeRegistry is mandatory');
			}
			this.typeRegistry=typeRegistry;
		},
		customizeTypeOptions:function(options){
			//clients may hook in here to customize the options for the type
			return options;
		},
		customizeAttributeOptions:function(options){
			//clients may hook in here to customize the options for the attribute
			return options;
		},
		getCustomTypeClass:function(type){
			//clients may hook in here to return custom classes for the given type
			return;
		},
		getCustomInstanceClass:function(type){
			//clients may hook in here to return custom classes for the given type
			return;
		},
		
		importType:function(jsonType){
			//to create the type from the json
			//and returns the type
			if (this.cache.contains(jsonType.name)) {
				return this.cache.item(jsonType.name);
			}
			//imports the jsonType
			//create the type as declared in the jsonType
			//this can be e.g. EntityType, ValueType etc.
			//this classes are already loaded and can be accessed synchronously
			var clazz=this.classLoader.getClass(jsonType.pfw_type);
			if (jsonType.enumeration) {
				//force an attribute 'name' in enumerations
				if (jsonType.attributes==null) {
					jsonType.attributes=[];
				};
				var attName={
				         'pfw_type':'prefabware.model.Attribute',
				         'name':'name',
				         'type':'prefabware.model.StringType',
				         'length':50
				      };
				jsonType.attributes.push(attName);
			}
			
			var customizedJsonType=lang.clone(jsonType);
				customizedJsonType=this.customizeTypeOptions(customizedJsonType);
				//options may contain a property attribute with names of attributes
				//this must be removed
			var type = new clazz(customizedJsonType);
			this.cache.add(jsonType.name,type);
			if (jsonType.enumeration) {
				var enum_=new prefabware.model._EnumerationType();
				dojo.safeMixin(type,enum_);
				//add the elements later, consider the customTypeClass before
			}
			var customTypeClass=this.getCustomTypeClass(type);
			if (customTypeClass!=null) {
				//if there is a custom class for that typename, mix it into the type
				//allowing to override methods like __label()
				type=dojo.safeMixin(type,new customTypeClass(type));
			}
			
			if (jsonType.enumeration) {
				//now add the elements, they need a attribute value and name
				array.forEach(jsonType.elements,function(element){
					if (element.name==null&&(typeof element.value=='string')) {
						//mostly the value is a string and the name is the same as the value
						element.name=element.value;
					}
					type.__addElement(element);
				});
			}
			
			
			var customInstanceClass=this.getCustomInstanceClass(type);
			if (customInstanceClass!=null) {
				//exchange type.create to allow creation of custom instances
				//a type class like EntitType may be used for many types !!
				aspect.after(type, "create", function(instance){
					var customInstance=declare.safeMixin(instance,new customInstanceClass(type));
					if (dojo.isFunction(customInstance.startup)) {
						customInstance.startup();
					}
					return customInstance;
				});
			}
			return type;
		},
		createAttributes:function(type,jsonType){
			var customizedJsonType=lang.clone(jsonType);
			customizedJsonType=this.customizeTypeOptions(customizedJsonType);
			
			var attributes=array.filter(customizedJsonType.attributes,function(attribute){
				//skip pfw_id and version, they will be added automatically by the CompositeType and EntityType
				//when the type is registered
				//TODO the modelserver does not need to send those attributes, if they are ignored here anyway
				return attribute.name!='pfw_id'&&attribute.name!='version';
			});
			array.forEach(attributes,function(jsonAttr){				
				//attribute.pfw_type is the attribute-class, we dont need it here
				//it is determined by type.addAttribute
				var attType=this.typeRegistry.findType(jsonAttr.type);
				if (attType==null) {
					//still not found ?
					throw 'cannot find type '+jsonAttr.type;
				}				
//				if (type.isStarted==true) {
				if (type.getAttribute(jsonAttr.name)==null) {
					var options=lang.clone(jsonAttr);
					options=this.customizeAttributeOptions(options, type);				
					options.type=attType;
					var attribute=type.addAttribute(options);
					if (options.isKey) {
						type.keyAttribute=attribute;
						}				
				}
			},this);
			return type;
		}		
	});
});

