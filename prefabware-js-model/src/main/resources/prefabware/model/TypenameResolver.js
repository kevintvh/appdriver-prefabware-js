//to resolve a Type from a list of Types
//receives names of types,and searchest for the entry with the closest name from the list typeNames
//uses the typeRegistry to get types for typenames to respect the sub/supertype relations
//the type names can also be generic like 'prefabware.app.E*'
define('prefabware/model/TypenameResolver', [ 'dojo'
		,'dojo/_base/array', 'dojo/_base/declare' ], function(dojo,array) {
	dojo.declare("prefabware.model.TypenameResolver", null, {		
		typeRegistry:null,
		constructor : function(typeRegistry) {
			this.typeRegistry=typeRegistry;
		},
		__typeNameMixin : function(typeOrTypeName) {
			var result={
					typeName:null
			};
			if ( typeof typeOrTypeName =='string') {
				result.typeName=typeOrTypeName;
			}else if(typeOrTypeName instanceof prefabware.model.Type){
				result.type=typeOrTypeName;
				result.typeName=typeOrTypeName.name;
			}
			// mixes function into the typeName that makes comparing it easier
			var that=this;
			result.isGeneric = function() {
				// its generic, when it contains a '*'
				return this.typeName.indexOf('*')>=0;
			};
			result.isType = function() {
				if (this.isGeneric()) {
					// if its generic, it cannot be a type
					return false;
				}
				// its a type, if the type can be found
				return this.getType()!=undefined;
			};
			result.getType = function() {
				if (this.isGeneric()) {
					// if its generic, do not try to load the type
					return undefined;
				}
				if (this.type) {
					// if the type is allready known, use it
					return this.type;
				}
				// returns the type with this name
				// returns undefined, if the type does not exist
				try {
					var type=that.typeRegistry.findType(this.typeName);
					return type;
				} catch (e) {
					// not found ?
					return undefined;
				}
			};
			result.isNamespace = function() {
				// returns true, if the name is a namespace
				// its a namespace if its not a type and its not generic
				return !this.isType()&&!this.isGeneric();
			};
			result.matchesName = function(type) {
				if (this.isGeneric()) {
					return false;
				};
				if (this.isNamespace()) {
					return false;
				};
				if (this.typeName==type.name) {
					return true;
				};
			};
			result.matchesGeneric = function(type) {
				if (this.isGeneric()) {
					// returns true, if typeName starts with this after cutting
					// the '*'
					var matcher=this.typeName.substring(0,this.length-2);
					return type.name.indexOf(matcher)==0;
				} else {
					return type.name==this.typeName;
				}
			};
			result.matchesNamespace = function(type) {				
				// returns true, if the typeName is directly in this namespace
				// returns false if typeName is in another namespace
				// returns false if typeName is in a sub-namespace of this
				// namespace
				if (this.isNamespace()) {
					return type.namespace()==this.typeName;
				}else{
					return false;
				}
			};
			result.matchesSuperType = function(type) {
				// returns true, if the this is a superType of type
				// returns true, if the this is same as type
				// else returns false
				return this.getType()==type||this.getType().isSuperTypeOf(type);
			};
			result.matchesType = function(type) {
				// returns true, if the this is a superType of type
				// returns true, if the this is same as type
				// else returns false
				return this.getType()==type;
			};
			return result;
		},
	
		resolve : function(type,candidates) {
			// returns : the typename of the candidate that matches best to the given type
			// returns null, if no matching candidate can be found
			// type : for which we want to find the best candidate for. the type
			// must be a Type not a String
			// candidates : array of types,type names, generic type names or namespace to select
			// the best one from
			this.__assertCandidates(candidates);
			prefabware.lang.log('resolving '+type.name +' from candidates '+candidates);
			var result=null;
			var resultRank=0;
			array.every(candidates,function(candidate_ori){
				var rank=0;
				var candidate=this.__typeNameMixin(candidate_ori);

				// selects the given candidate as result, if its closer to the
				// given type
				if (candidate.matchesName(type)) {
					// exact match, cant be better
					rank=100;
				}else if (candidate.matchesNamespace(type)) {
					// the candidate is a namespace (like a package )e.g.
					// prefabware.app
					// and it contains the typeName
					rank=80;
				}else if (candidate.matchesSuperType(type)) {
					// candidate is a supertype of type
					// the greater the distance between the type and the
					// candidate, the lower the rank
					rank=50-candidate.getType().superTypeDistance(type);
				}else if (candidate.matchesGeneric(type)) {
					// the candidate is a generic name, like prefabware.app.*
					// and it matches the typeName
					// has the lowest ranking, could be '*' that matches every
					// type and so would be too eager
					rank=40;				
				}else{
					prefabware.lang.log('cannot rank candidate '+candidate);
					rank=0;
				};
				
				prefabware.lang.log('candidate '+candidate+ ' rank '+rank);
				if (rank>resultRank) {
					result=candidate;
					resultRank=rank;					
				}
				// if its 100, return false, to escape from array.every loop
				return resultRank!=100;
			},this);
			// return the original String object that was passed in, the client
			// may have set properties on that
			// that he needs
			if (result!=null) {
				prefabware.lang.log('resolved '+type.name +' to '+result.typeName);
				return result.typeName;
			}else{
				prefabware.lang.log('resolved '+type.name +' to null');
				return null;
			}
		},
		__assertCandidates : function(candidates) {
			if (!candidates) {
				throw 'candidates must be an array of Type, but is '+candidates;
			}
		},
		
	});
});
