//a attribute qualified with the type it belongs to  
define('prefabware/model/QualifiedAttribute', 
		[ 'dojo',  
		  'dojo/_base/declare' ], function(dojo) {
	dojo.declare('prefabware.model.QualifiedAttribute', null, {
		attribute:null,
		parentType:null,//the type to which the attribute belongs
		name:null,		
		constructor : function(options) {
			this.attribute=options.attribute;
			this.parentType=options.type;
			this.name=this.parentType.name+'.'+this.attribute.name;
		},
	});
});
