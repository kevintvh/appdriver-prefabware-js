//definition of an application
define('prefabware/model/ReferenceAttribute', [ 
    'dojo',
    'prefabware/model/Attribute',
    'dojo/_base/declare' ], function(dojo,Attribute) {
	dojo.declare("prefabware.model.ReferenceAttribute", [prefabware.model.Attribute], {
		isReference:true,
		constructor : function(options) {
		if(options.name=='self'){
			//self is allways contained
			this.containment=false;
		}else if (options.type.isValueType) {
				//ValueTypes are allways contained
				if (options.containment===false) {
					throw('a attribute of type=ValueType must be containment');
				}
				this.containment=true;
			}else if (options.type.isEnumeration) {
				//enumerations are never contained
				if (options.containment===true) {
					throw('a attribute of type=enumeration cannot be contained');
				}
				this.containment=false;
			}else if (options.type.isInstanceOf(prefabware.model.EntityType)) {
					//EntityTypes may be containment as in Invoice.invoiceLine
					//default is not containment
					if (options.containment===undefined) {
						this.containment=false;
					}else{
						this.containment=options.containment;
					}
			}else{
				throw('attribute has unexpected type='+options.type.name);
			}
		},
		createDefault : function(instance) {
			//the default of an entityType is an Entity
			//but the default for a referece to an Entity==null
			//the default for a reference to a ValueType==null also
			//values for attributes that are referenced are only created when needed.
			//TODO
			//if a attributes needs another default, the default can be passed to the cnstructor as options.default 
			return null;
		},	
//		__serializableOfSingle : function(value) {
//			if (value==null||value==undefined) { 
//				return value;
//			}
//			if (!this.type.isEnumeration&&!this.containment) {
//				// its a real reference, not an embedded value
//				// and not a enumeration
//				// so just use id,type and version
//				//TODO works for Spring REST only
//				var link;
//				link = new Object();
//				link.rel=this.name;
//				
////				var selfLink=prefabware.rest.selfLink(value);
////				if (selfLink!=null) {
////					link.href=this.selfLink.href;
////				}
////				link.pfw_id = value.pfw_id;
////				link.pfw_type = value.pfw_type;
////				link.version = value.version;
//				return link;
//			} else {
//				return this.inherited(arguments);
//			}
//		}
	});
});
