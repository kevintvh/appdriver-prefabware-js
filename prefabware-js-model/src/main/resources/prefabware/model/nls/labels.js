define({
  root: {
	masterData : "Master Data",
	settings   : "Settings",
	name       : "Name",
    pfw_id     : "Id",
    pfw_type: "Type",
    version: "Version",
    createAction: "New",
    saveAction: "Save",
    deleteAction: "Delete",
    printAction: "Print",  
    address:"Adress"
  },
  de: true
});