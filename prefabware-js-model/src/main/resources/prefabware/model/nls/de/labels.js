define({ 
	masterData : "Stammdaten",
	settings   : "Einstellungen",
	name   : "Name",
    pfw_id     : "Id",
    pfw_type: "Typ",
    version: "Version",
    createAction: "Neu",
    saveAction: "Speichern",
    deleteAction: "Löschen",    
    printAction: "Drucken",    
    closeAction: "Schließen",    
    address:"Adresse"
});