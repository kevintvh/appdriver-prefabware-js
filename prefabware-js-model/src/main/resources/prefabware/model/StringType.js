//a enumeration
define('prefabware/model/StringType', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",'prefabware/model/Type',
		  'dojo/_base/declare' ], function(dojo,lang,locale,Type) {
	dojo.declare('prefabware.model.StringType', [prefabware.model.Type], {
		length:null,
		isString:true,// 
	stringOf : function(value) {
		return value;
	},
	create : function() {
		return '';
	},
	isValue : function(value) {
		//return true, if the value is a String
		return lang.isString(value);
	},
	serializableOf : function(value) {
		if (value==null) {
			return null;
		}
		return value;
	},
	});
});
