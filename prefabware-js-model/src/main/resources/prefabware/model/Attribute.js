//attribute of a type
define('prefabware/model/Attribute', [ 'dojo',  'dojo/_base/array','prefabware/util/OptionsMixin',
		'dojo/_base/declare' ], function(dojo, array) {
	dojo.declare("prefabware.model.Attribute", [prefabware.util.OptionsMixin], {
		name : null,
		type : null,
		// the length in character
		length : 10,
		// minimum cardinality of this attribute, 0=optional, 1=mandatory
		min : 0,
		// maximum cardinality of this attribute, max>1 means value is an array
		max : 1,
		order:1000,//the order of this attribute in the list of all attributes of the type
		label : null,//the label to use for this attribute
		// the attribute value is contained inside the instance of type.
		// primitives are allways contained
		containment : true,
		// readOnly=true, the attribute cannot be changed by the user
		isReadOnly : false,
		isOptional : false,
		isEager : false,
		labelOrder : 0,// >0 = the value of this attribute is used as label for the Composite that owns it
		descriptionOrder : 0,// >0 = the value of this attribute is used as label for the Composite that owns it
		isKey : false,//true=the value of this attribute is used as key for the Composite that owns it
		isSortDescending:false,//true= when this column is used to sort, use it in descending order
		isSystem:false,//true=this is a system internal attribute
		constructor : function(options) {			
			this.mixinOptions(options);//copy all properties from options into this
			if (options.default_ != undefined) {
				if (typeof options.default_ == "function") {
					//if its a function, use it
					this.createDefault=options.default_;
				}else{
					//if its a value use it as default
					this.createDefault=function(){
						return options.default_;
					};
				}
			}
			this.isLabel = this.labelOrder>0;
			this.isDescription = this.descriptioOrder>0;
			this.label = this.name;
		},
		createDefault : function(instance) {
			//creates a default for this attribute in the given instance
			//instance : the instance to create the default for, given to access other attributes
			//	when calculating the default
			//returns the default value for the attribute
			//implementors should NOT change the instance e.g. set the value
			return this.type.createDefault();
		},		
		stringOf : function(value) {
			if (value == undefined || value == null) {
				return '';
			}
			if (value.__label) {
				return value.__label();
			} else {
				if (this.type.labelOf) {
					return this.type.labelOf(value);
				} else {
					return value;
				}
			}
		},
		convertFrom : function(rawValue) {
			//converts the given rawValue into a value of this attributes type and returns it
			return this.type.convertFrom(rawValue);
		},
		serializableOf : function(value) {
			//returns the value or a copy of value that can be serialized using dojo.toJson()
			//value can be an 'instance' of this type
			//or an array of 'instances'
			var that = this;
			var result;
			if (dojo.isArray(value)) {
				result = array.map(value, function(element) {
					return that.__serializableOfSingle(element);
				});
			} else {
				result = that.__serializableOfSingle(value);
			}
			return result;
		},
		__serializableOfSingle : function(value) {
			//returns the value or a copy of value that can be serialized using dojo.toJson()
			//subclasses may override this method to provide their own serialization
			return this.type.serializableOf(value);
		},
		toString : function() {return this.declaredClass + ' '+this.name+' type='+this.type ;
		},
		isValue : function(value) {
			if (value==null) {
				//let the type decide
				this.type.isValue(value);
			}
					if (this.max > 1) {
						if (!dojo.isArray(value)) {
							// not null and not an array
							return false;
						} else {
							return array.every(value, function(element) {
								// check that all elements of the array are of
								// the desired type
								return this.type.isValue(element);
							},this);
						}
						;
					} else {
						return this.type.isValue(value);
					}
		}
	});
});