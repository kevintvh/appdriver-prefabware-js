//definition of an ValueType
define('prefabware/model/ValueType', [ 'dojo',  
		'prefabware/model/CompositeType',
		'prefabware/model/TypeRegistry',
		'prefabware/model/Value', 
		'prefabware/model/ReferenceAttribute',
		'dojo/_base/declare' ], function(dojo,Type,TypeRegistry,Value) {
	dojo.declare("prefabware.model.ValueType", [ prefabware.model.CompositeType ], {
		isValueType:true,//just to overcome classloading cycle
		constructor : function(name,labels) {
		},
		create : function() {
			return new prefabware.model.Value(this);
		},
	});
});
