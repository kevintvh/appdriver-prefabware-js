//definition of an application
define('prefabware/model/DateType', [ 
        'dojo',
        'dojo/_base/lang',
		'dojo/date/locale',
		'dojo/date/stamp',
		'prefabware/model/Type',
		'prefabware/model/Attribute', 'dojo/_base/declare' ], function(dojo,
		lang, locale, Type) {
	dojo.declare('prefabware.model.DateType', [ prefabware.model.Type ], {
		// the string value of a DataType is a String of format YYYY-MM-DD like
		// 1980-10-11
		isDate:true,// 
		constructor : function(name) {
		},
		create : function() {
			return new Date();
		},
		convertFrom : function(rawValue) {	
			//parses a date in iso format like  '2010-02-01' into a date
			if (rawValue===undefined||rawValue===null) {
				return rawValue;
			} else if (rawValue instanceof Date) {	
				//its allready a date, nothing to do
				return rawValue;
			} else if (dojo.isArray(rawValue)) {	
				//array of year month day [2013, 10, 10]
				//TODO month +1 necessary ?
				var date = new Date(rawValue[0], rawValue[1], rawValue[2]);
				//this.assertIsValue(date);
				return date;
			} else if (dojo.isString(rawValue)) {
				if (rawValue.length==0) {
					return null;
				}
				var array = this.__split(this.__truncate(rawValue));
				//var month = array[1] - 1;
				var date = new Date(array[0], array[1]-1, array[2]);
				//this.assertIsValue(date);
				return date;
			} else {
				throw 'canot create date of value='+rawValue;
			}
		},
		__split : function(value) {
			if (value==undefined) {
				return '';
			}
			return value.split("-");
			;
		},
		__truncate : function(value) {
			var array = this.__split(value);
			return array[0] + '-' + array[1] + '-' + array[2].substring(0, 2);
		},
		isValue : function(value) {
			if (value==null) {
				return true;
			}else if (!this.inherited(arguments)) {
				return false;
			}else{
				//its an invalid date
				return !isNaN(value.getTime());
			}
		},
		stringOf : function(value) {
			this.assertIsValue(value);
			//returns a date in iso format like  '2010-02-01'
			//this the format expected by html input of type date
			if (value instanceof Date) {
				if (isNaN(value.getTime())) {
					throw 'value is a invalid Date';
				};
				return dojo.date.stamp.toISOString(value).substr(0,10);
			}else if(value==null){
				return "";
			}else{
				//TODO throw exception !!
				return value;
				}
		},
		labelOf : function(value) {
			var label = locale.format(value, {
				selector : 'date',
				formatLength : 'short'
			});
			return label;
		},
		serializableOf : function(value) {
			if (value==null) {
				return null;
			}
			return this.__truncate(this.stringOf(value));
		},
	});
});
