//to resolve a Type from a list of Types
define('prefabware/model/TypeResolver', [ 'dojo'
		,'dojo/_base/array', 'dojo/_base/declare' ], function(dojo,array) {
	dojo.declare("prefabware.model.TypeResolver", null, {		
		typeRegistry:null,
		constructor : function(typeRegistry) {
			this.typeRegistry=typeRegistry;
		},
		
		resolve : function(type,candidates) {
			//returns : the candidate that matches best to the given type
			//returns : null, if no matching candidate can be found
			//type : for which we want to find the best candidate for. the type must be a Type not a String
			//candidates : array of types 
			this.__assertCandidates(candidates);
			prefabware.lang.log('resolving '+type.name +' from candidates '+candidates);
			var result=null;
			var resultRank=0;
			array.every(candidates,function(candidate){
				var rank=0;
				//selects the given candidate as result, if its closer to the given type
				if (candidate==type) {
					//exact match, cant be better					
					rank=100;
				}else if (candidate.isSuperTypeOf(type)) {
					//candidate is a supertype of type
					//the greater the distance between the type and the candidate, the lower the rank
					rank=50-candidate.superTypeDistance(type);
				}else{
					//throw 'cannot rank candidate '+candidate;
				prefabware.lang.log('no match for candidate '+candidate);
				};
				prefabware.lang.log('rank for candidate '+candidate+ ' is : '+rank);
				if (rank>resultRank) {
					result=candidate;
					resultRank=rank;					
				}
				//if its 100, return false, to escape from array.every loop
				return resultRank!=100;
			},this);
			if (result!=null) {
				prefabware.lang.log('resolved '+type.name +' to '+result.name);
			}else{
				prefabware.lang.log('resolved '+type.name +' to null');
			}
			return result;
		},
	
		__assertCandidates : function(candidates) {
			if (!candidates) {
				throw 'candidates must be an array of Type, but is '+candidates;
			}
		},
		
	});
});
