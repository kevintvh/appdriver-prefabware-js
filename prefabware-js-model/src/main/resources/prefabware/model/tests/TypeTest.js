// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.TypeTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.model.QualifiedAttribute");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.model.tests.TypeTest", [ 
{
	name : "extend",
	
	setUp : function() {
		window.setUp={};
		setUp.registry = new prefabware.model.TypeRegistry();
		setUp.typeResolver=new prefabware.model.TypeResolver(setUp.registry);
		setUp.STRING = setUp.registry.STRING;
		setUp.DECIMAL = setUp.registry.DECIMAL;
		setUp.BOOLEAN = setUp.registry.BOOLEAN;
		setUp.TYPE = setUp.registry.TYPE;
		setUp.COMPOSITE = setUp.registry.COMPOSITE;
		setUp.ENTITY = setUp.registry.ENTITY;
		
		setUp.partyType = new prefabware.model.EntityType('test.Party');
		setUp.partyType.addAttribute({
			name : 'name',
			type : setUp.STRING,
			length : 30,
		});
		setUp.registry.registerType(setUp.partyType);
		
		setUp.partyHolderType = new prefabware.model.EntityType('test.PartyHolder');
		setUp.partyHolderType.addAttribute({
			name : 'party',
			type : setUp.partyType
		});
		
		setUp.registry.registerType(setUp.partyHolderType);
		
		//setUp.registry.startup();
	},
	runTest : function() {
		var partyHolderType=setUp.partyHolderType;
		doh.assertEqual( partyHolderType,setUp.registry.findType('test.PartyHolder'));
		doh.assertNotEqual(null,partyHolderType.getAttribute('party'));
		var partyName=partyHolderType.getAttribute('party.name');
		doh.assertNotEqual(null,partyName);
		doh.assertEqual('party.name',partyName.name);
		doh.assertEqual('party',partyName.rootName);
		
		
		var partyType=setUp.partyType;
		doh.assertEqual( setUp.registry.TYPE,setUp.registry.STRING.superType);
		
		doh.assertEqual( partyType,setUp.registry.findType('test.Party'));
		doh.assertEqual( 'Party',partyType.simpleName());
		doh.assertEqual( 'test',partyType.namespace());
		doh.assertEqual( setUp.registry.ENTITY,partyType.superType);
		

		var  xType= new prefabware.model.EntityType('test.namespace.X');
		setUp.registry.registerType(xType);
		var subNsXType = new prefabware.model.EntityType('test.namespace.sub.X');
		setUp.registry.registerType(subNsXType);

		var partySelf=partyType.getAttribute('self');
		doh.assertEqual( partyType,partySelf.type);
		doh.assertTrue(partySelf.isSystem);
		var partySelfQ=partyType.getQualifiedAttribute('self');
		doh.assertEqual( partySelf,partySelfQ.attribute);
		doh.assertEqual( partyType,partySelfQ.parentType);
		
		doh.assertNotEqual(null,partyType.getAttribute('name'));
		doh.assertNotEqual(null,partyType.getAttribute('pfw_id'));
		doh.assertNotEqual(null,partyType.getAttribute('version'));
		doh.assertNotEqual(null,partyType.getAttribute('pfw_type'));
		
		doh.assertEqual( partyType,setUp.registry.findType('test.Party'));
		
		var personType = new prefabware.model.EntityType({name:'test.Person',superType:partyType});
		personType.addAttribute({
			name : 'firstName',
			type : setUp.STRING,			
			length : 30,
		});
		setUp.registry.registerType(personType);
		//setUp.registry.startup();
		doh.assertEqual( personType,setUp.registry.findType('test.Person'));
		doh.assertNotEqual(null,personType.getAttribute('firstName'));
		doh.assertNotEqual(null,personType.getAttribute('name'));
		
		var qName=personType.getQualifiedAttribute('name');
		doh.assertNotEqual(null,qName);
		doh.assertEqual('prefabware.model.QualifiedAttribute',qName.declaredClass);
		doh.assertEqual(personType.getAttributes().length,personType.getQualifiedAttributes().length);
		doh.assertEqual(personType,qName.parentType);
		doh.assertEqual(personType.getAttribute('name'),qName.attribute);
		doh.assertEqual('test.Person.name',qName.name);
		
		//second level inheritance
		var manType = new prefabware.model.EntityType({name:'test.Man',superType:personType});
		manType.addAttribute({
			name : 'isFather',
			type : setUp.BOOLEAN,			
		});
		setUp.registry.registerType(manType);
		//setUp.registry.startup();
		doh.assertEqual( manType,setUp.registry.findType('test.Man'));
		doh.assertNotEqual(null,manType.getAttribute('firstName'));
		doh.assertNotEqual(null,manType.getAttribute('name'));
		doh.assertNotEqual(null,manType.getAttribute('isFather'));
		
		doh.assertEqual( true,partyType.isSuperTypeOf(personType));
		doh.assertEqual( 1,partyType.superTypeDistance(personType));
		doh.assertEqual( 2,partyType.superTypeDistance(manType));
		doh.assertTrue( partyType.superTypeDistance(setUp.STRING)<0);
		
		
		
		return;
	},
	tearDown : function() {
	}
},
{
	name : "attribute filter",
	setUp : function() {
		
	},
	runTest : function() {
		var type2 = new prefabware.model.EntityType('test.Type2');
		//tha names of the attributes :
		//L=Label, O=Optional n=not
		var LO=type2.addAttribute({
			name : 'LO',
			labelOrder : 1,
			isOptional : true,
			type : setUp.STRING,
			length : 30,
		});
		var nLO=type2.addAttribute({
			name : 'nLO',
			isOptional : true,
			type : setUp.STRING,
			length : 30,
		});
	   var LnO=type2.addAttribute({
			name : 'LnO',
			labelOrder : 2,
			type : setUp.STRING,
			length : 30,
		});
	  var nLnO=type2.addAttribute({
			name : 'nLnO',
			type : setUp.STRING,
			length : 30,
		});
		
		setUp.registry.registerType(type2);
		
		//test attribute filter
		var filtered=type2.getAttributes({isLabel:true});
		doh.assertEqual(2,filtered.length);
		doh.assertEqual(0,filtered.indexOf(LO));
		doh.assertEqual(1,filtered.indexOf(LnO));
		
		filtered=type2.getAttributes({isSystem:true});
		doh.assertEqual(4,filtered.length,"expected: self,pfw_id, pfw_type, version");

		filtered=type2.getAttributes({isOptional:true,isSystem:false});
		doh.assertEqual(2,filtered.length);
		doh.assertTrue(filtered.indexOf(nLO)>=0);
		doh.assertTrue(filtered.indexOf(LO)>=0);

		filtered=type2.getAttributes({isOptional:false,isSystem:false});
		doh.assertEqual(2,filtered.length);
		doh.assertTrue(filtered.indexOf(nLnO)>=0);
		doh.assertTrue(filtered.indexOf(LnO)>=0);
		
		return;
	},
	tearDown : function() {
	}
},
{
	name : "convertFrom",
	setUp : function() {
		
	},
	runTest : function() {
		
		doh.assertTrue( setUp.BOOLEAN.convertFrom(true));
		doh.assertTrue( setUp.BOOLEAN.convertFrom("true"));
		doh.assertTrue( setUp.BOOLEAN.convertFrom(1));
		doh.assertTrue( setUp.BOOLEAN.convertFrom('1'));
		
		doh.assertFalse( setUp.BOOLEAN.convertFrom(false));
		doh.assertFalse( setUp.BOOLEAN.convertFrom("false"));
		doh.assertFalse( setUp.BOOLEAN.convertFrom(0));
		doh.assertFalse( setUp.BOOLEAN.convertFrom('0'));
		var exception=false;
		try {
			setUp.BOOLEAN.convertFrom("FALSE");
		} catch (e) {
			exception=true;
		}
		doh.assertTrue(exception,'exception expected');		
		
		return;
	},
	tearDown : function() {
	}
},
{
	name : "create Entity",
	setUp : function() {

	},
	runTest : function() {
		var partyType=setUp.partyType;
		doh.assertEqual( partyType,setUp.registry.findType('test.Party'));
		var party=partyType.create();
		doh.assertEqual( "prefabware.model.Entity",party.declaredClass);
		doh.assertTrue( party.pfw_id<0);
		doh.assertTrue( party.__isNew());
		party.__assignId();
		doh.assertFalse( party.__isNew());
		
		var party2=partyType.create();
		party2.__assignId();
		doh.assertNotEqual(party.pfw_id, party2.pfw_id);
		return;
	},
	tearDown : function() {
	}
},

]);