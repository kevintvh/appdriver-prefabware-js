// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.ReferenceProxy");
// Import in the code being tested.
dojo.require("prefabware.model.client.Proxy");
dojo.require("dojo._base.lang");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.client.CompositeProxy");
dojo.require("prefabware.model.client.ReferenceProxy");
dojo.require("prefabware.model.Entity");
doh
		.register(
				"prefabware.model.tests.ReferenceProxy",
				[
						{
							name : "resolveReference",
							STRING : null,
							DECIMAL : null,
							DATE : null,
							registry:null,
							setUp : function() {
									this.registry = new prefabware.model.TypeRegistry();
									this.STRING = this.registry.STRING;
									this.DECIMAL = this.registry.DECIMAL;
									this.DATE = this.registry.DATE;
									this.BOOLEAN = this.registry.BOOLEAN;
									this.TYPE = this.registry.TYPE;
									this.COMPOSITE = this.registry.COMPOSITE;
									this.ENTITY = this.registry.ENTITY;
								
								var textType = new prefabware.model.EntityType('com.prefabware.wapp.userauth.entity.Text');
								textType.addAttribute({name:'language',type:this.STRING});
								textType.addAttribute({name:'text',type:this.STRING});
								
								
								var labelType = new prefabware.model.EntityType('com.prefabware.wapp.userauth.entity.Label');
								labelType.addAttribute({name:'key',type:this.STRING});
								labelType.addAttribute({name:'description',type:this.STRING});
								var texts=labelType.addAttribute({name:'texts',type:textType});
								//make it one to many
								texts.other=999;
								texts.containment=true;
								
								this.registry.registerType(textType);
								this.registry.registerType(labelType);
							},
					  runTest : function() {
								var labelType = this.registry
								.findType('com.prefabware.wapp.userauth.entity.Label');
								var textType = this.registry
								.findType('com.prefabware.wapp.userauth.entity.Text');
												
						var data = new Object();
						data.pfw_type = textType.name;
						data.pfw_id = 4711;
						data.version = 2;
						data.$ref = '/wapp-user-auth/rest/textView/list/2';
						
						var proxy = new prefabware.model.client.ReferenceProxy(
								textType, data);
							    //makes the data an entitity
								doh.assertTrue(data.__isEntity);
								doh.assertEqual(data.pfw_type, textType.name);
								doh.assertEqual(4711,data.pfw_id);
								doh.assertEqual(2,data.version);
								
								//now the data should be a proxy
								doh.assertTrue(data.__isEntity);
								doh.assertTrue(data.__isProxy);
								doh.assertFalse(data.__isResolved);
								doh.assertEqual(data.pfw_type, textType.name);
								doh.assertEqual(4711,data.pfw_id);
								doh.assertEqual(2,data.version);
								//still should not be resolved
								doh.assertFalse(data.__isResolved);
								doh.assertFalse(data.language==undefined);
								doh.assertTrue(data.__isResolved);
								return;
							},
							tearDown : function() {
							}
						} ]);