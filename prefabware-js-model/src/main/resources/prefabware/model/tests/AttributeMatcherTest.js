// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.AttributeMatcherTest");
// Import in the code being tested.
dojo.require("prefabware.model.match.AttributeMatcher");
dojo.require("prefabware.model.match.AttributeNameMatcher");
dojo.require("dojo._base.lang");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
doh.register("prefabware.model.tests.AttributeMatcherTest", [ {
	name : "AttributeMatcherTest",
	registry:null,
	typeResolver:null,
	STRING : null,
	DECIMAL : null,
	setUp : function() {
		this.registry = new prefabware.model.TypeRegistry();
		this.typeResolver=new prefabware.model.TypeResolver(this.registry);
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
	},
	runTest : function() {
		var cityType = new prefabware.model.EntityType('test.City');
		cityType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			//use a simple value
			default_:'Cologne'
		});
		var partyType = new prefabware.model.EntityType('test.Party');
		partyType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			//use a simple value
			default_:'Smith'
		});
		partyType.addAttribute({
			name : 'city',
			type : cityType
		});
		partyType.addAttribute({
			name : 'firstName',
			type : this.STRING,
			length : 30,
			//use a function to calculate the default
			default_:function(instance){return instance.name+'y';}
		});
		
		this.registry.registerType(cityType);
		this.registry.registerType(partyType);
		
		doh.assertEqual( cityType,this.registry.findType('test.City'));
		doh.assertEqual( partyType,this.registry.findType('test.Party'));
		var nameAttribute= cityType.getQualifiedAttribute('name');
		doh.assertTrue(nameAttribute!=null);
		
		doh.assertEqual(50,new prefabware.model.match.AttributeMatcher({type:'.*',matches:"qattribute.attribute.type.name=='prefabware.model.StringType'"}).matches({qattribute:nameAttribute}).degree,"should match, type matches and evalMatcher also");
		doh.assertEqual(-1,new prefabware.model.match.AttributeMatcher({type:'.*',matches:"qattribute.attribute.type.name=='prefabware.model.DecimalType'"}).matches({qattribute:nameAttribute}).degree,"should not match, type matches but evalMatcher not");

		doh.assertEqual(50,new prefabware.model.match.AttributeMatcher({type:'.*'}).matches({qattribute:nameAttribute}).degree,"should match, type matches");
		doh.assertEqual(100,new prefabware.model.match.AttributeMatcher({type:'prefabware.model.StringType'}).matches({qattribute:nameAttribute}).degree,"should match, type matches");
		doh.assertEqual(-1,new prefabware.model.match.AttributeMatcher({qattribute:'com.prefabware.commons.Customer.a.*',type:'prefabware.model.StringType'}).matches({qattribute:nameAttribute}).degree,"no match expected, wrong name");
		doh.assertEqual(-1,new prefabware.model.match.AttributeMatcher({qattribute:'test.City.name*',type:'prefabware.model.DateType'}).matches({qattribute:nameAttribute}).degree,"no match expected, wrong type");
		doh.assertEqual(50,new prefabware.model.match.AttributeMatcher({qattribute:'test.City.n*'}).matches({qattribute:nameAttribute}).degree,"should match, no type specified");
		doh.assertEqual(50,new prefabware.model.match.AttributeMatcher({qattribute:'test.City.n*',type:'prefabware.model.StringType'}).matches({qattribute:nameAttribute}).degree,"should match, type matches and regex also");

		doh.assertEqual(50,new prefabware.model.match.AttributeNameMatcher({pattern:'com.prefabware.commons.Customer.n.*'}).matches({qattributeName:'com.prefabware.commons.Customer.name'}).degree,"partial match expected");
		doh.assertEqual(100,new prefabware.model.match.AttributeNameMatcher({pattern:'com.prefabware.commons.Customer.name'}).matches({qattributeName:'com.prefabware.commons.Customer.name'}).degree,"exact match expected");
		doh.assertEqual(-1,new prefabware.model.match.AttributeNameMatcher({pattern:'com.prefabware.commons.Customer.n.*'}).matches({qattributeName:'com.prefabware.commons.Person.name'}).degree,"no match expected");
		

		
		return;
	},
	tearDown : function() {
	}
} ]);