// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.RecursiveTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.model.QualifiedAttribute");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.model.tests.RecursiveTest", [ 
{
	name : "extend",
	
	setUp : function() {
		window.setUp={};
		setUp.registry = new prefabware.model.TypeRegistry();
		setUp.typeResolver=new prefabware.model.TypeResolver(setUp.registry);
		setUp.STRING = setUp.registry.STRING;
		setUp.DECIMAL = setUp.registry.DECIMAL;
		setUp.BOOLEAN = setUp.registry.BOOLEAN;
		setUp.TYPE = setUp.registry.TYPE;
		setUp.COMPOSITE = setUp.registry.COMPOSITE;
		setUp.ENTITY = setUp.registry.ENTITY;
		
		setUp.documentType = new prefabware.model.EntityType('test.DocumentType');
		setUp.template = new prefabware.model.EntityType('test.template');
		setUp.documentType.addAttribute({
			name : 'template',
			type : setUp.template,
			length : 30,
		});
		setUp.template.addAttribute({
			name : 'type',
			type : setUp.documentType,
			length : 30,
		});
		
		setUp.registry.registerType(setUp.documentType);
		setUp.registry.registerType(setUp.template);
		
	},
	runTest : function() {
		var documentType=setUp.documentType;
		
	},
	tearDown : function() {
	}
}
]);