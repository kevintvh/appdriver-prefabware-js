// THIS TEST HAS NEVER RUN GREEN
dojo.provide("prefabware.model.tests.TypeResolverTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.model.tests.TypeResolverTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	setUp : function() {
		this.registry = new prefabware.model.TypeRegistry();
		STRING = this.registry.STRING;
		DECIMAL = this.registry.DECIMAL;
		BOOLEAN = this.registry.BOOLEAN;
	},
	runTest : function() {
		var partyType = new prefabware.model.EntityType('test.Party');
		partyType.addAttribute({
			name : 'name',
			type : STRING,
			length : 30,
		});
		
		this.registry.registerType(partyType);		
		doh.assertEqual( partyType,this.registry.findType('test.Party'));
		
		var personType = new prefabware.model.EntityType({name:'test.Person',superType:partyType});
		personType.addAttribute({
			name : 'firstName',
			type : STRING,			
			length : 30,
		});
		this.registry.registerType(personType);
		
		doh.assertEqual( personType,this.registry.findType('test.Person'));
		
		
		//second level inheritance
		var manType = new prefabware.model.EntityType({name:'test.Man',superType:personType});
		manType.addAttribute({
			name : 'isFather',
			type : BOOLEAN,			
		});
		this.registry.registerType(manType);
		
		doh.assertEqual( manType,this.registry.findType('test.Man'));
		
		var resolver=new prefabware.model.TypeResolver();
		
		
		doh.assertEqual( manType,resolver.resolve(manType,[partyType,personType,manType]),'expected to resolve to manType');
		doh.assertEqual( personType,resolver.resolve(manType,[partyType,personType]));
		doh.assertEqual( personType,resolver.resolve(manType,[personType,partyType]));
		doh.assertEqual( null,resolver.resolve(STRING,[partyType,personType,manType]));
		return;
	},
	tearDown : function() {
	}
} ]);