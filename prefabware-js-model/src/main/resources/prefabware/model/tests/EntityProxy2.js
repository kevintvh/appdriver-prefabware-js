// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.EntityProxy");
//Import in the code being tested.
dojo.require("prefabware.model.tests.EntityModules");
dojo.require("prefabware.workplace.ModuleRegistry");
dojo.require("prefabware.model.client.Proxy");
dojo.require("dojo._base.lang");
dojo.require("prefabware.model.client.EntityResolver");
dojo.require("prefabware.model.client.EntityProxy");
dojo.require("prefabware.model.Entity");
doh.register("prefabware.model.tests.EntityProxy", [
 
  {
	  name: "createProxy",
	  setUp: function(){    },
	  runTest: function(){
		  var entity=new Object();
		  entity.id=4711;
		  entity.name='Stefan';
		  entity.surname='Isele';
		  
		  var reference=new Object();
		  reference.type="Customer";
		  reference.id=4711;
		  
		  var proxy = new prefabware.model.client.Proxy();
		  doh.assertTrue(proxy.name==undefined);
		  
		  proxy.createPropertyAccessors("name");
		  try
		  {
			  //this will throw an exception because it invokes the getter 
			  //and tries to resolve
			  doh.assertTrue(proxy.name!=undefined);
			  doh.fail("exception expected");
		  }
		  catch(err)
		  {  }
		  //fake resolve
		  proxy.__target=entity;    	
		  doh.assertTrue(proxy.name!=undefined);
		  doh.assertEqual(entity.name,proxy.name);
		  
		  proxy.createPropertyAccessors("id");
		  doh.assertEqual(entity.id,proxy.id);
		  proxy.id="4711_2";
		  doh.assertEqual(entity.id,proxy.id);
		  entity.id="4711_3";
		  doh.assertEqual(entity.id,proxy.id);
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {
	    name: "resolveProxy",
	    setUp: function(){    },
	    runTest: function(){
	    	var entity=new Object();
	    	entity.id=4711;
	    	entity.name='Stefan';
	    	entity.surname='Isele';
	    	
	    	var reference=new Object();
	    	reference.type="Customer";
	    	reference.id=4711;

	    	var proxy = new prefabware.model.client.Proxy();
	    	doh.assertTrue(proxy.name==undefined);
	    	
	    	proxy.createPropertyAccessors("name");
	    	
	    	proxy.__reference=reference;
	    	function doResolve(referenceArg) {
	    		//we do not really need the reference to resolve here
	    		doh.assertEqual(reference,referenceArg);
	    		return entity;
			};
			proxy.doResolve=doResolve;	
	    	 	
	    	doh.assertTrue(proxy.name!=undefined);
	    	doh.assertEqual(entity.name,proxy.name);
	    	
	    	proxy.createPropertyAccessors("id");
	    	doh.assertEqual(entity.id,proxy.id);
	    	proxy.id="4711_2";
	    	doh.assertEqual(entity.id,proxy.id);
	    	entity.id="4711_3";
	    	doh.assertEqual(entity.id,proxy.id);
	    	
	    	return;
	    },
	    tearDown: function(){
	    }},
	   
	    {
	    	name: "resolve",
	    	registry:null,
	    	setUp: function(){  
	    		this.registry=new prefabware.workplace.ModuleRegistry();
	    		var modules=new prefabware.model.tests.EntityModules();
	    		modules.registerAt(this.registry);
	    		
	    	},
	    	runTest: function(){
	    		var labelType=this.registry.findByName('com.prefabware.wapp.userauth.entity.Label').type;
	    		doh.assertNotEqual(null,labelType);
	    		
	    		
	    		var entity=new Object();
	    		entity.id=4711;
	    		entity.key='DELETE';
	    		entity.description='to delete an Element';
	    		
	    		var reference=new Object();
	    		reference.pfw_type=labelType;
	    		reference.pfw_id=2;
	    		reference.$ref='/wapp-user-auth/rest/labelView/list/2';
	    		
	    		var proxy = new prefabware.model.client.EntityProxy(labelType);
	    		doh.assertTrue(proxy.name==undefined);
	    		
	    		
	    		
	    		
	    		proxy.__reference=reference;
	    		
	    		var resolver=new prefabware.model.client.EntityResolver();
	    		
	    		
	    		
	    		function doResolve(referenceArg) {
	    			//thats a real resolve calling the server
	    			return resolver.resolve(referenceArg.$ref);
	    		};
	    		proxy.doResolve=doResolve;	
	    		
	    		doh.assertTrue(proxy.key!=undefined);
	    		doh.assertTrue(proxy.description!=undefined);
	    		doh.assertEqual(entity.key,proxy.key);
	    		doh.assertEqual(entity.description,proxy.description);
	    		
//		    	proxy.key='person';
//		    	doh.assertEqual(entity.key,proxy.key);
//		    	proxy.description="Person";
//		    	doh.assertEqual(entity.description,proxy.description);
	    		return;
	    	},
	    	tearDown: function(){
	    	}
	    }
  // ...
]);