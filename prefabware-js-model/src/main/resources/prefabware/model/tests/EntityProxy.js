// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.EntityProxy");
// Import in the code being tested.
dojo.require("prefabware.model.client.Proxy");
dojo.require("dojo._base.lang");
dojo.require("prefabware.model.client.EntityProxy");
dojo.require("prefabware.model.Entity");
dojo.require("prefabware.model.client.CompositeProxy");
dojo.require("prefabware.model.tests.EntityModules");
dojo.require("prefabware.workplace.ModuleRegistry");
doh
		.register(
				"prefabware.model.tests.EntityProxy",
				[
						{
							name : "resolveforEntity",
							registry : null,
							setUp : function() {
								this.registry = new prefabware.workplace.ModuleRegistry();
								var modules = new prefabware.model.tests.EntityModules();
								modules.registerAt(this.registry);

							},
							runTest : function() {
								var labelType = this.registry
								.findByName('com.prefabware.wapp.userauth.entity.Label').type;
								var textType = this.registry
								.findByName('com.prefabware.wapp.userauth.entity.Text').type;
						doh.assertNotEqual(null, labelType);
						var data = new Object();
						data.pfw_id = 4711;
						data.pfw_type = labelType.name;
						data.version = 1;

						data.key = 'testKey';
						data.description = 'description!';
						
						var reference = new Object();
						reference.pfw_type = textType.name;
						reference.pfw_id = 2;
						reference.version = 2;
						reference.$ref = '/wapp-user-auth/rest/textView/list/2';
						
						data.texts=[reference];

						var proxy = new prefabware.model.client.EntityProxy(
								labelType, data);

						doh.assertTrue(data.__isEntity);
						doh.assertTrue(data.__isProxy);
						doh.assertEqual(data.pfw_id, 4711);
						doh.assertEqual(data.pfw_type, labelType.name);						
						doh.assertEqual(data.version, 1);
						//the data is the original object, it is allready resolved
						doh.assertTrue(data.__isResolved);
						doh.assertEqual('testKey', data.key);
						doh.assertEqual('description!',
								data.description);
						
								var text=data.texts[0];
								doh.assertFalse(text==null);
								doh.assertTrue(text.__isEntity);
								doh.assertTrue(text.__isProxy);
								//the data is the original object, it is allready resolved
								doh.assertFalse(text.__isResolved);

								doh.assertTrue(text.language != undefined);
								doh.assertTrue(text.text != undefined);

								return;
							},
							tearDown : function() {
							}
						},
						{
							name : "createProxyForEntity",
							registry : null,
							setUp : function() {
								this.registry = new prefabware.workplace.ModuleRegistry();
								var modules = new prefabware.model.tests.EntityModules();
								modules.registerAt(this.registry);

							},
							runTest : function() {
								var labelType = this.registry
										.findByName('com.prefabware.wapp.userauth.entity.Label').type;
								doh.assertNotEqual(null, labelType);
								var data = new Object();
								data.pfw_id = 4711;
								data.pfw_type = labelType.name;
								data.version = 0;

								data.key = 'testKey';
								data.description = 'description!';

								
								new prefabware.model.client.EntityProxy(
										labelType, data);
								//now the data is the proxy!
								doh.assertTrue(data.__isProxy);
								doh.assertTrue(data.__isEntity);
								doh.assertTrue(data.__isResolved);
								doh.assertEqual(4711, data.pfw_id);

								doh.assertEqual(0, data.version);
								doh.assertEqual(labelType, data.__type);
								doh.assertEqual(labelType.name, data.pfw_type);

								return;
							},
							tearDown : function() {
							}
						} ]);