//a Registry for Types
// you MUST call startup before using it !
define('prefabware/model/TypeRegistry',
		[ 'dojo',  
		  'dojo/Evented',
		  'dojo/i18n!prefabware/model/nls/labels',
		  'dojo/_base/array',
		  'dojox/collections/Dictionary',
		  'prefabware/model/to/TypeImporter',
		  'prefabware/model/StringType', 
		  'prefabware/model/NumberType', 
		  'prefabware/model/DecimalType', 
		  'prefabware/model/DateType', 
		  'prefabware/model/BooleanType', 
		  'prefabware/model/PfwType', 
		  'prefabware/model/CompositeType', 
		  'prefabware/model/EntityType', 
		  'prefabware/model/ValueType',
		  'dojo/_base/declare'
		  ], function(dojo,Evented,pfwLabels,array) {
	
		dojo.declare("prefabware.model.TypeRegistry", [Evented], {
		types : null,
		typesToStartup : null,
		importer:null,
		loader:null,
		loading:null,//the names of types that are currently loaded, to detect cycles
		registering:null,//the names of types that are currently registered, to startup when all are registered
		STRING:null,
		NUMBER:null,
		DECIMAL:null,
		DATE:null,
		BOOLEAN:null,
		PFW_TYPE:null,
		TYPE:null,
		COMPOSITE:null,
		ENTITY:null,
		
		constructor : function(options) {
			if (options!=undefined) {
				this.loader=options.loader;
			}
			//the importer is allwas the same, the loader my be a different implementation
			this.importer=new prefabware.model.to.TypeImporter(this);
			this.registering=new  dojox.collections.Dictionary();
			this.loading=new  dojox.collections.Dictionary();
			this.types=new  dojox.collections.Dictionary();
			this.typesToStartup=new  dojox.collections.Dictionary();

			//types must be registered before they can be used as supertypes			
			this.TYPE=new prefabware.model.Type('prefabware.model.Type');
			this.registerType(this.TYPE);
			
			this.STRING=new prefabware.model.StringType('prefabware.model.StringType');
			this.STRING.superType=this.TYPE;
			this.registerType(this.STRING);
			
			this.NUMBER=new prefabware.model.NumberType('prefabware.model.NumberType');
			this.NUMBER.superType=this.TYPE;
			this.registerType(this.NUMBER);
			
			this.DECIMAL=new prefabware.model.DecimalType('prefabware.model.DecimalType');
			this.DECIMAL.superType=this.NUMBER;
			this.registerType(this.DECIMAL);
			
			this.DATE=new prefabware.model.DateType('prefabware.model.DateType');
			this.DATE.superType=this.TYPE;
			this.registerType(this.DATE);
			
			this.BOOLEAN=new prefabware.model.BooleanType('prefabware.model.BooleanType');
			this.BOOLEAN.superType=this.TYPE;
			this.registerType(this.BOOLEAN);
			
			this.PFW_TYPE=new prefabware.model.PfwType('prefabware.model.PfwType');
			this.PFW_TYPE.superType=this.STRING;
			this.registerType(this.PFW_TYPE);
			
			this.COMPOSITE=new prefabware.model.CompositeType('prefabware.model.CompositeType');
			this.COMPOSITE.superType=this.TYPE;
			this.COMPOSITE.isAbstract=true;//there will never be direct instances of CompositeType, only of subclasses
			
			this.COMPOSITE.addAttribute({name:"pfw_type", order:5000,type:this.PFW_TYPE,label:pfwLabels.pfw_type,readOnly:true}).isSystem=true;
			this.registerType(this.COMPOSITE);

			this.ENTITY=new prefabware.model.EntityType('prefabware.model.EntityType');
			this.ENTITY.superType=this.COMPOSITE;
			this.ENTITY.isAbstract=true;
			this.ENTITY.addAttribute({name:"pfw_id", order:5010,type:this.NUMBER,label:pfwLabels.id,readOnly:true}).isSystem=true;
			this.ENTITY.addAttribute({name:"version", order:5020,type:this.NUMBER,label:pfwLabels.version,readOnly:true}).isSystem=true;
			this.registerType(this.ENTITY);
		},
		registerType : function(type, deferStartup) {
			//test if type already exists, do not call findType here, it might try to load the type
			//deferStartup = true, startup will not be handled here, but later from the caller
			var existing=this.types.item(type.name);
			if (existing!=null) {
			if (existing==type) {
				return type;
			}else {
				throw('another type with name '+type.name+' is allready registered');
			}			
			}
			this.registering.add(type.name,type.name);
			if (type.superType!=null && this.findType(type.superType.name)==undefined) {
				throw('type '+type.name+' references the unregistered supertype '+type.superType.name);
			}
			//set supertype for all stringtypes e.g. enumerations
			if (type.superType==null&&type.isInstanceOf( prefabware.model.StringType)&&type!=this.STRING) {
				type.superType=this.STRING;
			}
			//all EnttyType extend ENTITY that declares the pfw_id and version
			if (type.superType==null&&type.isInstanceOf( prefabware.model.EntityType)) {
				type.superType=this.ENTITY;
			}
			//all Composites extend COMPOSITE that declares the pfw_type and version
			if (type.superType==null&&type instanceof prefabware.model.CompositeType) {
				type.superType=this.COMPOSITE;
			}
			this.types.add(type.name,type);
			this.typesToStartup.add(type.name,type);
			//call lifecycle listeners
			this.emit("registered", {sender:this,type:type});
			type.typeRegistry=this;
			//remove from list of currently loading
			this.registering.remove(type.name);
			if (deferStartup!=true) {
				this.triggerStartup(type);
			}
			
		},
		findType : function(typeName) {
			//this.startup(); //caused problems
			if (typeName==undefined||typeName==null||typeName.length==0) {
				throw 'cannot find type with an empty name'+typeName;
			}
			if (typeName.indexOf('*')>=0) {
				throw 'cannot find generic types '+typeName;
			}
			// finds the type with the given name
			// if it does not exist, tries to load and import it
			var type=this.types.item(typeName);
			if (type!=null) {
				//found it in the registry
				return type;
			}			
			if (this.loader!=null) {
				//allready loading the type, may be we are in a loop
//				if (this.loading.containsKey(typeName)) {
//					throw 'cannot load '+typeName+' . Tried, but run into a cycle '+this.loading.toString() ;
//				}
				//try to load it
				this.loading.add(typeName,typeName);
				var jsonType=this.loader.load(typeName);
				//remove from list of currently loading
				this.loading.remove(typeName);
				if (jsonType===undefined||jsonType===null) {
					throw 'cannot load '+typeName+' ';
				}
				type=this.importer.importType(jsonType);
				if (jsonType.superType!=null) {
					var superType=this.findType(jsonType.superType);
					//replace the name of the superType with the regarding type
					//cant do this on the JsonType, because that will be cloned,
					//which fails if its an complex object
					type.superType=superType;
				}
				this.registerType(type,true);
				//no that the type is in the cache,create the attributes
				this.importer.createAttributes(type,jsonType);
				console.log(jsonType,this.registering.getValueList());
				//startup now, after all attributes are created
				this.triggerStartup(type);
			}
			if (type==undefined) {
				this.loading.remove(typeName);
				throw 'could not find type '+typeName;				
			}
			return type;
		},
		triggerStartup : function(type) {
			type.startup();
		},
		startup : function() {
			var that=this;
			//can be called many times, allows late registration
//			array.forEach(this.typesToStartup.getValueList(),function(type){
//				if (typeof type.startup=='function') {
//					type.startup();
//				}
//				that.typesToStartup.remove(type.name);
//			});
			return this;
		}
		});
		;});
