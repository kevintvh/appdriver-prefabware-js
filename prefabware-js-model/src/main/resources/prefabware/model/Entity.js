//definition of an Entity
define('prefabware/model/Entity', [ 'dojo', 'prefabware/model/Composite',
		'prefabware/model/Type','dojo/_base/lang',
		'prefabware/model/EntityId',
		'prefabware/model/ReferenceAttribute',
		'dojo/_base/declare' ], function(dojo,Composite, Type,lang) {
	dojo.declare("prefabware.model.Entity", [prefabware.model.Composite], {
		//has to be prefixed, because the property pfw_id does also exist		
		version:null,
		__qid:null,
		__isEntity:true,
		constructor : function(type,id,version) {
			//__type is allready set by superclass
			this.pfw_id=id;
			this.version=version;
		},		
		__isNew : function() {
			return this.pfw_id==null||this.pfw_id<this.__type.__MIN_PERSISTED_REMOTE_ID;
		},
		__isPersistedLocal : function() {
			//true= this is persited to a local store
			return this.pfw_id>=this.__type.__MIN_PERSISTED_LOCAL_ID;
		},
		__equals : function(other) {
			//returns true, if this composite equals the other composite
			if (other==null||other==undefined) {
				return false;
			};
			if (this.__type!=other.__type) {
				return false;
			}
			//they are equal, if the id is equal
			return this.pfw_id==other.pfw_id;
		},
		__removeNewId : function() {
			// removes the id from the entity, if the entity is new
			// this is necessary to save the entity on the server
			if (this.__isNew()) {
				this.pfw_id = undefined;
			}
			return this;
		},
		__assignId : function() {
			// assigns a id>0 to this entity, if the current id<0
			// local stores use this when saving entities the first time
			if (this.__isNew()) {
				this.pfw_id = this.__type.__nextId();
			}
			return this;
		},
	});
	
});
