//a numeric type , will be right adjusted
define('prefabware/model/NumberType', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",'dojo/number','prefabware/model/Type','prefabware/lang','prefabware/model/Attribute',
		  'dojo/_base/declare' ], function(dojo,lang,locale,number,Type) {
	dojo.declare('prefabware.model.NumberType', [prefabware.model.Type], {
		length:null,		
		STYLE:{'text-align':'right'},
		constructor : function(name) {
		},
		create : function() {
			return 0;
		},
	style : function() {
		//returns a style that should be applied for values of this type
		return this.STYLE;
	},
	
	stringOf : function(value) {
		return value+'';
	},
	convertFrom : function(rawValue) {
		if (this.isValue(rawValue)) {
			return rawValue;
		}
		//if its a string, its expected to be in the locale format
		//data from the server comes as number
		var num = number.parse(rawValue, {
			locale: prefabware.lang.locale(),
		  });
		return num;
	},
	isValue : function(value) {
		//return true, if the value is a number
		//dojo.lang.numeric returns true also for strings that can be converted to a number
		//but here for values of strings false must be returned
		//lang.isNumeric does not exist anymore !
		//return !lang.isString(value)&&lang.isNumeric(value);
		return !isNaN(parseFloat(value)) && isFinite(value);;
	},
	});
});
