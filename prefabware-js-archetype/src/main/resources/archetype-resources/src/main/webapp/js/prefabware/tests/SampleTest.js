// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.tests.SampleTest");
//require other classes

dojo.require("dojo");
dojo.require("doh");
dojo.require("prefabware.Sample");
doh.register("prefabware.tests.SampleTest", [ {
	name : "sample test",
	sample:null,
	setUp : function() {
	this.sample=new prefabware.Sample();
	},
	runTest : function() {
		
		var artifactId=this.sample.getArtifactId('${artifactId}');
		doh.assertTrue(artifactId=='${artifactId}');
		
	},
	tearDown : function() {
	}
}
]);