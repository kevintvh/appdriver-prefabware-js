//a sample javascript class using dojo
define('prefabware/Sample', [ 'dojo', 
		'dojo/_base/array', 'dojo/_base/lang',		 
		'dojo/_base/declare'
		], function(dojo, array, lang) {
	dojo.declare("prefabware.Sample",  null, {
		
		getArtifactId : function(arg) {		
				return '${artifactId.toUpperCase()}';
				},
			});
});
