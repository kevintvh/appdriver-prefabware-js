//TODO this class should extend ui.ExtensionWidget
define('prefabware/plugins/prefabware/bootstrap/ExtensionHomepage', [ 
       'dojo',  
       'prefabware/plugin/Extension',
       'prefabware/lang',
       'dojo/_base/declare' ], function(){
		dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionHomepage", [prefabware.plugin.Extension], {
			widgetClass:null,//the widtget that shows the homepage
			constructor : function(options) {
				this.widgetClass=options.json.widgetClass;
			},
			getHomepageWidgetClass : function() {
				//async
				//return this.loader.createInstance(this.widgetClass);
				return this.widgetClass;
			},
		});
});
