//to create a table widget for an attribute
//to create a table for a list of entities from a store, use the ExtensionUnboundWidgetTable
define('prefabware/plugins/prefabware/bootstrap/ExtensionWidgetTable', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/dom-construct',
       'prefabware/plugins/prefabware/bootstrap/ExtensionWidget',
       'dojo/_base/declare' ], function(
		dojo, array,domConstruct) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionWidgetTable", [prefabware.plugins.prefabware.bootstrap.ExtensionWidget], {
		storePlugin:{inject:'prefabware.store',type:'plugin'},  
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},  
		
		createWidget : function(qattribute,params, srcNode) {
			var embedded=params.outer;
			var that=this;
			return that.storePlugin.createMemoryStore(qattribute.attribute.type).then(function(store){
				return that.bootstrapPlugin.fetchController().then(function(controller){
					return controller.createTable(qattribute.attribute.type,{store:store,embedded:embedded});
				});
			});
		},
	});
})
