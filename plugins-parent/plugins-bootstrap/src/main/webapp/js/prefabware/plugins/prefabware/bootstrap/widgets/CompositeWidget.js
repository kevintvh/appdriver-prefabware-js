//to present a Composite
//CompositeWidget is meant to be embedded inside an editor widget
define('prefabware/plugins/prefabware/bootstrap/widgets/CompositeWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-style',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/CompositeWidget.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_FeedbackWidget',
       'prefabware/plugins/prefabware/bootstrap/widgets/_EditorWidget',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer',
       'prefabware/plugins/prefabware/bootstrap/widgets/FormGroup',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/util/Parameters',
       'prefabware/plugins/prefabware/bootstrap/widgets/Label',
       'dojo/_base/declare' ], function(
		dojo, array,lang,parser,domConstruct,domClass,domStyle,template) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.CompositeWidget", [prefabware.plugins.prefabware.bootstrap.widgets._FeedbackWidget,dijit._Widget,prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer, prefabware.plugins.prefabware.bootstrap.widgets._EditorWidget,dijit._Widget, dijit._Container,dijit._Templated], {
		templateString:null,
		type:null,
		model:null,
		controller:null,
		label:null,
		qattribute:null,
		attribute:null,
		disabled:false,
		constructor : function(params,parent) {
		if (params.qattribute==undefined) {
			throw 'params.qattribute must be set';
		}
		this.attribute=params.qattribute.attribute;
		this.templateString= template;
		return;
		},
		startup : function() {
			var that=this;				
			this.inherited(arguments);	
			this.disable(this.attribute.isReadOnly);
			return;
		},
		disable : function(disabled) {
			dojo.query(this.containerNode).attr("disabled",disabled);
			array.forEach(this.getChildren(),function(child){
				if (typeof child.disable=='function') {
					child.disable(disabled);
				}
			});
			this.disabled=disabled;
			return;
		},
		buildRendering : function() {
			this.inherited(arguments);//at first create this dom, than append the widgets
			//places the widgets into the template
			var that=this;
			var createLabels=this.bindings.count>2;//additional label for every attribute only if more than 2
			var percentalWidth=100/this.bindings().length;
			array.forEach(this.bindings(),function(binding,i){
				var group=new prefabware.plugins.prefabware.bootstrap.widgets.FormGroup();
				var label=new prefabware.plugins.prefabware.bootstrap.widgets.Label({label:binding.widget.label});
					group.addChild(label);
				if (createLabels==false||binding.widget.createLabel==false) {
					//do not show the label
					domClass.add(label.domNode,"sr-only");
				}
				group.addChild(binding.widget);
				if (i>0) {
					//prepend a space for every except the first attribute
					domConstruct.place("<span> </span>", that.containerNode, 'last');
				}
				that.addChild(group);
				
				//validation feedback of the child widgets of this composite must be decorated on the FormGroup
				//of this CompositeWidget, otherwise ir destroys the layout.
				//thats a bit tricky to do :
				//The parent editor will later exchange this.setValidationMessage so it will decorate feedback on the
				//FormGroup of this CompositeWidget. Thats why we wrap the call in function 'f' here, so we can call
				//that exchanged setValidationMessage, and not the original one from inside f.
				//function f is called whenever setValidationMessage of one of the child-widgets is called.
				//We dont want to override a valdiation message from a child widget with 'none' from a later one.
				//So a message of type 'none' is only forwarded with the first child, which will then clear the feedback 
				//'none' of all other child widgets will be ignored
				//f needs access to the child widgets index, 
				//which is done using hitch when assigning binding.widget.setValidationMessage
				//binding.widget.setValidationMessage=lang.hitch(that,f, i);
				var f=function(i,message){					
					if (message.messageType=='none') {
						if (i===0) {
							//set none=clear feedback only for the first binding
							//or if i was not provided, e.g. when called from outside
							that.setValidationMessage(message);
						}
					}else{
						that.setValidationMessage(message);
					}
				};
				binding.widget.setValidationMessage=lang.hitch(that,f, i);
			});
			return;
		},
		
	});
});
