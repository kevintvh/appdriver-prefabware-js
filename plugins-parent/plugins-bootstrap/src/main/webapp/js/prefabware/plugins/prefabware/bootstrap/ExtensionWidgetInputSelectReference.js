//to register widgets for types
define('prefabware/plugins/prefabware/bootstrap/ExtensionWidgetInputSelectReference', [ 
       'dojo',  
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/bootstrap/ExtensionWidgetInput',
       'dojo/_base/array',
       'prefabware/app/WidgetBinding',
       'prefabware/app/WidgetBindingNumber',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionWidgetInputSelectReference", [prefabware.plugins.prefabware.bootstrap.ExtensionWidget], {
		storePlugin:{inject:'prefabware.store',type:'plugin'},  
		createBinding : function(qattribute,widget) {
			var that=this;
			return this.storePlugin.createStore({type:qattribute.attribute.type,cache:true}).then(function(store){
				//the widget my need the store, also
				widget.store=store;
				return that.fetchBindingClass().then(function(clazz){
					var binding=new clazz(qattribute,widget,store);
					return binding;
				});;
			});;
		},
	
	});
});
