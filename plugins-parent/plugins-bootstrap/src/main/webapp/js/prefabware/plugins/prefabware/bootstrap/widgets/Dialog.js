//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Dialog', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'prefabware/plugins/prefabware/bootstrap/widgets/TextWidget',
       'prefabware/plugins/prefabware/bootstrap/widgets/Modal',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Dialog", [prefabware.plugins.prefabware.bootstrap.widgets.Modal], {
		constructor : function(params) {	
			this.label=params.title;
		},
		postCreate : function() {
			this.inherited(arguments);
			this.addChild(new prefabware.plugins.prefabware.bootstrap.widgets.TextWidget({text:this.text}));
		},		
		startup : function() {
			this.inherited(arguments);
			//this.addChild(new prefabware.plugins.prefabware.bootstrap.widgets.TextWidget({text:this.text}));
		},		
	});
});
