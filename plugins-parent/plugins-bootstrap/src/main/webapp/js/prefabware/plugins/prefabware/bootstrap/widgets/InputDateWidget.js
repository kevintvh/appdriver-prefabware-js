//a input field for a date
//using http://www.eyecon.ro/bootstrap-datepicker/ plugin
//in contrast to other widgets set/get value here uses date objects and not string
//
define('prefabware/plugins/prefabware/bootstrap/widgets/InputDateWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/InputDateWidget.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,template) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.InputDateWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		templateString: template,
		icon:null,
		date_format:"dd.mm.yyyy",
		datepicker:null,
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}			
		},	
		_setValueAttr : function(date) {
			//expects a date object
			//set the value of the input
			this._set('value',date);
			//and set the value of the datepicker
			$(this.datepicker).datepicker('setValue', date);
		},	
		_getValueAttr : function() {
			//returns the value of the input
			//which is a string
			//the datepicker sets the value of the input
			//manually edited dates change also the input.value
			return $(this.datepicker).data('datepicker').date;
			//return this.inherited(arguments);
			
			//return $(this.datepicker).data('datepicker').date;
		},
		startup : function() {
			this.inherited(arguments);	
			var widget=this;
			var input=widget.attachPoint_input;
			this.datepicker=$(widget.domNode).datepicker();			
			this.datepicker.on('changeDate', function(ev){
			    //ev.date.valueOf()	
				$(widget.datepicker).datepicker('hide');
			  });
			dojo.connect(input,"blur",function(event){
				//stupid datepicker does not recognize manuell editing of the input field !
				//set the value from the input into the datepicker
				$(widget.datepicker).datepicker('setValue', input.value);
			});
			//$(widget.datepicker).datepicker('show');
			
		},	
		
	});
});
