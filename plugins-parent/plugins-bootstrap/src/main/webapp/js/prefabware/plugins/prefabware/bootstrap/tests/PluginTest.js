dojo.provide("prefabware.plugins.prefabware.bootstrap.tests.PluginTest");
dojo.require("prefabware.plugins.prefabware.bootstrap.Bootstrap");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.bootstrap.tests.PluginTest", [ {
	name : "bootstrap-plugin",

	setUp : function() {
		 var root=dojo.byId("logBody").parentNode;
		 var menuNode=dojo.create("div",null,root);
		 dojo.attr(menuNode, 'id', 'appdriver-menu'); // set
		 var centerNode=dojo.create("div",null,root);
		 dojo.attr(centerNode, 'id', 'appdriver-center'); // set
	},

	runTest : function() {
		 var pluginRegistry=new prefabware.plugin.PluginRegistry({namespace:'prefabware.plugins.prefabware.bootstrap.tests',
				fileName:'pluginTest-registry.json'});
		var workplace = new prefabware.plugins.prefabware.bootstrap.Bootstrap({pluginRegistry:pluginRegistry});
		workplace.startup();

		return;
	},
	tearDown : function() {
	}
} ]);