//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Documentation', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Documentation.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,template) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Documentation", [dijit._Widget, dijit._Templated ], {
		templateString: template,
		constructor : function() {			
		},
	});
});
