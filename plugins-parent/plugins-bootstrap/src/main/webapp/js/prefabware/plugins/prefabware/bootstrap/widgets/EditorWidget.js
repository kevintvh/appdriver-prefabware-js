//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/EditorWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-style',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/EditorWidget.html',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/EditorWidgetEmbedded.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_EditorWidget',
       'dijit/_Templated',
       'prefabware/plugins/prefabware/bootstrap/widgets/FormGroup',
       'prefabware/plugins/prefabware/bootstrap/widgets/Label',
       'dojo/_base/declare' ], function(
		dojo, array,lang,parser,domConstruct,domClass,domStyle,templateEditor,templateEditorEmbedded) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.EditorWidget", [dijit._Widget, dijit._Container,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._EditorWidget], {
		templateString:null,
		constructor : function(params,parent) {
		if (params.outer!=null) {
			this.embedded=true;
			this.templateString= templateEditorEmbedded;
		}else{
			this.templateString= templateEditor;
		}
		return;
		},
		buildRendering : function() {
			this.inherited(arguments);
			//places the widgets into the template
			//is called on startup
			//subclasses may override this
			var that=this;
			if (that.embedded) {
				domClass.remove(that.domNode,'input');
				//domClass.add(that.domNode,'input-append');
			};
			var percentalWidth=100/this.bindings().length;
			array.forEach(this.bindings(),function(binding,i){
				var group=new prefabware.plugins.prefabware.bootstrap.widgets.FormGroup();
				var label=new prefabware.plugins.prefabware.bootstrap.widgets.Label({label:binding.widget.label});
				group.addChild(label);
				if (binding.widget.createLabel==false) {
					//do not show the label
					domClass.add(label.domNode,"sr-only");
				}
				group.addChild(binding.widget);
				that.addChild(group);

				//use FormGroup for validation-feedback not the Input
				binding.widget.setValidationMessage=function(message){
					if (message.messageType=='none') {
						group.clearFeedback();
					}else{
						group.setFeedbackMessage(message);
					}
				};
				
			});
			return;
		},
	});
});
