//binding for select2
define('prefabware/plugins/prefabware/bootstrap/WidgetBindingSelectReference2', [
	'dojo',
    'dojo/aspect',
    'dojo/_base/array',
	'prefabware/app/WidgetBinding',
	'dojo/_base/declare' ], function(dojo, aspect,array,WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingSelectReference2", [prefabware.app.WidgetBinding], {
		constructor : function(attribute,widget,store) {
			//this binding needs no store, the store is completely managed by the widget
//			aspect.after(widget,"onLoadOptions",function(){
//				return fquery('').then(function(result){
//					widget.__select(widget.selected);
//				});				
//			},true);
		},
		setValue : function(entity) {	
			if (entity==null) {
				this.widget.setSelected(null);
			}else{
			this.widget.setSelected(entity);
			}
		},		
		getValue : function() {
			return this.widget.getSelected();			
		},		
	});
});
