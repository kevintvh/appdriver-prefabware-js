//the ui controller
//can openEditor,openView, openBox, showError,showWarning etc.
define('prefabware/plugins/prefabware/bootstrap/ControllerStrategy', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/window",
       "dojox/fx/scroll",
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, array,aspect,domConstruct,domStyle,win,fx) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ControllerStrategy", null, {

		uiStrategy:{
			controller:null,//the ExtensionController
			newRow:{
				//the position where to put a new row, first or last
				position:'first'
					},
			onRowClose : function(row){
				//call this method when a row is closed
				this.rows.remove(row);
				if (this.rows.size==0) {
					this.controller.openHomepage();
				}
			},
			beforeAddRow : function(row) {
			//closes and removes all existing rows
			array.forEach(this.rows.getValueList(),function(row){
				row.destroy();
			});
				},
			newModal:{
				//the position where to put a new modal, first or last
				position:'first'
				},
			fadeIn : function(node) {
					  //before the caller must set
				      //domStyle.set(node, "opacity", "0");
				      var fadeArgs = {
				        node: node
				      };
				      dojox.fx.fadeIn(fadeArgs).play();
			},
			scrollTo : function(node) {
				//scroll to the node
				 dojox.fx.smoothScroll({
				 node: node,
				 win: window,
				 duration: 1000 }).play();
					},
			beforeOpenView : function() {
				//before opening a new view, close all open views, so only one is open at a time
				array.forEach(this.views.getValueList(),function(view){
					view.box.destroy();
				});
				},
			addView : function(view) {
				this.views.add(view.pfw_type.name,view);
				},
		},

	});
});
