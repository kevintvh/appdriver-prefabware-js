define({
	openHomepage:"Home",
	sSearch:"",
    sLengthMenu: "_MENU_ Zeilen pro Seite",
    sZeroRecords: "Keine Daten gefunden",
    sInfo: "_START_-_END_ von _TOTAL_ Sätzen",
    sInfoEmpty: "0 Sätze",
    sInfoFiltered: "(gefiltert von _MAX_ Sätzen)",
    sFirst: "Erste",
    sLast: "Letzte",
    sNext :"Nächste",
    sPrevious  :"Vorige",
});