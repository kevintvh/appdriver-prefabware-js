//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Panel', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Panel.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo,array,template) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Panel", [dijit._Widget, dijit._Container, dijit._Templated], {
		templateString: template,
		constructor : function(params,parent,label,iconClass) {			
		},
		//the panel must NOT be an ActionContainer, otherwise the ExtensionController will add actions to the panel instead 
		//of the editor !
	});
});
