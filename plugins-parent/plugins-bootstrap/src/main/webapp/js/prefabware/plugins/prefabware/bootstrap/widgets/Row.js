//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Row', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Row.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Row", [dijit._Widget, dijit._Container, dijit._Templated ], {
		templateString: html,
		constructor : function(params) {			
		},
		startup : function() {
			this.inherited(arguments);
			parser.parse(this.srcNodeRef); 
		},
	});
});
