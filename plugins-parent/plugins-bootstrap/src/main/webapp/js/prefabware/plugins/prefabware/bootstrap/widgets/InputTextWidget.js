//a view that can be shown in the workspace
//for drag and drop tried
//<div class="control-group dojoDndItem" data-dojo-type="dojo/dnd/Moveable">
//could drag but not drop
define('prefabware/plugins/prefabware/bootstrap/widgets/InputTextWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domStyle) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.InputTextWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		templateString: dojo.cache('prefabware.plugins.prefabware.bootstrap.widgets', 'InputTextWidget.html'),
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
		},	
		decorateFeedback : function(message) {
			//adds the feedback from the widget
			//you may access store nodes etc. in this.__feedback for easy access in clearFeedback
		},
	});
});
