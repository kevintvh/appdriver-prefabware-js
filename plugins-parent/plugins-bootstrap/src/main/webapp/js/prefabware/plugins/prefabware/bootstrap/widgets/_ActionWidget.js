//base class for widgets that is based on an Action and has a click event
//subclasses must provide intheir html :
//data-dojo-attach-event="click:_onClick"
define('prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/_base/event',
       'dijit/_Widget',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,event) {
	//do not extend _Widget here, this must be done by the subclass to keep the order !
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget",null , {
		icon:"",//icon 
		label:"", 
		fragment:"#",//the fragment of the link 
		action:null,//
		preventDefault:false,
		constructor : function(params) {			
		},
		buildRendering : function() {			
			if (this.action!=null&&(typeof this.action.fragment=="function")) {
				this.fragment="#"+this.action.fragment();
			}
			return this.inherited(arguments);
		},
		startup : function() {
			this.inherited(arguments);	
			//mark the root of this widget with the actionId
			if (this.action!=null) {
				dojo.attr(this.domNode,"data-pfw-action",this.action.actionId);
			};
			return;
		},
		_onClick: function(e){
			//call this method from the html
			//using data-dojo-attach-event="click:_onClick"
			if (this.preventDefault) {
				e.preventDefault();
			}
			return this.onClick(e);
		}, 
		onClick: function(e){ 
			//event.stop(e); stopping the event may cause problems, e.g. bootstrap menus do not close after click
			if (this.action!=null) {
				this.action.execute(e);
			}
		} 
	});
});
