//to show notifications with gritter
//deprecated, cannot set the color easily, does not look great
//use bootstrpa-notify instead
define('prefabware/plugins/prefabware/bootstrap/widgets/Gritter', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'prefabware/plugins/prefabware/bootstrap/widgets/Gritter.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Gritter", [dijit._Widget, dijit._Templated ], {
		templateString: html,
		title:null,
		text:null,
		class_name:null,
		constructor : function(params) {			
		},
		startup : function() {
			$.gritter.add({
				// (string | mandatory) the heading of the notification
				title: this.title,
				// (string | mandatory) the text inside the notification
				text: this.text,
				sticky: this.sticky,
				class_name:this.class_name
			});
		},
	});
	__static.removeAll=function(){
		$.gritter.removeAll();
	};
});
