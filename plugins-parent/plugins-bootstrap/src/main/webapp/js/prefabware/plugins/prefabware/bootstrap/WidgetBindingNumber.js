define('prefabware/plugins/prefabware/bootstrap/WidgetBindingNumber', [ 
       'dojo',
        
       'dojo/number',
       'prefabware/app/WidgetBinding',
       'dojo/_base/declare' ], function(dojo, number,WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingNumber", [prefabware.app.WidgetBinding], {

		setValue : function(value) {	
			//value is an instanceof numeric
			//returns a String, localized with the current locale
			var format = number.format(value, {
				//locale: 'en-us',//nummeric values always arive here allways in american format
			  });
			
			this.widget.set("value", format);
		},		
		getValue : function() {
			//the widget returns a string with the localized represnetation of the number
			var localized=this.widget.get("value");	
			var num = number.parse(localized, {
				//locale: 'en-us',//nummeric values always arive here allways in american format
			  });
			
			return num;
		},		
	});
});