//to register widgets for types
define('prefabware/plugins/prefabware/bootstrap/ExtensionUnboundWidget', [ 
       'dojo',  
       'prefabware/plugins/prefabware/ui/ExtensionWidget',
       'dojo/_base/array',
       'prefabware/lang',
       'prefabware/app/WidgetBinding',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
		dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionUnboundWidget", [prefabware.plugins.prefabware.ui.ExtensionWidgetUnbound], {
			createLabel:true,
			constructor : function(options) {
			},
		});
});
