define({
  root: {
	  openHomepage:"Home",
	  openApi:"REST API",
	  //texts for datatables
	  search:"Search",//this our placeholder
	  sSearch:"",//we use a placeholder, so the label should be empty
      sLengthMenu: "Display _MENU_ records per page",
      sZeroRecords: "Nothing found - sorry",
      sInfo: "Showing _START_ to _END_ of _TOTAL_ records",
      sInfoEmpty: "Showing 0 to 0 of 0 records",
      sInfoFiltered: "(filtered from _MAX_ total records)",
      sFirst: "First",
      sLast: "Last",
      sNext :"Next",
      sPrevious  :"Previous",
  },
  de: true,
  es: true,
});