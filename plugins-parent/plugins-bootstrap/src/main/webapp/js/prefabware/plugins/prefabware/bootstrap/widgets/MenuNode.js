//looks like MenuItem but MUST NOT extend _ActionWidget !!
define('prefabware/plugins/prefabware/bootstrap/widgets/MenuNode', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/MenuNode.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dojo/_base/declare' ], function(
		dojo, array,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.MenuNode", [dijit._Widget, dijit._Container,dijit._Templated, prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: html,
		constructor : function(params) {		
			this.preventDefault=true;
		},
		onClick: function(e){ 
            /* do nothing, DO NOT stop the event ! */ 
			//event.stop(e);
			return;
		}, 
	});
});
