//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/ActionButton', [ 
       'dojo',  	
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/ActionButton.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,actionButtonHtml) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.ActionButton",  [dijit._Widget,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: actionButtonHtml,
		icon:' icon-warning-sign',//icon 
		style:"btn-default",		
		constructor : function(params) {
		},
		buildRendering : function(params) {
			this.inherited(arguments);
			dojo.addClass(this.attachPoint_click, this.style);
			
		},
		postCreate : function(params) {
			if (this.label!=null) {
				this.label="  "+this.label;
			}
		},
		loading : function() {
			$(this.attachPoint_click).button("loading");
		},
		notLoading : function() {
			$(this.attachPoint_click).button("reset");
		},
	});
});
