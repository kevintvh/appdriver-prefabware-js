//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/tests/AmountWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/parser',
       'dojo/dom-class',
       'prefabware/plugins/prefabware/bootstrap/widgets/EditorWidget',
       //'dojo/dnd/Source',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/util/Parameters',
       'dojo/_base/declare' ], function(
		dojo, array,lang,parser,domClass) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.tests.AmountWidget", [prefabware.plugins.prefabware.bootstrap.widgets.CompositeWidget], {
		__placeWidgets : function() {
			//places the widgets into the template
			//is called on startup
			var that=this;
			domClass.remove(that.domNode,'input');
			domClass.add(that.domNode,'input-append');
			
			array.forEach(this.bindings(),function(binding){
				if (binding.attribute.name=='value') {
					binding.widget.placeAt(that.domNode,'last');
				}else if (binding.attribute.name=='currency') {
					binding.widget.placeAt(that.domNode,'last');		
				}
			});
			return;
		},

	});
});
