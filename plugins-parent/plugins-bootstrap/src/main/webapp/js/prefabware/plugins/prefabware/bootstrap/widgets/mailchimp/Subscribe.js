//the Subscribepage
//TODO add error handling, add error marker
define('prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/Subscribe', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/Subscribe.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/ActionButton',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,htmlTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.Subscribe", [dijit._Widget, dijit._Templated], {
		templateString: htmlTemplate,
		uiPlugin:{inject:'prefabware.ui',type:'plugin'},
		actionPlugin:{inject:'prefabware.action',type:'plugin'},
		constructor : function(params) {	
			this.label="Newsletter bestellen";
			this.icon="";
		},
		buildRendering : function() {
			this.inherited(arguments);
//			var btn=new prefabware.plugins.prefabware.bootstrap.widgets.ActionButton({label:"Newsletter abbonieren"});
//			btn.placeAt(this.attachPoint_button);
//			
		},
		startup : function() {
			this.inherited(arguments);
			var that=this;
			this.actionPlugin.fetchExtensionById("prefabware.mailchimp.action.subscribe")
				.then(function(extension){
					that.action=extension.createAction();
				return;
			});
		},
		_onClick: function(e){ 
			return this.onClick(e);
		},
		disable : function(disabled) {
			dojo.query(this.attachPoint_input).attr("disabled",disabled);
			dojo.query(this.attachPoint_click).attr("disabled",disabled);
			this.disabled=disabled;
			return;
		},
		onClick: function(e){ 
			var that=this;
			if (this.action!=null) {
				that.disable(true);
				var email=this.attachPoint_input.value;
				this.action.execute({email:email}).then(function(result){
					if (result="subscribed") {
						that.uiPlugin.notify({title:'Newsletter abonniert.',text:email+' wurde als Newsletter-Empfänger registriert. ',msgType:'success'});
						that.attachPoint_input.value="";
						that.disable(false);
					}else{
						that.uiPlugin.notify({title:'Fehler',text:email+' konnte nicht als Newsletter-Empfänger registriert werden. ',msgType:'error'});
						that.disable(false);
					}
				},function(err){
						that.uiPlugin.notify({title:'Fehler',text:email+' konnte nicht als Newsletter-Empfänger registriert werden. '+err,msgType:'error'});
						that.disable(false);
				});
				
			}
		} 
	});
});
