dojo.provide("prefabware.plugins.prefabware.bootstrap.tests.menu.MenuTest");
dojo.require("prefabware.plugins.prefabware.bootstrap.Bootstrap");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo._base.lang");
dojo.require("dojo.Deferred");
dojo.require("dojo.io.script");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.bootstrap.tests.menu.MenuTest", [ {
	name : "bootstrap-plugin",
	setupDeferred:null,
	def :null,
	setUp : function() {
		 this.setupDeferred=new dojo.Deferred();
		 var that=this;
		 var root=dojo.byId("logBody").parentNode;
		 var menuNode=dojo.create("div",null,root);
		 dojo.attr(menuNode, 'id', 'appdriver-menu'); // set
		 var sidebarNode=dojo.create("div",null,root);
		 dojo.attr(sidebarNode, 'class', 'appdriver-sidebar'); // set
		 var centerNode=dojo.create("div",null,root);
		 dojo.attr(centerNode, 'id', 'appdriver-center'); // set
		 dojo.io.script.get({ url:"../../jquery/jquery.js"})
		 	.then(function(){
		 		return dojo.io.script.get({ url:"../../bootstrap/js/bootstrap.js"});
		 	})
		 	.then(function(){
		 		return dojo.io.script.get({ url:"../../bootstrap-modal/js/bootstrap-modalmanager.js"});
		 	})
		    .then(function(){
		    	return dojo.io.script.get({ url:"../../bootstrap-modal/js/bootstrap-modal.js"});
		    })
	    	.then(function(){
	    		return dojo.io.script.get({ url:"../../datatables/media/js/jquery.dataTables.js"});
	    	})
    		.then(function(){
	    		return dojo.io.script.get({ url:"../../datatables/paging.js"});
    		})
    		.then(function(){
    			return dojo.io.script.get({ url:"../../ivaynberg/select2/select2.js"});
    		})
		    .then(function(){that.setupDeferred.resolve()}); 
	},

	runTest : function() {
			var that=this;
			that.setupDeferred.then(function(){
			that.def = new doh.Deferred();
			that.doRunTest();	
			});
			return this.def;
	},
	
	doRunTest : function() {
		var that=this;
		 var pluginRegistry=new prefabware.plugin.PluginRegistry({namespace:'prefabware.plugins.prefabware.bootstrap.tests.menu',
				fileName:'menuTest-registry.json'});
		pluginRegistry.startup().then(function(registry){
			var uiPlugin=registry.findPlugin('prefabware.ui');
			uiPlugin.createMenus().then(function(menus){
				doh.assertFalse(menus==null);
				doh.assertEqual(4,menus.length);
				
				var menu=menus[0];
				var sidebar=menus[1];
				
				var ar=menu.getValueList();
				doh.assertEqual(3,ar.length);
				var mr0=ar[0];
				var mr1=ar[1];
				var mr2=ar[2];
				
				mr0.proxy.doResolve()
				.then(function(item0){
					doh.assertEqual('prefabware.plugins.prefabware.bootstrap.widgets.MenuNode',item0.declaredClass);
				})
				.then(dojo._base.lang.hitch(mr1.proxy,mr1.proxy.doResolve))
				.then(function(item1){
					doh.assertEqual('prefabware.plugins.prefabware.bootstrap.widgets.MenuSubItem',item1.declaredClass);
				})
				.then(dojo._base.lang.hitch(mr2.proxy,mr2.proxy.doResolve))
				.then(function(item2){
					doh.assertEqual('prefabware.plugins.prefabware.bootstrap.widgets.MenuSubItem',mr2.item.declaredClass);
					that.def.callback(true);
				})
				
			});
		});

		
	},
	tearDown : function() {
	}
} ]);