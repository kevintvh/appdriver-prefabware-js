//a bootstrap checkbox
define('prefabware/plugins/prefabware/bootstrap/widgets/Checkbox', [ 
       'dojo',  
       'dojo/dom-attr',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Checkbox.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domAttr,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Checkbox", [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		templateString: html,
		constructor : function(params) {			
		},	
		startup : function() {
			this.inherited(arguments);	
			//initially let uniform decorate it, so we cann just update it later
			//$(this.attachPoint_input).uniform();			
		},
		_setValueAttr : function(string) {
			//expects a string 'true' or 'false' 
			//because the binding sets value as a string
			if (string=='true') {
				domAttr.set(this.attachPoint_input, 'checked', 'checked');
			}else{
				domAttr.remove(this.attachPoint_input, 'checked');
			}
			//$.uniform.update(this.attachPoint_input);
		},	
		_getValueAttr : function() {
			return this.attachPoint_input.checked;
		},
	});
});
