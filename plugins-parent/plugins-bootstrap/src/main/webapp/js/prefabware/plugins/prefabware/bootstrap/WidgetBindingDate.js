define('prefabware/plugins/prefabware/bootstrap/WidgetBindingDate', [ 'dojo',   'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo, WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingDate", [prefabware.app.WidgetBinding], {

		setValue : function(value) {	
			//value is an instanceof date
			//an input of type date needs a String of format 'jjjj-mm-dd'
			//so wa cannot use attribute.labelOf which returns the date formatted according to the locale
//			var string;
//			if (value==null) {
//				string="";
//			}else{
//				string=dojo.date.locale.format(value, {datePattern: "dd.MM.yyyy", selector: "date"});
//			}
//			this.widget.set("value", string);
			this.widget.set("value", value);
		},		
		getValue : function() {
			//the widget returns a string in format 'dd.mm.jjjj'
			return this.widget.get("value");
//			var string=this.widget.get("value");
//			if (string==null||string.trim().length==0) {
//				return null;
//			}
//			var array = string.split(".");
//			var jjjj_mm_dd=array[2]+'-'+array[1]+'-'+array[0];
//			return this.attribute.convertFrom(jjjj_mm_dd);
		},		
	});
});