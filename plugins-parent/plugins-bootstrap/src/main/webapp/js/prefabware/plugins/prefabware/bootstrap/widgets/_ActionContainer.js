//base class for widgets with a click event
//the widget must declare an attach point attachPoint_click in its template or html
//like :
//data-dojo-attach-point="attachPoint_click"
define('prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/promise/all',
       "dojo/_base/lang",
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,promiseAll,lang) {
	//do NOT extend e.g. _WidgetBase here, this must be dune by the subclass itself
	//otherwise the inheritancechain may have the wrong order
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer", null, {
		actions:null,
		actionButtons:null,
		actionLinks:null,
		actionOptions:null,
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'}, 
		constructor : function(options) {
			this.actionButtons=new dojox.collections.Dictionary();
			this.actionLinks=new dojox.collections.Dictionary();
			this.actions=new dojox.collections.Dictionary();
			if (options.actionOptions==null) {
				this.actionOptions={};
			}
		},
		startup : function() {
			this.inherited(arguments);
			var that=this;
			var aps=array.map(this.actions.getValueList(),function(action){
				return that._createActionWidget(action);
			},this);
			return promiseAll(aps).then(function(actionWidgets){
				array.forEach(actionWidgets,function(actionWidget){
					that._placeActionWidget(actionWidget);
					actionWidget.startup();
				},that);
			}).then(function(){
				array.forEach(that.actionButtons.getValueList(),function(actionWidget){
					that._placeActionWidget(actionWidget);
					actionWidget.startup();
				},that);
			});
		},
		
		_placeActionWidget : function(actionWidget) {
			//places the given action widget in the domnode of this widget
			//subclasses can overwrite this method
			//prepends a span as a seperator between widgets
			//TODO do it as an Box, prepend a span to seperate 
			//domConstruct.place("<span> </span>", this.attachPointActionLink, 'last');
			domConstruct.place(actionWidget.domNode, this.attachPointActionLink, 'last');
		},
		addAction : function(action) {
			//adds an action to the container
			//creates a button with the style, size etc of the container for the action and adds it
			this.actions.add(action.actionId,action);
		},
		_prepareActionWidgetOptions : function(action) {
			//to prepare the options used to create the action widget
			//subclasses can overwrite this mmethod to manipulate the options
			var options={label:action.label,icon:action.icon,action:action,tooltip:action.label};
			//overwrite the default options with actionOptions specified for this container
			lang.mixin(options, this.actionOptions);
			return options;
		},
		_createActionWidget : function(action) {
			//async
			var options=this._prepareActionWidgetOptions(action);			
			return this.bootstrapPlugin.fetchActionButtonWidget(options);
		},
		addActionButton : function(actionButton) {
			this.actionButtons.add(actionButton.action.actionId,actionButton);
		},
		getActionButton : function(actionId) {
			this.actionButtons.item(actionId);
		},
		addActionLink : function(actionLink) {
			this.actionLinks.add(actionLink.action.actionId,actionLink);
		},
		getActionButon : function(actionId) {
			this.actionLinks.item(actionId);
		},
		
	});
});
