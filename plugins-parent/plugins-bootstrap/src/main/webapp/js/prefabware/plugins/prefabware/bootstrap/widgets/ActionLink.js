define('prefabware/plugins/prefabware/bootstrap/widgets/ActionLink', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/ActionLink.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dijit/_Widget',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.ActionLink", [dijit._Widget,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: html,
	});
});
