//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/TextWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/TextWidget.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,template) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.TextWidget', [dijit._Widget, dijit._Templated ], {
		templateString: template,
		text:null,
		constructor : function(params,parent) {
			if (params.text==undefined) {
				throw 'params.label must be set';
			}			
		},	
		startup : function() {			
			this.inherited(arguments);
			//TODO
			domConstruct.place('<h4>'+this.text+'</h4>',this.attachPoint_text,'last');
		},	
		postCreate : function() {
			this.inherited(arguments);
		},	
		
	});
});
