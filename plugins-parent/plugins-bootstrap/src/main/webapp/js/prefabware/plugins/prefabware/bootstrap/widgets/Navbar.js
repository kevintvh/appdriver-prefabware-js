//a bootstrap navbar
define('prefabware/plugins/prefabware/bootstrap/widgets/Navbar', [ 
       'dojo',  
       'dojo/dom-attr',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Navbar.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domAttr,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Navbar", [dijit._Widget, dijit._Container, dijit._Templated ], {
		templateString: html,
		constructor : function(params,srcNode) {
			if (params.templateString!=null) {
				this.templateString=params.templateString;
			}
		},	
		startup : function() {
			this.inherited(arguments);			
		},
		//TODO user _SearchInput here !!
		search : function(options) {
			//clients can hook in here	
			//options.string is the string to search for
		},
		onSearchKeyDown : function(event) {
			//is bound in the html with data-dojo-attach-event
			switch(event.keyCode) {
	        case dojo.keys.UP_ARROW:	            
	            break;
	        case dojo.keys.DOWN_ARROW:	           
	            break;
	        case dojo.keys.ENTER:
	        	this.search({string:this.searchInput.value});
	        	//do not submit
	        	event.preventDefault();
	            break;
	        default:
	            //
	    }
		},
		addNode : function(options) {
			// adds the given item to the sidebar
			// options={parent:parent,name:name,extension:extension}
			var params={label:options.label,icon:options.icon,action:options.action};
			var node= new prefabware.plugins.prefabware.bootstrap.widgets.MenuNode(params);
			if (options.node!=null) {
				//the node has a parent
				prefabware.lang.throwError("subitems are not suported in Bootstrap 3");
				options.node.addChild(node);
			}else{
				this.addChild(node);
			}
			//do not startup here, because the ExtensionMenuVisitable wants to attach per aspect
			//to onClick and thats not possible after startup
			//node.startup();
			return node;
		},
		addItem : function(options) {
			// adds the given item to the sidebar
			// options={parent:parent,name:name,extension:extension}
			var that=this;
			var params={label:options.label,icon:options.icon,action:options.action};
			var item= new prefabware.plugins.prefabware.bootstrap.widgets.MenuItem(params);
			this.addChild(item);
			return item;
		},
		addSubItem : function(options) {
			// adds the given item to the sidebar
			// options={parent:parent,name:name,extension:extension}
			var that=this;
			var params={label:options.label,icon:options.icon,action:options.action};
			var subItem= new prefabware.plugins.prefabware.bootstrap.widgets.MenuSubItem(params);
			options.node.addChild(subItem);

			subItem.startup();
			return subItem;
		},
	});
});
