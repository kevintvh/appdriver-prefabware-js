define('prefabware/plugins/prefabware/bootstrap/action/ActionOpenApi', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.action.ActionOpenApi", [prefabware.plugins.prefabware.action.Action], {
		serverPlugin:{inject:'prefabware.server',type:'plugin'},
		
		startup : function() {
			//will be called after inject
			//subclasses may overwrite this method
			return true;
		},
		onExecute : function(context) {
			//async
			return this.serverPlugin.findServer("prefabware.server.api").then(function(server){
				window.open(server.getUrl()+'/', "API");
			});
		},		
			
	});
});