define('prefabware/plugins/prefabware/bootstrap/widgets/MenuSubItem', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/MenuSubItem.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/MenuItem',
       'dojo/_base/declare' ], function(
		dojo, array,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.MenuSubItem", [prefabware.plugins.prefabware.bootstrap.widgets.MenuItem], {
		templateString: html,	    
	});
});

