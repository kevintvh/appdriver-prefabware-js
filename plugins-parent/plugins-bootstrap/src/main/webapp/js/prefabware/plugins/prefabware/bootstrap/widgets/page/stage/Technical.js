//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/page/Technical', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/page/Technical.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,htmlTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.page.Technical", [dijit._Widget, dijit._Templated], {
		templateString: htmlTemplate,
		constructor : function(params) {			
		},
	});
});
