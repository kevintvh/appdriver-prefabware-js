//a widget to select an item from a list
//uses http://harvesthq.github.com/chosen/
define('prefabware/plugins/prefabware/bootstrap/widgets/InputSelectReferenceWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       "dojo/query", 
       "dojo/NodeList-data",
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,query) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.InputSelectReferenceWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], /**
	 * @author Stefan Isele
	 *
	 */
	{
		templateString: dojo.cache('prefabware.plugins.prefabware.bootstrap.widgets', 'InputSelectReferenceWidget.html'),
		store:null,
		chosen:null,//the jquery modul chosen
		selected:null,//the currently selected item
		chzn_results:null,
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
		},	
		onLoadOptions : function(term) {
			//calles can hook to this method to load the choices
			//for the given search term
			//the caller must than call addOption(entity) for each choice
			//that may happen async, than a deferred should be returned
			
		},
		addOption : function(item,label) {
			//the item of the option and the label to present to the user
			//adds the given item as an option
			var option=domConstruct.place('<option>' + label + '</option>', this.attachPoint_input, 'last');
			option.data("item", item);
		},
		//a choice is an option selected by the entered term
		onLoadChoices : function() {
			//calles can hook to this method to load the choices
			//for the given search term
			//the caller must than call addChoice(entity) for each choice
			//that may happen async
		},
		addChoice : function(label) {
			//adds the given entity as a choice
			//domConstruct.place('<li class="active-result">' + label + '</li>', this.chzn_results, 'last');
		},
		__select : function(item) {
			//selects the option with the given item
			var that=this;
			array.some(this.getOptions(),function(option,index){
				var optionItem=option.data("item");
				if (optionItem==item) {
					that.attachPoint_input.selectedIndex=index;
					return true;
				}
				return false; 
			});			
		},
		setSelected : function(item) {
			//remember the selection after relaoding the options
			this.selected=item;
			this.__select(item);
		},
		getOptions : function() {
			//returns an array with the current options of the select
			return dojo.query('option',this.attachPoint_input);
		},
		getSelected : function() {
			var options=this.getOptions();
			var index=this.attachPoint_input.selectedIndex;
			if (options.length>=index) {
				return this.getOptions()[index].date('item');
			}else{
				return null;
			}
		},
		startup : function() {
			var widget=this;
			var select=widget.attachPoint_input;
			this.chosen=$(this.attachPoint_input).chosen();
			var chzn_id=select.id+'_chzn';
			this.chzn_container=dojo.byId(chzn_id);
			dojo.attr(this.chzn_container,"tabindex",1);
			this.chzn_results=dojo.query('#'+chzn_id+' ul.chzn-results')[0];
			
		  	var chzn_input=dojo.query('#'+chzn_id+' div input');
		  	
		  	//clear the options
		  	domConstruct.empty(widget.attachPoint_input);
		  	var deferred=this.onLoadOptions();
		  	if (deferred==undefined) {
		  		widget.__select(widget.selected);
		  		widget.chosen.trigger("liszt:updated");
		  	}else{
		  		deferred.then(function(){
		  			widget.__select(widget.selected);
		  			widget.chosen.trigger("liszt:updated");
		  		});
		  	};
		  	$(chzn_input).autocomplete({
				  source: function( request, response ) {
					 //domConstruct.empty(widget.chzn_results);
					  widget.onLoadChoices(request.term).then(
							  function(loaded){
							  //response();
							  });
				  }
				});
		},
		
	});
});



