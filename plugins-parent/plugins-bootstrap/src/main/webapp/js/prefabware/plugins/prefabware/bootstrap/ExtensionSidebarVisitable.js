//to be visited by the ExtensionVisitor
//to create the SidebarNodes,-Items and -SubItems
define('prefabware/plugins/prefabware/bootstrap/ExtensionSidebarVisitable', [ 
       'dojo', 
       'prefabware/plugins/prefabware/ui/ExtensionMenuVisitable',
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/promise/all',
       'prefabware/lang',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,aspect,all) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionSidebarVisitable", [prefabware.plugins.prefabware.ui.ExtensionMenuVisitable], {
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},   	
		resourcePlugin:   {inject:'prefabware.resource',type:'plugin'},  	
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
		},	
		
		createNode : function(options){
			var that=this;
			//async
			//creates a menu node for the given element
			//options={parent:parent,name:name,extension:extension}
			//the node gets its label and icon using the name of the segment of the menu-path
			//returns a promise with the node
			return this.bootstrapPlugin.fetchSidebarWidget().then(function(sidebar){
				//nodes get no action
			return that.resourcePlugin.getLocalization(options.name,'label').then(function(label){
				return that.resourcePlugin.getLocalization(options.name,'icon').then(function(icon){
					var itemOptions={id:options.extension.nextItemId(),label:label,icon:icon};
					if (options.parent) {
						itemOptions.node=options.parent.item;
					}
					return sidebar.addNode(itemOptions).then(function(si){
						//resolve all children when the node is clicked
						//cannot use aspect here because the onClick is allready registered as dom-hook
						var aspectHandle=null;
						aspectHandle=aspect.after(si, "onClick",function(){
							var pc=array.map(options.proxy.children,function(child){
								aspectHandle.remove();
								return child.doResolve();
							});
							return all(pc);
						},true);
						si.startup();
						return si;
					});
					});
				});
			});
		},
		createSubItem : function(options){
			//async
			//creates an item in a node 
			//options={parent:parent,name:name,extension:extension}
			//returns a promise with the subItem
			var extension=options.extension;
			return this.bootstrapPlugin.fetchSidebarWidget().then(function(sidebar){
				return extension.fetchAction().then(function(action){
				return extension.fetchLabelAndItem().then(function(labelAndItem){
					var itemOptions={id:extension.nextItemId(),label:labelAndItem.label,icon:labelAndItem.icon,action:action,type:extension.getType(),node:options.parent.item};
					return sidebar.addSubItem(itemOptions);
					});
				});
				});
		},
		createItem : function(options){
			//async
			//creates an item directly in the menu, without any node inbetween
			//returns a promise subItem with the Item
			var extension=options.extension;
			return this.bootstrapPlugin.fetchSidebarWidget().then(function(sidebar){
				return extension.fetchAction().then(function(action){
				return extension.fetchLabelAndItem().then(function(labelAndItem){
					var itemOptions={id:extension.nextItemId(),label:labelAndItem.label,icon:labelAndItem.icon,action:action,type:extension.getType()};
					return sidebar.addItem(itemOptions);
					});
				});
				});
		},
	});
});
