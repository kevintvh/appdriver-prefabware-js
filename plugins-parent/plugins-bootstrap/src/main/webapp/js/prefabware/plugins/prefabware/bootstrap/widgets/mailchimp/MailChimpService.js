//to load types from a server. the loader only loads the raw json object with the type definition
//but does not convert them into a type nor register it to the typeRegistry
//load types using .load(typeName);
define('prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/MailChimpService', [
        'dojo',
        'dojo/Deferred',
        'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.MailChimpService", null, {
		url:null,
		constructor : function(url) {
			if (url==undefined||url==null) {
				throw 'parameter url is mandatory';
			}
			this.url=url;
		},
		subscribe:function(email){
			//async
			//signs up the given email adress
			//returns true if successfull
			return this.callServer(this.url+'/subscribe'+'?email='+email)
			.then(function(success){
				//do something with success
				return success;
			});
		},
		callServer:function(url){
			//async
			var deferred=new dojo.Deferred();
			
			//does really resolve calling the server
			dojo.xhrPost({
				url : url,
				sync : false ,
				handleAs : 'json',
				// Timeout in milliseconds:
				timeout : 5000,
				// Event handler on successful call:
				load :function(data) {
					return deferred.resolve(data);
				},

				// Event handler on errors:
				error : function(response, ioArgs) {
					return deferred.reject(response);
				}
			});
			return deferred.promise;
		}		
	});
});