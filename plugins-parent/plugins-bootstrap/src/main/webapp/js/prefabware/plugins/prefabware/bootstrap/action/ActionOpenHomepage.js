define('prefabware/plugins/prefabware/bootstrap/action/ActionOpenHomepage', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.action.ActionOpenHomepage", [prefabware.plugins.prefabware.action.Action], {
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},
		onExecute : function(context) {
			//async
			return this.bootstrapPlugin.fetchController().then(function(controller){
				return controller.openHomepage();
			});
		},		
			
	});
});