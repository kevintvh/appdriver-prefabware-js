//to register widgets for types
define('prefabware/plugins/prefabware/bootstrap/ExtensionWidget', [ 
       'dojo',  
       'prefabware/plugins/prefabware/ui/ExtensionWidget',
       'dojo/_base/array',
       'prefabware/lang',
       'prefabware/app/WidgetBinding',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
		dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionWidget", [prefabware.plugins.prefabware.ui.ExtensionWidget], {
			createLabel:true,
			constructor : function(definition) {
				if (this.json.createLabel!=null) {
					this.createLabel=this.json.createLabel;
				}
			},
		});
});
