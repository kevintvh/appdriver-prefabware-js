//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/MenuItem', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/MenuItem.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.MenuItem", [dijit._Widget,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: html,
		constructor : function(params) {
			this.preventDefault=true;
		},
		startup : function() {
			this.inherited(arguments);
		},
		onClick: function(e){ 
			this.inherited(arguments);
			//patch for mobiles :close the navbar menu on click
			//whithout the patch, the menu stays open
			var navbarToggle = $('.navbar-toggle');
		     if (navbarToggle.is(':visible')) {
		     navbarToggle.trigger('click');
		    }
		}, 
	});
});
