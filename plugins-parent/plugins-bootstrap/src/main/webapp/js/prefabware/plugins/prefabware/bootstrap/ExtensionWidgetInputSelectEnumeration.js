//to register widgets for types
define('prefabware/plugins/prefabware/bootstrap/ExtensionWidgetInputSelectEnumeration', [ 
       'dojo',  
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/bootstrap/ExtensionWidgetInput',
       'dojo/_base/array',
       'prefabware/app/WidgetBinding',
       'prefabware/app/WidgetBindingNumber',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionWidgetInputSelectEnumeration", [prefabware.plugins.prefabware.bootstrap.ExtensionWidget], {
		storePlugin:{inject:'prefabware.store',type:'plugin'},  
	
	createWidget : function(qattribute,params, srcNode) {
			var widget=this.inherited(arguments);
			widget.lazy=false;
			return widget;
	},			
	createBinding : function(qattribute,widget) {
		var that=this;
			return that.fetchBindingClass().then(function(clazz){
				var binding=new clazz(qattribute,widget);
				return binding;
			});
	},
	});
});
