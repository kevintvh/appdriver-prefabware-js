//to show an overlay with loading indicator
define('prefabware/plugins/prefabware/bootstrap/widgets/Loading', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Loading.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/Modal',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	//the superlassconstructors are called from right to left
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Loading", [prefabware.plugins.prefabware.bootstrap.widgets.Modal], {
		templateString: html,
		startup : function() {
			this.inherited(arguments);
		},
		show : function() {
			this.inherited(arguments);
			//allways show as loading, thats its main purpose..
			$(this.modal).modal('loading');
		},
		hide : function() {
			this.inherited(arguments);
		},
	});
});
