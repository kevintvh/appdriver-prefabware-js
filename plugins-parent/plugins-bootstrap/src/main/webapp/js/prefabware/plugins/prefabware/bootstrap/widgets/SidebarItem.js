//a entry that can be shown on the sidebar
define('prefabware/plugins/prefabware/bootstrap/widgets/SidebarItem', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/SidebarItem.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.SidebarItem", [dijit._Widget,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: html,
		constructor : function(params) {		
			this.preventDefault=true;
		},
	});
});
