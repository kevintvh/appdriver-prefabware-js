//the home page of the free version
define('prefabware/plugins/prefabware/bootstrap/widgets/page/FreeHome', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/page/FreeHome.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/ActionButton',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,htmlTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.page.FreeHome", [dijit._Widget, dijit._Templated], {
		templateString: htmlTemplate,
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},
		injector:{inject:'injector',type:'injector'},//to inject into objects created by this
		constructor : function(params) {			
		},
		startup : function() {
			this.inherited(arguments);
		},
	});
});
