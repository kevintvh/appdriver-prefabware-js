//a editor using an external template
define('prefabware/plugins/prefabware/bootstrap/widgets/TemplatedEditor', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-style',
       'dojo/Deferred',
       'prefabware/plugins/prefabware/bootstrap/widgets/_EditorWidget',
       'dijit/_Templated',
       'prefabware/lang',
       'prefabware/util/Parameters',
       'prefabware/plugins/prefabware/bootstrap/widgets/Label',
       'dojo/_base/declare' ], function(
		dojo, array,lang,parser,domConstruct,domClass,domStyle) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.TemplatedEditor", [dijit._Widget, dijit._Container,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._EditorWidget], {
		templateString:null,
		uiPlugin:{inject:'prefabware.ui',type:'plugin'},  
		constructor : function(params,parent) {
			prefabware.lang.assert(params.templateString!=null,"a template must be provided");
			this.templateString=params.templateString;
		},
		__removeDummies : function() {
			var ap=this.attachPointActionLink;
			if (ap!=null) {
				//remove dummy content
				domConstruct.empty(ap);
			}				
		},
		buildRendering : function() {
			this.inherited(arguments)
			this.__removeDummies();
			//places the widgets into the template
			var that=this;			
			array.forEach(this.bindings(),function(binding,i){
				//looks for an attachPoint_<attribute> where <attribute>=binding.attribute.name
				//if it exists, attaches the binding.widget there
				var name=binding.attribute.name;
				var ap=that["attachPoint_"+name];
				if (ap!=null) {
					//remove dummy content
					domConstruct.empty(ap);
					binding.widget.placeAt(ap,'last');
				}				
			});
			return;
		},
	});
});
