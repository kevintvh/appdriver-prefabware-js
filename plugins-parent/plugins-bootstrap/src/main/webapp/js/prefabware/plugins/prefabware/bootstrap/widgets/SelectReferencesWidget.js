//a widget to select multiple entities from non contained 1:n relation
//uses https://github.com/ivaynberg/select2
define('prefabware/plugins/prefabware/bootstrap/widgets/SelectReferencesWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/promise/all',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/query", 
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/SelectReferencesWidget.html',
       "dojo/NodeList-data",
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,promiseAll,lang,domConstruct,domStyle,query,template) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.SelectReferencesWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		pageSize:10,//maximum number of element per page
		select2:null,//the select2 node
		templateString: template,
		store:null,
		isEmbedded:false,
		resourcePlugin       : {inject:'prefabware.resource',type:'plugin'},
		selected:null,//the array with the selected entites
		
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
			//TODO theres a mess with embedded, its false or an object
			if (params.embedded===false) {
				this.isEmbedded=false;
			}else{
				this.isEmbedded=true;
			}
			this.selected=[];
		},	
				
		setSelected : function(entities) {
			this.selected=entities;
			if (this.select2!=null) {
				if (entities!=null) {
					//set a list of ids
					var ids=array.map(entities,function(entity){
						return entity.pfw_id;
					});
					this.select2.select2("val",ids);
				}else{
					this.select2.select2("val",null);
				}
			}
		},
		getSelected : function() {
			return this.selected;
		},
		__beautify : function(){
			//this has to run AFTER the value was set !!
			var widget=this;
			//asynchron
  			widget.select2=$(widget.attachPoint_input).select2({
  				placeholder: "Select a "+widget.attribute.name,  				//TODO 18i
  			    allowClear: true,
  			    multiple: true,
  			  containerCssClass: "form-control",
  			  minimumInputLength: 0,//TODO depnds an number of elements and length of the attribute
  				//selected:widget.selected,
  				//style:"full-width"
  				//style:widget.domNode.style.width
  			initSelection : function (element, callback) {
  					var entity = null;
  					var ids=element.val();//ids="1,2"
  					if (ids==null||ids.length==0) {
						return;
					}
  					var ps=array.map(ids.split(","),function(id){
  						return widget.store.get(id).then(function(entity){
  							return widget.__entityToItem(entity);
  						})  						
  					});
  					 return promiseAll(ps).then(function(items){
  						 callback(items);
  					 });
			    },
  			query: function(options){
  				//is called with
//  				 element: opts.element,
//                 term: search.val(),
//                 page: this.resultsPage,
//                 context: null,
//                 matcher: opts.matcher,
//                 callback: this.bind(function (data) {
  				var name=widget.attribute.type.keyAttribute.name;
  				//term is the string entered by the user. append a '*' so the story will search for all beginning with the string
  				var p_query={};
  				p_query[name]=options.term+'*';
  				//options.page is the requested page (1based)
  				//the store expects start, the number of the first record to fetch, 0-based
  				//so the first row to query (start) the first row of the page 
  				var p_options={start:(options.page-1)*widget.pageSize,
  							   count:widget.pageSize};
  				return widget.store.query(p_query, p_options).then(function(entities){
  					var formatted=array.map(entities,function(entity){
  						return widget.__entityToItem(entity);
  					})
  					//entities.total  = number of all available entites for thet query
  					//entities.length = number of entities in the current result
  					//p_options.start = number of the first entity in the current result
  					var more=(entities.total-(p_options.start+entities.length))>0
  					options.callback({more:more,results:formatted});
  				});
  			},
  			formatResult:function(item, container ){
  				var entity=item.entity;
  				return entity.__label();  				
  			},
  			formatSelection:function(item, container ){
  				var entity=item.entity;
  				return entity.__label();  				
  			},
  		    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
  		    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
  			});
  			$(widget.domNode).on("change",widget.select2,function(event){
  				var ar=widget.selected;
  				if (ar==null) {
					ar=[];
				}
  				if (event.added) {
  					ar.push(event.added.entity);
				}else if (event.removed) {
					array.some(ar,function(entity){
						//TODOC identity of entites is not guaranteed
						//so we have to loop using __equals
						if (entity.__equals(event.removed.entity)) {
							//we found the removed entity
							var i = ar.indexOf(entity);
							if(i != -1) {
								ar.splice(i, 1);
							}
							return true;
						}else{
							return false;
						}
					});
				}else{
					ar=[];
				}
				widget.selected=ar;
  				widget._onChange(event);
  				});
  			
		},
		__entityToItem : function(entity) {
			if (entity) {
				//formats the entity so that it can be used as a value for select2				
				return {id:entity.pfw_id,text:entity.__label(),entity:entity};
			}else{return null;}
		},
		startup : function() {
			this.inherited(arguments);	
			this.__beautify();
			
		},
		destroy : function() {
			if (this.select2!=null) {
				$(this.select2).select2("destroy");
			}
			this.inherited(arguments);	
		},
	});
});



