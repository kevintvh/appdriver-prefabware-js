// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.TypeTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugins.prefabware.ui.Plugin");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.plugins.prefabware.bootstrap.widgets.EditorWidget");
dojo.require("prefabware.app.EditorBinding");
dojo.require("dojo._base.lang");
doh.register("prefabware.plugins.prefabware.bootstrap.widgets.tests.EditorTest", [ {
	name : "extend",
	registry:null,
	typeRegistry:null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry();
		this.registry.registryNamespace='prefabware.plugins.prefabware.bootstrap.widgets.tests';
		
	},
	
	runTest : function() {
		var deferred=new doh.Deferred();	
		this.registry.startup().then(function(registry){
		var modelPlugin=registry.findPlugin('prefabware.model');
		doh.isNot(null,modelPlugin);
		
		var typeRegistry=modelPlugin.getTypeRegistry();
		uiPlugin=registry.findPlugin('prefabware.ui');
		bootstrapPlugin=registry.findPlugin('prefabware.bootstrap');
		var invoiceType=typeRegistry.findType('com.prefabware.business.commons.WithAmount');
		doh.isNot(null,invoiceType);
		
		var node = dojo.create('div',null,dojo.body());
		bootstrapPlugin.createEditor(invoiceType,null,node).then(function(editor){
			doh.assertNotEqual(null,editor);
			editor.startup();
			deferred.callback(true);
			return;
		});
		});
		return deferred;
	},
	tearDown : function() {
	}
} ]);