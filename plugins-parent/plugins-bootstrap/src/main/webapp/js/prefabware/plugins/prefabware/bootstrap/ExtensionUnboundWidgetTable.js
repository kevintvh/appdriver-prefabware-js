//To show a table of entities.
//The widget is not bound to an attribute but uses a store directly to read and update
define('prefabware/plugins/prefabware/bootstrap/ExtensionUnboundWidgetTable', [ 
       'dojo',  
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/bootstrap/ExtensionUnboundWidget',
       'dojo/_base/array',
       'prefabware/app/WidgetBinding',
       'prefabware/app/WidgetBindingNumber',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionUnboundWidgetTable", [prefabware.plugins.prefabware.bootstrap.ExtensionUnboundWidget], {
		
	});
});
