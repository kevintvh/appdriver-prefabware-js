//a Table with rows
define('prefabware/plugins/prefabware/bootstrap/widgets/Table', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-attr',
       "dojo/dom-geometry",
       "dojo/window",
       "dojo/on",
       'dojo/query',
       'dojox/collections/Dictionary',
       'dojo/promise/all',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Table.html',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Table_th.html',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Table_th_action.html',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Table_row_action.html',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Table_row_action_a.html',
       'dojo/Deferred',
       'prefabware/lang',
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,domClass
		,domAttr,domGeom,win,on,query,dictionary,promiseAll
		,template
		,template_th
		,template_th_action
		,template_row_action
		,template_row_action_a
		) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Table", 
			[ prefabware.plugins.prefabware.bootstrap.widgets._InputWidget
			 ,prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer
			 ,dijit._Container
			 ,dijit._Templated ],{
		templateString: template,
		templateString_th: template_th,
		templateString_th_action: template_th_action,
		//the td for the rowactions
		templateString_row_action: template_row_action,
		//the link for a row inside the td
		templateString_row_action_a: template_row_action_a,
		oLanguage:{},//the text constants and messages of datatable, one object all instances of Table will share
		store:null,
		resourcePlugin:{inject:'prefabware.resource',type:'plugin'},  	
		storeObservable:null,
		observerHandles:null,
		pfw_type:null,
		view:null,
		columns:null,
		columnNames:null,//array with the names of all columns
		rowActions:null,
		json:null,//the last loaded json
		jQueryTable:null,
		jQueryPagination:null,
		paginationNode:null,
		paginationTotal:0,
		sDom:"<''<'span6'><'span3'>r>t<''<'span6'i><'span9'p>>",
		//TODO the buttons are drawn always, but this option allows the controller, to move them into the box if wanted
		renderTableActions:true,//draw the table action buttons in the head of the table
		constructor : function(params) {
			if (params.store==undefined||params.store==null) {
				throw 'a store is required';
			}
			//this.storeObservable=  Observable(Cache(entityStore, entityStore));
			if (params.view==undefined||params.view==null) {
				throw 'a view is required';
			}
			var that=this;
			this.observerHandles=[];
			this.rowActions=new dojox.collections.Dictionary();
			array.forEach(params.view.rowActions,function(action){
				that.addRowAction(action);
			});
			//also add the default rowaction, as this table does not support a default row action
			that.addRowAction(params.view.defaultRowAction);
		},
		
		__oLanguage : function ( ){
			//the text constants and messages of datatable
//			   "oLanguage": {
//		            "sLengthMenu": "Display _MENU_ records per page",
//		            "sZeroRecords": "Nothing found - sorry",
//		            "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
//		            "sInfoEmpty": "Showing 0 to 0 of 0 records",
//		            "sInfoFiltered": "(filtered from _MAX_ total records)"
//		        }
			if (this.oLanguage.length==0) {
				return prefabware.lang.deferredValue(this.oLanguage);
			};
			var that=this;
			var keys=["sSearch","sLengthMenu","sZeroRecords"
			          ,"sInfo","sInfoEmpty","sInfoFiltered"
			          ,"sFirst","sLast","sNext","sPrevious"
			          ];
			return this.resourcePlugin.getLocalization(keys,'label').then(function(labels){
				
				var o=labels;
				//sme texts are nested in oPaginate, move them there
				o.oPaginate={sFirst:o.sFirst
								,sLast:o.sLast
								,sNext:o.sNext
								,sPrevious:o.sPrevious
							};
				//and delete them from the wrong position
				delete o.sFirst;
				delete o.sLast;
				delete o.sNext;
				delete o.sPrevious;
				that.oLanguage=o;
				//cache the result
				return that.oLanguage;
			});
		},
		__get : function( aoData, sKey ){
			var i;
			    for( i=0, iLen=aoData.length ; i<iLen ; i++ ){
			        if ( aoData[i].name == sKey ){
			            return aoData[i].value;
			        };
			    };
			  //  return null;
			    throw 'could not find '+sKey;
		},
		__createTableColumns : function() {
			var that=this;
			//if no columns configured, create them here
			var candidates=this.columns;
			var tableColumns =array.map(candidates,function(column){
				//mDataProp must be set, if not dataTable tries do get the values by property index and not by name !
				var attr=that.pfw_type.getAttribute(column.name);
				prefabware.lang.assert(attr!=null,'cannot find attribute with name '+column.name+' for type '+that.pfw_type);
				var sClass=null;
				if (column["class"]!=null) {
					sClass=column["class"];
				}
				return { sTitle:column.label, 
					     mDataProp:column.name,
					     sClass:sClass,
					     sDefaultContent: '*default',//if the attribute value is undefined, use an empty string
					     'bUseRendered':false,//if set to true, the table setd the rendered value into the entity which is absolutely unwanted
					     fnRender : function ( oSettings, iRow, i ) {
					    	 var entity=oSettings.aData;
					    	 var value=entity.__valueOf(attr.name);
					    	 if (attr.type.isString&&value!=null&&value.length>30) {
					    		 //reduce to max. column width
								return value.substring(0,28)+"..";
					    	 }else if (entity.__valueOf(attr.name)=='*default') {
									entity.__setValueOf(attr.name,null);
									return '';
							}else{
					    	 	return entity.__labelOf(attr.name);
							}
					     }
				};
			});
			return tableColumns;
		},
		__createTableHeaders : function() {
			var candidates=this.columns;

			array.forEach(candidates,
					function(column) {
						var th = lang.replace(this.templateString_th, {
							label : column.label
						});
						domConstruct.place(th,this.dataTable_thead_tr,'last');
					}, this);
			//header for action
//			//do not place a text here, 
//			var th_action = lang.replace(this.templateString_th_action, {
//				label : ''
//			});
			domConstruct.place(this.templateString_th_action,this.dataTable_thead_tr,'last');
			return;
		},
		updateRow : function(entity,row) {
			//updates the row with the given index with the given entity
			//datatables uses jquery.isPlainObject to check, whether entity is an array, a normal object
			//or a column value. Sadly it considers entity not as an normal object, but as a column value
			//which leads to errors.
			//to circumvent this, we update every single column
			array.forEach(this.columnNames,function(colN,colI){
				var value = entity.__valueOf(colN,entity);
				// last 2 parameters bRedraw, bAction=false means do not call the server etc.
				this.jQueryTable.fnUpdate(value,row,colI,false,false);
			},this);
		},
		addRow : function(entity) {
			//refreshes the table, reloads the current page
			this.jQueryTable.fnAddData(entity,true);
		},
//		deleteRow : function(entity) {
//			//refreshes the table, reloads the current page
//			this.jQueryTable.fnDeleteRow(entity);
//		},
		gotoPage : function(page) {
			//{string|int}: 
			//Paging action to take: "first", "previous", "next" or "last" or page number to jump to (integer), note that page 0 is the first page.
			this.jQueryTable.fnPageChange( page );
		},
		pendingDisplayLength:0,//change to this number of rows in in process
		adjustNumberOfRows : function() {
			// sets the numbers of wrows of the table according to the screensize
			// returns ture, if the number of rows was changed so the caller can 
			// call fnDraw if appropiate
			if (this.pendingDisplayLength!=0) {
				//there is a change pending, do not try to adjust again
				//would be wrong
				return true;
			}
			try {
				var ts = domGeom.position(this.dataTable);
				var hs= domGeom.position(dojo.query("nav div")[0]);
				var vs = win.getBox();
				var available=vs.h-hs.h;
				var scale=available/ts.h;
				var oSettings = this.jQueryTable.fnSettings();
				var before=oSettings._iDisplayLength;
				this.pendingDisplayLength=before;
				if (before==null) {
					bofore=10;
				}
				after=Math.round(before*scale)-2;
				oSettings._iDisplayLength=after;
				return after!=before;
			} catch (e) {
				// if errors ocur, st nothing peding 
				this.pendingDisplayLength;
			}
		},
		refresh : function() {		
			//clear the cache
			this.adjustNumberOfRows();
			if (typeof this.store.evictAll == "function") {
				this.store.evictAll();
			}
			return this.search(this.searchOptions);
		},
		__updateListener : function(entity, oldIndex, newIndex) {
			//this table is registered as a listener to the store
			//so this method will be called whenever the store inserts,updates,deletes
			//the table has to update its content accordingly
			//WITHOUT reloading !
			var that=this;
			//resolve the proxies of only those attributes that are used as columns
			entity.__resolveProxies(that.columnNames).then(function(){
				//listens to changes to the resultset of the store
				//store.put comes here with oldIndex=the index and newIndex=-1
				if (oldIndex==-1 && newIndex >-1 ) {
					//its an insert
					that.addRow(entity);
				}else if (oldIndex >-1 && newIndex>-1) {
					//its an update
					that.updateRow(entity, oldIndex);
				}else if (oldIndex>-1 && newIndex ==-1 ) {
					//its a delete
					that.jQueryTable.fnDeleteRow(oldIndex);
				}
			});
			return;
		},
		sendMessage : function(msgId,msgParams) {
			//is called when the table needs to send a message
			//clients may hook in here to handle the message e.g. show it to the user
		},
		searchOptions:null,//the options used for the last search
		search : function(options) {
			//clients call this to search in the tables data
			//will create a query and send i t to the server
			if (this.jQueryTable==null) {
				return;//better no search than exception
			}
			if (options==null) {
				options={string:""};
			}
			this.searchOptions=options;//remember them for refresh
			
			var p=new dojo.Deferred();
			var h=this.jQueryTable.bind('draw', function () { 
				p.resolve();
				h.unbind();
				})
			this.jQueryTable.fnFilter(options.string);
			return p;
		},
		startup : function() {
			if (this._started) {
				return;
			}
			this.inherited(arguments);
			var that=this;
			//set the localized texts of datatable 
			return that.__oLanguage().then(function(oLanguage){
				 that._createTable();				 
		  });
		},
		_createTable : function() {
				$.extend( $.fn.dataTable.ext.oJUIClasses, {
					//set the css classes for the sort icon
			    sSortAsc: 'sorting headerSortUp',
			    sSortDesc: 'sorting headerSortDown',
			    sSortable: 'sort',
			    sSortableAsc: 'sorting headerSortUp',
			    sSortableDesc: 'sorting headerSortDown',
			   // sSortableNone: 'sort sort-none'
			});
			//cannot create the table in postCreate
			//than the listeners are not yet attached to the widgets and do not fire! 
			this.inherited(arguments);
			var that=this;
			
			$.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource ) {
				var table=this;
				 var api=this.oApi;
				 this.oApi._fnAjaxUpdate( oSettings);
				};
			
			array.forEach(that.actionButtons.getValueList(),function(actionButton){
				actionButton.startup();
			});
			
			this.columns= this.view.columns;
			this.columnNames=array.map(this.columns,function(col){
				return col.name;
			});

			this.tableColumns=that.__createTableColumns();
			that.__createTableHeaders();
			//make the html table a datatable
			var haeaderDrawn=false;
			$.extend( $.fn.dataTableExt.oStdClasses, {
			    "sWrapper": "dataTables_wrapper form-inline"
			} );
			this.jQueryTable=$(this.dataTable).dataTable( {
				 "fnHeaderCallback": function( nHead, aData, iStart, iEnd, aiDisplay ) {
						if (that.renderTableActions==false||haeaderDrawn) {
							return;
						}
						//draw the header only once, to not add actions many times
						var div = query("th:last-child div",nHead)[0];
						if (div!=null) {
							//sometimes the div does not yet exist...
							array.forEach(that.actionButtons.getValueList(),function(actionButton,i){
								if (i>0) {
								//prepend a spacer
								domConstruct.place('<span> </span>',div,'last');
								}
								domConstruct.place(actionButton.domNode,div,'last');
							});
							haeaderDrawn=true;
						}
					},
				"fnDrawCallback": function(){
					that.pendingDisplayLength=0;
				},
				 	'aoColumns':that.tableColumns,
				 	"bJQueryUI": true,
				 	//"sSortAsc":"headerSortUp",
				 	//texts and messages
				 	"oLanguage": that.oLanguage,
				 	
				    //do not show pagination, no 'p'
				 	"bPaginate": true,
				 	"sPaginationType": "bootstrap",//this requires paging.js
				 	"bFilter": true,
				    "sDom": that.sDom,
				    // because this datatable is nested inside the div of this widget we must not use class .row here !
				    //"sDom": "<'row'<'span6'l><'span3'f>r>t<'row'<'span6'i><'span9'p>>",
				 	//'sDom': 'tr',//draw the table only 't', and show pRocessiong 'r'
			        "bProcessing": false,
			        "bServerSide": true,
			        "bAutoWidth": false,//without that, datatable set a style width=0px
			        "sAjaxSource": "../examples_support/server_processing.php",
			        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			        	var entity=aData;
			        	var action_td=domConstruct.place(that.templateString_row_action,nRow,'last');
			        	var jQDataTable=this;
			        	array.forEach(that.rowActions.getValueList(),function(action){
			        		var class_;
			        		var onClick;
			        		class_=action.icon;
			        		if (action.actionId=='delete') {
			        			//TODO move to controller
			        			onClick=function(event){
			        				var context={type:that.pfw_type,entity:entity,action:action};
			        				context.event=event;
			        				context.table=that;
			        				action.onExecute=function(){
			        				var result=	that.store.remove(aData.pfw_id);
			        				dojo.when(result,
			        					function(){
			        						that.sendMessage('DLT0001',{entity:entity.__fullLabel()});
			        						return;
			        					},
			        					function(result){
			        						that.sendMessage('DLT0002',{entity:entity.__fullLabel(),reason:result.toString()});
			        						return;
			        					});
			        				};
			        				action.execute(context);
			        			};
			        		}else {							
			        			onClick=function(event){
			        				var context={type:that.pfw_type,entity:entity,action:action};
			        				context.table=that;
			        				context.event=event;
			        				action.execute(context);
			        			};
			        		}
			        		var rowAction_a= lang.replace(that.templateString_row_action_a, {
			        			icon  : class_,
			        			label : action.label
			        		});
			        		var rowAction_node=domConstruct.place(rowAction_a,action_td,'last');
			        		//you cannot attach whitespace or LF to the template, so add it here
			        		domConstruct.place("<span> </span>",action_td,'last');
			        	
			        		var rowAction_a_i=dojo.query("i",rowAction_node);
			        		dojo.connect(rowAction_node, "onclick", onClick);
			        		dojo.connect(rowAction_a_i, "onclick", onClick);
			        	});
			        	  return nRow;
			        	},
			        "fnServerData": function ( sSource, aoData, fnCallback ) {
			        	var start=that.__get(aoData, "iDisplayStart");
			        	var count=that.__get(aoData, "iDisplayLength");
			        	var sEcho=that.__get(aoData, "sEcho");
			        	var sSearch=that.__get(aoData, "sSearch");
			        	var query={};
			        	var keyAtt=that.pfw_type.keyAttribute;
			        	var queryAtt=sSearch;
			        	if (keyAtt.type.isString) {
			        		queryAtt=queryAtt+'*';
						}
			        	if (queryAtt=="") {
							queryAtt="*";
						}
			        	query[that.pfw_type.keyAttribute.name]=queryAtt;
			        	
			        	var sort={};
			        	var sortColIndex=that.__get(aoData, "iSortCol_0");
			        	var sortAttr=that.tableColumns[sortColIndex];
			        	
			        	sort.attribute=sortAttr.mDataProp;
			        	var dir=that.__get(aoData, "sSortDir_0");
			        	sort.descending=dir=='desc';
			        	that.beforeDataLoading();
			            var rawResult=that.store.query(query, {
			                start: start,
			                count: count,
			                sort:[sort],
			                properties:that.columnNames//names of the properties to query from the server
			              });
			            rawResult.then(function(value){
			                // Do something when the process completes
			            }, function(err){
			              // Do something when the process errors out			            	
			            	return;
			            }, function(update){
			              // Do something when the process provides progress information
			            	return;
			            });

			              //observer must be attached to the returned promise
			              //there is allway only one valid resultset at a time, so remove all other listeners
			            array.forEach(that.observerHandles,function(handle){
							handle.remove();
						});
			              that.observerHandles.push(rawResult.observe(lang.hitch(that, that.__updateListener), true));
			              rawResult.then(function(results){
			            	  if (results==null) {
								return {total:0};
							}
			            	  var outOfItems;
			            	  //results.total=number of all entites available as result for the query
			            	  if(results.total < count){
			            		  // We have found all the items and are at the end of our set.
			            		  outOfItems = true;
			            	  }else{
			            		  outOfItems = false;
			            	  }
								//the result may contain unresolved properties. resolve them now, so that jQuery can read the values
								var promises=[];
							    array.forEach(results,function(row){
							    	promises.push( row.__resolveProxies(that.columnNames) );
							    });	 
							    return promiseAll(promises).then(function(){
									return results;
								});
						}).then(function(results){
							that.json=  {
								    "sEcho": sEcho,
								    "iTotalRecords": results.total,
								    "iTotalDisplayRecords": results.total,
								    "aaData": results
								    };
								  //provide the data to the table
								  fnCallback(that.json);
								  that.onDataLoaded();
								 // that.__createPagination(results.total);
						});
			        }							
			    } );
			//if there is no data, fnDrawCallback will not be called by jquery datatable
			  //call it once directly, to render the action new button
			  if (that.jQueryTable!=null) {
		//		  that.jQueryTable.fnDraw();
			  	}
			 return;
		},
		beforeDataLoading : function() {
			//clients can hook in here to get notified when table starts to load data
		},
		onDataLoaded : function() {
			//clients can hook in here to get notified when the data was loaded
		},
		addRowAction : function(action) {
			action.label="";
			this.rowActions.add(action.actionId,action);
		},
		_findRowAction : function(actionId) {
			return this.rowActions.item(actionId);
		},
		destroy: function(/*Boolean*/ preserveDom){
			if (this.jQueryTable!=null) {
				//true=remove the whole table from the dom
				  this.jQueryTable.fnDestroy();
			 }
			array.forEach(this.observerHandles,function(handle){
				handle.remove();
			});
			this.inherited(arguments);
		}
		
	});
});
