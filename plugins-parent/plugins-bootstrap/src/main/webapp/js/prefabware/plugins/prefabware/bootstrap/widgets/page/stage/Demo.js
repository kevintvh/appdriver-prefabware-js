//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/page/Demo', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       "dojo/query",
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/page/Demo.html',
       'dijit/_Widget',
       'dojo/Deferred',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,query,htmlTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.page.Demo", [dijit._Widget, dijit._Templated], {
		templateString: htmlTemplate,
		constructor : function(params) {			
		},
		startup : function() {
			this.inherited(arguments);
			var that=this;
//			var url="https://www.evernote.com/shard/s120/sh/4d96bea0-5da2-4da1-a605-7eed5543dddc/c538d7b428ce9f769cb2ccc546db1127?content=";
//			this.callServer(url)
//				.then(function(response){
//					var contentNode=query(".note-content",response)[0];
//					domConstruct.place(contentNode, that.attachPoint_evernote, "after");
//				});
			
		},
		callServer:function(url){
			//async
			var deferred=new dojo.Deferred();
			
			//does really resolve calling the server
			dojo.xhrGet({
				url : url,
				sync : false ,
				handleAs : 'json',
				// Timeout in milliseconds:
				timeout : 5000,
				// Event handler on successful call:
				load :function(data) {
					return deferred.resolve(data);
				},

				// Event handler on errors:
				error : function(response, ioArgs) {
					return deferred.reject(response);
				}
			});
			return deferred.promise;
		}		
	});
});
