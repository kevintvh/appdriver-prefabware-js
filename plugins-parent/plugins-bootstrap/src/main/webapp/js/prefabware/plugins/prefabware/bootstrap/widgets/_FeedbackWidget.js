//to show feedback to the user e.g. validation messages
//takes care only of its own decoration
//children may mixin _FeedbackWidget themselves
//should have an attachPoint_input, where e.g. feedback message will be set
define('prefabware/plugins/prefabware/bootstrap/widgets/_FeedbackWidget', [ 
       'dojo',       
       'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._FeedbackWidget", [], {		
		__feedback:null,//state
		constructor : function() {
			this.__feedback={
					has_feedback_css:"has-feedback",//the css class to show that widget has
					lastMessage:null//the last message set was set
			};
		},		
		findFeedbackDecorator : function(messageType) {
		   //returns a hahs with 
		   //	css : the css class to use for decoration
		   //	icon : the css class to use as icon
		   //TODO move to plugin ?
			if (messageType=='error') {					
				return  {css:"has-error",icon:"fa fa-ban"};
			}else if (messageType=='warning') {
				return  {css:"has-warning",icon:"fa fa-warning"};
			}else if (messageType=='info') {
				return {css:"has-info",icon:"fa fa-info"};
			}else if (messageType=='success') {
				 return {css:"has-success",icon:"fa fa-check"};
			}
		},
		clearFeedback : function() {
			//removes the feedback from the widget
		},
		decorateFeedback : function(message) {
			//adds the feedback to the widget
			//you may access store nodes etc. in this.__feedback for easy access in clearFeedback
		},
		setFeedbackMessage : function(message) {
			//sets the feedback according to the given message
			// message ={messageType:"error" ,text:"theres an error"}
			
			// remove the old decoration
			this.clearFeedback();
			this.decorateFeedback(message);
			this.__feedback.lastMessage=message;
			return;
		},
	
	});
});
