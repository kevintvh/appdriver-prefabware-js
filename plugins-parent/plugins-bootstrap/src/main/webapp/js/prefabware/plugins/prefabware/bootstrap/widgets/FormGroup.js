//a bootstrap form-group
define('prefabware/plugins/prefabware/bootstrap/widgets/FormGroup', [ 
       'dojo',  
       'dojo/dom-class',
       'dojo/dom-construct',
       'dojo/_base/array',
       'prefabware/plugins/prefabware/bootstrap/widgets/_FeedbackWidget',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/FormGroup.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domClass,domConstruct,array,FeedbackWidget,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.FormGroup", [prefabware.plugins.prefabware.bootstrap.widgets._FeedbackWidget,dijit._Widget,dijit._Container,dijit._Templated ], {
		templateString: html,
		constructor : function(params) {			
		},	
		startup : function() {
			this.inherited(arguments);	
		},	
		disable : function(disabled) {
			dojo.query(this.containerNode).attr("disabled",disabled);
			array.forEach(this.getChildren(),function(child){
				if (typeof child.disable=='function') {
					child.disable(disabled);
				}
			});
			this.disabled=disabled;
			return;
		},
		decorateFeedback : function(message) {
			var fb=this.__feedback;
			var cn=this.containerNode
			//adds the feedback to the widget
			var dec=this.findFeedbackDecorator(message.messageType);
			//save for clearFeedback
			fb.lastCss=dec.css;
			domClass.add(cn,fb.lastCss);
			domClass.add(cn,fb.has_feedback_css);
			fb.lastIcon=domConstruct.place('<span class="fa "'+dec.icon+'" form-control-feedback" data-original-title=""></span>',cn,'last');
			fb.lastText=domConstruct.place('<span class="help-block">'+message.text+'</span>',cn,'last');
		},
		clearFeedback : function() {
			var fb=this.__feedback;
			if (fb.lastCss!=null) {
				domClass.remove(this.containerNode,fb.lastCss);
			}
			if (fb.has_feedback_css) {
				domClass.remove(this.containerNode,fb.has_feedback_css);
			}
			if (fb.lastIcon!=null) {
				domConstruct.destroy(fb.lastIcon);
			};
			if (fb.lastText!=null) {
				domConstruct.destroy(fb.lastText);
			};
		},
	});
});
