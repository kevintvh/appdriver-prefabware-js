//a bootstrap navbar
define('prefabware/plugins/prefabware/bootstrap/widgets/Sidebar', [ 
       'dojo',  
       'dojo/dom-attr',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Sidebar.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domAttr,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Sidebar", [dijit._Widget, dijit._Container, dijit._Templated ], {
		templateString: html,
		uiPlugin:{inject:'prefabware.ui',type:'plugin'}, 
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'}, 
		constructor : function(params,srcNode) {
//			if (params.label==undefined) {
//				params.label="params.label must be set";
//			}
		},	
		startup : function() {
			this.inherited(arguments);	
		},
		addNode : function(options) {
			//async !!
			if (options.node!=null) {
				//the node has a parent
				prefabware.lang.throwError("subitems are not suported in Bootstrap 3");
				options.node.addChild(node);
			}
			var that=this;
		// adds the given item to the sidebar
		// options={parent:parent,name:name,extension:extension}
		var params={extensionId:"prefabware.bootstrap.sidebarNode",label:options.label,icon:options.icon,action:options.action};
		return this.bootstrapPlugin.fetchSidebarNode(params).then(function(node){
			//add it as a child only, if its not allready attached somewhere else
			//this alloes nodes to be placed using nodeSelector of the widget extension
			if (node.domNode.parentNode==null) {
				that.addChild(node);
			}
			return node;
		});
	},
	addItem : function(options) {
		// adds the given item to the sidebar
		// options={parent:parent,name:name,extension:extension}
		var that=this;
		var params={label:options.label,icon:options.icon,action:options.action};
		var item= new prefabware.plugins.prefabware.bootstrap.widgets.SidebarItem(params);
		this.addChild(item);

		return item;
	},
	addSubItem : function(options) {
		// adds the given item to the sidebar
		// options={parent:parent,name:name,extension:extension}
		var that=this;
		var params={label:options.label,icon:options.icon,action:options.action};
		var subItem= new prefabware.plugins.prefabware.bootstrap.widgets.SidebarSubItem(params);
		options.node.addChild(subItem);
		return subItem;
	},
	});
});
