//the homepage
define('prefabware/plugins/prefabware/bootstrap/widgets/page/Home', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/page/Home.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/ActionButton',
       'prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/Subscribe',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,htmlTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.page.Home", [dijit._Widget, dijit._Templated], {
		templateString: htmlTemplate,
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},
		injector:{inject:'injector',type:'injector'},//to inject into objects created by this
		constructor : function(params) {			
		},
		startup : function() {
			this.inherited(arguments);
			var subscribe=new prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.Subscribe();
			this.injector.injectInto(subscribe);
			subscribe.placeAt(this.attachPoint_newsletter);
			subscribe.startup();
			this.createLinkToPage("prefabware.plugins.prefabware.bootstrap.widgets.page.Enterprise",this.attachPoint_enterprise);
			this.createLinkToPage("prefabware.plugins.prefabware.bootstrap.widgets.page.Features",this.attachPoint_features);
			this.createLinkToPage("prefabware.plugins.prefabware.bootstrap.widgets.page.Example",this.attachPoint_technical);
		},
		createLinkToPage : function(widgetClassName,attachPoint) {
			var that=this;
			
			var enterpriseBtn=new prefabware.plugins.prefabware.bootstrap.widgets.ActionButton({label:"mehr..."});
			enterpriseBtn.placeAt(attachPoint);
			enterpriseBtn.startup();
			enterpriseBtn.onClick=function(){
				return that.bootstrapPlugin.fetchController()
					.then(function(controller){
						return controller.openPage(widgetClassName);
				});
			}; 
		},
	});
});
