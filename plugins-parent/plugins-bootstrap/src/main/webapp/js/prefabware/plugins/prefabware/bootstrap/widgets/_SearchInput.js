//a widget with a search input field
//the widget must declare 
//  data-dojo-attach-point="searchInput"
//  and
//	data-dojo-attach-event="onkeydown: onSearchKeyDown"
//
define('prefabware/plugins/prefabware/bootstrap/widgets/_SearchInput', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dijit/_Widget',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct) {	
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._SearchInput", null, {
		search : function(options) {
			//clients can hook in here	
			//options.string is the string to search for
		},
		onSearchKeyDown : function(event) {
			//is bound in the html with data-dojo-attach-event
			switch(event.keyCode) {
	        case dojo.keys.UP_ARROW:	            
	            break;
	        case dojo.keys.DOWN_ARROW:	           
	            break;
	        case dojo.keys.ENTER:
	        	this.search({string:this.searchInput.value});
	        	//do not submit
	        	event.preventDefault();
	            break;
	        default:
	            //
	    }	}
	});
});
