package com.prefabware.js.plugins.bootstrap;

import static org.junit.Assert.*; 

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.js.test.DojoTestSupport.TestPage;
public class ClickIntegrationTest {
	private WebDriver driver;
	private DojoTestSupport testSupport;

	@Before
	public void before() throws Exception {
		testSupport=new DojoTestSupport("http://localhost:8080/plugins-bootstrap/js");
    	TestPage testPage = testSupport.add("prefabware/plugins/prefabware/bootstrap/tests/VisualPluginTest.html");
    	//run the doh test and wait until its finished
    	testSupport.testOne(testPage);
    	driver=testSupport.driver;
	}
	@After
	public void after() throws Exception {		
		//testSupport.tearDown();
	}

	@Test
	@Ignore("is only successfull if called manually")
	public void test() throws Exception{
		Thread.sleep(100);
		List<WebElement> elements = driver.findElements(By.id("testLayout"));
		assertNotNull(elements);
		assertFalse(elements.isEmpty());
		WebElement tab = elements.get(0);
		WebElement tr=tab.findElements(By.cssSelector("table tr")).get(2);
		WebElement td=tr.findElements(By.cssSelector("td")).get(1);
		WebElement button=td.findElements(By.cssSelector("button")).get(0);
		button.click();
		//now the testpage is visible
		//switch to the frame
		driver.switchTo().frame("testBody");

		//click the first menuitem
		WebElement menuItem = driver.findElement(By.id("prefabware.ui.menus_1"));
		menuItem.click();
		
		
		
		
	}
}
