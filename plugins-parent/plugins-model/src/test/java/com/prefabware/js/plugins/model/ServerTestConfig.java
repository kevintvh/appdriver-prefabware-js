package com.prefabware.js.plugins.model;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.model.rest.server.ModelServerConfiguration;
import com.prefabware.model.rest.server.web.ModelServerWebConfiguration;


@Configuration
@Import({ ModelServerConfiguration.class,ModelServerWebConfiguration.class })
public class ServerTestConfig {

}
