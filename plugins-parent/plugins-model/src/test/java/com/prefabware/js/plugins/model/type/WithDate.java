package com.prefabware.js.plugins.model.type;

import java.util.Date;

import com.prefabware.meta.domain.annotation.EntityType;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@EntityType(eager=true)
public class WithDate {
	@KeyAttribute
	public Date date;

}
