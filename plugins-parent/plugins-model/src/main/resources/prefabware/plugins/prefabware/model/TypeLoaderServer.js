//to load types from a server. the loader only loads the raw json object with the type definition
//but does not convert them into a type nor register it to the typeRegistry
//load types using .load(typeName);
define('prefabware/plugins/prefabware/model/TypeLoaderServer', [
        'dojo',
        'dojo/Deferred',
        'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.model.TypeLoaderServer", null, {
		url:null,
		constructor : function(url) {
			if (url==undefined||url==null) {
				throw 'parameter url is mandatory';
			}
			this.url=url;
		},
		load:function(typeName){
			//async
			return this.callServer(this.url+'?typeName='+typeName);
		},
		callServer:function(queryUrl){
			//sync
			var json=null;
			//var deferred=new dojo.Deferred();
			
			//does really resolve calling the server
			dojo.xhrGet({
				// The target URL on the webserver, per convention this is the
				// reference itself
				url : queryUrl,
				// call synchron, beacuse the typeRegistry is completely sync 
				sync : true,
				// send JSON
				contentType : 'application/json',
				// receive JSON
				handleAs : 'json',
				// Timeout in milliseconds:
				timeout : 5000,
				// Event handler on successful call:
				load :function(data) {
					json=data;
					return ;
					//return deferred.resolve(data);
				},

				// Event handler on errors:
				error : function(response, ioArgs) {
					debug.dir(response);
					// return the response for succeeding callbacks
				}
			});
			return json;
//			return deferred.promise;
		}		
	});
});