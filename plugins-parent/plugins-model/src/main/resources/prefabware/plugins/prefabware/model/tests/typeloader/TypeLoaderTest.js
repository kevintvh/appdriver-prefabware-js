// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.model.tests.typeloader.TypeLoaderTest');
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.model.tests.typeloader.TypeLoaderTest', [ {
	name : "recursive",
	registry : null,
	extensionPoint : null,
	extension : null,
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.model.tests.typeloader',
			fileName : 'typeLoaderTest-registry.json'
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		setup=prefabware.test.resolveSetup();
		
		setup.registry.startup().then(function(registry){
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			var loader=that.typeRegistry.loader;
			loader.add("com.prefabware.business.commons.SelfRefering","prefabware.plugins.prefabware.model.tests.typeloader", "self-refering-type.json");
			
			var amountType=that.typeRegistry.findType('com.prefabware.business.commons.SelfRefering');
			doh.assertTrue(amountType!=null);			
			testDeferred.callback(true);
		});
		
		return testDeferred;
	},
	tearDown : function() {
	}
},
{
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		setup=prefabware.test.resolveSetup();
		setup.registry.startup().then(function(registry){
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			var loader=that.typeRegistry.loader;
			loader.add("com.prefabware.business.commons.Currency","prefabware.plugins.prefabware.model.tests.typeloader", "currency-type.json");
			loader.add("com.prefabware.business.commons.Amount","prefabware.plugins.prefabware.model.tests.typeloader", "amount-type.json");

			var amountType=that.typeRegistry.findType('com.prefabware.business.commons.Amount');
			doh.assertTrue(amountType!=null);
			var amount=amountType.create();
			doh.assertTrue(amount.isCustomAmountClass);
			
			var currencyType=that.typeRegistry.findType('com.prefabware.business.commons.Currency');
			doh.assertTrue(currencyType!=null);
			
			var code=currencyType.getAttribute('code');
			doh.assertTrue(code!=null);
			doh.assertTrue(code.isKey);//should be configrued by attributeCustomizer
			testDeferred.callback(true);
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);