// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.model.tests.defaultExtension.DefaultExtensionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugins.prefabware.model.tests.defaultExtension.DefaultExtensionTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugins.prefabware.model.tests.defaultExtension',fileName:'defaultExtensionTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			
			var modelPlugin = that.registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			var typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(typeRegistry==null);
			
			var STRING = typeRegistry.STRING;
			var DECIMAL = typeRegistry.DECIMAL;
			var BOOLEAN = typeRegistry.BOOLEAN;
			var TYPE = typeRegistry.TYPE;
			var COMPOSITE = typeRegistry.COMPOSITE;
			var ENTITY = typeRegistry.ENTITY;

			var partyType = new prefabware.model.EntityType('test.Party');
			partyType.addAttribute({
				name : 'name',
				type : STRING,
				length : 30,
			});
			var personType = new prefabware.model.EntityType('test.Person');
			personType.superType=partyType;
			personType.addAttribute({
				name : 'firstName',
				type : STRING,
				length : 30,
			});
			typeRegistry.registerType(partyType);
			typeRegistry.registerType(personType);
			
			var plugin=that.registry.findPlugin('defaultExtension.a');
			doh.assertTrue(plugin!=null);
			plugin.matchExtensions("defaultExtension.a.extension1",{type:partyType})
				.then(function(extensions1){
					doh.assertTrue(extensions1!=null);
					doh.assertTrue(extensions1.length==1,"default should be created");
					var extension1=extensions1[0];
					doh.assertEqual('prefabware.plugins.prefabware.model.tests.defaultExtension.Extension1Default',extension1.declaredClass);
					return extension1;
				})
				.then(function(extension1){
					plugin.matchExtensions("defaultExtension.a.extension1",{type:partyType})
						.then(function(extensions2){
							doh.assertTrue(extensions2!=null);
							doh.assertTrue(extensions2.length==1,"extension must be returned");
							var extension2= extensions2[0];
							doh.assertTrue(extension2===extension1,"default extension should be reused");
							return extension2; 
						});
				})
				.then(function(extension2){
					plugin.matchExtensions("defaultExtension.a.extension1",{type:personType})
					.then(function(extensions3){
						doh.assertTrue(extensions3!=null);
						doh.assertTrue(extensions3.length==1);
						var extension3= extensions3[0];
						doh.assertTrue(extension3.id=='extensionPerson');
						testDeferred.callback(true);
				});
			});
		});
		return testDeferred;	
	},
	tearDown : function() {
	}
},
]);