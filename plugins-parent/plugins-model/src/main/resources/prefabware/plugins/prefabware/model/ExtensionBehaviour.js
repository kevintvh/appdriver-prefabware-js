//to provide behaviour for a type
//the type may react to events of an instance of the type
//and e.g calculate some attribute values
define('prefabware/plugins/prefabware/model/ExtensionBehaviour', [ 
       'dojo',
       'prefabware/plugin/TypeExtension',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare('prefabware.plugins.prefabware.model.ExtensionBehaviour', [prefabware.plugin.TypeExtension], {
		constructor : function(options) {
			this.behaviorClass=options.json.behaviorClass;
			return;
		},
		apply:function(options){
			//async
			//triggers the behaviour with the given event
			return this.loader.createInstance(this.behaviorClass).then(function(bhv){
				return bhv.apply(options);
			});
		},
	});
});
