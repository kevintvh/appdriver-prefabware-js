//to run reports for entities
define('prefabware/plugins/prefabware/model/ExtensionModelService', [ 
       'dojo',
       'prefabware/plugins/prefabware/service/ExtensionService',
       'prefabware/plugins/prefabware/model/TypeLoaderServer',
       'prefabware/model/TypeRegistry',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugins.prefabware.model.ExtensionModelService", [prefabware.plugins.prefabware.service.ExtensionService], {
		__createService : function() {
			this.service=new prefabware.plugins.prefabware.model.TypeLoaderServer(this.url);
		},
	});
});