//to load types from the model-server
//uses a typeLoaderFile to load early used types on startup
define('prefabware/plugins/prefabware/model/ExtensionTypeLoaderService', [ 'dojo',
                                        'dojo/_base/lang',
                                        'dojo/_base/array',
                                        'prefabware/lang',
                                        'prefabware/plugins/prefabware/model/TypeLoaderServer',
                                        'prefabware/model/to/TypeLoaderList',
                                        'prefabware/plugin/Extension',
                                        'dojo/_base/declare' ], function(
		dojo, lang,array) {
	dojo.declare('prefabware.plugins.prefabware.model.ExtensionTypeLoaderService', [prefabware.plugin.Extension], {
		servicePlugin:{inject:'prefabware.service',type:'plugin'},
		modelPlugin:{inject:'prefabware.model',type:'plugin'},
		startupLoader:null,//the id if the extension with the startupLoader to load early used types on startup
		service:null,//to load a single type using the model-server 
		loader:null,//combined loader
		constructor : function(options) {
			this.startupLoaderId=options.json.startupLoader;//can be null
		},
		startup : function() {
			var that=this;			
			return this.servicePlugin.findService('prefabware.service.model').then(function(service){
				that.service=service;
				that.loader = new prefabware.model.to.TypeLoaderList();
			}).then(function(){
				var p= that.modelPlugin.fetchExtensionById(that.startupLoaderId,true).then(function(extension){
					if (extension!=null) {
						that.startupLoader = extension.getTypeLoader();				;
						//register the startup loader BEFORE the serviceLoader
						that.loader.register(that.startupLoader);
					}
					that.loader.register(that.service);
				});
				return p;
			});
		},
		getTypeLoader : function() {
			//sync
			return this.loader;
		},		
	});
});

