//to provide behaviour for a type
//the type may react to events of an instance of the type
//and e.g calculate some attribute values
define('prefabware/plugins/prefabware/model/tests/behaviour/CityEditorBehaviour', [ 
       'dojo',
       'dojo/_base/declare',
       'prefabware/lang'],function(dojo) {
		dojo.declare('prefabware.plugins.prefabware.model.tests.behaviour.CityEditorBehaviour', null, {
		constructor : function(options) {
			return;
		},
		apply:function(options){
			//async
			var city=options.entity;
			var event=options.event;
			if (city.counter==null) {
				city.counter=1;
			}else{
				city.counter++;
			}
			if (city.name2==null||city.name2.length==null) {
				city.name2=city.name;
			}
			return prefabware.lang.deferredValue({entity:city});
		},
	});
});
