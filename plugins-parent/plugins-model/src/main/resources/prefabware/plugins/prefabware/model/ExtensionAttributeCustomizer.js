//to customize the types of the TypeRegistry
//TODO its customizes attributes, not types !
define('prefabware/plugins/prefabware/model/ExtensionAttributeCustomizer', [ 
       'dojo','dojo/_base/lang',
       'prefabware/plugin/Extension',
       'prefabware/model/match/AttributeMatcher',
       'prefabware/model/TypeRegistry',
       'dojo/_base/declare' ], function(
		dojo, lang,Extension) {
	dojo.declare("prefabware.plugins.prefabware.model.ExtensionAttributeCustomizer", [prefabware.plugin.Extension], {
		options:null,
		constructor : function(id) {
		},
		startup : function() {

			this.options=this.json.options;
		},
		customizeAttributeOptions : function(options,type) {
			//customizes the options that are used to create the attribute
			//mixin the options declared for this extension
			return lang.mixin(lang.clone(options),this.options);
		},
	});
});
