//
define('prefabware/plugins/prefabware/model/tests/typeloader/Amount', [ 
                                        'dojo',
                                        
                                        'prefabware/model/Value',
                                        'dojo/_base/declare' ],
                                function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.model.tests.typeloader.Amount", [prefabware.model.Value], {
		isCustomAmountClass:true,//to make test easier
		constructor : function() {
		},
		__label : function() {
			return this.currency.__format(this.value);
		},

	});
});
