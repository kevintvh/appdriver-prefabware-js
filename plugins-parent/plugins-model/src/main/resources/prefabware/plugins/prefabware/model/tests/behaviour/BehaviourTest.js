// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugin.model.tests.behaviour.BehaviourTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugin.model.tests.behaviour.BehaviourTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.model.tests.behaviour',
			fileName : 'behaviourTest-registry.json'
		});
		this.typeRegistry = this.registry.typeRegistry;
	},
	runTest : function() {
		var def=new doh.Deferred();
		this.registry.startup();
		var modelPlugin = this.registry.findPlugin('prefabware.model');
		var typeRegistry = modelPlugin.getTypeRegistry();
		doh.assertTrue(typeRegistry != undefined);

		var cityType = new prefabware.model.EntityType('test.City');
		cityType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
		});
		cityType.addAttribute({
			name : 'name2',
			type : this.STRING,
			length : 30,
		});
		cityType.addAttribute({
			name : 'counter',
			type : this.NUMBER,
			length : 30,
		});

		typeRegistry.registerType(cityType);

		doh.assertEqual(cityType, typeRegistry.findType('test.City'));
		var city=cityType.create();
		city.name="new Value1";//set the name to a new value
		// editorBehaviour is the default behaviour in an editor
		modelPlugin.applyBehaviour({entity:city,event:{sender:"editor",trigger:"beforeLoad"}}).then(function(){
			doh.assertEqual(city.name,city.name2);
			doh.assertEqual(city.counter,1);
			def.callback(true);
		});
		
		return doh.def;		

	},
	tearDown : function() {
	}
} ]);