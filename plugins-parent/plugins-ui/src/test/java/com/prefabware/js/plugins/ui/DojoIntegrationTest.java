package com.prefabware.js.plugins.ui;
import org.junit.Before;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;

public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/plugins-ui/js");
    	testSupport.add("prefabware/plugins/prefabware/ui/tests/history/HistoryTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/ui/tests/mediaQuery/MediaQueryTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/ui/tests/iconLabel/IconLabelTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/ui/tests/composite/CompositeTest.html");
    	//TODO make it green !
    	//testSupport.add("prefabware/plugins/prefabware/ui/tests/menu/MenuTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/ui/tests/PluginTest.html");    	
    }
    
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}