//to start a search in a searchable. 
//Example : a input field where the user enters the search options
define('prefabware/plugins/prefabware/ui/ExtensionSearcher', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       "dojo/on",
       'dojo/_base/declare' ], function(
		dojo, Extension,array,on) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionSearcher", [prefabware.plugin.Extension], {
		searchable:null,//provides .search(..)
		qualifiers:qualifier,//
		constructor : function() {
		},
		startup : function() {			
		},
		startSearch : function(options) {
			return searchable.search(options);
		},
		
	});
});