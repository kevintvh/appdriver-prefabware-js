//plugins may contribute models for types
define('prefabware/plugins/prefabware/ui/ExtensionEditorModelDefault', [ 'dojo'
       ,'dojo/_base/array'
       ,'dojo/aspect'
       ,'prefabware/plugins/prefabware/ui/ExtensionEditorModel'
	   ,'dojo/_base/declare' ], function(dojo, array, aspect) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionEditorModelDefault", [	prefabware.plugins.prefabware.ui.ExtensionEditorModel], {
		constructor : function() {
		},
	});
});
