dojo.provide("prefabware.plugins.prefabware.ui.tests.menu.MenuTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");
dojo.require('prefabware.plugins.prefabware.ui.ExtensionMenu');
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo._base.lang");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.ui.tests.menu.MenuTest", [ {
	name : "menu test",
	timeout : 1000,
	registry : null,
	uiPlugin:null,
	modelPlugin:null,
	setUpDeferred : null,
	setUp : function() {
		var that = this;
		 var root=dojo.byId("logBody").parentNode;
		 var menuNode=dojo.create("div",null,root);
		 dojo.attr(menuNode, 'id', 'appdriver-menu'); // set
		 var sidebarNode=dojo.create("div",null,root);
		 dojo.attr(sidebarNode, 'class', 'appdriver-sidebar'); // set
		 var centerNode=dojo.create("div",null,root);
		 dojo.attr(centerNode, 'id', 'appdriver-center'); // set
		this.setUpDeferred = new doh.Deferred();
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.ui.tests.menu',
			fileName : 'menuTest-registry.json'
		});
		this.registry.startup().then(function(registry) {
			that.uiPlugin = registry.findPlugin('prefabware.ui');
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			var loader=that.typeRegistry.loader;
			loader.add("com.prefabware.business.commons.Currency","prefabware.plugins.prefabware.ui.tests.menu", "currency-type.json");
			that.setUpDeferred.resolve();
		});
	},

	runTest : function() {
		var that = this;
		var testDeferred = new doh.Deferred();
		var options={json:{menu:'a.b.c.d'}};
		var em=new prefabware.plugins.prefabware.ui.ExtensionMenu(options);
		doh.assertTrue(em.startsWith('a'));
		doh.assertTrue(em.startsWith('a.b'));
		doh.assertTrue(em.startsWith('a.b.c'));
		doh.assertTrue(em.startsWith('a.b.c.d'));
		doh.assertFalse(em.startsWith('b'));
		doh.assertFalse(em.startsWith('c.d'));

		doh.assertEqual("",em.parentPath('a'));
		doh.assertEqual("a",em.parentPath('a.b'));
		doh.assertEqual("a.b",em.parentPath('a.b.c'));
		doh.assertEqual("a.b.c",em.parentPath('a.b.c.d'));
		
		this.setUpDeferred.then(function() {
			that.uiPlugin.createMenus().then(function(menus){
				doh.assertFalse(menus==null);
				doh.assertEqual(2,menus.length);
				var menu=menus[0];
				var sidebar=menus[1];
				doh.assertEqual(5,menu.count);
				doh.assertEqual(3,sidebar.count);
				var mr=menu.getValueList();
				var mr0=mr[0];
				var mr1=mr[1];
				var mr2=mr[2];
				var mr3=mr[3];
				var mr4=mr[4];

				mr0.proxy.doResolve()
				.then(function(item0){
					doh.assertEqual('node',item0.type);
					doh.assertEqual('masterData',item0.options.name);
				})
				.then(dojo._base.lang.hitch(mr1.proxy,mr1.proxy.doResolve))
				.then(function(item1){
					doh.assertEqual('node',item1.type);
					doh.assertEqual('commons',item1.options.name);
				})
				.then(dojo._base.lang.hitch(mr2.proxy,mr2.proxy.doResolve))
				.then(function(item2){
					doh.assertEqual('subItem',item2.type);
					doh.assertEqual('Currency',item2.options.name);
				})
				.then(dojo._base.lang.hitch(mr3.proxy,mr3.proxy.doResolve))
				.then(function(item3){
					doh.assertEqual('subItem',item3.type);
					doh.assertEqual('QuantityUnit',item3.options.name);
				})
				.then(dojo._base.lang.hitch(mr4.proxy,mr4.proxy.doResolve))
				.then(function(item4){
					doh.assertEqual('item',item4.type);
					doh.assertEqual('CurrencyShortcut',item4.options.name);
				});
				
				
				

				testDeferred.callback(true);
				
			});
		});

		return testDeferred;
	},
	
} ]);