// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.prefabware.resource.tests.history.HistoryTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.prefabware.resource.tests.history.HistoryTest", [ 
{
	name : "history test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	type:null,
	setUp : function() {
		this.type = new prefabware.model.EntityType('test.HistoryTest');
		
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.ui.tests.history',fileName:'plugin-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		var counter=0;
			var stub = {
				navigate : function(c) {
					counter = c;
				}
			};
		this.registry.startup().then(function(){
			var uiPlugin=that.registry.findPlugin('prefabware.ui');
			var fragment=function(){
				return ""+arguments[0];
			};
			uiPlugin.trackNavigation({target:stub,methodName:"navigate",fragment:fragment}).then(function(){
				stub.navigate(1);
				doh.assertEqual(1,counter);
				stub.navigate(2);
				doh.assertEqual(2,counter);
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);