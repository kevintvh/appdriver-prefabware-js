//a Plugin
define('prefabware/plugins/prefabware/ui/Plugin', [ 'dojo', 
		'dojo/_base/array', 'dojo/_base/lang','dojo/aspect',
		'dojo/promise/all',
		'dojo/_base/declare',
		'prefabware/plugins/prefabware/ui/EditorController',
		'prefabware/app/EditorBinding',		
		'prefabware/plugin/Plugin' ], function(dojo,  array, lang,aspect,promiseAll) {
	dojo.declare('prefabware.plugins.prefabware.ui.Plugin',
			[ prefabware.plugin.Plugin ], {
				storePlugin:{inject:'prefabware.store',type:'plugin'},  	
				actionPlugin:{inject:'prefabware.action',type:'plugin'},  	
				resourcePlugin:{inject:'prefabware.resource',type:'plugin'},  	
				nextId:{id:1},
				getApplication : function() {
					//async
					return this.fetchExtension('prefabware.ui.application').then(function(extension){
						return extension.getApplication();
					});;
				},	
				notify : function(options) {
					//async
					//options={text:'a text',type:'success'}
					return this.fetchExtension('prefabware.ui.notifier').then(function(extension){
						return extension.notify(options);
					});;
				},		
				trackNavigation : function(options) {
					//async
					//options={target:target,methodName:'methodName'}
					return this.fetchExtension('prefabware.ui.history').then(function(extension){
						return extension.trackNavigation(options);
					});
				},
				navigateBack : function() {
					return this.fetchExtension('prefabware.ui.history').then(function(extension){
						return extension.navigateBack();
					});
				},
				fetchAction : function(actionId,type) {
					//async
					//fetches the action from the actionPlugin
					return this.actionPlugin.fetchAction(actionId,type);
				},		
				createMessage : function(msgId,msgParms) {
					//async
					return this.resourcePlugin.createMessage(msgId,msgParms);
				},	
				labelOf : function(key) {
					//async
					return this.resourcePlugin.getLocalization(key,'label');
				},
				fetchLocalizations : function(options) {
					//async
					//fetches the localization for the given key
					//options = {key:key,types:["icon","label"]}
					//options.key can be a string, a type or an entity
					//options.type is an array of the localizationtypes to return
					//the result will contain a property for every element of options.types with the required localization
					//returns a promise that resolves to {icon:"icon",label:"label"}
					var key=this.localizationKey(options.key);
					return this._fetchLocalizations({key:key,types:options.types});
				},
				_fetchLocalizations : function(options) {
					//async
					//fetches the localizations for the given key
					//options = {key:"key",types:["icon","label"]}
					//options.key MUST be a string
					//options.type is an array of the localization types to return
					//the result will contain a property for every element of options.types with the required localization
					//returns a promise that resolves to {icon:"icon",label:"label"}
					var that=this;
					var ps={};		
					array.forEach(options.types,function(type){
						ps[type]=that.resourcePlugin.getLocalization(options.key,type);
					});
					return promiseAll(ps);
				},
				localizationKey : function(object) {
					//returns the localization key for the given object
					//object can be string, a type, or an entity
					if (typeof object=="string") {
						return object;
					}else if(object instanceof prefabware.model.Type){
						return object.name;
					}else if(object.isEntity){
						return entity.__label();
					}
				},
				
				fetchController : function(){
					//async
					//returns the controller for the ui
					return this.fetchExtension('prefabware.ui.controller');
				},
				getColumns : function(type) {
					var extension=this.findExtensionForType(type,'prefabware.ui.view');
					//the viewExtension is generic, for many types
					//so we have to pass it the current type to create the columns for
					return extension.getColumns(type);
				},
				fetchTypeIcon : function(type) {
					//async
					var that=this;
					return this.resourcePlugin.getLocalization(type.name,'icon')
						.then(function(icon){
							if (icon!=type.name) {
								//icon==type.name is returned from the resource plugin if
								//no icon could be found. 
								return icon;
							}else{
								//try to find the actual no-icon-found- icon
								//because the type.name may be very long and destroying the ui,
								//we will try to find a better alternative
								return that.resourcePlugin.getLocalization('no-icon-found','icon');
							}
						});
				},
				fetchTypeLabel : function(type) {
					//async
					return this.resourcePlugin.getLocalization(type.name,'label');
				},
				
				createView : function(type,store,outer) {
					//async
					var that=this;
					var view=null;
					return this.matchExtension('prefabware.ui.view',{type:type}).then(function(extension){
						if (!extension) {
							//create an extension for a default View
							//the viewExtension is generic, for many types
							//so we have to pass it the current type to create the columns for
							return that.__createViewExtension(type).then(function(extension){
								return extension;
							});;
						}
						return extension;						
					}).then(function(extension){
						view={
								type : type,							
								outer : outer,
								// TODO the store of the view extension
								// should be used
								store : store,
//								viewActions : extension.getViewActions(type),
//								rowActions : extension.getRowActions(type),
//								defaultRowAction : extension.getDefaultRowAction(type),
								extension : extension
						};					
						var pColumns=extension.getColumns(type).then(function(columns){
								view.columns =columns;
								return view;
							});
						var pViewActions = extension.getViewActions(type).then(function(viewActions){
							view.viewActions=viewActions;
						});
						var pRowActions = extension.getRowActions(type).then(function(rowActions){
							view.rowActions=rowActions;
						});
						var pDefaultRowAction = extension.getDefaultRowAction(type).then(function(defaultRowAction){
							view.defaultRowAction=defaultRowAction;
						});
						
						return promiseAll([pColumns,pViewActions,pRowActions,pDefaultRowAction]);
					}).then(function(){
						return view;
					});
				},
							
				__createEditorModel : function(type,outer) {
					var that=this;
					//async
					return this.matchExtension('prefabware.ui.editormodel',{type:type}).then(function(extension){
						if (!extension) {
							//create an extension for a default Editor
							return that.____createEditorModelExtension(type).then(function(extension){
								return extension;
							});
						}else{return extension;}
					}).then(function(extension){
						var model=extension.createEditorModel(type,outer);
						return model;
					});
				},
				createTableParms : function(type,params_i) {
					//async
				    	var params=this.__assertParams(params_i);	
				    	params.qattribute=type.getQualifiedAttribute('self');
				    	params.pfw_type=type;
				    	var pStore=null;
				    	var pLabel=null;
				    	var pView=null;
				    	if (params.store==undefined) {
				    		pStore=this.storePlugin.createStore({type:type}).then(function(store){
				    			params.store=store;	
							});
						}
				    	if (params.label==undefined) {
				    		pLabel=this.labelOf(type.name).then(function(label){
				    			params.label=label;	
							});
				    	}
				    	if (params.view==undefined) {
				    		pView=this.createView(type,params.store,params.outer).then(function(view){
				    			params.view=view;
							});
				    	}
				    	return promiseAll(prefabware.lang.selectNotNull([pStore,pLabel,pView])).then(function(){
							return params;
						});
				    },
				createEditorModelAndController : function(type,outer) {
						//async
					var that=this;
					var widgetParams=[];
						return that.__createEditorModel(type,outer).then(function(model){
							var totalWidth=0;
							array.forEach(model.qattributes,function(qattribute){
								totalWidth=totalWidth+qattribute.attribute.length;
							},that);							
							var pBindings=array.map(model.qattributes,function(qattribute){
								var width=qattribute.attribute.length;
								var widthPercent=width/totalWidth*100;
								var widgetParam={outer:{},width:width,widthPercent:widthPercent};
								widgetParams.push(widgetParam);
								var binding = that.createBindingAndWidget(qattribute,widgetParam);
								return binding;
							},that);
							return promiseAll(pBindings).then(function(bindings){
								var binding=new prefabware.app.EditorBinding(type,bindings);
								var controller=new prefabware.plugins.prefabware.ui.EditorController(binding);
								that.injector.injectInto(controller);
								var params={model:model,controller:controller,outer:outer};
								array.forEach(widgetParams,function(widgetParam){
									//set the outer for the widgets
									widgetParam.outer.model=model;
									widgetParam.outer.controller=controller;
								});
								return params;
							});
						});
					},
				createBindingAndWidget : function(qattribute,params) {
					//async
					//creates a binding and a widget and returns the binding
					//the widget can than be accessed as binding.widget
						var that=this;
						if (!params) {
							params={};
						}
						var binding=null;
						return that.matchExtension('prefabware.ui.widget',{qattribute:qattribute}).then(function(extension){
							return extension.createBindingAndWidget(qattribute,params, 'widget'+(++that.nextId.id)).then(function(newBinding){
								binding=newBinding;
							}).then(function(){
								that.createTooltip(qattribute,binding.widget.domNode);
								//the widget may need access to the resource plugin
								that.injector.injectInto(binding.widget);
								return binding;
							});
						});
					},
				createWidget : function(params) {					
						//async
						//creates a widget that is not bound to an attribute
						//to create a bound widget use createBindingAndWidget
						// params.extensionId = the extension id to use
						var that=this;
						return that.fetchExtension(params.extensionId).then(function(extension){
							return extension.createWidget(params);
						});
					},
				createTooltip : function(qattribute,srcNode){
					//async
					//returns the controller for the ui
						return this.fetchExtension('prefabware.ui.attributetooltip').then(function(ext){
							if (ext!=null) {
								return ext.createTooltip(qattribute,srcNode);
							}
						});
					},
				createMenus : function() {
					//async
					//to create the menu
					return this.fetchExtensions('prefabware.ui.menuVisitor').then(function(visitors){
						//there can be different visitora with different targets
						var promises=array.map(visitors,function(visitor){
							 return visitor.createMenu();
						});
						return promiseAll(promises);
					});
				},
				walkMenus2 : function(createItem,createNode,createSubItem) {
					var structure = {
							//structure is the mnus structure
							layout:{},
							create : function(extension){
								var thatStructure=this;
								var extMenu = extension.json;
								var id      = extMenu.menu;// e.g. 'masterData.creditRisk'
								
								var split = id.split('.');//array of the parts of the id
								var last  = split[split.length - 1];// the menu
								var path  = id.split('.',split.length - 1).join('.');// the path to the menu
								//loop over all elements of the menu items path
								dojo.forEach(split,function(name,i){
									var full=id.split('.',i+1).join('.');//the full id of the current element
									//get the according element of the structure
									var cur=lang.getObject(full,false,thatStructure.layout); 							
									if (cur==undefined) {
										//if there is no according element in the structure, create one
										if (i==0&&split.length>1) {
											//first element of many? -> create a node
											cur = thatStructure.createNode({parent:null,name:name,extension:extension});
											//store the created root in the structure	
											lang.setObject(full,cur,this.layout);		
										}else if(i!=0&&name!=last){
											//not first, not last ->create intermediate submenu
											var parentPath=path.split('.',i).join('.');
											var parent=lang.getObject(parentPath,false,thatStructure.layout);
											cur=thatStructure.createNode({parent:parent,name:name,extension:extension});
											//store the created menu element in the structure	
											lang.setObject(full,cur,thatStructure.layout);		
										}else{
											//create a menu item
											//because its the first and only
											//or because its the last one
											var parentPath=path.split('.',i).join('.');
											var parent=lang.getObject(parentPath,false,thatStructure.layout);
											cur=thatStructure.createItem({parent:parent,name:name,extension:extension});
											//store the created menu element in the structure	
											lang.setObject(full,cur,thatStructure.layout);	
										}
									}
								},this);
							}
					};
					return this.fetchExtensions('prefabware.ui.menus').then(function(extensions){
						array.forEach(extensions,function(extension, i) {
							structure.create(extension);
						});
					});
				},
				isVisible : function(attribute) {
					//TODO make configurable
					//at the moment hide all system attributes from the user
					return attribute.isSystem==false;
				},
				getMenus : function() {
					var menus = new Object();

					array.forEach(this.findExtensionsByPoint('prefabware.ui.menus'),
							function(extension, i) {
								var extMenu = extension.json;// like
								// 'masterData.creditRisk'

								var split = extMenu.id.split('.');
								var item = split[split.length - 1];// the menu
								
								var path = extMenu.id.split('.',split.length - 1).join();// the path to the menu
								// item
								lang.setObject(extMenu.id, item, menus);
							});
					return menus;
				},
				__assertParams : function(params_i) {
					var params=null;
					if (params_i==null||params_i==undefined) {
						params={};
					}else{
						params=dojo.mixin(params,params_i);
					}
					return params;
				},
			});
});