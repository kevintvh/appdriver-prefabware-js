// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.prefabware.resource.tests.iconLabel.IconLabelTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.prefabware.resource.tests.iconLabel.IconLabelTest", [ 
{
	name : "localizations test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	type:null,
	setUp : function() {
		this.type = new prefabware.model.EntityType('test.IconLabelTest');
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.ui.tests.iconLabel',fileName:'iconLabelTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var uiPlugin=that.registry.findPlugin('prefabware.ui');
			uiPlugin.fetchLocalizations({key:that.type,types:["icon","label"]}).then(function(iconLabel){
				doh.assertEqual("test-icon",iconLabel.icon);
				doh.assertTrue(prefabware.lang.startsWith(iconLabel.label,"test-label"));
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
},
{
	name : "iconLabel test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	type:null,
	setUp : function() {
		this.type = new prefabware.model.EntityType('test.IconLabelTest');
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.ui.tests.iconLabel',fileName:'iconLabelTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var uiPlugin=that.registry.findPlugin('prefabware.ui');
			uiPlugin.fetchTypeIcon(that.type).then(function(icon){
				doh.assertEqual("test-icon",icon);
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);