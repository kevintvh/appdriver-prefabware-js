//to be called on widget lifecycle events
define('prefabware/plugins/prefabware/ui/ExtensionWidgetTrigger', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,globalLabels) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionWidgetTrigger", [prefabware.plugin.Extension], {
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
		},	
		
		afterWidgetStartup : function(widget){
			//async
			// will be called after startup of the widget
			
		},
	});
});
