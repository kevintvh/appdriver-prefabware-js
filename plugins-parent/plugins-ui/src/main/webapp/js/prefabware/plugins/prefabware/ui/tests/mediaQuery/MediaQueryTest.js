// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.prefabware.resource.tests.mediaQuery.MediaQueryTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.dom-construct");
dojo.require("doh");
doh.register("prefabware.plugin.prefabware.resource.tests.mediaQuery.MediaQueryTest", [ 
{
	name : "mediaQuery test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	node:null,
	css:'<style type="text/css">'+
	    '@media #pfw_mediaQuery (min-width: 950px) { #pfw_mediaQuery { width:950px}}'+
	    '@media #pfw_mediaQuery (min-width: 450px) and  (max-width: 950px){ #pfw_mediaQuery { width:460px}'+
	    '@media #pfw_mediaQuery (max-width: 450px) { #pfw_mediaQuery { width:950px}'+
		'</style>',
	setUp : function() {
		this.cssNode=dojo.place(this.css,document.head,"first");
		this.node=dojo.place('<div id="pfw_mediaQuery"></div>',document.body,"first");
		
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.ui.tests.mediaQuery',fileName:'mediaQueryTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var uiPlugin=that.registry.findPlugin('prefabware.ui');
			
			testDeferred.callback(true);
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);