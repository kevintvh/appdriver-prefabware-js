//to create a widget that is not bound to an attribute 
define('prefabware/plugins/prefabware/ui/ExtensionWidgetUnbound', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/promise/all',
       "dojo/_base/lang",
       "dojo/query",
       'prefabware/plugin/Extension',
       'prefabware/lang',
       'prefabware/util/ResourceFile',
       'prefabware/util/Template',
       'prefabware/app/WidgetBinding',
       'prefabware/app/WidgetBindingNumber',
       'dojo/_base/declare' ], function(
		dojo, array,aspect, promiseAll,lang,domQuery) {
		dojo.declare("prefabware.plugins.prefabware.ui.ExtensionWidgetUnbound", [prefabware.plugin.Extension], {
			uiPlugin:   {inject:'prefabware.ui',type:'plugin'},  
			injector:   {inject:'injector',type:'injector'},
			//the regex used for i18n replacement in templates
			//scans for all placeholder like ${i18n.user} and captures only 'user', without ${i18n.}
			i18nRegEx:/\$\{i18n\.([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g,
			//the css selector to find the dom node where the widget wil be hooked in
			//if the selector selects more than one result, the first one will be used
			//e.g. ".appdriver-navbar"
			//if nodeSelector==null, this Extension will not place the widget 
			nodeSelector:null,
			//how the widget will be hooked in 
			//'first'=as first child
			//'last'= as list child
			//'over'= the widget will use the dom node as its own
			relative:"first",
			singleton:false,//create only one instance of this widget
			widgetOptions:null,//options that will be passed to the widget
			templates:null,		
			
		constructor : function(options) {
			this.nodeSelector=options.json.nodeSelector;
			this.relative=options.json.relative;
			this.singleton=options.json.singleton;
			this.widgetOptions=options.json.widgetOptions;
			//the qualified name of a file with the template e.g. "prefabware.widgets.table.html"
			//or a hash where the key is the name and the valu is the qulafiedname of the template
			//e.g. {template:"prefabware.widgets.table.html",template_th:"prefabware.widgets.table_th.html"}
			if (this.json.templates!=null) {
				//convert string to hash if necessary
				//by convention if there is only one template its name in the widget is 'templateString'
				this.templates=prefabware.lang.toHash(this.json.templates,"templateString");
			}
		},
		fetchWidgetClass : function() {
			//async
			return this.loader.load(this.json.widgetClass);
		},
		createWidget : function(params,srcNode) {
			//async
			var that=this;
			if (that.singleton==true&&that.widget!=null) {
				return prefabware.lang.deferredValue(that.widget);
			};
			return this.getTemplates().then(function(templates){
				return that.fetchWidgetClass().then(function(clazz){
					params=that.__assertParams(params);
					lang.mixin(params,that.widgetOptions);
					//this will override templates in the params. but this is not used anyway at the moment 
					lang.mixin(params,templates);
					return that._createAndMaybePlaceWidget(clazz,params,srcNode);
				});
			}); 
		},
		getTemplates : function() {
			//async
			// returns an object with all the templates for the widget
			// {templateX:"<div></div>"}
			// key=name of the template
			var p={};
			   for(var name in this.templates) {
				  p[name]=this.getTemplate(this.templates[name]);					
					}
			return promiseAll(p);
		},
		getTemplate : function(qualifiedName) {
			//async
			//gets the template with the given name
			//replaces all ${i18n.user} variables
			//returns the template
			//TODO rename to nls ?
			//TODO cache i18n translation !   
			var that=this;			
			//template  is a dot seperated name like 
			//prefabware.plugins.prefabware.ui.widgets.SidebarItem.html
			var resource=prefabware.util.ResourceFile({qualifiedName:qualifiedName,async:true});			
			return resource.startup().then(function(){
				var raw=resource.getContentAsString();
				//the raw template may contain i18n placeholder like ${i18n.user}. that must be shown as "User" or "Anwender" depending on the browsers locale
				//those will be replaced here. 
				var template = new prefabware.util.Template({template:raw,regEx:that.i18nRegEx});
				//find all placeholder like "${i18n.user}"
				//and return as key only the word after the dot e.g.  "user"  (without ${i18n.})
				var keys=template.findKeys();
				var values={};//this will contain all i18n values like "User"
				var ps=array.map(keys,function(key){
					//get the translation for all keys
					return that.uiPlugin.labelOf(key).then(function(label){
						lang.setObject(key, label, values);
						});
				});
				return promiseAll(ps).then(function(){					
					//when all promises are resolved
					return template.replace(values);
				});
			}); 
		},
		 _createAndMaybePlaceWidget : function(clazz,params,srcNode) {
	        	//sync
	        	// finally creates the widget using the given clazz and params
			    // if a srcNode is defined, this will be used to attach the widget
	        	// if no srcNode is defned and a nodeSelector is configured, it will place the widget relative to the node selected
	        	// with the nodeSelector, depending in this.relative
	        	var widgetNode=null;
	        	if (srcNode==null&&this.nodeSelector!=null) {
	        		var widgetNodes = domQuery(this.nodeSelector);
	        		prefabware.lang.assert(widgetNodes!=null&&widgetNodes.length>0,"dom element with css selector "+this.nodeSelector+" not found");
	        		widgetNode=widgetNodes[0];
	        		if (this.relative=='over'&&srcNode==null) {
	        			//use the node as the widgets domNode
	        			srcNode=widgetNode;
	        		}
				}
	        	//create the widget, maybe with the supplied sourceNode or the node found by the nodeSelector or null
	        	this.widget=new clazz(params,srcNode);
	        	if (this.nodeSelector!=null&&this.relative!='over') {
	        		//place the widget relative to the selected node
	        		//if a selector was configured
	        		this.widget.placeAt(widgetNode,this.relative);
	        	}
	        	this.injector.injectInto(this.widget);
	        	aspect.after(this.widget, "startup",lang.partial(lang.hitch(this,"_afterWidgetStartup"),this.widget),true);
	        	return this.widget;
	        },	
		
	    _afterWidgetStartup : function(widget) {
	    	//invoke all widget trigger
	    	return this.uiPlugin.matchExtensions("prefabware.ui.widgetTrigger",{value:"afterWidgetStartup"}).then(function(exts){
	    		var ps=new Array();
	    		array.forEach(exts,function(ext){
	    			ps.push(ext.afterWidgetStartup(widget));
	    		});
	    		return promiseAll(ps);
			});
	        },
		__assertParams : function(params) {
			if (params==null) {
				params={};
			}
			return params;
		},
	});
});
