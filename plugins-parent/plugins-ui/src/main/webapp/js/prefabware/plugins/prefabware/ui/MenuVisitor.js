//to create the menu items
define('prefabware/plugins/prefabware/ui/MenuVisitor', [ 
         'dojo'
         ,'dojo/_base/array'
         ,'dojo/aspect'
         ,'prefabware/lang'
		 ,'dojox/collections/Dictionary'
		 ,'prefabware/plugins/prefabware/ui/menu/MenuElementProxy'
		 ,'dojo/_base/Deferred'
		 ,'dojo/_base/declare' ], function(dojo,array,aspect) {
	dojo.declare("prefabware.plugins.prefabware.ui.MenuVisitor", null, {
		uiPlugin : null,
		controller : null,// the ui.controler
		menuExtensions : null,// the ui.menus
		menuVisitable : null,// the ExtensionMenuVisitable
		created : null,// items created so far
		useProxies:false,
		constructor : function(options) {
			this.uiPlugin=options.uiPlugin;
			this.itemFactory=options.itemFactory;
			this.menuExtensions=options.menuExtensions;
			this.menuVisitable=options.menuVisitable;
			if (options.useProxies==true) {
				this.useProxies=true;
			}
			this.created = new dojox.collections.Dictionary();
		},
		visit : function() {
			// async
			array.forEach(this.menuExtensions,this.visitMenuExtension,this);
			//now that all proxies are created, resolve the top level ones
			//so they get visible
			array.forEach(this.created.getValueList(),function(entry){
				if (entry.proxy.parent==null||!this.useProxies) {
					//its a top level node, resolve the proxy immediatly,so the node is visible
					//if the node is clicked, it resolves its children, so the menu items gets created
					entry.proxy.doResolve();
				}
			},this);
			return this.created;
		},
		visitMenuExtension : function(menuExtension) {
			//a menuExtension defines a path in the menu
			//e.g. sales.incoices.invoice
			var that=this;
			var id = menuExtension.menu;// e.g. 'masterData.creditRisk'

			var split = menuExtension.menuArray;// array of the parts of the id
			var last = menuExtension.menuItem;// the menu
			var path = menuExtension.menuPath;// the path to the menu
			// iterate over all segments of the menu items path
			var segmentIterator={
					index:0,//the next index
					hasNext:function(){
						return this.index<menuExtension.menuArray.length;
					},						
					next:function(){
						var iterator=this;
						prefabware.lang.assert(this.hasNext(),'next() called, but hasNext==false');
						var full = id.split('.', this.index + 1).join('.');// the full name of the current segment
						var name = split[this.index];// the full name of the current segment
						// element
						// get the item created for the current segment
						var cur = that.created.item(full);
						var proxy=null;
						// if there is no according element in the structure, create it
						if (cur == undefined) {
							proxy=new prefabware.plugins.prefabware.ui.menu.MenuElementProxy();
							proxy.name=name;
							proxy.index=iterator.index;//for easy debug
							if (full!=menuExtension.menu&&menuExtension.startsWith(full)) {
								//we are in the path of the menu, so create a Node
								var parent=that.created.item(menuExtension.parentPath(full));
								proxy.isNode=true;
								proxy.children=[];
								proxy.parent=parent;
								if (parent!=null) {
									//add it to the parent, so he can resolve its children on first click
									parent.proxy.children.push(proxy);
								}
								proxy.resolver=function(){
									proxy.promise=that.menuVisitable.createNode({
									parent : parent,
									proxy : proxy,//allow the node to resolve the proxy.children
									name : name,
									itemFactory:that.itemFactory,
									extension : menuExtension
								});
								prefabware.lang.assert(proxy.promise!=null,that.menuVisitable.declaredClass+'.createNode must not return null');
								return proxy.promise;
								};
							}else if(full==menuExtension.menu){
								//we have reached the end, create an item or subitem
								var parent=that.created.item(menuExtension.parentPath(full));								
								if (parent==null) {
									//no parent ? create an item
									proxy.isItem=true;
									proxy.resolver=function(){
									proxy.promise = that.menuVisitable.createItem({
										parent : parent,
										name : name,
										itemFactory:that.itemFactory,
										extension : menuExtension
									});									
										prefabware.lang.assert(proxy.promise!=null,that.menuVisitable.declaredClass+'.createItem must not return null');
										return proxy.promise;
									};
								}else{
									//has a parent node ? so create a subitem
									proxy.isSubItem=true;
									proxy.parent=parent;
									proxy.resolver=function(){
									proxy.promise = that.menuVisitable.createSubItem({
										parent : parent,
										name : name,
										itemFactory:that.itemFactory,
										extension : menuExtension
									});
									prefabware.lang.assert(proxy.promise!=null,that.menuVisitable.declaredClass+'.createSubItem must not return null');
									return proxy.promise;
									};
									//add it to the parent, so he can resolve its children on first click
									parent.proxy.children.push(proxy);
									}
							}
						}
								if (proxy!=null) {
								var entry={item:null,proxy:proxy,isNode:proxy.isNode,isItem:proxy.isItem,isSubItem:proxy.isSubItem};
								that.created.add(full,entry);
								//when the proxy was resolved, set the item into the entry
								aspect.after(proxy, "doResolve",function(){entry.item=proxy.item;},true);
								}
								iterator.index++;
								if (iterator.hasNext()) {
									//continue
									return iterator.next();
								}else{
									return that.created;
								}
					},//function next
			};
			//----end of iterator defintion
			//no use it
			return segmentIterator.next();
		},
	});
});
