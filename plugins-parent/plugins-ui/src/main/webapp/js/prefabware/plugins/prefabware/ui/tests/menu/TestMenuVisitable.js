//to be visited by the ExtensionVisitor
//to create the MenuNodes,-Items and -SubItems
define('prefabware/plugins/prefabware/ui/tests/menu/TestMenuVisitable', [ 
       'dojo', 
       'prefabware/plugins/prefabware/ui/ExtensionMenuVisitable',
       'dojo/_base/array',
       'prefabware/lang',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.ui.tests.menu.TestMenuVisitable", [prefabware.plugins.prefabware.ui.ExtensionMenuVisitable], {
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
		},	
		
		createNode : function(options){
			//async
			//creates a menu node for the given element
			//options={parent:parent,name:name,extension:extension}
			//returns a promise with the node
			return prefabware.lang.deferredValue({type:'node',isNode:true,options:options});
		},
		createSubItem : function(options){
			//async
			//creates an item in a submenu for the element inside the parent
			//options={parent:parent,name:name,extension:extension}
			//returns a promise with the subItem
			return prefabware.lang.deferredValue({type:'subItem',options:options});
		},
		createItem : function(options){
			//async
			//creates a menu item
			//returns a promise subItem with the Item
			return prefabware.lang.deferredValue({type:'item',options:options});
		},
	});
});
