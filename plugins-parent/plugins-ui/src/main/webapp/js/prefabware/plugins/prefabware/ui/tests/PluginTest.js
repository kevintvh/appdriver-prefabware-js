// DID NEVER RUN !
dojo.provide("prefabware.plugins.prefabware.ui.tests.PluginTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.ui.tests.PluginTest", [ {
	name : "ui-plugin",
	timeout : 3000,
	registry : null,

	cityType:null,
	setUpDeferred : null,
	setUp : function() {
		var that = this;
	
		this.setUpDeferred = new doh.Deferred();
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.ui.tests',
			fileName : 'pluginTest-registry.json'
		});
		this.registry.startup().then(function(registry) {
			var modelPlugin = registry.findPlugin('prefabware.model');
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);

			that.cityType = new prefabware.model.EntityType('test.City');
			that.cityType.addAttribute({
				name : 'name',
				type : that.typeRegistry.STRING,
				length : 30,
			});

			that.cityType.addAttribute({
				name : 'pfw_id',
				type : that.typeRegistry.STRING,
				length : 30,
			});
			that.typeRegistry.registerType(that.cityType);
			that.setUpDeferred.resolve();
		});
	},

	runTest : function() {
		var that = this;
		var testDeferred = new doh.Deferred();
		this.setUpDeferred.then(function() {
			
			
			dojo.promise.all([
			     that.testCreateEditorParms(),
			     that.testActionLabel(),
			     that.testCreateTableParms(),
			     that.testMessage(),
			     that.testLabel(),
			     ]).then(function(results){
				 testDeferred.callback(true);
			  });
			
			
		});

		return testDeferred;
	},
	testActionLabel : function() {
		//to test how to get a message
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		var actionId='create';
		uiPlugin.fetchAction(actionId,{type:that.cityType,i18nKey:actionId}).then(
				function(action) {
					doh.assertFalse(action == null);					
					//TODO nls problem
					doh.assertEqual(actionId,action.actionId);					
					deferred.resolve(true);
				});
		return deferred;
	},
	testCreateEditorParms : function() {
		//to test how to get a message
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		uiPlugin.createEditorModelAndController(that.cityType).then(
				function(params) {
					doh.assertFalse(params == null);					
					doh.assertTrue(params.model!=null);					
					doh.assertTrue(params.controller!=null);					
					deferred.resolve(true);
				});
		return deferred;
	},
	testCreateTableParms : function() {
		//to test how to get a message
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		uiPlugin.createTableParms(that.cityType).then(
				function(table) {
					doh.assertFalse(table == null);					
					doh.assertTrue(table.view.columns.length>0);					
					doh.assertEqual("prefabware.plugins.prefabware.action.ActionOpenEditor",table.view.defaultRowAction.declaredClass);					
					deferred.resolve(true);
				});
		return deferred;
	},
	testMessage : function() {
		//to test how to get a message
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		uiPlugin.createMessage("DLT0002", {
			entity : 'TestEntity'
		}).then(
				function(msg) {
					doh.assertFalse(msg == null);						
					doh.assertEqual(msg.id,
							'DLT0002');
					deferred.resolve(true);
				});
		return deferred;
	},
	testLabel : function() {
		//to test how to get a label
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		return uiPlugin.labelOf("pfw_id").then(function(label){
			doh.assertEqual(label,"Id");
			deferred.resolve(true);
		});;
		return deferred;
	},
	tearDown : function() {
	}
} ]);