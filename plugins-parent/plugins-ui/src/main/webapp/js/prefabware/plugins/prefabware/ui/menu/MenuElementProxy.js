//a proxy for a menu element
define('prefabware/plugins/prefabware/ui/menu/MenuElementProxy', [ 'dojo',
		'dojo/_base/lang', 'prefabware/lang','dojo/_base/declare' ], function(dojo, lang) {
	dojo.declare("prefabware.plugins.prefabware.ui.menu.MenuElementProxy",
			null, {
				resolver : null,// a function that must be called to resolve
								// this proxy, should return a promise
				item : null,// the resolved menu item
				resolved:false,
				constructor : function() {
					return;
				},

				doResolve : function() {
					if (this.resolved) {
						return prefabware.lang.deferredValue(this.item);
					}
					var that = this;
					return this.resolver().then(function(item) {
						that.resolved=true;
						that.item = item;
						return item;
					});
				}
			});
});
