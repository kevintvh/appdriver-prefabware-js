//to support 
//	browser back and forward buttons
//	bookmarks
define("prefabware/plugins/prefabware/ui/ExtensionHistory", [ 
       "dojo", 
       "prefabware/plugin/Extension",
       "dojo/_base/array",
       "dojo/aspect",
       "dojo/_base/lang",
       "dojo/hash",
       "dojox/collections/Dictionary",
       "dojo/_base/declare" ], function(
		dojo, Extension,array,aspect,lang,dojoHash) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionHistory", [prefabware.plugin.Extension], {
		inProcess:null,//the hash that is in process
		track:true,//is false, during forward or back navigation to not track again
		stack:null,
		current:null,
		constructor : function() {
			this.stack=new  dojox.collections.Dictionary();
		},
		startup : function() {
			dojo.subscribe("/dojo/hashchange", this, this._onHashChange);
			return;
		},		
		_stackToString : function() {
			string={size:this.stack.count,
					entries:array.map(this.stack.getValueList(),function(item){return item.args[0].toString();})};
			return dojo.toJson(string);
		},
		_log : function(options) {
			prefabware.lang.log("ExtensionHistory event "+options.event);
			prefabware.lang.log("ExtensionHistory hash "+options.hash);
			prefabware.lang.log("ExtensionHistory inProcess "+this.inProcess);
			prefabware.lang.log("ExtensionHistory count "+this.stack.count);
			prefabware.lang.log("ExtensionHistory stack "+this._stackToString());
			return;
		},		
		_onHashChange : function(hash) {
			this._log({event:"_onHashChange.before",hash:hash});
			if (hash.length==0) {
				return;
			}
			if (hash==this.inProcess) {
				this.inProcess=null;
				return;
			}
			var old=this.inProcess;//remember cause we want to remove it
			this.inProcess=hash;//do this early, the call will triger hashChange again ! and the guard-clause uses the inProcess
			//the hash of the page has changed through back/forward navigation
			//now load the according content
			var call =this.stack.item(hash);
			if (call!=null) {
				this._log({event:"_onHashChange.executeCall",hash:hash});
				this.track=false;//do not track my own back navigation
				if (this.stack.count>1) {
			//TODO we can only remove if we go backwards !		
				//do not remove the first entry otherwise we cannot navigate back !
					//this.stack.remove(old);
				}
				document.title = hash;    
				var f=lang.hitch(call.target, call.methodName);//to make f will run in the scope of target and will access the right 'this'
				f.apply(call.target,call.args);//call the navigation method
				this.track=true;
			}
			this._log({event:"_onHashChange.after",hash:hash});
			return;
		},		
		navigateBack : function(options) {
			window.history.back();
		},
		trackNavigation : function(options) {
			// the given method of the target will be tracked for navigation
			//every call to this function will be pushed on the history stack
			//when the user navigates back or forward, the recorded call will be executed again to show the according content
			var target=options.target;
			var methodName=options.methodName;
			//fragment must be a function returning the fragment for the history entry
			//it will be applied the arguments that where used to call the tracked method
			var fragment=options.fragment;
			prefabware.lang.assert(fragment!=null,"options.fragment must not be null");
			if (typeof fragment=="string") {				
				fragment=function(){return options.fragment;};
			}
			prefabware.lang.assert(typeof fragment=="function","options.fragment must be a string or a function returning the fragment");
			var that=this;
			
			aspect.after(target, methodName, function(result, args){
				if (!that.track) {
					return;//do not track own forward/back navigation
				}
				//this function is called, after a tracked method was called
				//we save target,method and args to execute the same call again when navigating back
				var key=fragment.apply(target,args);
				var value={target:target,methodName:methodName,args:args};
				//create a new entry in the history
				that._log({event:"trackingNavigation.before",hash:key});
//				var currentHash=dojoHash();
//				if (currentHash!=null&&currentHash.length>0) {
//					dojoHash(currentHash);
//					that.inProcess=currentHash;
//				}else{
					//no hash in the url, so set the hash of the current url 
					dojoHash(key);
					//change the title AFTER setting the hash !!!
					document.title = key;    
					that.inProcess=key;
//				}
				//put the hash inProcess and its call into the stack
				that._log({event:"trackingNavigation.after",hash:key});
				that.stack.add(key,value);
				return result;
				}, false);
			//return the original result !
		},		
	});
});