//a menu-structure that can be presented as menu (normally in the top) or as sidebar 
//configured for a specific type and action
define('prefabware/plugins/prefabware/ui/ExtensionMenu', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/lang',
       'prefabware/lang',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionMenu", [prefabware.plugin.Extension], {
		actionPlugin:{inject:'prefabware.action',type:'plugin'}, 
		modelPlugin:{inject:'prefabware.model',type:'plugin'}, 
		uiPlugin:{inject:'prefabware.ui',type:'plugin'}, 
		type:null,//the name of the type to use for this menu item
		target:"menu",//where does the menuentry belong ? 'menu' or 'sidebar'
		menu:null,//the complete path to the menu entry like a.b.c
		menuArray:null,//an array with all segments of the menu like [a,b,c]
		menuPath:null,//the path of the menu, without the last element like 'a.b'
		menuItem:null,//the item of the menu,  like 'c'
		__itemId:1,
		constructor : function(options) {
			if (options.json.menu==null) {
				prefabware.lang.throwError("no attribute 'menu' defined in extension :"+dojo.toJson(options.json));
			}
			if (options.json.target!=null) {
				this.target=options.json.target;
			}
			this.type=options.json.type;
			this.menu=options.json.menu;
			this.menuArray=this.menu.split('.');
			this.menuPath=this.menu.split('.',this.menuArray.length - 1).join('.');
			this.menuItem=this.menu[this.menuArray.length - 1];
		},
		nextItemId : function() {
			this.__itemId++;
			// '.' in the id causes provblems in html
			return this.id.split('.').join('_')+'_'+this.__itemId;
		},
		startsWith : function(path) {
			//returns true, if this menuPath starts with the given path
			return this.menu.indexOf(path)==0;
		},
		parentPath : function(path) {
			//returns the parent of the given path,if this menuPath starts with the given path
			//this.menuPath=a.b.c.d.e
			//parentPath('a.b.c.d')='a.b.c'
			if (!this.startsWith(path)&&this.menu!=path) {
				prefabware.lang.throwError("menuPath "+this.menuPath+' does not start with '+path+' cannot calculate parentPath');
			}
			var segments=path.split('.');
			return segments.splice(0,segments.length-1).join('.');
		},
		fetchLabelAndItem : function() {
			//async
			//explizit icon defined ?
			var that=this;
			var result={};
			//definition in extension has priority
			result.icon=this.json.icon;
			result.icon=this.json.label;
			var typeP=[];
			return this.fetchAction().then(function(action){
				if (action!=null) {
					if (result.icon==null) {
						result.icon=action.icon;
					}
					if (result.label==null) {
						result.label=action.label;
					}
				};
				if (result.icon==null||result.label==null) {
					//still not found ?
					//last chance, get label and icon of the type
					var type=that.getType();
					if (type!=null) {
						typeP.push(that.uiPlugin.fetchTypeIcon(type).then(function(icon){
							result.icon=icon;
						}));
						typeP.push(that.uiPlugin.fetchTypeLabel(type).then(function(label){
							result.label=label;
						}));
					}else{
						result.label=that.id;
					}
				}
				return promiseAll(typeP).then(function(){
					return result;
				});
			});
		},
		
		getType : function() {
			if (this.type==null) {
				return null;
			}
			//returns the type configured for this menu extension
			return this.modelPlugin.findType(this.type);
		},
		hasAction : function() {
			return this.json.actionId!=null;
		},		
		fetchAction : function() {
			//async
			var that=this;
			if (this.hasAction()) {
				return this.actionPlugin.fetchAction(that.json.actionId,that.getType());
			}else{
				return prefabware.lang.deferredValue(null);
			}
		},		
	});
});
