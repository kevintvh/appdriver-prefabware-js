//a element in the sidebar
define('prefabware/plugins/prefabware/ui/widgets/SidebarItem', [ 
       'dojo', 
       'dojo/_base/array',
       'prefabware/plugins/prefabware/ui/widgets/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array) {
	dojo.declare("prefabware.plugins.prefabware.ui.widgets.SidebarItem", [prefabware.plugins.prefabware.ui.widgets._Widget, dijit._Templated ], {
		templateString: dojo.cache("prefabware.plugins.prefabware.ui.widgets", "SidebarItem.html"),
	    //  your custom code goes here
		pfw_iconClass:null,//will replace the placeholder in the template
		pfw_label:null,//will replace the placeholder in the template, cannot use _label, thats allready used by Widget
		constructor : function(params,parent,label,iconClass) {			
			params=this.__assertParams(params);
			this.pfw_label=label;
			this.pfw_iconClass=iconClass;
		},
	});
});
