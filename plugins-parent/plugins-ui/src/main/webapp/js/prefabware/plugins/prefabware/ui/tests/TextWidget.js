//base for widgets
define('prefabware/plugins/prefabware/ui/tests/TextWidget', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'prefabware/plugins/prefabware/ui/widgets/_Widget',
       'dijit/_Widget',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct) {
	dojo.declare("prefabware.plugins.prefabware.ui.tests.TextWidget", [dijit._Widget,prefabware.plugins.prefabware.ui.widgets._Widget], {
		constructor : function(params,parent) {
		},		
		startup : function() {
			this.inherited(arguments);	
		},
	});
});
