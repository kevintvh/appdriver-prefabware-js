//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/ui/ExtensionViewDefault', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/plugins/prefabware/ui/ExtensionView',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, array,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionViewDefault", [prefabware.plugins.prefabware.ui.ExtensionView], {
		startup : function() {
			this.inherited(arguments);
		},	
	});
});
