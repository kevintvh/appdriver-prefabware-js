define({ 
	'DLT0002.title' : "Delete ${entity} failed !",
	'DLT0002.label' : "Could not delete ${entity} \nReason : ${reason}",
});