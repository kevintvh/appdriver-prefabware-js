//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/ui/ExtensionEditorBinding', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'prefabware/app/EditorBinding',
       'dojo/_base/array',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionEditorBinding", [prefabware.plugin.Extension, prefabware.plugins.prefabware.model.TypeRegistryAccessMixin,prefabware.plugins.prefabware.model.TypeAcceptorMixin], {
		
		createBinding : function(model,srcNodeRef,params) {
		},
	});
});
