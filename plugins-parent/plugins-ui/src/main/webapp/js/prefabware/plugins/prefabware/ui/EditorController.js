//to provide the logic for an editor
//does not know nothing about the ui
//communicates with the ui only through the EditorBinding by binding.setValue(entity) and binding.commit(entity)
//delegates all ui manipulation to an EditorUI
define('prefabware/plugins/prefabware/ui/EditorController', [ 'dojo',
                                  'dojo/_base/array',
                                  'dojo/_base/lang',
                                  'dojo/aspect',
                                  'prefabware/lang',
		], function(dojo,array,lang,aspect) {
	dojo.declare('prefabware.plugins.prefabware.ui.EditorController', null, {
		entity : null,
		entityType : null,
		events:{
			beforeLoad :{sender:'editor',trigger:'beforeLoad'},
			onChange   :{sender:'editor',trigger:'onChange'},
			beforeSave :{sender:'editor',trigger:'beforeSave'}
		},
		default_:null,//the object with defaults for new entites
		before : null,//the last entity that was set to this editor
		binding:null,//the editors user interface bindings
		modelPlugin:{inject:'prefabware.model',type:'plugin'}, 
		constructor : function(binding) {
			//entityType = the type of entity
			//ui = the ui of the editor		
			this.entityType=binding.type;
			this.binding=binding;
			aspect.after(this.binding, "onChange",lang.hitch(this,"_onChange"),true);
		},		
		startup : function() {
			this.binding.startup();
		},	
		_onChange : function() {
			//will be called when one of the widgets is changed
			//get the data from the widgets and merge it into the entity
			//TODO commit only changed properties
			this.commit();
			//apply the according behaviour
			this.applyBehaviour({entity:this.entity,event:this.events.onChange});
			return;
		},
		setEntity : function(entityIn) {
			// memorize the unchanged version of the entity
			this.before = entityIn;
			var entity;
			if (entityIn==null||entityIn==undefined) {
				entity=this.entityType.createDefault();
			}else{
				entity=entityIn;
				//resolve all direct proxied attributes of the entity				
			}
			prefabware.lang.assert("the entity must have the same type as this EditorController",entity==null||entity.__type===this.entityType);
			this.applyBehaviour({entity:entity,event:this.events.beforeLoad});
			//load the values of the entity
			this.binding.setValue(entity);
			//set as current instance
			this.entity = entity;
		},
		onSaveSuccess : function(saved) {
			//the saved entity
		},
		applyBehaviour : function(options_p) {
			if (options_p.entity==null) {
				//no entity, nothing to apply to
				return;
			}
			//options.event = the event
			//applies the behaviours to the current entity
			//caller must take care, that the entity is prepared, e.g. widget values have been comitted
			var options=lang.mixin({},options_p);
			var that=this;
			//apply all behaviours configured for the entity and event
			this.modelPlugin.applyBehaviour(options)
				.then(function(result){
					//load the values of the entity
					that.binding.setValue(result.entity);
				});
		},
		validate : function() {
			//validates, that the entered values are valid for the type of the attribute
			return this.binding.validate();
		},
		save : function() {
			//call this to save the editor
			//this method could e.g. be called by actionSave
			var vr=this.validate();
			if (!vr.success){
				return false;
			}
			//get the data from the widgets and merge it into the entity
			this.commit();
			// 1. apply behaviours to calculate field values
			this.applyBehaviour({entity:this.entity,event:this.events.beforeSave});
			this.onSave(this.entity,this.before,this);
			this.onSaveSuccess(this.entity);
		},
		onSave:function(entity,before,editor){
			//this editor does not know how to save,
			//subclasses can attach here using aspect.after
			//to save where ever they may need
			//returns the saved entity, may set a entity, thats returned from the save operation using setEntity(entity)
			return ;
		},
		close : function() {
		},
		commit: function() {
			//writes the values from the ui into the entity of this editor
			//and returns that entity
			var entity  = this.assertEntity(this.entity);
			this.entity = this._commitToEntity(entity);
			return this.entity;
		},
		_commitToEntity: function(entity) {
			//writes the values from the ui into the given entity
			entity=this.assertEntity(entity);
			return this.binding.commit(entity);
		},
		assertEntity : function(entity) {
			//to be shure to have an instance of entity
			if (entity==null||entity==undefined) {
				entity=this.entityType.createDefault();
			}
			return entity;
		},
		getEntity : function() {
			//returns the current entity of this editor
			//this is the entity set by setEntity, if unchanged
			//or the entity after the last commit
			//TODO we clone to much here !!
			var clone;
			if (this.entity!=null) {
				clone=this.entity.__clone();
			}else{
				clone=this.assertEntity(this.entity);
			}
			return clone;
		},
	});
});