//to register widgets
define('prefabware/plugins/prefabware/ui/ExtensionWidget', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/promise/all',
       "dojo/_base/lang",
       'prefabware/lang',
       'prefabware/plugins/prefabware/ui/ExtensionWidgetUnbound',
       'dojo/_base/declare' ], function(
		dojo, array,allPromises,lang) {
		dojo.declare("prefabware.plugins.prefabware.ui.ExtensionWidget", [prefabware.plugins.prefabware.ui.ExtensionWidgetUnbound], {
		constructor : function(options) {
		},
		fetchBindingClass : function() {
			//async
			//the class of binding to create
			//optional
			if (this.json.bindingClass) {
				return this.loader.load(this.json.bindingClass);
			}else{
			return prefabware.lang.deferredValue(prefabware.app.WidgetBinding);
			}
		},
		createWidget : function(qattribute,params, srcNode) {
			//async
			//TODO this has a lot in common with the inherited ExtensionWidgetUnbound.createWidget
			var that=this;
			return this.getTemplates().then(function(templates){
				return that.fetchWidgetClass().then(function(clazz){
					//some widgets like TimeTextBox need at least an empty params object
					params=that.__assertParams(params);
					params.qattribute=qattribute;
					params.createLabel=that.createLabel;
					if (params.label==undefined) {
						that.uiPlugin.labelOf(qattribute.attribute.label).then(function(label){
							params.label=label;
						});
					}
					if (params.icon==undefined&&that.json.icon!=undefined) {
						params.icon=that.json.icon;
					}
					//if there is no special template, provided in the parms
					if (params.templateString==null) {
						//and a template is configured in the json of this extension
						if (templates!=null&&templates.templateString!=null) {
							//use it
							params.templateString=templates.templateString;
						}
					}
					var widget=that._createAndMaybePlaceWidget(clazz,params,srcNode);
					return widget;
				});
			}); 
		},
		createBindingAndWidget : function(qattribute,params, srcNodeRef) {
			//async
			//creates a widget, binds it and returns the binding
			var that=this;
			params=this.__assertParams(params);
			if (params.disabled==undefined) {
			}
			var widget=null;
			return this.createWidget(qattribute,params,srcNodeRef).then(function(newWidget){
				widget=newWidget;
				return that.createBinding(qattribute, widget);
			});
		},		
		createBinding : function(qattribute,widget) {
			//async
			//binds the attribute to the given widget
			//returns a WidgetBinding
			return this.fetchBindingClass().then(function(clazz){
				return new clazz(qattribute,widget);
			});
		},
	});
});
