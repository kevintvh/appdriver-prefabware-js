//the ui controller
//can openEditor,openView
define('prefabware/plugins/prefabware/ui/ExtensionController', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,aspect) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionController", [prefabware.plugin.Extension], {
// thats what subclasses have to implement :		
//		showView : function(type,embedded) {
//			return view;
//		},
//		openEditor : function(entity,embedded) {
//			return editor;
//		},	
//		openEditorForType : function(type,embedded) {
//			// shows an editor for a new entity of the given type
//			// allways creates a new editor
//			return this.openEditor(entity,embedded);		
//		},
//		showModalEditor : function(entity,type){
//			return editor;
//		},
//		addMenuNode : function(options) {},
//		addMenuItem : function(options) {},
//		addMenuSubItem : function(options) {},
		viewOnNewEditor : function(view) {
			//will be called if the action new is executed on the view
			//call the editor with entity==null, so he can create its own defaults
			//the same as opening en editor for a selected line
			this.viewOnSelectionChange(null,view);
		},
		viewOnSelectionChange : function(entity,view) {
			var action=view.defaultRowAction;
			//do NOT use the entity from the store !! 
			//changing that, the entity is also directly changed, which we do not want
			//we have to wait until save before changing the entity !!
			// this must not save directly to the server, but just set its
			// value into the views entity
			var clone=null;
			if (entity==undefined||entity==null) {
				clone==null;
			}else{
				clone= entity.__clone();
			};
			var editor;
			var openEditor=function(){
				if (view.openEditor) {
					//the view may provide a method to openEditor
					//if it exists, use it. 
					editor=view.openEditor(clone,view.__type);
					//when saving the editor, save to the views store
					aspect.after(editor, "onSave",view.store.saveEditor,true);
					return editor;
				}else{
					throw 'cannot show editor for '+ clone.__label();
				}
			};			
			//supply openEditor, so the action might call it
       		var context={type:view.type,item:entity,action:action,openEditor:openEditor};
       		action.execute(context);
		},
	});
});
