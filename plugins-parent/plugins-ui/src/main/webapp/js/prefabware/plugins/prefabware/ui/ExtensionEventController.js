//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/ui/ExtensionEventController', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       "dojo/on",
       'dojo/_base/declare' ], function(
		dojo, Extension,array,on) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionEventController", [prefabware.plugin.Extension], {
		constructor : function() {
		},
		startup : function() {
			//chrome does not fire keypress for escape key
			 on(document, "keydown", function(event) {
			        prefabware.lang.log('key pressed'+ event.keyCode);
			    });
			 on(document, "keypress", function(event) {
				 prefabware.lang.log('key pressed'+ event.keyCode);
			 });
			return;
		},		
	});
});