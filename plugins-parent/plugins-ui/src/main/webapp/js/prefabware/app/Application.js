//definition of an application
define('prefabware/app/Application', [ 'dojo', 'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.app.Application", null, {
		id:null,
		labels:null,
		constructor : function(id,labels) {
			this.id=id;
			this.labels=labels;
		},
		label: function(){
			return this.labels.applicationTitle;
		},
		labelOf : function(string) {
			if (string==undefined||string==null) {
				return '';
			}
			var label=this.labels[string];
			if (label==undefined) {
				label=string +'?';
			}
			return label;
		},
		
	});
});
