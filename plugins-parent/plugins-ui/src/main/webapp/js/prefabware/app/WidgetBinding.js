//to bind the attribute of an entity to a widget
//gives uniform acces to the values of all widgets
//value is allways ment to be of attribute.type 
//subclasses may override the getValue/setValue to provide conversions
//binding trough dojo.Statefull would not help set the original value in the widget
define('prefabware/app/WidgetBinding', [ 'dojo',  'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.app.WidgetBinding", null, {
		qttribute:null,
		attribute:null,
		widget:null,
		default_:null,//the default value for this widget. its values are of attribute.type serialized to json e.g. String  
		readOnly:null,
		constructor : function(qattribute,widget) {
			//attribute of the entity, var attribute=this.entityType.getAttribute(attrName);
			//widget to bind to
			//entity to get and set values
			this.qattribute=qattribute;
			this.attribute=qattribute.attribute;
			this.readOnly=this.attribute.readOnly;
			this.widget=widget;
		},
		isAttached : function() {
			//return true if  the widget of this binding is attached to the dom
			return this.widget!=null&&this.widget.domNode!=null&&this.widget.domNode.parentNode!=null;
		},
		load : function(entity) {
			//loads the widget with the value from the entity
			this.setValue(entity.__valueOf(this.attribute.name));
			//returns this binding
			return this;
		},
		commit : function(entity) {
			//do never commit to read only attributes
			//these are e.g. pfw_typ and pfw_id
			if (this.attribute.readOnly) {
				throw 'cannot commit to attribute '+this.attribute.name+' its readOnly !!';
			}
			//writes the value of the the widget into the attribute of the entity
			entity.__setValueOf(this.attribute.name,this.getValue());
			//returns this binding
			return this;
		},
		validate : function() {
			//validate the value of the widget
			//normally just try to get the value of it
			//returns a validationResult like {success:true,message:message}
			try {
				var value=this.getValue();
				//check if the instance is of the desired type
				if (!this.attribute.isValue(value)) {
					var text='value of '+this.attribute.name+' is not a valid '+this.attribute.type.simpleName();
					var message={messageType:'error',text:text};
					this.setValidationMessage(message);
					return {success:false,message:text};
				}				
			} catch (e) {
				var text='cannot getValue of widget '+this.attribute.name;
				var message={messageType:'error',text:text};
				this.setValidationMessage(message);
				return {success:false,message:text};
			}
			return {success:true}; 
		},
		getValue : function() {
			//get the value of the widget
			var value=this.widget.get("value");			
			return this.attribute.convertFrom(value);
		},
		setToDefault : function() {
			
			//sets the value to the default
			if (this.default_!=undefined&&this.default_!=null) {
				//TODO type conversion, deserialize in editorExtension, fallback to type.default there !!!
				this.setValue(this.default_);
			}else{
				this.setValue(this.attribute.type.createDefault());
			}
		},
		setValue : function(value) {
			//set the value of the widget
			//value=entity.attribute
			var string = this.attribute.stringOf(value);
			this.widget.set("value", string);
		},	
		setValidationMessage : function(marker) {
			//'error', 'warning', 'success', 'none'
			this.widget.setValidationMessage(marker);
		}
	});
});
