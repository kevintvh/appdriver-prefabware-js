//binds a store to a grid
define('prefabware/app/WidgetBindingGrid', [ 'dojo', 
		'prefabware/app/WidgetBinding', 'dojo/_base/declare' ], function(dojo,
		dijit, WidgetBinding) {
	dojo.declare("prefabware.app.WidgetBindingGrid",
			[ prefabware.app.WidgetBinding ], {
				attribute : null,
				widget : null,

				constructor : function(attribute, widget) {

				},
				__assertStore : function() {
					// the store must be set
					// the store contains all instances of the referenced type
					// TODO filter !!!
					if (!this.widget.store) {
						throw 'widget.store of ' + this.widget.id
								+ ' must be set';
					}
					this.store = this.widget.store;
				},
				getValue : function() {
					// get the value of the widget
					// returns an array of all entities contained in the grid
					// the store is always in sync with the grid. changes from
					// editors opened for a row
					// are allways made to the store of the grid.
					// so we can easily get the data from the store here
					this.__assertStore();
					return this.store.data;
				},
				setValue : function(value) {
					this.__assertStore();
					this.store.clear();
					if (value == undefined || value == null) {
						this.store.setData([]);
					} else {
						this.store.setData(value);
					}
					this.widget.refresh();
				},
				setToDefault : function() {
					//the default for a grid is an empty store
					this.setValue(null);
				},
			});
});
