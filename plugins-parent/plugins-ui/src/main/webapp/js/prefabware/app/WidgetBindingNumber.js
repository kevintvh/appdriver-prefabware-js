//to bind the attribute of an entity to a widget
//gives uniform acces to the values of all widgets
//value is allways ment to be of attribute.type 
//subclasses may override the getValue/setValue to provide conversions
//binding trough dojo.Statefull would not help set the original value in the widget
define('prefabware/app/WidgetBindingNumber', [ 'dojo',  'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.app.WidgetBindingNumber", [prefabware.app.WidgetBinding], {
		constructor : function(attribute,widget) {},
		setValue : function(value) {
			//in contrast to a normal Text, do not set the value as string here !
				this.widget.set("value", value);
		},	
	});
});
