//a combobox to select a value for a :1 relation
//the value, that corresponds to the antity.attribute is widget.item not widget.value !
define('prefabware/app/WidgetBindingComboBoxEnum',
		[ 'dojo',  'prefabware/app/WidgetBinding',
		  'dojo/aspect',
		  'dojo/_base/declare'
		  ], function(dojo,WidgetBinding,aspect) {
	dojo.declare("prefabware.app.WidgetBindingComboBoxEnum", [prefabware.app.WidgetBinding], {
		attribute:null,
		widget:null,

		constructor : function(attribute,widget) {
			//the store must be set
			//the store contains all elements of the enumeration
			//TODO filter !!!
			if (!widget.store) {
				throw 'widget,store of '+widget.id+' must be set';
			}
			//the store must contain data with value and id
			//in case of a String enumeration value===id
		},
		getValue : function() {
			//get the value of the widget
			return this.widget.item.pfw_id;
		},
		setValue : function(value) {
			var store=this.widget.store;
			var that=this;
			//the store may not have been loaded with the enum values, yet
			//so delay until its loaded
			if (store.isLoaded===false) {
				aspect.after(store, "afterLoad",function(){
					//call again !
					that.setValue(value);
				},true);
				return;
			}
			//value is the value of entity.attribute
			//value is a of the enumerated type e.g. String
			//this is the current item selected in the combobox
			//show the user the label of the value
			this.widget.set("value", value);
			//setValue above sets the item ===undefined
			//have to repair this here
		//	this.widget.item=value;
		},
		setToDefault : function() {
			var that=this;
			//TODO labelAttribute or do we need a keyAttribute ?
			var attributeName=this.attribute.type.labelAttribute.name;
			//sets the value to the default
			if (this.default_!=undefined&&this.default_!=null) {
				//TODO type conversion, deserialize in editorExtension, fallback to type.default there !!!
				var query={};
				query[attributeName]=this.default_;
				this.widget.store.query(query).forEach(function(result){
					  // the result is an entity and a proxy, the store handels that for us
					that.setValue(result);
					});
			}else{
				this.widget.set("value", '');
			}
		},
	});
});
