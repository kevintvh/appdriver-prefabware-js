//to create messages
define('prefabware/app/message/MessageProvider', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/text',
       'dojox/collections/Dictionary',
       'prefabware/app/message/Message',
       'prefabware/util/OptionsMixin',
       'dojo/_base/declare' ], function(dojo,array) {
	dojo.declare("prefabware.app.message.MessageProvider", [ prefabware.util.OptionsMixin], {
		messageResource:null,//"prefabware.util.ResourceFile"
		i18nResource:null,//"prefabware.util.ResourceI18n"
		
		messageDefs:null,
		constructor : function(options) {
			this.mixinOptions(options);
			this.messageDefs=new dojox.collections.Dictionary();
			this.__readMessageFile();
		},
		__readMessageFile : function() {
			var json=this.messageResource.getContentAsJson();
			array.forEach(json.messages,function(messageDef){
				this.messageDefs.add(messageDef.id,messageDef);
			},this);
			return;
		},
		__findMessageDefinition : function(id) {
			return this.messageDefs.item(id);
		},
		__translated : function(id,key,default_) {
			var translated=this.i18nResource.getLocalization()[id+'.'+key];
			if (translated==undefined) {
				return default_;
			}else{
				return translated;
			}
		},
		createMessage : function(id,msgParms) {
			var json=this.__findMessageDefinition(id);
			if (json==null||json==undefined) {
				return null;
			}
			var title=this.__translated(id,'title',json.title);
			var text=this.__translated(id,'text',json.text);
			return new prefabware.app.message.Message({id:json.id,icon:json.icon,title:title,text:text,msgType:json.msgType,msgParms:msgParms});
		},
	});
});
