//a combobox to select a value for a :1 relation
//the value, that corresponds to the antity.attribute is widget.item not widget.value !
define('prefabware/app/WidgetBindingComboBoxReference', [ 'dojo',  'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo,WidgetBinding) {
	dojo.declare('prefabware.app.WidgetBindingComboBoxReference', [prefabware.app.WidgetBinding], {
		attribute:null,
		widget:null,

		constructor : function(attribute,widget) {
			//the store must be set
			//the store contains all instances of the referenced type
			//TODO filter !!!
			if (!widget.store) {
				throw 'widget,store of '+widget.id+' must be set';
			}
		},
		getValue : function() {
			//get the value of the widget
			return this.widget.item;
		},
		setValue : function(value) {			
			//value is the value of entity.attribute
			//value is a Value or Composite or an Entity itself
			//this is the current item selected in the combobox
			//show the user the label of the value
			var set;
			if (value) {
				if (value.__label) {
					set=value.__label();
				}else{
					set=value;
				}
				this.widget.set("value", set);
			}else{
				this.widget.set("value", "");
			}
			//setValue above sets the item ===undefined
			//have to repair this here
			this.widget.item=value;
		},
		setToDefault : function() {
			var that=this;
			//TODO labelAttribute or do we need a keyAttribute ?
			var attributeName=this.attribute.type.labelAttribute.name;
			//sets the value to the default
			if (this.default_!=undefined&&this.default_!=null) {
				//TODO type conversion, deserialize in editorExtension, fallback to type.default there !!!
				var query={};
				query[attributeName]=this.default_;
				this.widget.store.query(query).forEach(function(result){
					  // the result is an entity and a proxy, the store handels that for us
					that.setValue(result);
					});
			}else{
				this.widget.set("value", '');
			}
		},
	});
});
