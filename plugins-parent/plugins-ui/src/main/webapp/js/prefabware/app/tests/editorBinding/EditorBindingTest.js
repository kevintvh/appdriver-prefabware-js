// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.TypeTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.app.Editor");
dojo.require("prefabware.app.EditorBinding");
dojo.require("dojo._base.lang");
doh.register("prefabware.app.tests.editorBinding.EditorBindingTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	registry:null,
	typeResolver:null,
	setUp : function() {
		this.registry = new prefabware.model.TypeRegistry();
		this.typeResolver=new prefabware.model.TypeResolver(this.registry);
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
		this.BOOLEAN = this.registry.BOOLEAN;
		this.TYPE = this.registry.TYPE;
		this.COMPOSITE = this.registry.COMPOSITE;
		this.ENTITY = this.registry.ENTITY;
	},
	runTest : function() {
		var partyType = new prefabware.model.EntityType('test.Party');
		partyType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
		});
		
		this.registry.registerType(partyType);
		doh.assertEqual( partyType,this.registry.findType('test.Party'));
		var nameAttr=partyType.getAttribute('name');
		var nameBinding={
				name:null,
				attribute:nameAttr,
				getValue : function() {
					return this.name;
				},
				setToDefault : function() {
					this.setValue('defaultName');					
				},
				setValue : function(value) {
					this.name=value;
				},	
				commit : function(entity) {
					entity.__setValueOf('name',this.getValue());
					return this;
				},
		};
		
		var binding=new prefabware.app.EditorBinding(partyType,[],false);
		binding.createBindings=function(){
			return [nameBinding];
		};
		binding.createUiActions=function(){
			
		};
		binding.startup();
		var editor=new prefabware.app.Editor(partyType,binding);
		//the editor should show the default values
		doh.assertEqual( 'defaultName',nameBinding.name);
		
		var party=partyType.createDefault();
		party.name='Stefan Isele';
		editor.setEntity(party);
		//now the value should be set to the ui
		doh.assertEqual( 'Stefan Isele',nameBinding.name);
		//fake a user change
		nameBinding.name="changed";
		//the change is not yet commited so the editor.entity has still the original value
		doh.assertEqual( 'Stefan Isele',editor.getEntity().name);
		editor.commit();
		//now the editor.entity should have the changed value
		doh.assertEqual( 'changed',editor.getEntity().name);
		
		return;
	},
	tearDown : function() {
	}
} ]);