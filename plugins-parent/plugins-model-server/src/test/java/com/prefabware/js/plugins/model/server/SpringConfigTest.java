package com.prefabware.js.plugins.model.server;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.prefabware.commons.logging.LoggingSupport;
import com.prefabware.environment.junit.JUnitEnvironment;
import com.prefabware.jersey.SpringBeanHandlerInstantiator;
import com.prefabware.spring.commons.BeanFactory;
import com.prefabware.spring.commons.BeanFactorySupport;
@Ignore
public class SpringConfigTest {
	private BeanFactorySupport support;
	private BeanFactory beanFactory;

	private LoggingSupport log;

	@Before
	public void setUp() throws Exception {
		JUnitEnvironment environment = new JUnitEnvironment();

		this.support = new BeanFactorySupport();
		Package pkg = this.getClass().getPackage();
		this.support.addAllSpringBeansXml();
		this.support.add(pkg, "applicationContext.xml");
		this.beanFactory = this.support.getBeanFactory();

		log = new LoggingSupport(pkg.getName());
		
	}

	@Test
	public void testConfig() throws Exception {
		SpringBeanHandlerInstantiator handler = this.beanFactory.getBean(SpringBeanHandlerInstantiator.class);
		assertNotNull(handler);
		 ModelService modelService = this.beanFactory.getBean(ModelService.class);
		assertNotNull(modelService);
	}
	
	@After
	public void tearDown() {
		if (this.beanFactory != null) {
			this.beanFactory.close();
		}
	}

	private void log(String string, Object... args) {
		System.out.println(String.format(string, args));
	}

}
