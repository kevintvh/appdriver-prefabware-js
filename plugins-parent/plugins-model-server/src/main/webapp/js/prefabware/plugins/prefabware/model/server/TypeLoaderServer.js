//to load types from a server. the loader only loads the raw json object with the type definition
//but does not convert them into a type nor register it to the typeRegistry
//load types using .load(typeName);
define('prefabware/model/server/TypeLoaderServer', [
        'dojo',
        
        'dojo/_base/array',
        'dojo/cache',
        'prefabware/commons/ClassLoader',
        'dojo/_base/declare' ], function(dojo,array,cache,dictionary) {
	dojo.declare("prefabware.model.server.TypeLoaderServer", null, {
		url:null,
		constructor : function(url) {
			if (url==undefined||url==null) {
				throw 'parameter url is mandatory';
			}
			this.url=url;
		},
		load:function(typeName){
			return this.callServer(typeName,this.url+'/'+typeName);
		},
		callServer:function(typeName,url){
			var jsonType=null;
			//does really resolve calling the server
			dojo.xhrGet({
				// The target URL on the webserver, per convention this is the
				// reference itself
				url : url,
				// call synchron, we have to wait for the result
				sync : true,
				// send JSON
				contentType : 'application/json',
				// receive JSON
				handleAs : 'json',
				// Timeout in milliseconds:
				timeout : 5000,
				// Event handler on successful call:
				load :function(data) {
					jsonType=data;
				},

				// Event handler on errors:
				error : function(response, ioArgs) {
					debug.dir(response);
					// return the response for succeeding callbacks
				}
			});
			return jsonType;
		}		
	});
});