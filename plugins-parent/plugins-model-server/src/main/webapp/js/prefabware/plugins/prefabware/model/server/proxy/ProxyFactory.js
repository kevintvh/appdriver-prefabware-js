//to create a proxy
define('prefabware/model/server/proxy/ProxyFactory', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",
		  'prefabware/model/Attribute',
		  'prefabware/model/server/proxy/ValueProxy',
		  'prefabware/model/server/proxy/ReferenceProxy',
		  'prefabware/model/server/proxy/EntityProxy',
		  
		  'dojo/_base/declare' ], function(dojo,lang,locale,Type) {
	dojo.declare('prefabware.model.server.proxy.ProxyFactory', null, {
		constructor : function(name) {
		},
		createProxy : function(type,data) {
			//type : the type to proxy
			//data : the element to proxy
			//looks strange but is intended :
			//the proxies are created inside the value, and we return
			//the value, so allready existing references to that object will then reference the prox 
	 		if (data.$ref && !data.__isProxy) {
	 			var refProxy= new prefabware.model.server.proxy.ReferenceProxy(type,data,this);		 			
	 			return data;
	 		} else if(data.pfw_id && data.pfw_type && !data.__isEntity && !data.__isProxy){
	 			//value is an unproxied entity
	 			//create an entity proxy for the value to proxy its unresolved references
				var entityProxy= new prefabware.model.server.proxy.EntityProxy(type,data,this);	
				return data;
	 		} else if(data.pfw_type && !data.__isEntity && !data.__isProxy){
	 			//value is an unproxied ValueType
	 			//create an Composite proxy
	 			var compositeProxy= new prefabware.model.server.proxy.ValueProxy(type,data,this);		 			
				return data;
			} else{
				//simple value, just return ist
				return data;}
		},	
	
	});
});
