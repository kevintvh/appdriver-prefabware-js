// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugin.model.tests.lifecycle.LifecycleTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugin.model.tests.lifecycle.LifecycleTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.model.tests.lifecycle',
			fileName : 'lifecycleTest-registry.json'
		});
		this.typeRegistry = this.registry.typeRegistry;
	},
	runTest : function() {

		this.registry.startup();
		var modelPlugin = this.registry.findPlugin('prefabware.model');
		var typeRegistry = modelPlugin.getTypeRegistry();
		doh.assertTrue(typeRegistry != undefined);

		var cityType = new prefabware.model.EntityType('test.City');
		cityType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			// use a simple value
			default_ : 'Cologne'
		});
		var partyType = new prefabware.model.EntityType('test.Party');
		partyType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			// use a simple value
			default_ : 'Smith'
		});
		partyType.addAttribute({
			name : 'city',
			type : cityType
		});
		partyType.addAttribute({
			name : 'firstName',
			type : this.STRING,
			length : 30,
			// use a function to calculate the default
			default_ : function(instance) {
				return instance.name + 'y';
			}
		});

		typeRegistry.registerType(cityType);
		typeRegistry.registerType(partyType);

		doh.assertEqual(cityType, typeRegistry.findType('test.City'));
		var listenerCalled=false;
		
		
		cityType.on('defaultsApplied',function(event){
			//check the event properties
			doh.assertEqual(cityType, event.sender);
			doh.assertEqual(cityType, event.type);
			doh.assertTrue( event.instance!=undefined);
			listenerCalled=true;
		});
		var city=cityType.createDefault();
		doh.assertTrue( listenerCalled);
		

	},
	tearDown : function() {
	}
} ]);