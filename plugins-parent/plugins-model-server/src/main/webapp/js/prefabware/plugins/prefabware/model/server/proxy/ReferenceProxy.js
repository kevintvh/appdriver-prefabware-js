//a proxy for a reference
//
define('prefabware/model/server/proxy/ReferenceProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/model/server/proxy/CompositeProxy',
		  'dojo/_base/declare' ],
		  function(dojo, lang,CompositeProxy) {
	dojo.declare("prefabware.model.server.proxy.ReferenceProxy", [prefabware.model.server.proxy.CompositeProxy], {		
		constructor : function(type,data) {
			//the super constructor was called automatically and now
			
			if (!data.$ref) {
				throw 'object must be a reference with reference.$ref';
			};
			data.__initializeProxy();   
		},
		__postCreate : function(data) {
		
			this.inherited(arguments);
		},
				
		__initializeProxy : function() {
			this.__reference={};
			this.__reference.$ref=this.$ref;
			this.__reference.type=this.__type;
			//and now create the trigger properties
			//create a property for every attribute, that will trigger resolve
			//and uses doGet doSet as accessors
			var attrs=this.__type.getAttributes();
	    	for (var i=0; i<attrs.length; i++){
	    		var attr=attrs[i];
	    		if (!attr.isSystem) {
	    			//the system properties are directly accessible and have values set,
	    			//no need to resolve before acessing them
	    			var attrName=attr.name;
	    			this.createPropertyAccessors(attrName);
				}
	    	  }	 
			}
	});
});
