//a dynamic proxy to allow e.g. lazy resolving of references
//this Proxy is used to extend an existing data object, that contains the data for all its attributes.
//create an instance of this EntityProxy, pass the type and the data as constructor arguments
//CAUTION : than the data will be as well a Entity as a EntityProxy.
//continue using the data object, but forgett the created proxy
define('prefabware/model/server/proxy/EntityProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/model/Entity','prefabware/model/server/proxy/CompositeProxy',
		  'dojo/_base/declare' ],
		  function(dojo,lang,Entity,CompositeProxy) {
	dojo.declare("prefabware.model.server.proxy.EntityProxy", [prefabware.model.server.proxy.CompositeProxy], {			
		constructor : function(type,data) {
			if (data.pfw_id==undefined||data.version==undefined) {
				throw 'object must have a pfw_id';
			};
			data.__isResolved=true;			
		},
		
		__initializeProxy : function() {
			//create a proxy for every reference
			//keep original arrays
			//do not create trigger properties, because this proxy is completely resolved 
			//and uses doGet doSet as accessors
			this.__target={};
			var attrs=this.__type.getAttributes();
	    	for (var i=0; i<attrs.length; i++){
	    		var attr=attrs[i];
	    		if (!attr.isSystem&&attr.declaredClass=='prefabware.model.ReferenceAttribute') {
	    			//add trigger properties for references or value types
	    			var attrName=attr.name;
	    			//store the original reference data in the target
	    			this.__target[attrName]=this[attrName];
	    			this.createPropertyAccessors(attrName);
				}
	    	  }	 
			},
	});
});
