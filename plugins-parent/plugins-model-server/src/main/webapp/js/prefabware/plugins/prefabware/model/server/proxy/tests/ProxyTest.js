// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.plugins.prefabware.model.server.proxy.tests.ProxyTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.to.TypeImporter");
dojo.require("prefabware.plugins.prefabware.model.server.TypeLoaderServer");
dojo.require("prefabware.model.EntityType");
dojo.require("dojo._base.lang");
dojo.require("dojo.cache");
doh.register("prefabware.plugins.prefabware.model.server.proxy.tests.ProxyTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	pluginRegistry:null,
	typeRegistry:null,
	loader:null,
	importer:null,
	setUp : function() {
		this.pluginRegistry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.model.server.proxy.tests',fileName:'ProxyTest-registry.json'});
		this.pluginRegistry.startup();
		this.typeRegistry=this.pluginRegistry.findPlugin('prefabware.model').getTypeRegistry();
	},
	runTest : function() {
		doh.assertFalse(this.typeRegistry==null);
		doh.assertFalse(this.typeRegistry.loader==null);
		this.importer = new prefabware.model.to.TypeImporter(this.typeRegistry,this.typeRegistry.loader);
		this.loader=this.typeRegistry.loader;
		this.loader.add("com.prefabware.business.commons.Currency","prefabware.plugins.prefabware.model.server.proxy.tests", "currency-type.json");
		this.loader.add("com.prefabware.business.commons.Amount","prefabware.plugins.prefabware.model.server.proxy.tests", "amount-type.json");
		var amountType=this.typeRegistry.findType("com.prefabware.business.commons.Amount");
		doh.assertFalse(amountType===null);
		doh.assertFalse(amountType===undefined);
		doh.assertFalse(amountType.getAttribute('currency')===undefined);
		//doh.assertFalse(amountType.getAttribute('name')===undefined);
		return;
	},
	tearDown : function() {
	}
} ]);