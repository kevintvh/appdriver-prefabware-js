//a proxy for all kinds o composites
// Entity, Reference and Value
//
define('prefabware/model/server/proxy/CompositeProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/model/server/proxy/Proxy',
		  'prefabware/model/server/proxy/EntityResolver',
		  'dojo/_base/declare' ],
		  function(dojo,lang,Proxy,EntityResolver) {
	dojo.declare("prefabware.model.server.proxy.CompositeProxy", [prefabware.model.server.proxy.Proxy], {
		//all properties here are prefixed with __ to avoid naming conflicts with the properties of the entity
		//TODO composite and entity do not need a resolver, only reference proxies
		proxyFactory:null,
		__resolver :null,
		__type:null,	
		__composite:null,	
		constructor : function(type,data,proxyFactory) {
			//cannot initialize this field above, the legacy loader has problems with the cycle dependency
			this.proxyFactory=proxyFactory;
			this.__resolver= new prefabware.model.server.proxy.EntityResolver(proxyFactory);
			if (data.pfw_type==undefined) {
				throw 'object must have pfw_type';
			};
			this.__type=type;
			//here the instance for the received data is created
			this.__composite=type.create(data);
			//mix the composite and all its functions into data
			//to make it the required type e.g. Entity
			this.__composite.__mixInto(data);
			//mix this into data
			lang.mixin(data, this);
			//data now is a proxy, all its attributes did allready exist in data
			//so the only thing we do is proxy the references and the valueTypes
			data.__initializeProxy();
		},
		__postCreate : function(data) {
			
		},
		doGet : function(propertyName, value) {
			if (value instanceof Array) {
				for ( var intx = 0; intx < value.length; intx++) {
					var element = value[intx];
					value[intx] = this.__doGet(propertyName, element);
				}
			} else if (value!=undefined&&value!=null&&value.pfw_type) {
				value = this.__doGet(propertyName, value);
			}
			return value;
		},
	
	copyUnproxiedProperties : function(from,to){
		//copies the unproxied properties from -> to
		this.copyProperty('pfw_id', from, to);
		this.copyProperty('pfw_type', from, to);
		this.copyProperty('version', from, to);
		//
		this.copyProperty('$ref', from, to);
		this.copyProperty('__id', from, to);
		this.copyProperty('__parent', from, to);
		this.copyProperty('_loadObject', from, to);
		return to;
	},
	copyProperty : function(propertyName,from,to){
		//copies the property with the given name from -> to
		//only, if its defined in from
		if (propertyName in from) {
			to[propertyName]=from[propertyName];
		}
		return to;
	},
	
	doResolve: function (referenceArg) {
			if (this.__reference) {
				return this.__resolver.resolve(this.__reference);
			} else {
				return {};
			}
		},
	__doGet : function(propertyName,value) {
		//if the value is itself an unresolved reference, create a reference proxy for it
		var refAttr=this.__type.getAttribute(propertyName);
		var refType=refAttr.type;
 		return this.proxyFactory.createProxy(refType,value);
	},
	});
});
