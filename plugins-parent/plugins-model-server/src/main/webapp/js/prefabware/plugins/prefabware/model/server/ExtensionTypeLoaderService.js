//to load types via the modelService
define('prefabware/plugins/prefabware/model/server/ExtensionTypeLoaderServer', [ 
       'dojo',
       'prefabware/plugin/Extension',
      
       'prefabware/model/TypeRegistry',
       'prefabware/model/to/TypeLoaderServer',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugins.prefabware.model.server.ExtensionTypeLoaderServer", [prefabware.plugin.Extension], {
		servicePlugin:{inject:'prefabware.service',type:'plugin'},
		typeLoader:null,
		getTypeLoader : function() {
			if (this.typeLoader==null) {
				var service=this.servicePlugin.findService('prefabware.service.model');
				//var url= target +'/'+ 'modelService';
				this.typeLoader = new prefabware.model.to.TypeLoaderServer(service.getUrl());
				}
			return this.typeLoader;
		},	
	});
});
