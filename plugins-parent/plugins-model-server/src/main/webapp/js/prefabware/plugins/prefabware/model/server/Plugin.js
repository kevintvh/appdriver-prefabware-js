//a Plugin
define('prefabware/plugins/prefabware/model/server/Plugin', [ 
       'dojo','prefabware/plugin/Plugin','dojo/_base/array',
       'dojo/aspect',
       'dojo/promise/all',
       'prefabware/model/TypeRegistry',
       'prefabware/model/TypenameResolver',
       'prefabware/plugins/prefabware/model/ExtensionAttributeCustomizer',
       'prefabware/plugins/prefabware/model/ExtensionTypeCustomizer',
       'prefabware/plugins/prefabware/model/ExtensionTypeLifecycle',
       'prefabware/plugins/prefabware/model/server/ExtensionTypeLoaderServer',
       'dojo/_base/declare' ], function(
		dojo, Plugin,array,aspect,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.model.server.Plugin", [prefabware.plugin.Plugin], {
		typeRegistry:null,
		typeResolver:null,
		afterStartup : function() {
			this.typeRegistry=new prefabware.model.TypeRegistry();
			this.typeResolver=new prefabware.model.TypenameResolver(this.typeRegistry);		
			var plugin=this;
			aspect.after(this.typeRegistry.importer,"customizeTypeOptions",dojo.hitch(plugin,plugin.__customizeTypeOptions),true);
			aspect.after(this.typeRegistry.importer,"customizeAttributeOptions",dojo.hitch(plugin,plugin.__customizeAttributeOptions),true);
			aspect.after(this.typeRegistry.importer,"getCustomTypeClass",dojo.hitch(plugin,plugin.__getCustomTypeClass),true);
			aspect.after(this.typeRegistry.importer,"getCustomInstanceClass",dojo.hitch(plugin,plugin.__getCustomInstanceClass),true);
			//attach lifecycle listeners to registry
			this.typeRegistry.on('registered',function(event){
				//attach lifecycle listeners to registered type
				event.type.on('defaultsApplied',dojo.hitch(plugin,plugin.__defaultsApplied));
				event.type.on('changesApplied',dojo.hitch(plugin,plugin.__changesApplied));
			});
			var p1=this.findExtensionByPoint('prefabware.model.typeLoader').then(function(ext){
				if (ext!=undefined) {
					//loader is optional
					this.typeRegistry.loader=ext.getTypeLoader();
				}
			});
			
			var p2=this.findExtensionsByPoint('prefabware.model.types').then(function(extensions){
				array.forEach(extensions,function(extension){
					extension.instance.registerAt(this.typeRegistry);
				},this);
			});
			return promiseAll(p1,p2);
		},	
		getTypeLifecycle : function(type) {
			var pointId='prefabware.model.type.lifecycle';
			var extension=this.findExtensionForType(type,pointId);					
			return extension;
		},
		getTypeRegistry : function() {
			return this.typeRegistry;
		},		
		findType : function(name) {
			return this.getTypeRegistry().findType(name);
		},	
		getTypeResolver : function() {
			this.getTypeRegistry();
			return this.typeResolver;
		},	
		__defaultsApplied:function(event){
			//is called after defaults for the type were applied
			//event ={instance:instance,type:type,...};
			this.matchExtension('prefabware.model.type.lifecycle',this.__optionsType(event.type)).then(function(ext){
				if (ext.length>0) {
					array.forEach(ext,function(extension){
						extension.defaultsApplied(event);
					});
				}
			});
			return;
		},
		__optionsType:function(typeOrName){
			//returns the options to use for matchExtension by type or typename
			return {value:typeOrName};
		},
		__changesApplied:function(event){
			//is called after changes to the instance of the type were applied
			//event ={instance:instance,type:type,...};
			this.matchExtension('prefabware.model.type.lifecycle',this.__optionsType(event.type)).then(function(ext){
			if (ext.length>0) {
				array.forEach(ext,function(extension){
					extension.defaultsApplied(event);
					extension.changesApplied(event);
				});
			}
			});
			return;
		},
		__getCustomTypeClass:function(typeName){
			var ext=this.findExtensionForType('prefabware.model.typeCustomizer',this.__optionsType(typeName));
			if (ext!=undefined) {
				return ext.getCustomTypeClass(typeName);
			}
			return null;
		},
		__getCustomInstanceClass:function(typeName){
			var ext=this.findExtensionForType('prefabware.model.typeCustomizer',this.__optionsType(typeName));
			if (ext!=undefined) {
				return ext.getCustomInstanceClass(typeName);
			}
			return null;
		},
		__customizeTypeOptions:function(options){
			//options.name is the types name
			var ext=this.findExtensionForType('prefabware.model.typeCustomizer',this.__optionsType(options.name));
			if (ext!=undefined) {
				options=ext.customize(options);
			}
			return options;
		},
		__customizeAttributeOptions:function(options,type){
			var filter=function(extension){
				return extension.accepts(type.name+'.'+options.name);
			};
			var customizerPointId='prefabware.model.attributeCustomizer';
			var extensions=this.filterExtensions(filter,customizerPointId);
			if (extensions==undefined||extensions.length===0) {
				// no problem, nothing to do, customization is optional
			} else if (extensions.length > 1) {
				throw 'found ' + extensions.length
						+ ' typeCustomizer, expected exactly 1' + pointId;
			} else {
				//if exactly one extension was found, let it customize
				return extensions[0].customizeAttributeOptions(options,type);
			}
		},
	});
});
