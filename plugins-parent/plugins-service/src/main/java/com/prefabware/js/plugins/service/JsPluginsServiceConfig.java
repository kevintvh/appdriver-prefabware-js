package com.prefabware.js.plugins.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.commons.maven.Maven;
import com.prefabware.js.commons.JsCommonsConfig;
import com.prefabware.js.inject.JsInjectConfig;
import com.prefabware.js.plugin.JsPluginConfig;
import com.prefabware.js.plugins.resource.JsPluginsResourceConfig;
import com.prefabware.js.plugins.server.JsPluginsServerConfig;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@Configuration
@Import({JsPluginsServerConfig.class,JsPluginsResourceConfig.class,JsPluginConfig.class,JsInjectConfig.class,JsCommonsConfig.class})
public class JsPluginsServiceConfig {
	@Bean
	Maven jsPluginsServiceMaven() {
		return new Maven(this.getClass().getPackage().getName()+".filtered");
	}
	@Bean
	WebJar jsPluginsServiceWebJar(@Qualifier("jsPluginsServiceMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	
	@Bean
	ResourceMapping rmJsPluginsServiceWebJar(@Qualifier("jsPluginsServiceMaven") Maven m,@Qualifier("jsPluginsServiceWebJar") WebJar wj) {
		String path = m.getProperty("pfw.static.path");
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
}
