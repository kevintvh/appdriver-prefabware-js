//a remote service
//TODO should be possible to use this without explicit service class
define('prefabware/plugins/prefabware/service/ExtensionService', [ 
       'dojo',
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.service.ExtensionService", [prefabware.plugin.Extension], {
		serverPlugin:{inject:'prefabware.server',type:'plugin'},  	
		name:null,
		port:null,
		urlPath:null,
		url:null,
		service:null,
		constructor : function() {
			this.serverId=this.json.serverId;
			this.name=this.json.name;
			this.port=this.json.port;
			this.urlPath=this.json.urlPath;
		},	
		__createService : function() {
			// this method is called automatically directly after startup
			// subclasses may override this method to create the service
			// and set it as this.service
			//this is just a simple default :
			this.service = {serverId:this.serverId,name:this.name,port:this.port,urlPath:this.urlPath,url:this.url};
		},
		findService : function() {
			// this method is sync
			// subclasses may override this method and may e.g. use parameters
			return this.service;
		},
		startup : function() {
			// async
			// subclasses should not override this method, instead use
			// __createService
			var that=this;
			//find the server of this service
			return this.serverPlugin.findServer(this.serverId).then(function(server){
				var url=server.getUrl();
				if (that.urlPath!=null&&that.urlPath.length>0) {
					url=url+'/'+that.urlPath;
				}
				that.url=url;
				return
			}).then(function(){
				that.__createService();
			});
			}	
	});
});
