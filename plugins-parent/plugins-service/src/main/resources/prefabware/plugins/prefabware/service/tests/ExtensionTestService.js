//to run reports for entities
define('prefabware/plugins/prefabware/service/tests/ExtensionTestService', [ 
       'dojo',
       'prefabware/plugins/prefabware/service/ExtensionService',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugins.prefabware.service.tests.ExtensionTestService", [prefabware.plugins.prefabware.service.ExtensionService], {
		__createService : function() {
			this.service={url:this.url,comment:'this is the test server !'};
		},
	});
});