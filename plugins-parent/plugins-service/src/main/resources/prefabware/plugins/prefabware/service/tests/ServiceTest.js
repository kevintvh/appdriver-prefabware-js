// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.prefabware.service.tests.ServiceTest");
//require all plugins 
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
dojo.require("dojo.Deferred");
doh.register("prefabware.plugin.prefabware.service.tests.ServiceTest", [ 
{
	name : "service",
	timeout:1000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.service.tests',fileName:'serviceTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var servicePlugin=that.registry.findPlugin('prefabware.service');
			doh.assertEqual(servicePlugin.declaredClass,"prefabware.plugins.prefabware.service.Plugin");
			
			servicePlugin.findService('prefabware.service.appdriver.rest').then(function(service){
				doh.assertFalse(service==null);
				doh.assertEqual('http://localhost:8080/prefabware-appdriver/rest',service.url);
				
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);