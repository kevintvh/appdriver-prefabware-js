//a action that is assigned to a type
// matcher.className is optional
//{'extensionPoint':'prefabware.action.action',
//         							  'icon':'dijitEditorIcon dijitEditorIconNewPage',
//         							  'matcher'   : {
//				            			'className'  : 'prefabware.plugins.prefabware.action.ActionMatcher',
//					 					'actionId'   : 'create',
//         							  	'type':'MyType',
//					              					}
define('prefabware/plugins/prefabware/action/ExtensionAction', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/lang', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/declare' ], function(
		dojo,Extension,array,lang) {
	dojo.declare("prefabware.plugins.prefabware.action.ExtensionAction", [prefabware.plugin.Extension], {
		injector:{inject:'injector',type:'injector'},  	
		actionId:null,//this is used as key for the icon and label
		actionClassName:null,
		actionClass:null,
		constructor : function(options) {
			//get nullsafe, json.matcher may be null
			this.actionId=lang.getObject("json.matcher.actionId",false,options);
			if (this.actionId===null) {
				this.actionId=this.id;
			}
			if (options.json.actionClass!=null) {
				this.actionClassName=options.json.actionClass;
			}else{
				this.actionClassName='prefabware.plugins.prefabware.action.Action';
			}
		},
		startup : function() {
			var that=this;
			return this.loader.load(this.actionClassName).then(function(clazz){
				that.actionClass=clazz;
			});
		},	
		createAction : function(options) {
			//this extension does not know, for which type it was matched
			//so it cannot get the label, this is done by the ActionPlugin 
			var aOptions={actionId:this.actionId};
			lang.mixin(aOptions,options);
			var action=new this.actionClass(aOptions);
			this.injector.injectInto(action);
			action.startup();
			return action;
		},
	});
});
