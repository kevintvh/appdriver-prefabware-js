// DID NEVER RUN !
dojo.provide("prefabware.plugins.prefabware.action.tests.PluginTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.model.Type");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.action.tests.PluginTest", [ {
	name : "action-plugin",
	timeout:1000,
	registry : null,
	
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.action.tests',
			fileName : 'pluginTest-registry.json'
		});
	},
	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		var p1=null;
		var p2=null;
		var type=new prefabware.model.Type("MyType");//the simplest possible type
		this.registry.startup().then(function(registry){
		var plugin=registry.findPlugin('prefabware.action');
			doh.assertTrue(plugin != null);
			doh.assertEqual("prefabware.plugins.prefabware.action.Plugin",plugin.declaredClass);
			p1=plugin.fetchAction("testAction",type).then(function(action) {
				doh.assertEqual("prefabware.plugins.prefabware.action.tests.TestAction",action.declaredClass);
				doh.assertEqual(plugin,action.actionPlugin,"the actionPlugin should have been injected into the action");
				doh.assertEqual('icon-file',action.icon);
				doh.assertEqual('testAction',action.actionId);
				if (prefabware.lang.locale().indexOf('de')!==0) {
					doh.assertEqual('test action de',action.label);
				}
			});
			var actionId="actionWithoutLabel";
			p2=plugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(action) {
				doh.assertEqual('actionWithoutLabel',action.icon);
				doh.assertEqual('actionWithoutLabel',action.label);
				doh.assertEqual('actionWithoutLabel',action.actionId);
			});
		});
		dojo.promise.all([p1,p2]).then(function(){
			testDeferred.callback(true);
		});
		return testDeferred;
			},
	tearDown : function() {
	}
} ]);