define('prefabware/plugins/prefabware/action/tests/TestAction', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/when',
       'prefabware/plugins/prefabware/action/Action',
       'prefabware/util/OptionsMixin',
       'dojo/_base/declare' ], function(
		dojo,Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.action.tests.TestAction", [prefabware.plugins.prefabware.action.Action,prefabware.util.OptionsMixin], {
		actionPlugin:{inject:'prefabware.action',type:'plugin'}, 
		constructor : function(options) {
		},
		startup : function() {
		},	
	});
});