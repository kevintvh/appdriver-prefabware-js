//a Plugin
define('prefabware/plugins/prefabware/action/Plugin', [ 'dojo', 
		'dojo/_base/array', 
		 'prefabware/lang',
		 'dojo/promise/all',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo, array, lang,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.action.Plugin",[ prefabware.plugin.Plugin ], {
			modelPlugin    :{inject:'prefabware.model','type':'plugin'},
			resourcePlugin :{inject:'prefabware.resource','type':'plugin'},
			typeRegistry   :null,
				afterStartup : function() {
					this.typeRegistry=this.modelPlugin.typeRegistry;
				},
				fetchActions : function(actionIds,type) {
					//TODO remove
					var pActions=new Array();
					array.forEach(actionIds,function(actionId){
						pActions.push(this.fetchAction(actionId,type));
					},this);
					return promiseAll(pActions);
				},
				fetchAction : function(actionId,options) {
					//async
					//returns allways a new action instance, because callers may attach to the execute method
					if (options==null) {
						options={};
					}
					var that=this;
					var extensionOptions={actionId:actionId};	
					//legacy calls with a second parameter type or a string with a typename
					if (options instanceof prefabware.model.Type) {
						extensionOptions.type=options;	
					}else if (typeof options=='string') {
							var type=this.typeRegistry.findType(options);
							extensionOptions.type=type;	
					}else{
					//make one object from all arguments
						extensionOptions=dojo.mixin(extensionOptions,options);
					}
					var outer={};
					var i18nKey=null;
					//returns the extension for this type with the given actionId
					return this.matchExtension('prefabware.action.action',extensionOptions).then(function(extension) {
						//decide what to take as key for label and icon
						//actions in menu use a name like the type.name
						//actions of buttons use a verb like the actionId
						if (options.i18nKey!=null) {
							//1st - if a key was provided with the options, use it
							i18nKey=options.i18nKey;
						}else if (extensionOptions.type!=null){
							//2st - the name of the type for which the extension was matched is the key
							i18nKey=extensionOptions.type.name;
						}else{
							//3nd - use the actionId 
							i18nKey=extension.actionId;
						}
						outer.extension=extension;
						return that.resourcePlugin.getLocalization(i18nKey,'label');
						})					
						.then(function(label){
							 outer.label=label;
							})
						.then(function(){
							//the icon for an action is allway 
							return that.resourcePlugin.getLocalization(i18nKey,'icon');
						})
						.then(function(icon){
							return outer.extension.createAction({icon:icon,label:outer.label,type:extensionOptions.type});
						});
				},
			});
});
