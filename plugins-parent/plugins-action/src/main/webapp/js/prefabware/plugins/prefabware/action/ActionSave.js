define('prefabware/plugins/prefabware/action/ActionSave', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.action.ActionSave", [prefabware.plugins.prefabware.action.Action], {
		isPersistenceAction:true,
	});
});