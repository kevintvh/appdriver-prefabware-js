//to access the actions of the prefabware.action plugin
define('prefabware/plugins/prefabware/action/_ActionAccessMixin', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo,Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.action._ActionAccessMixin", [prefabware.plugin.Extension], {
		actionPlugin:{inject:'prefabware.action',type:'plugin'}, 
		__getAction : function(actionId,type) {
			//returns the action registerd for the given id and type
			var action=this.actionPlugin.getAction(actionId,type);
			//action.label=this.extendedPlugin.getApplication().labelOf(action.actionId);
			return action;
		},
		__getActions : function(actionIds,type) {
			var actions=new Array();
			var that=this;
			array.forEach(actionIds,function(actionId){
				var action=that.__getAction(actionId,type);
				actions.push(action);
			});
			return actions;
		},
	});
});
