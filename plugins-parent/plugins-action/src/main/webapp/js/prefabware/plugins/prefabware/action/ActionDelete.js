define('prefabware/plugins/prefabware/action/ActionDelete', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.action.ActionDelete", [prefabware.plugins.prefabware.action.Action], {
		isPersistenceAction:true,
	});
});