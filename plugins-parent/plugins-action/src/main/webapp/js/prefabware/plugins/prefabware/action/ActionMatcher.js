//matches the actionId aginst the configured one
//and matches the name of a type aginst the configured name
//if this matcher should match against an actionId only, without a type
//leave this.type==null
define('prefabware/plugins/prefabware/action/ActionMatcher', [ 'dojo',
        'prefabware/model/match/TypeMatcher', 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.action.ActionMatcher",
			[ prefabware.model.match.TypeMatcher ], {
				actionId:null,
				constructor : function(options) {
					this.actionId = options.actionId;
				},
				accepts : function(options) {
					//return true, if this matcher accepts the given options
					//it doesnt matter if he matches or not, just if he can calculate a match on that
					if (options.actionId==null) {
						return false;
					}
					if (this.type!=null) {
						//consider the TypeMatcher.accept
						return this.inherited(arguments);
					}else{
						return true;
					}
				},
				matches : function(options) {
					if (this.actionId!=options.actionId) {
						//if its not the same kind of action, never matches						
						return this.NO_MATCH;
					}
					if (this.type!=null) {
						//consider the TypeMatcher.match
						return this.inherited(arguments);
					}else{
						//its a match
						var that=this;
						result=	{
								matcher:that,
								degree: this.EXACT_MATCH.degree
								};
						prefabware.lang.log('with this.actionId='+this.actionId+' matches options.actionId='+options.actionId+' result.degree='+result.degree);
						return result;
					}
				},
			});
});
