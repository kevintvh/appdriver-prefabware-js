package com.prefabware.js.plugins.action;
import org.junit.Before;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;

public class DojoIntegrationTest {
    private DojoTestSupport testSupport; 
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/plugins-action/js");
    	testSupport.add("prefabware/plugins/prefabware/action/tests/PluginTest.html");    	
    }
    
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}