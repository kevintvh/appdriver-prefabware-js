define('prefabware/plugins/prefabware/adminlte/widgets/SidebarNode', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/adminlte/widgets/SidebarNode.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/SidebarNode',
       'dojo/_base/declare' ], function(
		dojo, array,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.adminlte.widgets.SidebarNode", [prefabware.plugins.prefabware.bootstrap.widgets.SidebarNode], {
		templateString: html,
		constructor : function(params) {
			this.preventDefault=true;
		},
		buildRendering : function() {
			this.inherited(arguments);
			/* Sidebar tree view */
		    //$(".sidebar .treeview").tree();
			$(this.domNode).tree();
			return;
		}, 
		onClick: function(e){ 
			return;
		}, 
	});
});
