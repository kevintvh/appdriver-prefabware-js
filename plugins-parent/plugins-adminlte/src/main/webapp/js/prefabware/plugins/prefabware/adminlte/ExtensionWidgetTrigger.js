//to be called on widget lifecycle events
define('prefabware/plugins/prefabware/adminlte/ExtensionWidgetTrigger', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,globalLabels) {
	dojo.declare("prefabware.plugins.prefabware.adminlte.ExtensionWidgetTrigger", [prefabware.plugin.Extension], {
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
		},	
		
		afterWidgetStartup : function(widget){
			//async
			// will be called after startup of the widget
			//Activate tooltips
			if (widget==null||widget.domNode==null) {
				return;
			}
			var node=$(widget.domNode);
			if (typeof $.fn.tooltip==="function") {
			node.find("[data-toggle='tooltip']").tooltip();
			}
		    /*
		     * We are gonna initialize all checkbox and radio inputs to 
		     * iCheck plugin in.
		     * You can find the documentation at http://fronteed.com/iCheck/
		     */
			if (typeof $.fn.iCheck==="function") {
		    node.find("input[type='checkbox'], input[type='radio']").iCheck({
		    	//_line did not work !
		        checkboxClass: 'icheckbox_minimal',
		        radioClass: 'iradio_minimal'
		    });
			}
		    //Make sortable Using jquery UI
		    //see http://jqueryui.com/sortable/
		    //TODO activate with https://bitbucket.org/sisele/prefabware-js/issue/35
			if (typeof $.fn.sortable==="function") {
		    $(".connectedSortable").sortable({
		        placeholder: "sort-highlight",
		        connectWith: ".connectedSortable",
		        handle: ".box-header, .nav-tabs",
		        forcePlaceholderSize: true,
		        zIndex: 999999
		    }).disableSelection();
			}
		},
	});
});
