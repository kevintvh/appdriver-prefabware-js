// a box is a container for a table or form
// it has abuilt in search input to search in its content
// the content is invisible by default
define("prefabware/plugins/prefabware/adminlte/widgets/Box", [  
       "dojo",  
       "dojo/_base/array",
       "dojo/aspect",
       "dojo/dom-construct",
       "dojo/dom-style",
       
       "dojo/text!prefabware/plugins/prefabware/adminlte/widgets/Box.html",
       "prefabware/plugins/prefabware/bootstrap/widgets/_SearchInput",
       "prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer",
       "dijit/_Widget",
       "dijit/_Container",
       "dijit/_Templated",
       "dojo/_base/declare" ], function(
		dojo, array,aspect,domConstruct,domStyle,html) {
	dojo.declare("prefabware.plugins.prefabware.adminlte.widgets.Box", [dijit._Widget, dijit._Container,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._SearchInput,prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer  ], {
		templateString: html,
		span:12,//full width
		icon:' icon-warning-sign',//icon 
		label:' box label',//icon
		style:'default',//style
		id:null,
		constructor : function(options) {
			
		},
		startup : function() {
			this.inherited(arguments); 
		},		
	
		showSearch : function(value) {
			if (value==null||value==true) {
				domStyle.set(this.searchFormGroup,"display","");
			}else{
				domStyle.set(this.searchFormGroup, "display", "none");
			}
			
		},		
		buildRendering : function() {
			this.inherited(arguments);
			var box=$(this.domNode);
			 /*     
		     * Add collapse and remove events to boxes
		     */
		    box.find("[data-widget='collapse']").click(function() {
		        //Find the box parent        
		       // var box = $(this).parents(".box").first();
		        //Find the body and the footer
		        var bf = box.find(".box-body, .box-footer");
		        if (!box.hasClass("collapsed-box")) {
		            box.addClass("collapsed-box");
		            bf.slideUp();
		        } else {
		            box.removeClass("collapsed-box");
		            bf.slideDown();
		        }
		    });
		    
//		    box.find("[data-widget='remove']").click(function() {
//		        //Find the box parent        
//		        //var box = $(this).parents(".box").first();
//		        box.slideUp();
//		    });
		},
		_onCloseClick : function() {
			//called when the close button was clicked
			var box=$(this.domNode);
			box.slideUp();
		},
		addAction : function(action) {
			//if an action'close' is added, close the box when it is executed
			var that=this;
			if (action.actionId=='close') {
				aspect.after(action,"onExecute",function(){
					that._onCloseClick();
				});
			}
			this.inherited(arguments);
		},
		_placeActionWidget : function(actionWidget) {
			//places the given action widget in the domnode of this widget
			//position 0 is the searchbox		
			//prepends a span as a seperator between buttons
			domConstruct.place("<span> </span>", this.attachPointActionLink, 2);
			domConstruct.place(actionWidget.domNode, this.attachPointActionLink, 3);
		},
		
	});
});
