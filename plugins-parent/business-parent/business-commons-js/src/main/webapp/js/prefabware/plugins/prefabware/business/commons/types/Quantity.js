//
define('prefabware/plugins/prefabware/business/commons/types/Quantity', [ 
                                        'dojo',
                                        'dojo/number',
                                        'dojo/_base/declare' ],
                                function(dojo,localequantityUnit) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.types.Quantity",null, {
		__label : function() {
			var unitName='';
			if (this.quantityUnit!=null) {
				unitName=this.quantityUnit.name;
			}
			return this.quantityValue+' '+unitName;
		},
		__resolveThisProxy : function() {
			var that=this;
			return this.inherited(arguments).then(function(resolved){				
				if (that.quantityUnit!=null&&that.quantityUnit.__isProxy&&!that.quantityUnit.__isResolved) {
					return that.quantityUnit.resolve().then(function(){
						return prefabware.lang.deferredValue(resolved);
					});
				}else{
					return prefabware.lang.deferredValue(resolved);
				}
			});
		}

	});
});
