// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.business.commons.tests.service.TypeLoaderTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.business.commons.tests.service.TypeLoaderTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.business.commons.tests.service',
			fileName : 'typeLoaderTest-registry.json'
		});
	},
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		
		this.registry.startup().then(function(registry){
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);

			var amountType=that.typeRegistry.findType('com.prefabware.business.currency.Amount');
			doh.assertTrue(amountType!=null);
			var amount=amountType.create();
			doh.assertTrue(amount.isCustomAmountClass);
			
			var currencyType=that.typeRegistry.findType('com.prefabware.business.currency.Currency');
			doh.assertTrue(currencyType!=null);
			
			var code=currencyType.getAttribute('code');
			doh.assertTrue(code!=null);
			doh.assertTrue(code.isKey);//should be configrued by attributeCustomizer
			testDeferred.callback(true);
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);