//to load types from a server. the loader only loads the raw json object with the type definition
//but does not convert them into a type nor register it to the typeRegistry
//load types using .load(typeName);
define('prefabware/plugins/prefabware/business/commons/service/DocumentService', [
        'dojo',
        'dojo/Deferred',
        'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.service.DocumentService", null, {
		url:null,
		constructor : function(url) {
			if (url==undefined||url==null) {
				throw 'parameter url is mandatory';
			}
			this.url=url;
		},
		createReport:function(entityType,entityId){
			//async
			//runs the report for the entity of the type and id
			//returns a link to the download
			return this.callServer(this.url+'/createReport'+'?entityType='+entityType+'&entityId='+entityId)
			.then(function(downloadUrl){
				//the downloadUrl may be surounded by quotes, remove them !
				return downloadUrl.split('"').join('');
			});
		},
		callServer:function(queryUrl){
			//async
			var deferred=new dojo.Deferred();
			
			//does really resolve calling the server
			dojo.xhrGet({
				// The target URL on the webserver, per convention this is the
				// reference itself
				url : queryUrl,
				sync : true,
				// send JSON
				contentType : 'application/pdf',
				// receive JSON
				//handleAs : 'json',
				// Timeout in milliseconds:
				timeout : 5000,
				// Event handler on successful call:
				load :function(data) {
					return deferred.resolve(data);
				},

				// Event handler on errors:
				error : function(response, ioArgs) {
					debug.dir(response);
					return deferred.reject(response);
				}
			});
			return deferred.promise;
		}		
	});
});