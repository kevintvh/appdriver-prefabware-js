define('prefabware/plugins/prefabware/business/commons/action/ActionCreateReport', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.action.ActionCreateReport", [prefabware.plugins.prefabware.action.Action], {
		servicePlugin:{inject:'prefabware.service',type:'plugin'},
		service:null,
		type:null,
		constructor : function() {
		},
		startup : function() {
			return this.servicePlugin.findService('prefabware.service.document').then(function(service){
				this.service=service;
			});
		},
		onExecute : function(context) {
			//clients can hook here with aspect.after
			//to run when the action is executed
			return service.createReport(this.type.name,context.id)
			.then(function(downloadLink){
				window.open(downloadLink);
			});
		},
	});
});