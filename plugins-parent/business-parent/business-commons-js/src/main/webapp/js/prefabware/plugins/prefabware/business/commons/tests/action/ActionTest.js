// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.business.commons.tests.action.ActionTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.business.commons.tests.action.ActionTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.business.commons.tests.action',
			fileName : 'actionTest-registry.json'
		});
	},
	runTest : function() {
		var testDeferred = new doh.Deferred();
		
		this.registry.startup().then(function(registry){
			var actionPlugin = registry.findPlugin('prefabware.action');
			doh.assertFalse(actionPlugin==null);
			var storePlugin = registry.findPlugin('prefabware.store');
			doh.assertFalse(storePlugin==null);
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			var typeRegistry = modelPlugin.getTypeRegistry();
			var personType=typeRegistry.findType("com.prefabware.business.party.Person");
			doh.assertFalse(personType==null);
			storePlugin.createStore({type:personType}).then(function(store){
					return store.query({name:'*'});
			}).then(function(persons){
			actionPlugin.fetchAction('createReport',"com.prefabware.business.party.Person").then(function(action){
				var id=persons[0].pfw_id;
				action.execute({type:"com.prefabware.business.party.Person",id:id})
					.then(function(){
						testDeferred.callback(true);
					},function(args){
						testDeferred.errback(args);
					});
			});
			});
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);