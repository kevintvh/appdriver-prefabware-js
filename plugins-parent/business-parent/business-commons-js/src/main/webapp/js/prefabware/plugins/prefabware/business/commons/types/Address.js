//
define('prefabware/plugins/prefabware/business/commons/types/Address', [ 
                                        'dojo',
                                        'dojo/currency',
                                        'dojo/_base/declare' ],
                                function(dojo,localeCurrency) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.types.Address",null, {
		__label : function() {
			var cityName='';
			if (this.city!=null) {
				cityName=this.city.name;
			}
			return this.street+', '+this.zipCode+' '+cityName;
		},
		__resolveThisProxy : function() {
			var that=this;
			return this.inherited(arguments).then(function(resolved){				
				if (that.city!=null&&that.city.__isProxy&&!that.city.__isResolved) {
					return that.city.resolve().then(function(){
						return prefabware.lang.deferredValue(resolved);
					});
				}else{
					return prefabware.lang.deferredValue(resolved);
				}
			});
		}

	});
});
