//to run reports for entities
define('prefabware/plugins/prefabware/business/commons/ExtensionDocumentService', [ 
       'dojo',
       'prefabware/plugins/prefabware/business/commons/service/DocumentService',
       'prefabware/plugins/prefabware/service/ExtensionService',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.ExtensionDocumentService", [prefabware.plugins.prefabware.service.ExtensionService], {
		__createService : function() {
			this.service=new prefabware.plugins.prefabware.business.commons.service.DocumentService(this.url);
		},
		createReport : function(id) {
			this.service.createReport(id);
		},		
	});
});