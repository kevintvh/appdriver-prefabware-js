//
define('prefabware/plugins/prefabware/business/commons/types/Amount', [ 
                                        'dojo',
                                        'prefabware/model/Value',
                                        'prefabware/lang',
                                        'dojo/_base/declare' ],function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.types.Amount", [prefabware.model.Value], {
		constructor : function() {
		},
		__label : function() {
			return this.currency.__format(this.value);
		},
		__resolveThisProxy : function() {
			var that=this;
			return this.inherited(arguments).then(function(resolved){				
				if (that.currency!=null&&that.currency.__isProxy&&!that.currency.__isResolved) {
					return that.currency.resolve().then(function(){
						return prefabware.lang.deferredValue(resolved);
					});
				}else{
					return prefabware.lang.deferredValue(resolved);
				}
			});
		}
	});
});
