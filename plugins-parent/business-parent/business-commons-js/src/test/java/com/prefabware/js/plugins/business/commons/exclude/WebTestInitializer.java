package com.prefabware.js.plugins.business.commons.exclude;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.prefabware.business.commons.include.BusinessTestConfiguration;
import com.prefabware.business.commons.web.include.BusinessWebTestConfiguration;
import com.prefabware.environment.bootstraped.StartupSupport;
import com.prefabware.rest.server.Constants;

 
public final class WebTestInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer{
	@Override
	protected WebApplicationContext createRootApplicationContext() {
		WebApplicationContext rootCtx = super.createRootApplicationContext();
		new StartupSupport((ConfigurableApplicationContext) rootCtx);
		return rootCtx;
	}
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{BusinessWebTestConfiguration.class};
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[]{BusinessTestConfiguration.class};
	}
	@Override
	protected String[] getServletMappings() {
		return new String[]{ Constants.API_MAPPING};
	}
}