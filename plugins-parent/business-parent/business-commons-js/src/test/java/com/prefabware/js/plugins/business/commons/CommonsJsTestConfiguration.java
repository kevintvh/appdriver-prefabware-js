package com.prefabware.js.plugins.business.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.prefabware.business.commons.BusinessConfiguration;
import com.prefabware.business.commons.include.CommonsEntityBuilder;
import com.prefabware.rest.server.spring.RestServerJpaConfiguration;
import com.prefabware.rest.server.spring.config.RestServerJacksonNonWebConfig;


@Configuration
@EnableTransactionManagement
@Import({BusinessConfiguration.class,RestServerJacksonNonWebConfig.class})
public class CommonsJsTestConfiguration extends RestServerJpaConfiguration{
	
	@Bean
	public CommonsEntityBuilder testData() {
		return new CommonsEntityBuilder();
	};
	@Bean
	public String[] jpaEntityPackages() {
		return new String[]{"com.prefabware.business", "com.prefabware.jpa"};
	}
}