define({
	name : 'Name',
	firstName : 'Vorname',
	dateOfBirth : 'Geburtsdatum',
	creditRisk : "Kreditrisiko",
	amount : "Betrag",
	User : "Anwender",
	Role : "Rolle",
	role : "Rolle",
	invoiceDate : "Rechnungsdatum",
	number : "Nummer",
	text : "Text",
	Right_ : "Berechtigung",
	applicationTitle : 'Rechnungsverwaltung',
	title : 'Titel',
	code : 'Code',
	symbol : 'Symbol',
	decimals : "Dezimalst.",
	description : "Beschreibung",
	value : "Wert",
	amount : "Betrag",
	product : "Produkt",
	price : "Preis",
	quantity : "Menge",
	quantityValue : "Wert",
	quantityUnit : "Mengeneinh.",
	baseUnit:"Basiseinheit",	
    factor:"Faktor",	
	info : "Info",
	invoice : "Rechnung",
	invoiceDate : "Rechnungsdatum",
	invoiceLine : "Rechnungsposition",
	lines : "Positionen",
	country:"Land",
	city:"Stadt",
	street:"Straße",
	basicData:'Basisdaten',
	masterData:'Stammdaten',
	transactionData:'Bewegungsdaten',
	currency : "Währung"
});