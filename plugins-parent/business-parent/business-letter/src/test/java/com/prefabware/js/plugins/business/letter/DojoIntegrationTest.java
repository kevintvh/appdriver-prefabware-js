package com.prefabware.js.plugins.business.letter;
import org.junit.Before;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;

public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/business-letter/js");
    	testSupport.add("prefabware/plugins/prefabware/business/letter/tests/PluginTest.html");    	
    }
    
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}