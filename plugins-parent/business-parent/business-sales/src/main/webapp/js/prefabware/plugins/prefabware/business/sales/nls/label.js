define({
  root: {
	  	sales:'Sales',
		com_prefabware_business_sales_entity_Customer : 'Customer',
		com_prefabware_business_sales_invoice_Invoice : 'Invoice',
		com_prefabware_business_sales_invoice_InvoiceLine : 'Invoice lines',
		Invoice:'Invoice',
		CurrencyAndValues:'Currency and Values',
		Customer:'Customer',
		customer:'Customer',
		paymentTerms:'Payment term',
		totalNet:'Total net',
		totalVat:'Total VAT',
		totalGross:'Total gross',
  },
  de: true
});