package com.prefabware.js.plugins.business.sales.web;
import org.junit.Before;

import org.junit.Ignore;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;
//no tests !!!
@Ignore
public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/business-sales/js");
    	//test needs behavoir which doesnt work
    	//testSupport.add("prefabware/plugins/prefabware/business/sales/tests/invoiceLine/InvoiceLineTest.html");    	
    }
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}