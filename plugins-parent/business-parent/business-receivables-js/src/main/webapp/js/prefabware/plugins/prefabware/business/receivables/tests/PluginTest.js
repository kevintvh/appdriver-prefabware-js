// DID NEVER RUN !
dojo.provide("prefabware.plugins.prefabware.business.commons.tests.PluginTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.business.commons.tests.PluginTest", [ {
	name : "plugin-business-receivables",
	timeout : 1000,
	typeRegistry : null,
	registry : null,

	setUpDeferred : null,
	setUp : function() {
		var that = this;
	
		this.setUpDeferred = new doh.Deferred();
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.business.commons.tests',
			fileName : 'pluginTest-registry.json'
		});
		this.registry.startup().then(function(registry) {
			var commonsPlugin = registry.findPlugin('prefabware.business.commons');
			doh.assertFalse(commonsPlugin==null);
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			var loader=that.typeRegistry.loader;
			loader.add("com.prefabware.business.commons.Currency","prefabware.plugins.prefabware.business.commons.tests", "currency-type.json");
			loader.add("com.prefabware.business.commons.Amount","prefabware.plugins.prefabware.business.commons.tests", "amount-type.json");
			
			that.setUpDeferred.resolve();
		});
	},

	runTest : function() {
		var that = this;
		var testDeferred = new doh.Deferred();
		this.setUpDeferred.then(function() {
						
			dojo.promise.all([
			     that.testTypes(),
			    // that.testLabel(),
			     ]).then(function(results){
				 testDeferred.callback(true);
			  });
		});

		return testDeferred;
	},
	testTypes : function() {
		//to test how to get a message
		var deferred = new dojo.Deferred();
		var that = this;
		
		var amountType=that.typeRegistry.findType('com.prefabware.business.commons.Amount');
		doh.assertFalse(amountType==null);
		var amount=amountType.create();
		amount.__resolveProxies();
		deferred.resolve(true);
		
//		var uiPlugin = that.registry.findPlugin('prefabware.ui');
//		uiPlugin.createMessage("UIM0001", {
//			entity : 'TestEntity'
//		}).then(
//				function(msg) {
//					doh.assertFalse(msg == null);
//					doh.assertEqual('Löschen von {entity} fehlgeschlagen !',
//							msg.title);
//					msg.replace();
//					doh.assertEqual(msg.title,
//							'Löschen von TestEntity fehlgeschlagen !');
//					deferred.resolve(true);
//				});
		return deferred;
	},
	testLabel : function() {
		//to test how to get a label
		var deferred = new dojo.Deferred();
		var that = this;
		var uiPlugin = that.registry.findPlugin('prefabware.ui');
		return uiPlugin.labelOf("pfw_id").then(function(label){
			doh.assertEqual(label,"Id");
			deferred.resolve(true);
		});;
		return deferred;
	},
	tearDown : function() {
	}
} ]);