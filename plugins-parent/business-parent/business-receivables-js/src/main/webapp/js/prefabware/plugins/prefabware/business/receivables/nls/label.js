define({
  root: {
	  	receivables:'Receivables',
	  	reports:'Reports',
	  	com_prefabware_business_receivables_entity_Amt:"Office",
	  	com_prefabware_business_receivables_entity_ProfitCenter:"Profit center",
	  	com_prefabware_business_receivables_entity_Dezernat:"Department",
	  	com_prefabware_business_receivables_entity_OffenePosten:"Open item",
	  	com_prefabware_business_receivables_entity_ReportOpNachHoehe:"Open items by value",
  },
  de: true
});