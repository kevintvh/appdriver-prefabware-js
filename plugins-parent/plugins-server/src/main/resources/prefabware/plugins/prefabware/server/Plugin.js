//a Plugin
define('prefabware/plugins/prefabware/server/Plugin', [ 'dojo',
		'dojo/_base/array', 'dojo/_base/lang',
		 'prefabware/plugins/prefabware/server/ExtensionServer',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo, array, lang) {
	dojo.declare("prefabware.plugins.prefabware.server.Plugin",
			[ prefabware.plugin.Plugin ], {
		findServer : function(id) {
			//async
			//returns a promise with a server of the  given extensionid
			return this.fetchExtensionById(id);
				},
			});
});
