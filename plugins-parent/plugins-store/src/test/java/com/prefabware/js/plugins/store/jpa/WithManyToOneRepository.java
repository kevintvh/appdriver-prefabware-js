package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="withManyToOne")
public interface WithManyToOneRepository extends PagingAndSortingRepository<WithManyToOne, Long> {
	public List<WithManyToOne> findByCode(@Param("code")String code);
}
