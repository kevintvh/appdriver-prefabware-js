package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@Entity
public class WithManyToOne extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;	
	
	@KeyAttribute
	public String code;
	
	@ManyToOne
	public WithString one;
	
	public WithManyToOne() {
		super();
	}

	@Override
	public String toString() {
		return "code" + code ;
	}
	
}
