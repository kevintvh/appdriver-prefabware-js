package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="country")
public interface CountryRepository extends PagingAndSortingRepository<Country, Long> {
	public List<Country> findByCode(@Param("code")String code);
}
