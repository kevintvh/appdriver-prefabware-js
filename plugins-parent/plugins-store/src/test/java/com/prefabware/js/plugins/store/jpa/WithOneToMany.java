package com.prefabware.js.plugins.store.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@Entity
public class WithOneToMany extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;	
	
	@KeyAttribute
	public String code;
	
	@OneToMany
	public List<WithString> many=new ArrayList<>();
	
	public WithOneToMany() {
		super();
	}

	@Override
	public String toString() {
		return "code" + code ;
	}
	
}
