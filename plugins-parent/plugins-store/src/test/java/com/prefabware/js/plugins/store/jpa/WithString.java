package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@Entity
public class WithString extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;	
	
	@KeyAttribute
	public String code;
	
	public WithString() {
		super();
	}

	@Override
	public String toString() {
		return "code" + code ;
	}
	
}
