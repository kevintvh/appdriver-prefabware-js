package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="withString")
public interface WithStringRepository extends PagingAndSortingRepository<WithString, Long> {
	public List<WithString> findByCode(@Param("code")String code);
}
