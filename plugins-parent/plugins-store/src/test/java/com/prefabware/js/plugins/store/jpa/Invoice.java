package com.prefabware.js.plugins.store.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.Attribute;
@Entity
public class Invoice extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;
	@Attribute(order=10)
	public String number;
	
	@Attribute(order=50,contained=true)
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@OrderBy("number ASC")
	public List<InvoiceLine> lines=new ArrayList<InvoiceLine>();
		
		public Invoice() {
			super();
		}
		public boolean add(InvoiceLine e) {
			return lines.add(e);
		}
		public BigDecimal getTotalGross(){
			return new BigDecimal("123.45");
		}
	}


