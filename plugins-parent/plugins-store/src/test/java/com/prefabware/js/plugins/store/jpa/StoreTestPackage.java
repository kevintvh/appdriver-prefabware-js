package com.prefabware.js.plugins.store.jpa;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.prefabware.jpa.config.EntityPackage;
@EnableJpaRepositories(basePackageClasses = { CityRepository.class,
		CountryRepository.class })
public class StoreTestPackage extends EntityPackage {

	public StoreTestPackage() {
		super(Invoice.class);
	}

}
