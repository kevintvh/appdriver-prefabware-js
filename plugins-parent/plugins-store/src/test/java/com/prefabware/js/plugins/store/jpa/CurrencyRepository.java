package com.prefabware.js.plugins.store.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="currency")
public interface CurrencyRepository extends PagingAndSortingRepository<Currency, Long> {
	public Page<Currency> findByCodeLikeIgnoreCase(@Param("code")String code,Pageable pageable);
}
