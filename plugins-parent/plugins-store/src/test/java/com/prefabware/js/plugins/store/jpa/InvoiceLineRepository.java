package com.prefabware.js.plugins.store.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
/**
 * this repository must be defined so that spring can calculate hrefs for InvoiceLines !
 * without this, Invoices do not contain lines in JSON
 * @author Stefan Isele
 *
 */
@RestResource(path="invoiceLine")
public interface InvoiceLineRepository extends PagingAndSortingRepository<InvoiceLine, Long> {
	public Page<InvoiceLine> findByNumberLikeIgnoreCase(@Param("number")String number,Pageable pageable);
}
