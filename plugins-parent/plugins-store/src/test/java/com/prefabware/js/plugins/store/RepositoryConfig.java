package com.prefabware.js.plugins.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prefabware.js.plugins.store.jpa.Country;

@Configuration
public class RepositoryConfig extends
RepositoryRestMvcConfiguration {

@Autowired(required = false)
private Jackson2ObjectMapperBuilder objectMapperBuilder;

@Bean
@ConfigurationProperties(prefix = "spring.data.rest")
@Override
public RepositoryRestConfiguration config() {
return super.config();
}

@Override
protected void configureJacksonObjectMapper(ObjectMapper objectMapper) {
if (this.objectMapperBuilder != null) {
	this.objectMapperBuilder.configure(objectMapper);
}
}

	@Override
	protected void configureRepositoryRestConfiguration(
			RepositoryRestConfiguration config) {
		config.exposeIdsFor(Country.class);
	}
}