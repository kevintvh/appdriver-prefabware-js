package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import com.prefabware.jpa.JpaEntity;

@Entity
public class WithEmbeddedReference extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;

	/**
	 * named code1 here to avoid repeated column mapping
	 */
	public String code1;
	@Embedded public EmbeddableWithReference embeddedWithReference;

	public WithEmbeddedReference() {
		super();
	}
}