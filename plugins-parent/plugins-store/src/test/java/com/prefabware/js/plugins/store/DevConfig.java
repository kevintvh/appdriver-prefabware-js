package com.prefabware.js.plugins.store;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import com.prefabware.web.ResourceMappings.ResourceMapping;


/**
 * to map the src/main/resources of this module to the root of the classpath, * 
 * this allows to use changed resources without build or restart  
 * This config is ment to be used during development only
 * If you use this config, you have to make sure that src/main/resources is actually in your classpath,
 * e.g. by adding it in the runtime config of your ide
 * Beans of this config may be annotated with @Primary to overwrite those with the same name 
 * that may be in the classpath for non-dev use 
 * @author stefan
 *
 */
public class DevConfig {
	@Bean()
	@Primary()
	ResourceMapping rmJsPluginsStoreWebJar() {
		return new ResourceMapping("/webjars/prefabware/plugins/prefabware/store/**",
				"classpath:/prefabware/plugins/prefabware/store/");
	}
}
