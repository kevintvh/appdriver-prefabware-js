package com.prefabware.js.plugins.store.jackson;

import java.util.LinkedHashSet;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.prefabware.js.plugins.store.jpa.Amount;
import com.prefabware.js.plugins.store.jpa.EmbeddableWithReference;
import com.prefabware.rest.server.DateTimeDeserializer;
import com.prefabware.rest.server.DateTimeSerializer;
import com.prefabware.rest.server.ValueObjectSerializer;
import com.prefabware.rest.server.spring.RestServerJacksonSupport.RegisterableModule;
/**
 * to serialize model types t json
 * @author Stefan Isele
 *
 */
@Component
public class StoreTestModule extends SimpleModule implements RegisterableModule,ApplicationListener<ContextRefreshedEvent> {
	private static final long serialVersionUID = 1L;
	@Autowired AutowireCapableBeanFactory beanFactory;
	Set<Object> toAutowire=new LinkedHashSet<Object>();
	public StoreTestModule() {}
	@Override public void setupModule(SetupContext context) {
		  SimpleSerializers serializers = new SimpleSerializers();
		  SimpleDeserializers deserializers = new SimpleDeserializers();
		  serializers.addSerializer(Amount.class, createVoSerializer(Amount.class));
		  serializers.addSerializer(EmbeddableWithReference.class, createVoSerializer(EmbeddableWithReference.class));
		  serializers.addSerializer(DateTime.class,new DateTimeSerializer());
		  context.addSerializers(serializers);
		  deserializers.addDeserializer(DateTime.class, new DateTimeDeserializer());
		  context.addDeserializers(deserializers);
		  super.setupModule(context);
		}
	private <VO> ValueObjectSerializer<VO> createVoSerializer(Class<VO> clazz) {
		ValueObjectSerializer<VO> amountSer = new ValueObjectSerializer<VO>(clazz);
		toAutowire.add(amountSer);
		return amountSer;
	}
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		for (Object bean : toAutowire) {
			beanFactory.autowireBean(bean);		
		}
		return;
	}
}
