package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="withOneToMany")
public interface WithOneToManyRepository extends PagingAndSortingRepository<WithOneToMany, Long> {
	public List<WithOneToMany> findByCode(@Param("code")String code);
}
