package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.prefabware.jpa.JpaEntity;

@Entity
public class InvoiceLine extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;

	public Integer number;
	@ManyToOne(fetch=FetchType.EAGER)
	public Product product;
	@Embedded
	public Amount price;
	public InvoiceLine() {
		super();
	}
}