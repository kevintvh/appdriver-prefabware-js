package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@Entity
public class Product extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;
	@KeyAttribute
	public String number;
	public String name;
}
