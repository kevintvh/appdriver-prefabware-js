
//all methods with parameter type can be called with a String or a prefabware.model.Type
define('prefabware/plugins/prefabware/store/StoreHelper', [ 'dojo', 'dojo/_base/lang', 'dojo/aspect',
                                         'dojo/_base/array',
                                         "dojo/store/JsonRest", "dojo/store/Observable", "dojo/store/Cache", "dojo/store/Memory",
                                         'prefabware/plugins/prefabware/store/proxy/ProxyFactory',
                                         'prefabware/model/Entity',
		'dojo/data/ItemFileWriteStore', 'dojo/_base/declare' ], function(dojo, lang, aspect,ARRAY, JsonRest, Observable, Cache, Memory) {
	dojo.declare("prefabware.plugins.prefabware.store.StoreHelper", null, {
		proxyFactory : new prefabware.plugins.prefabware.store.proxy.ProxyFactory(),
		jsonTarget : null,
		constructor : function(jsonTarget) {
			if (jsonTarget==null) {
				throw('jsonTarget is required');
			}
			this.jsonTarget=jsonTarget;
		},
		
		getViewStoreTarget : function(type) {
			// returns the url for store of a view of the given type
			return this.jsonTarget +'/'+ this.getFirstLetterLowerCase(this.getSimpleName(type)) + 'View/list/';
		},
		getEnumStoreTarget : function(type) {
			// returns the url for store of a view of the given type
			return this.jsonTarget +'/'+ 'enumerationService/values/' + type.name;
		},
				
		createEnumStore : function(type) {
			//the enum store is a memory store, initialized with the elements from the server
			var viewStore=this.createViewStore(type);
			var store = new dojo.store.Memory({
				// attention : for the memory store its idProperty not idAttribute
				//for enums the id is the value itself
				//because it must be the same as the according value in the entity
				idProperty :'value'
			});
			store.isLoaded=false;
			store.afterLoad=function(){
				//callers may attach here to be informed when the enum store is loaded
				store.isLoaded=true;
			};
			
			dojo.when(viewStore.query(), function(array){
				// this function will be called with array once query() is complete, 
				// regardless of whether it returns a promise, or directly 
				// returns a value
				ARRAY.forEach(array,function(element){
					store.put(element);
				});
				store.afterLoad();
				});
				
				this.mixinStore(store);
				store.byId = dojo.hitch(store,this.__queryById);
				return store;
		},
		createViewStore : function(type) {
			var that=this;
			var target;
			if (type.isEnumeration) {
				target=this.getEnumStoreTarget(type);
			}else{
				target=this.getViewStoreTarget(this._typeName(type));
			}
			var jsonrest=		JsonRest({
				target:target, 
				idProperty: "pfw_id",
				put: function(entity){
					return JsonRest.prototype.put.call(this, entity).then(function(data){
						if (data!=null) {
						that.proxyFactory.createProxy(type,data);
						//the proxy is the same instance as data
						}
						return data;
					});
				},
				
			});
			
			var viewStore =  Observable(Cache(
			jsonrest, Memory({idProperty: "pfw_id",})));
			
			this.mixinStore(viewStore);
			viewStore.byId = dojo.hitch(viewStore,this.__getById);//
			viewStore.type=type;
			return viewStore;
		},
		
		
		__getById : function(id) {
			//TODO allwas return a proxied entity !!!
			//get returns a single object as the result
			return this.get( id);
		},
		__queryById : function(id) {
			return this.query({ pfw_id:id });
		},
		__deleteItem : function(item) {
			try {
               this.remove(item.pfw_id);
            } catch (e) {
            	//DELETE is asynchrounus, cannot catch errors here ! 
            	console.error('cannot delete '+item.__label()+' reson '+e);
            }
		},
		__newItem : function(item) {
			this.put(item,{overwrite:true});
			return item;
		},
		__setValue : function(item,attrName,value) {
			item[attrName]=value;
			//put with override
			this.newItem(item);
			return;
		},
		__clear : function() {			
				// rollback all changes made, else changed items cannot be
				// cleared
				if (this.revert) {
					//not all stores do support this
					this.revert();
				}
				// clear the data of the store
				var items = this.data;
				for ( var index = 0; index < items.length; index++) {
					var item = items[index];
					this.remove(item);					
				}
				return;
			},

		mixinStore : function(store) {
			//the currently selected entity in the grid
			store.selectedEntity=null;
			// give the store a method byId, as JsonRestStore has
			store.newItem = dojo.hitch(store,this.__newItem);
			store.deleteItem = dojo.hitch(store,this.__deleteItem);
			store.setValue = dojo.hitch(store,this.__setValue);
			store.clear = dojo.hitch(store,this.__clear);
			store.queryById = dojo.hitch(store,this.__queryById);
		},
		createReferenceStore : function(options) {
			// creates a store for a reference attribute
			var type = options.type;
			var attribute = options.attribute;

			var store = new dojo.store.Memory({
				// attention : for the memory store its idProperty not idAttribute
				idProperty :'pfw_id'
			});
			
			this.mixinStore(store);
			store.byId = dojo.hitch(store,this.__queryById);
			
			return store;
		},		
		getSimpleName : function(type) {			
			// returns the simple name of a qualified type name
			// returns 'User' for 'com.prefabware.User'
			var parts = this._typeName(type).split('.');
			return parts[parts.length - 1];
		},
		getFirstLetterLowerCase : function(name) {
			// returns the name with the first letter lowercase
			// returns 'user' for 'User'
			return name.charAt(0).toLowerCase() + name.substring(1, name.length);
		},
		_typeName : function(type) {
			// returns the name of the type
			// if type is a String returns it
			// if type is a prefabware.model.Type, returns type.name
			if (dojo.isString(type)) {
				return type;
			} else {
				// TODO check type
				return type.name;
			}
		}
	});
});
