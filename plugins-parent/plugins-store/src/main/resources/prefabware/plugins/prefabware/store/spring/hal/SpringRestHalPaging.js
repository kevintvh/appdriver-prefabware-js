//to build the paging query
define('prefabware/plugins/prefabware/store/spring/hal/SpringRestHalPaging', [ 
       'dojo',
       'dojo/_base/lang',
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/util/UrlBuilder',
       'prefabware/lang',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, lang,array,all) {
	dojo.declare("prefabware.plugins.prefabware.store.spring.hal.SpringRestHalPaging", null, {
		constructor : function() {
			
		},
		appendPaging: function(urlBuilder,p_page,p_size,p_sort){
			//paging and sorting
			//expects an UrlBuilder,
			//adds the paging query
			//and returns the UrlBuilder
			var page=0;
			var size=20;
			if (p_page!=null) {
				page=p_page;
			}
			if (p_size!=null) {
				size=p_size;
			}
			
			urlBuilder.query("page",page);
			urlBuilder.query("size",size);
			
			if (p_sort!=null) {
				//sort is optional
				//sort can be a value or an array
				var sortA=prefabware.lang.toArray(p_sort);
				//but spring can only handle one element
				var sort=sortA[0];
				var value=sort.attribute;
				if (value=='pfw_id') {
					//TODO better solution
					value='id';
				}
				//sort dir
				if (sort.descending==true) {
					value=value+',desc';	
				};
				urlBuilder.query("sort",value);
			};
			return urlBuilder;
		},		
	});
});