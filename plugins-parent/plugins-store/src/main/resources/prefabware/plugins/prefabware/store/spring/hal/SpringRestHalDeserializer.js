//desserializes JSON Entities sent from Spring REST repositories and services
//JSON must be created using HATEOAS and HAL
define('prefabware/plugins/prefabware/store/spring/hal/SpringRestHalDeserializer', [ 
       'dojo',
       'dojo/_base/lang',
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/lang',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, lang,array,all) {
	dojo.declare("prefabware.plugins.prefabware.store.spring.hal.SpringRestHalDeserializer", null, {
		map:null,//
		//the serializer needs the store to save new entities that are inside an array
		//before the array can be serialized using the links of the elements
		storePlugin:{inject:'prefabware.store',type:'plugin'},
		constructor : function() {
			
		},
		deserialize: function(json,type,targetEntity){
			//async			
			var entityP=this.__deserializeJson(json,type,targetEntity);
			return entityP;
		},
		__deserializeJson: function(json,type,targetEntity){
			var that=this;
			//async			
			if (json._embedded!=null) {
			   return that.__deserializeEmbedded(json,type,targetEntity);
			}
			else if (0==Object.keys(json).length) {
				//TODO this can be the result of a 404
				//or an empty array
				return that.__deserializeEmptyEmbedded(json,type,targetEntity);
			}
			else if (json.content==null) {
				//options.entity may be an entity, where the result must be merged into
				var pEntity= that.__deserializeComposite(json,type,targetEntity);
				return pEntity;
			}else{
				//if it has a property 'content', its an array				
				//it contains data of 1 ore more entities
				var ar =json.content;
				var pEntities= array.map(ar,function(ej){
					return store.__deserializeComposite(ej,type);
				});
				//pEntites is an array of promises, each resolving to a single entity
				//caution: using all() on a single promise leads to absolutely unwanted results here !
				//but because of array.map we allways have an array here
				//its not possible to set an additional property to the result of all();
				//like pResult.total=entityJson.page.totalElements;
				//so wrap the result again just to set the additional total
				var pResult= all(pEntities).then(function(entities){
					if (json.page!=null) {
						entities.total=json.page.totalElements;
					}
					return entities;
				});
				return pResult;
			}
		},
		__deserializeEmbedded: function(json,type,targetEntity){
			var that=this;
			// json is an array returned for the url of a collection attribute
			// http://localhost:8080/withOneToMany/1/many
			// {
			//  "_embedded" : {
			//	    "nameOfRepositoryPath" : [ 
			//	      array of json objects
			//        ..
			// nameOfRepositoryPath is the name of the repository path of the element type
			// its unknown here.
			// the array contains data of 1 ore more entities
			// no paging, no self link of the root object			
			var keys=Object.keys( json._embedded );
			var ar =json._embedded[keys[0]];
			
			var pEntities= array.map(ar,function(entityJson){
				return that.__deserializeComposite(entityJson,type);
			});
			var pResult= all(pEntities).then(function(entities){
				if (json.page!=null) {
					entities.total=json.page.totalElements;
				}
				return entities;
			});		
			return pResult;		
		},
		__deserializeEmptyEmbedded: function(json,type,targetEntity){
			var that=this;
			// json is returned for the url of a collection attribute that is null (or empty)
			// http://localhost:8080/withOneToMany/1/many
			// {}
			// its just an empty object, nothing else
			// no paging, no self link of the root object			
			return prefabware.lang.deferredValue([]);		
		},
	
		__deserializeIdentity: function(entityJson,objectType,targetEntity){
			//deserializes the id and the selflink for an entity from the given json
			var entity=null;	
			//id and selflink only for entities not for valuetypes
			if (targetEntity==null) {
				//no entity to merge into was given, so a completely new one has to be deserialized
				
				//entity -> http://localhost:8080/withString/1
				//one to many reference -> http://localhost:8080/withManyToOne/1
			var selfLink=lang.getObject("_links.self",false,entityJson);			  
			if (selfLink) {
				//set the id if necessary
				var id=prefabware.util.rest.hrefId(selfLink.href);
				if (entityJson.pfw_id==null) {
					entityJson.pfw_id=id;
				}else{
					//different ids ? 
					prefabware.lang.assert(entityJson.pfw_id==id,"the entity.pfw_id '+entity.pfw_id+' is different than the id from the selfLink "+selfLink);
				}
			}
			//pfw_id and pfw_type must be set before create is called
			if (entityJson.pfw_id==null&&entityJson.id!=null) {
				//TODO unify and allways use id ...
				entityJson.pfw_id=entityJson.id;
			}
			prefabware.lang.assert(!objectType.isEntityType||entityJson.pfw_id!=null,"no pfw_id found for entityJson "+entityJson);
			entity=objectType.create(entityJson);
			entity.__prefabware.selfLink=selfLink;
			}else{
			//an entity to merge into was given, use it as it is
			entity=targetEntity;
			}
			//any way now we should have a valid entity
			prefabware.lang.assert(entity.pfw_id!=null,"could not determine pfw_id for entityJson "+entityJson);
			prefabware.lang.warn(entity.__prefabware!=null&&entity.__prefabware.selfLink!=null,"could not determine selfLink for entityJson "+entityJson);
			return entity;
		},
			
		__deserializeComposite: function(entityJson,objectType,targetEntity){
			var that=this;
			//TODO this should be refactored into a deserializer
			//async, have to wait for eager proxies to resolve
			//processes attributes of composites, not only enttities !
			var proxyP=[];
			//entity is optional.
			//normal case          : entity==null a new instance will be created
			//refreshing an entity : entity!=null and the entityJson will be merged into entity
			entityJson.pfw_type=objectType.name;
			
			//extract the id from the self link
			//this happens only for root entities
			var entity=null;		
			if (objectType.isEntityType) {
			entity=this.__deserializeIdentity(entityJson,objectType,targetEntity);
			}else{
			entity=objectType.create(entityJson);
			}
			//map with all references, without the self link
			var links=entityJson._links;
			//set the selfLink
			array.forEach(objectType.getQualifiedAttributes(),function(qa){
				var attr=qa.attribute;
				var jsonValue=entityJson[attr.name];
				if(targetEntity!=null&&jsonValue===undefined){
					//if merging into an existing entity, only process attributes that have a value in the json
					return;
				}else if(attr.isSystem){
					//allready set, do nothing
				}else if(attr.type.isEnumeration){
					//its a Enumeration set the value of the according enum constant
	    			entity[attr.name]=attr.type[jsonValue];
				}else if (attr.isReference&&!attr.type.isValueType) {
					//the entity under construction is the targetEntity for attribute deserialization
					return that.__deserializeReferenceAttribute(entityJson,objectType,attr,entity);
				}else if (jsonValue!=null&&attr.isReference&&attr.type.isValueType) {
					//if the value is null, dont do nothing, its just null
					//its a Reference to a ValueObject that may have
					//simple values
					//references to entities
					//other ValueObjects
					//those attributes must be set to
					var voP=that.__deserializeComposite(jsonValue,attr.type).then(function(vo){
						entity[attr.name]=vo;
					});
					proxyP.push(voP);
				} else{
				//a simple value
				entity[attr.name]=attr.convertFrom(jsonValue);
				}	
			});
			prefabware.lang.assert(!entity.__isEntity||entity.pfw_id!=null,"no pfw_id found for entityJson "+entityJson);
			if (proxyP.length==0) {
				return prefabware.lang.deferredValue(entity);
			}else{
				return all(proxyP).then(function(){
					return entity;
				});
			}
		},
		__deserializeReferenceAttribute: function(entityJson,objectType,attr,targetEntity){
		var that=this;
		//its a Reference to another Entity
		//there should be a link in the jsonvar links=entityJson._links;
		var links=entityJson._links;
		if (links==null) {
			//no links at all, no reference
			return;
		}
		//create a proxy for the given link
		var link=links[attr.name];
		//if the attribute is null, there may be no link
		if (link==null) {
			//no link for the attribute, no reference
			return;
		}
		var __reference={};
//		var segments = link.href.split('/');
//		var id = segments[segments.length - 2];
//		__reference.pfw_id=id;
		that.storePlugin.createStore({type:attr.type}).then(function(attstore){
			//the link does not allways include the id of the referenced entity
			//by spring HAL default its a special URL like 
			//http://localhost:8080/plugins-store/city/2/country
			//but for references in valuetypes it may be a real link
			//http://localhost:8080/plugins-store/country/11
			__reference.type=attr.type;
			__reference.store=attstore;
			__reference.href=link.href;
			__reference.link=link;
			//set also the id if it is available
			__reference.id=prefabware.util.rest.hrefId(link.href);
			if (attr.max==1) {
				var refEntity=attr.type.create();//a reference proxy must be an entity also
				var proxy=new prefabware.plugins.prefabware.store.proxy.ReferenceProxy(attr.type,__reference);
				var __serializable=proxy.__serializable;
				var __clone=proxy.__clone;
				//mixin proxy into entity causes errors, because the proxied attributes will be touched
				lang.mixin(proxy,refEntity); 
				//restore the functions of the proxy
				proxy.__serializable=__serializable;
				proxy.__clone=__clone;
				targetEntity[attr.name]=proxy;
				if (attr.isEager) {
					//resolve eager properties immediately
					proxyP.push(proxy.resolve());
				}
			} else {
				//its an array
				var ar=[];
				var proxy=new prefabware.plugins.prefabware.store.proxy.ReferenceArrayProxy(attr.type,__reference);
				//mixin proxy into entity causes errors, because the proxied attributes will be touched
				lang.mixin(ar,proxy); 
				targetEntity[attr.name]=ar;
			}
		});
		}
	});
});