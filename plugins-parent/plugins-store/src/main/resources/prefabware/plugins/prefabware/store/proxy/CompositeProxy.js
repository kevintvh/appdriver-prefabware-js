//a proxy for all kinds o composites
// Entity, Reference and Value
//
define('prefabware/plugins/prefabware/store/proxy/CompositeProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/plugins/prefabware/store/proxy/Proxy',
		  'dojo/_base/declare' ],
		  function(dojo,lang,Proxy,EntityResolver) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.CompositeProxy", [prefabware.plugins.prefabware.store.proxy.Proxy], {
		//all properties here are prefixed with __ to avoid naming conflicts with the properties of the entity
		//TODO composite and entity do not need a resolver, only reference proxies
		proxyFactory:null,
		__resolver :null,
		__type:null,	
		__composite:null,	
		constructor : function(type,data,proxyFactory) {
			//cannot initialize this field above, the legacy loader has problems with the cycle dependency
			this.proxyFactory=proxyFactory;
			this.__resolver= proxyFactory.entityResolver;
//			if (data.pfw_type==undefined) {
//				throw 'object must have pfw_type';
//			};
			this.__type=type;
			//here the instance for the received data is created
			this.__composite=type.create(data);
			//mix the composite and all its functions into data
			//to make it the required type e.g. Entity
			this.__composite.__mixInto(data);
			//mix this into data
			lang.mixin(data, this);
			//data now is a proxy, all its attributes did allready exist in data
			//so the only thing we do is proxy the references and the valueTypes
			data.__initializeProxy();
		},
		__postCreate : function(data) {
			
		},
		doGet : function(propertyName, value) {
			if (value instanceof Array) {
				for ( var index = 0; index < value.length; index++) {
					var element = value[index];
					value[index] = this.__doGet(propertyName, element);
				}
			} else if (value!=undefined&&value!=null&&value.pfw_type) {
				value = this.__doGet(propertyName, value);
			}
			return value;
		},
	
	copyUnproxiedProperties : function(from,to){
		//copies the unproxied properties from -> to
		this.copyProperty('pfw_id', from, to);
		this.copyProperty('pfw_type', from, to);
		this.copyProperty('version', from, to);
		//
		this.copyProperty('$ref', from, to);
		this.copyProperty('__id', from, to);
		this.copyProperty('__parent', from, to);
		this.copyProperty('_loadObject', from, to);
		return to;
	},
	copyProperty : function(propertyName,from,to){
		//copies the property with the given name from -> to
		//only, if its defined in from
		if (propertyName in from) {
			to[propertyName]=from[propertyName];
		}
		return to;
	},
	
	doResolve: function (referenceArg) {
			if (this.__reference) {
				return this.__resolver.resolve(this.__reference);
			} else {
				return {};
			}
		},
	__doGet : function(propertyName,value) {
		//if the value is itself an unresolved reference, create a reference proxy for it
		var qAttribute=this.__type.getQualifiedAttribute(propertyName);
 		return this.proxyFactory.createProxyForAttribute(qAttribute,value);
	},
	});
});
