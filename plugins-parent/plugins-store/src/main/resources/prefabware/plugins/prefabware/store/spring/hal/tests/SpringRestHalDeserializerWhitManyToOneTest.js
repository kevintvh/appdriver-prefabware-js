dojo.provide("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWhitManyToOneTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("prefabware.plugins.prefabware.store.spring.SpringRestSerializer");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWhitManyToOneTest", [ 
{
	name : "setUp",
	timeout : 500,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.hal.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			
			that.withStringType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.WithManyToOne');
			
			return that.storePlugin.createStore({type:that.withStringType}).then(function(store){
				that.store=store;
				return that;
			}).then(function(that){
				return that;
			});
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},		
	runTest : function() {
		//only setup
		return;
	},
	tearDown : function() {
	}
},
//------------------------------------------------------------------------------------------------------
{
	name : "store.get()",
	timeout : 500,
		
	setUp : function() {	
		prefabware.test.runSetup();
	},		
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.store;
				return store.get(1).then(function(withString){
					doh.assertTrue(withString!=null);
					doh.assertEqual("withManyToOne1",withString.code);
					doh.assertTrue(withString.one.__isProxy);
					doh.assertEqual("http://localhost:8080/withManyToOne/1/one",withString.one.__reference.href);
					deferred.callback(true);
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
//------------------------------------------------------------------------------------------------------
{
	name : "store.put()",
	timeout : 500,
		
	setUp : function() {	
		prefabware.test.runSetup();
	},		
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.store;
				return store.get(1).then(function(entity){
					doh.assertTrue(entity!=null);
					doh.assertTrue(entity.code.indexOf("withManyToOne1"==0));
					doh.assertTrue(entity.one.__isProxy);
					doh.assertEqual("http://localhost:8080/withManyToOne/1/one",entity.one.__reference.href);
					//resolve the proxies, so entity.one is resolved
					return entity.__resolveProxies().then(function(x){
					//now update
					var one=entity.one;
					var version=entity.version;
					var code=entity.code+","+new Date().toTimeString();
					entity.code=code;
					entity.one=null;
					return store.put(entity).then(function(updated){
						doh.assertEqual(version+1,updated.version);
						doh.assertEqual(code,updated.code);
						//updated.one is not null, it has an url pointing to null...
						//have to resolve the porxies to find the current value
						return updated.__resolveProxies().then(function(x){
							doh.assertEqual(null,updated.one);
							code=entity.code+","+new Date().toTimeString();//have to change something so update really happens
							updated.code=code;
							updated.one=one;//set the original reference
							//update again
							return store.put(updated).then(function(updated2){
								doh.assertEqual(version+2,updated2.version);
								doh.assertEqual(code,updated2.code);
								return updated2.__resolveProxies().then(function(x){
								doh.assertEqual(one.code,updated2.one.code);
								deferred.callback(true);
							});
							});
						});
					});
					//
					});
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
]);