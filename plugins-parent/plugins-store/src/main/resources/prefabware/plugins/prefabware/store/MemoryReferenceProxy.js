//a proxy for a reference
//
define('prefabware/plugins/prefabware/store/MemoryReferenceProxy',
		[ 'dojo'
		  , 'dojo/_base/lang'
		  ,'dojo/_base/array'
		  ,'prefabware/commons/Proxy',
		  'dojo/_base/declare' ],
		  function(dojo, lang,array,Proxy) {
	dojo.declare("prefabware.plugins.prefabware.store.MemoryReferenceProxy", [prefabware.commons.Proxy], {		
		__store:null,
		__isEntity:true,
		constructor : function(type,reference) {
			this.__reference=reference;
			array.forEach(type.getAttributes(),function(attr){
				if (!attr.isSystem) {
					//the system properties are directly accessible and have values set,
	    			//no need to resolve before acessing them
				this.defineProperty(attr.name);
				}
			},this);
			return; 
		},
	
		__clone : function() {
			//do not clone proxies
			return this;
		},
	doResolve: function(reference){
			var that=this;
			prefabware.lang.assert(reference!=null);
			prefabware.lang.assert(reference.id!=null);
			prefabware.lang.assert(reference.store!=null);
			var store=reference.store;
			var id=reference.id;
			return store.get(id).then(function(result) {
				that.__resolved={};
				//that.__resolved.link=;
				return result;
			});
	}
});
});
