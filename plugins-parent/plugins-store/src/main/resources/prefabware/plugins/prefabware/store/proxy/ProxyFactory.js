//to create a proxy
define('prefabware/plugins/prefabware/store/proxy/ProxyFactory', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",
		  'prefabware/lang',
		  'prefabware/plugins/prefabware/store/proxy/ValueProxy',
		  'prefabware/plugins/prefabware/store/proxy/ReferenceProxy',
		  'prefabware/plugins/prefabware/store/proxy/EntityProxy',
		  
		  'dojo/_base/declare' ], function(dojo,lang,locale) {
	dojo.declare('prefabware.plugins.prefabware.store.proxy.ProxyFactory', null, {
		entityResolver:null,
		constructor : function(entityResolver) {
			prefabware.lang.assert(entityResolver!=null,'entityResolver must not be null');
			this.entityResolver=entityResolver;
		},
		createProxy : function(type,data) {
			//type : the type to proxy
			//data : the element to proxy
			//looks strange but is intended :
			//the proxies are created inside the value, and we return
			//the value, so allready existing references to that object will then reference the prox
			if (data.__isProxy) {
				//is allready a proxy, nothing to do
				return data;
			}
			if (type.isEnumeration) {
				//do not proxy enums even though they are references
				return data;
			}
			prefabware.lang.assert(type.isEntityType,'type '+type.name+' is no EntityType');
	 			//value is an unproxied entity
	 			//create an entity proxy for the value to proxy its unresolved references
				new prefabware.plugins.prefabware.store.proxy.EntityProxy(type,data,this);	
				return data;
		},	
		createProxyForAttribute : function(qattribute,data) {
			if (data.__isProxy) {
				//is allready a proxy, nothing to do
				return data;
			}
			var attribute=qattribute.attribute;
			var type=attribute.type;
			if (type.isEnumeration) {
				//do not proxy enums even though they are references
				return data;
			}
			//type : the type to proxy
			//data : the element to proxy
			//looks strange but is intended :
			//the proxies are created inside the value, and we return
			//the value, so allready existing references to that object will then reference the prox 
			if (attribute.isReference&&!type.isValueType) {
				new prefabware.plugins.prefabware.store.proxy.ReferenceProxy(type,data,this);		 			
				return data;
			} else if(attribute.isReference&&type.isValueType){
				//value is an unproxied ValueType
				//it may have references itself, so wrap a proxy around it
				new prefabware.plugins.prefabware.store.proxy.ValueProxy(type,data,this);		 			
				return data;
			} else{
				//simple value, just return ist
				return data;}
		},	
	});
});
