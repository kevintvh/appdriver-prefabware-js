define(["dojo", 'dojo/_base/lang'
        ,'dojo/_base/array'
        ,'dojo/request/xhr'
        ,'dojo/promise/all'
        ,'prefabware/lang'
        ,'prefabware/util/UrlBuilder'
        ,'prefabware/plugins/prefabware/store/spring/hal/SpringRestHalSerializer'
        ,'prefabware/plugins/prefabware/store/spring/hal/SpringRestHalDeserializer'
        ,'prefabware/plugins/prefabware/store/spring/hal/SpringRestHalPaging'
        ,'prefabware/plugins/prefabware/store/proxy/ReferenceProxy'
        ,'prefabware/plugins/prefabware/store/proxy/ReferenceArrayProxy'
        ,'dojo/store/util/QueryResults'
        ,'prefabware/util/rest'
        ,'prefabware/util/Parameters'
], function(dojo, lang, array,xhr,all ){

// module:?
//		prefabware/plugins/prefabware/store/spring/SpringRestStore
// to communicate with a remote Spring REST Repository
//	just wrap it around an EntityStore	

var SpringRestStore = function(store,type,storePlugin){
	// extends the given store to work with entites
	// so that
	// - a default sort is allways added to all queries
	// the store must have a property=type
	// can be applied to async stores only
//	var parameters = new prefabware.util.Parameters([{name:'store',required:true}
//													 ,{name:'type',required:true}
//													 ,{name:'proxyFactory',required:true}
//		]);
//	  parameters.check(arguments);
	store.isSpringRestStore=true;//for test and debug
	store.originalTarget=store.target;//we will change the target for queries temporarily, so keep the original
	store.__serializer=new prefabware.plugins.prefabware.store.spring.hal.SpringRestHalSerializer();
	store.__deserializer=new prefabware.plugins.prefabware.store.spring.hal.SpringRestHalDeserializer();
	store.__paging=new prefabware.plugins.prefabware.store.spring.hal.SpringRestHalPaging();
	storePlugin.injector.injectInto(store.__serializer);
	storePlugin.injector.injectInto(store.__deserializer);
	store.__deserialize= function(entityJson,entityToRefresh){
		return store.__deserializer.deserialize(entityJson,type,entityToRefresh).then(function(entity){
			entity.__store=store;//save the store in the entity to allow self refresh and lazy loading of optional attributes
			return entity;
		});
	};
	store.__processRawResult= function(rawResult){
		array.forEach(rawResult,function(resource){
			store.__deserializeEntity(resource.content[0]);
		});
		return rawResult;
//		return rawResult.then(function(result){
//			var resources = result.content;
//			return resources;
//		});
	};
	//var originalPut = store.put;//cannot use original put, need access to the RepsonseHeader
	store.__xhr= function(url, options){
		//Spring REST returns a Loacation in the ResponseHeader
		//Location:http://localhost:8080/plugins-store/city/22
		var promise=xhr(url, {
			method: options.method,
			data: options.json,
			handleAs: "text",
			headers: lang.mixin({
				"Content-Type": "application/json",
				Accept: this.accepts,
				"If-Match": options.overwrite === true ? "*" : null,
				"If-None-Match": options.overwrite === false ? "*" : null
			}, this.headers, options.headers)
		});
		return promise;//error should be handled at the end of the promise chain
	};
	store.__xhrPost= function(object, options){
		//write  new entity
		return store.__toJson(object).then(function(json){
			//when writing a new entity, never include the id
			delete object.id;
			delete object.pfw_id;
			
			var method="POST";
			var url=store.target;
			var promise=store.__xhr(url,{json:json,method:method});
			return promise.response.then(function(response) {
				//location is the url of the posted resource
				var location=response.getHeader('Location');
				var elements=location.split('/');
				var id=elements[elements.length-1];
				//get the resource
				var entity=store.get(id,options);
				return entity;
			});
		});
	};
	store.__xhrPut= function(object, options){
		//update an existing entity
		return store.__toJson(object).then(function(json){
		var method="PUT";
		var urlB=new prefabware.util.UrlBuilder();
		urlB.parse(store.originalTarget);
		urlB.appendPath(object.pfw_id);
		var url=urlB.build();		
		return store.__xhr(url,{json:json,method:method}).then(function(){
			//Spring REST returns nothing
			//so reload the entity
			return store.get(object.pfw_id,options);
		});
		});
	};
	store.__xhrGet= function(url,options){
		//async
		//returns allways a single promise
		//that resolves to a single entity 
		//or to an array of entities.
		//In the later case a field total with the number of all available elements will be added		
		
		var headers = lang.mixin({ Accept: this.accepts }, this.headers, options.headers || options);
		
		var xhrP= xhr(url, {
			method: "GET",
			handleAs: "json",
			headers: headers
		}).
		then(function(json){
			//options.entity may be an entity which must be used as target of the deserilization 
			return store.__deserialize(json,options.entity);
		}, function(err){
			 // Spring includes links to attributes, even if they are null
			 // when trying to 'get' such a link, a ResourceNotFoundException
			 // is thrown.
			 // ignore those errors here, just return null to indicate nothing was found
			return null;
		  });
		return xhrP;
	};
	
	store.__toJson= function(entity){
		return store.__serializer.serialize(entity).then(function(serialized){
			return dojo.toJson(serialized.value);
			});
	},
	store.remove= function(id, options){
		// summary:
		//		Deletes an object by its identity. This will trigger a DELETE request to the server.
		// id: Number
		//		The identity to use to delete the object
		// options: __HeaderOptions?
		//		HTTP headers.
		options = options || {};
		//TODO this is just to fix the problem that target does not end with a /
		//remove this when ISSUE 36 is fixed
		var target=store.target;
		if (target.lastIndexOf("/")!=target.length-1) {
			target=target +"/";
		}
		var xhrP= xhr(target + id, {
			method: "DELETE",
			headers: lang.mixin({}, this.headers, options.headers)
		});
		return xhrP;
//		return xhr("DELETE", {
//			url: target + id,
//			headers: lang.mixin({}, this.headers, options.headers)
//		});
	},
	store.put= function(object, options){
		if (options==null) {
			options={};
		}
		
		if (object==null) {
			prefabware.lang.throError("object for store.put is null");
		}
		//if the entity was persisted local, give it a new id so it is recognized as new
		//TODO this should have happend before ?
		if (object.__isPersistedLocal()) {
			object.pfw_id=object.__type.__newId();
		}
		
		var that=this;
			if (object.__isNew()||object.__isPersistedLocal()) {
				return that.__xhrPost(object, options);
			}else{
				return that.__xhrPut(object, options);
			}
	};
	store.query= function(p_query, p_options){
		//async
		//to call a service using a query
		//options.entity = 
		//options.appendPath = a path to append to the target of this store
		//                     can be null, than the default finder-method will be used
		var query=dojo.mixin({}, p_query);
		var options=dojo.mixin({}, p_options);
		if (options==null) {
			options={};
		}
		
		//query is a simple query like {name:'Frankfurt'}
		//queries must use a url like
		//http://localhost:8080/app-crm/country/?sort=isoCode,asc
		
		var keys=Object.keys(query);		
		
		var urlB;
		if (options.urlPath!=null) {
			urlB=new prefabware.util.UrlBuilder();
			//use the url from the options, if set
			urlB.parse(store.baseUrl);			
			urlB.appendPath(options.urlPath);
		}else{			
			//originalTarget="http://localhost:8080/app-crm/invoice/"
			urlB=this._calcFinderUrl(options,keys,query) ;
		}
		//query
		array.forEach(keys,function(key){
			var attributeName=key;
			var attributeValue=query[attributeName].toString();//toString is important here, QueryStore may have changed the value from a string to an object with a method test
			urlB.query(attributeName,attributeValue.replace("*","%"));
		});
		
		//paging and sorting
		if (options.count!=null&&options.start!=null) {
			//deprecated OPTIONS start & count
			var page=options.start/options.count;
			var size=options.count;
			urlB=store.__paging.appendPaging(urlB,page,size,options.sort);
		}
		if (options.page!=null&&options.size!=null) {
			//better use  page & size			
			urlB=store.__paging.appendPaging(urlB,options.page,options.size,options.sort);
		}		
		var url=urlB.build();

		if (options.properties!=null) {
			urlB.query("properties",options.properties.join());
		}
		var url=urlB.build();
		var result= this.__xhrGet(url,options).then(function(resolved){
			if (resolved==null) {
				//the result may resolve to null, which leads to exceptions in QueryResult
				return [];
			}else{
				return resolved;
			}
		});
		//QueryResult makes forEach, filter etc. available on the promise
		//Observable expects it
		queryResults=dojo.store.util.QueryResults(result);
		queryResults.isQueryResults=true;//to identify later
		return queryResults;
	};

	store._calcFinderUrl = function(options,keys,query) {
		//to calculate the default url for findermethods of a Spring data rest repository
		//only one parameter is used, TODO could be enhanced
		var urlB=new prefabware.util.UrlBuilder();
		urlB.parse(this.originalTarget);
		var finder="";
		if (options.appendPath!=null) {
			//use the supplied path
			finder=options.appendPath;
		}else if(keys.length==0){			
         //use the root of the resources path 	
			finder="";
		}else if(keys.length==1){
			var attributeName=keys[0];
			var attributeValue=query[attributeName].toString();//toString is important here, QueryStore may have changed the value from a string to an object with a method test
			// query may be something like =	{pfw_id:"*"};
			// * means findAll doesnt matter for which attribute
			// the url to find all is e.g. http://localhost:8080/plugins-store/city			
		if (attributeValue=='*') {
			// thats just the target without any query
		}else{
			//see http://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/repositories.html#repositories.query-methods.query-lookup-strategies
			//the query method has a name 
			//for one key-attribute : findBy<Key1>LikeIgnoreCase
			//for two key-attribute : findBy<Key1>LikeAnd<Key2>LikeIgnoreCaseAll
			//findByNameLikeIgnoreCase
			var finder=null;
			finder='search/findBy'+prefabware.lang.firstLetterUpperCase(attributeName)+'LikeIgnoreCase';
		}
		};
		urlB.appendPath(finder);
		return urlB;
	};
	store.get = function(id, options) {
// options.entity!=null means refresh the given entity instead of creating a new instance		
// id==null is allowed, when options.target!=null
// this happens when a ReferenceProxy for an attribute is resolved		
// a entity returned from a spring REST respository does not contain an attribute id
// its contained in its 'self' link		
// example of a returned entity		
//		{
//			  "name" : "Berlin",
//			  "version" : 0,
//			  "links" : [ {
//			    "rel" : "self",
//			    "href" : "http://localhost:8080/plugins-store/city/1"
//			  }, {
//			    "rel" : "country",
//			    "href" : "http://localhost:8080/plugins-store/city/1/country"
//			  } ]
//			}
		if (options==null) {
			options={};
		}
		prefabware.lang.assert(id!=null||options.target!=null,"id must be set or options.target must contain the url");
		var urlB=new prefabware.util.UrlBuilder();
		if (options.target==null) {
			//a entity with an id is requested
			urlB.parse(store.originalTarget);
			urlB.appendPath(id);		
		}else {
			//a url for a entity reference attribute is requested
			urlB.parse(options.target);
		}
		if (options.properties!=null) {
			urlB.query("properties",options.properties.join());
		}
		var url=urlB.build();
		return this.__xhrGet(url,{}).then(function(result){
			if (lang.isArray(result)) {
				return all(result);
			}else{
				return result;
			}
		});
	};
	return store;
};

lang.setObject("prefabware.plugins.prefabware.store.spring.SpringRestStore", SpringRestStore);

return SpringRestStore;
});
