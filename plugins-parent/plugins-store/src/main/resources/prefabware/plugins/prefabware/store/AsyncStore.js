define([ "dojo", 'dojo/_base/lang', 'dojo/_base/array',
		'dojo/store/util/QueryResults', 'prefabware/lang',
		'prefabware/util/Parameters' ], function(dojo, lang, array) {

	// module:
	// prefabware/plugins/prefabware/store/AsyncStore

	var AsyncStore = function(/* Store */store) {
		// converts the api of a given synchron store into the api of a
		// asynchron store
		// so that
		// all stores can be treated equally
		store.isAsyncStore = true;// for test and debug
		store.__toPromise = function(result) {
			if (result != null && typeof result.then == 'function') {
				// if the result is a promise, return it as it is
				return result;
			} else {
				// if the result is NOT a promise, make one
				return prefabware.lang.deferredValue(result);
			}
		};
		var originalQuery = store.query;
		store.query = function(query, options) {
			var result = originalQuery.apply(this, arguments);
			if (result != null && result.isQueryResults) {
				//if its allready a QueryResults, do not wrap again
				//isQueryResults must be set manually when creating it, its NOT part of the dojo api
				return result;
			} else {
				return dojo.store.util.QueryResults(store.__toPromise(result));
			}
		};
		var originalPut = store.put;
		store.put = function(objects, options) {
			var result = originalPut.apply(this, arguments);
			return store.__toPromise(result);
		};
		var originalGet = store.get;
		store.get = function(objects, options) {
			var result = originalGet.apply(this, arguments);
			return store.__toPromise(result);
		};
		var originalRemove = store.remove;
		store.remove = function(id, options) {
			var result = originalRemove.apply(this, arguments);
			return store.__toPromise(result);
		};

		return store;
	};

	lang
			.setObject("prefabware.plugins.prefabware.store.AsyncStore",
					AsyncStore);

	return AsyncStore;
});
