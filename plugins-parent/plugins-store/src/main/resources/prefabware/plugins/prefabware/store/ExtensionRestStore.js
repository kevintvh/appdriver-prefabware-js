//a extension for a SpringRestStore
//TODO rename to ExtensionSpringRestStore
define('prefabware/plugins/prefabware/store/ExtensionRestStore', [ 
       'dojo', 
       'dojox/collections/Dictionary',
       'dojo/store/Cache',
       'dojo/store/Memory',
       "dojo/store/Observable",
       "dojo/store/JsonRest", 
       'prefabware/lang',
       'prefabware/commons/Cache',
       'prefabware/plugins/prefabware/store/spring/SpringRestStore',
       'prefabware/plugins/prefabware/store/QueryStore',
       'prefabware/plugins/prefabware/store/AsyncStore',
       'prefabware/plugins/prefabware/store/ExtensionStore',
       'dojo/_base/declare' ], function(
		dojo) {
	dojo.declare("prefabware.plugins.prefabware.store.ExtensionRestStore", [prefabware.plugins.prefabware.store.ExtensionStore], {
		globalCache:null,//cache for all stores created by this extension
		//cache this extension to not resolve the matcher again and again
		//this has NOTHING to do with caching the data in the store.
		cache:true,
		cacheStore:true,//this controlls whether the store itself uses a cache
		cacheCreateStore:true,//by default cache createStore for RestStores
		storePlugin:{inject:'prefabware.store',type:'plugin'}, 
		servicePlugin:{inject:'prefabware.service',type:'plugin'}, 
		constructor : function(definition) {
			if (this.json.cache!=undefined) {
				this.cache=this.json.cache;
			}
			if (this.json.cacheStore!=undefined) {
				this.cacheStore=this.json.cacheStore;
			}
		},		
		startup : function() {
			this.globalCache=this.storePlugin.globalCache;
		},	
		createStore : function(options) {
			var that=this;
			var type=options.type;
			return this.getViewStoreTarget(type).then(function(options){
				//path names do NOT end with a slash !
				//			if (!prefabware.lang.endsWith(target,'/')) {
				//				target=target+'/';
				//			}
			var jsonrest=dojo.store.JsonRest({
				target:options.target, 
				baseUrl:options.baseUrl, 
				idProperty: "pfw_id",
			});
			
			var springStore=prefabware.plugins.prefabware.store.spring.SpringRestStore(jsonrest,type,that.storePlugin);
			var observable =null;
			var urlCache=null;
			observable = dojo.store.Observable(springStore);				
			if (that.cacheStore) {
				var urlToKey=function(url){return url;};
				var optionsToKey=function(options){
					var key=dojo.mixin({},options);
					if (key.entity!=null) {
						//options.entity is set, the result must be merged into the entity
						//this should never be cached
						key.entity=null;
						key.forceFail=new Date().getTime();//will cause the cache to fail
					}
					return dojo.toJson(key);
					};
				//cache all calls to __xhrGet of the store using the url as key
				urlCache=prefabware.commons.Cache(observable,['__xhrGet'],[urlToKey,optionsToKey]);
				//all cache use the same map, so evicting one evicts the other
				urlCache.map=that.globalCache.map;
			}
			var queryStore= prefabware.plugins.prefabware.store.QueryStore(observable,type,urlCache);
			//cannot use evict, dojo.store.Cache.evict allready exists
			//so fake a method evictAll TODO where is it used ?
			queryStore.evictAll=dojo.hitch(urlCache,urlCache.evict);
			return queryStore;
			});
		},
		getViewStoreTarget : function(type) {
			//async
			// returns the url for store of a view of the given type
			return this.servicePlugin.findService('prefabware.service.store').then(function(service){
				return {target:service.urlForType(type),baseUrl:service.url};
			});
		},
		
	});
});
