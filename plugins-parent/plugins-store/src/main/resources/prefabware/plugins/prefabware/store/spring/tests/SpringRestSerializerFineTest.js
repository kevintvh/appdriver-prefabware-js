dojo.provide("prefabware.plugins.prefabware.store.spring.tests.SpringRestSerializerFineTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("prefabware.plugins.prefabware.store.spring.SpringRestSerializer");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.tests.SpringRestSerializerFineTest", [ 
{
	name : "setup",
	timeout : 3000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			that.countryType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Country');
			that.cityType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.City');
			that.invoiceType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Invoice');
			that.invoiceLineType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.InvoiceLine');
			return that.storePlugin.createStore({type:that.countryType}).then(function(store){
				that.countryStore=store;
				return that;
			}).then(function(){
				return that.storePlugin.createStore({type:that.cityType}).then(function(store){
					that.cityStore=store;
					return that;
				}).then(function(){
					return that.storePlugin.createStore({type:that.invoiceType}).then(function(store){
						that.invoiceStore=store;
						return that;
				}).then(function(){
					return that.countryStore.query({code:'DEU'},{urlPath:'country/search/findByCode'}).then(function(result){
						that.countryDE=result[0];
						return that;
					}).then(function(){
						return that.countryStore.query({code:'ESP'},{urlPath:'country/search/findByCode'}).then(function(result){
							that.countryES=result[0];
							return that;
						});
					});
				});
				});;
			});;
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
	},	
	runTest : function() {
		return;
	},
	tearDown : function() {
	}
},
{
	name : "test serialization",
	timeout : 3000,
	
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		var that=this;
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.invoiceStore;
			var serializer=store.__serializer;
			var ps={};
			
			var country=setup.countryType.create();
			country.name="tc1";
			country.code="tc1iso";
			ps.p1=serializer.serialize(country).then(function(serialized){
				doh.assertEqual({"code":"tc1iso","name":"tc1",version:null,id:-1},serialized.value,"country serialition failure : "+serialized.value);
			});
			
			var city=setup.cityType.create();
			city.name="tcity";
			city.country=country;
			try {
				
			ps.p2=serializer.serialize(city).then(function(serialized){
					doh.assertEqual({"code":"tc1iso","name":"tc1",version:null},serialized.value,"city serialition failure : "+serialized.value);
			},function(e){
				//exception inside the then
			});
			} catch (e) {
				//city with a reference to unsaved country, would fail in JPA
				//expection an exception from the serializer also
				doh.assertEqual(e.id,1);
				doh.assertEqual(e.sender,"SpringRestSerializer");
			}
			city.country=setup.countryES;
			ps.p3=serializer.serialize(city).then(function(serialized){
				var expected={id:city.pfw_id,"name":"tcity","size":null,"country":"http://localhost:8080/plugins-store/country/&id",version:null};
				expected.country=expected.country.replace("&id",setup.countryES.pfw_id);
				doh.assertEqual(expected,serialized.value);
			},function(e){
				//exception inside the then
			});
			
			dojo.promise.all(ps).then(function(){
				deferred.callback(true);
			});
		});
		return deferred;
	},
	tearDown : function() {
	}
}
]);