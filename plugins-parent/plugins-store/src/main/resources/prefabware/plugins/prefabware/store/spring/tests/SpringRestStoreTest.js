dojo.provide("prefabware.plugins.prefabware.store.spring.tests.SpringRestStoreTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo._base.array");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.tests.SpringRestStoreTest", [ 
{
	name : "finder",
	timeout : 1000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			that.countryType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Country');
			that.cityType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.City');
			that.invoiceType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Invoice');
			that.invoiceLineType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.InvoiceLine');
			return that.storePlugin.createStore({type:that.countryType}).then(function(store){
				that.countryStore=store;
				return that;
			}).then(function(){
				return that.storePlugin.createStore({type:that.cityType}).then(function(store){
					that.cityStore=store;
					return that;
				}).then(function(){
					return that.storePlugin.createStore({type:that.invoiceType}).then(function(store){
						that.invoiceStore=store;
						return that;
				}).then(function(){
					return that.countryStore.query({code:'Deutschland'},{urlPath:'country/search/findByCode'}).then(function(result){
						that.countryDE=result[0];
						return that;
					}).then(function(){
						return that.countryStore.query({code:'Spanien'},{urlPath:'country/search/findByCode'}).then(function(result){
							that.countryES=result[0];
							return that;
						});
					});
				});
				});;
			});;
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var query={name:'Frankfurt'};
		var keys=Object.keys(query);
		var finder='findBy'+prefabware.lang.firstLetterUpperCase(keys[0]);
		doh.assertTrue(finder=='findByName');
		
	},
	tearDown : function() {
	}
},

{
	name : "InvoiceController, query with custom urlPath",
	timeout : 3000,
	setup:null,
	setUp : function() {
	},
	runTest : function() {
		var that=this;
		var deferred=new doh.Deferred();
		prefabware.test.runSetup().then(function(setup){
			var store=setup.invoiceStore;
			return store.query({id:1},{urlPath:'invoice/one'}).then(function(result){
				invoice=result[0];
				deferred.callback(true);
				return that;
			});
			
		});		
	},
	tearDown : function() {
	}
},
	
{
	name : "Invoice 1:n",
	timeout : 3000,
	setup:null,
	setUp : function() {
	},	
	runTest : function() {
		var test=this;
		var deferred=new doh.Deferred();
		prefabware.test.runSetup().then(function(setup){
			var invoiceType=setup.invoiceType;
			var invoiceLineType=setup.invoiceLineType;
			doh.assertTrue(invoiceType!=null);
			doh.assertTrue(invoiceLineType!=null);
			var store=setup.invoiceStore;
			doh.assertTrue(store!=null);
			var pResult=store.get(1);
			return pResult.then(function(result){
				doh.assertTrue(result!=null);
				var invoice=result;
				doh.assertTrue(invoice!=null);
				
				doh.assertTrue(invoice.pfw_id>0);
				doh.assertTrue(invoice.__isEntity);
				doh.assertTrue(invoice.__type.name===invoiceType.name);
				
				var lines=invoice.lines;
				doh.assertFalse(lines==null);
				doh.assertTrue(lines.__type.name===invoiceLineType.name);
				doh.assertTrue(dojo.isArray(lines));
				
				var clonedInvoice=invoice.__clone();
				doh.assertTrue(clonedInvoice.lines!=null);
				doh.assertTrue(dojo.isArray(clonedInvoice.lines));
				
				
				invoice.__resolveProxies().then(function(){
					doh.assertEqual(2,invoice.lines.length);
					invoice.lines[0].number++;
					//doh.assertEqual("",store.__toJson(invoice));
					store.put(invoice).then(function(updated) {
						doh.assertTrue(updated.pfw_id>0);
						doh.assertTrue(updated.__isEntity);
						doh.assertTrue(updated.__type===invoiceType);
					// TODO issue #17 should it really be incremented ?
					//	doh.assertEqual(invoice.version+1,updated.version);
						updated.__resolveProxies().then(function(){
							var line0=test.findInvoiceLine(updated.lines[0].pfw_id, invoice.lines);
							// TODO issue #17 should it really be incremented ?
							// doh.assertEqual(line0.version+1,updated.lines[0].version);
							deferred.callback(true);
						});
					});
				});
			});
		});
		return deferred;
	},
	findInvoiceLine : function(id,lines) {
		var found=dojo._base.array.filter(lines,function(line){
			return id==line.pfw_id;
		});
		if (found.length==1) {
			return found[0];
		}else{
		doh.assertTrue(false,'could not find line with id '+id);	
		}
	},
	tearDown : function() {
	}
},

]);