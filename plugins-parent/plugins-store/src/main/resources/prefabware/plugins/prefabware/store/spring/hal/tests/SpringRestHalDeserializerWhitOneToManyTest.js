dojo.provide("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithOneToManyTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("prefabware.plugins.prefabware.store.spring.SpringRestSerializer");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithOneToManyTest", [ 
{
	name : "SpringRestHalDeserializer",
	timeout : 500,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.hal.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			
			that.withStringType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.WithOneToMany');
			
			return that.storePlugin.createStore({type:that.withStringType}).then(function(store){
				that.withStringStore=store;
				return that;
			}).then(function(that){
				return that;
			});
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},		
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.withStringStore;
				return store.get(1).then(function(entity){
					doh.assertTrue(entity!=null);
					doh.assertEqual("withOneToMany1",entity.code);
					doh.assertTrue(entity.many.__isProxy);
					doh.assertEqual("http://localhost:8080/withOneToMany/1/many",entity.many.__reference.href);
					return entity.__resolveProxies().then(function(){
						dojo.isArray(entity.many)
						//the entity references 49 elements..
						//see TestData
						doh.assertTrue(entity.many.length=49);
						doh.assertTrue(entity.many[0].__isEntity);
						deferred.callback(true);
					});
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
}
]);