define({
  root: {
	 'DLT0001':{'title':'Deleted {entity}','text':'Deleted {entity} successfully'},
	 'DLT0002':{'title':'Delete {entity} failed !','text':'Could not delete {entity}'},
	 'DLT0003':{'title':'Confirm delete','text':'Do you really want to delete {entity} ?'},
	 'SAV0001':{'title':'Saved {entity}','text':'Saved {entity} successfully'},
	 'SAV0002':{'title':'Saved {entity} failed !','text':'Could not save {entity} !'},
  },
  de: true
});