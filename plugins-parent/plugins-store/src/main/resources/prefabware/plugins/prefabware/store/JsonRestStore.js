define(["dojo", 'dojo/_base/lang','dojo/_base/array','dojo/_base/Deferred'
], function(dojo, lang, array,Deferred ){

// module:
//		prefabware/plugins/prefabware/store/JsonRestStore

var JsonRestStore = function(/*Store*/ store,type,proxyFactory){
	//TODO this query function belongs into the EntityStore, because 
	//its responsible for converting the data into Entities, which must be applied
	//to all kinds of store , also MemoyStore !
	// enhances a JsonRestStore so that
	// - query does not contain null entitites
	// - query does return entities, not json-objects
	//store = lang.delegate(store);	
	this.type=type;
	var originalQuery = store.query;
	var originalPut = store.put;
	store.isJsonRestStore=true;//for test and debug
	store.put= function(object, directives){
		arguments[0]=object.__serializable();
		var result =originalPut.apply(this, arguments);
		return result.then(function(result){
			proxyFactory.createProxy(type,result);
			return result;
		});
	},
	store.query= function(query, options){
//moved to entity store		
//		if (options===undefined) {
//			options={};
//		}
//		//default sort for the key column of the entity
//		if (options.sort===undefined) {					
//			//no key, no sort
//			if (type.keyAttribute!=undefined) {
//				var attributeName=type.keyAttribute.name;
//				var descending=type.keyAttribute.isSortDescending;
//				options.sort=[{attribute:attributeName, descending: descending}];
//			}
//		}
		var originalResult =originalQuery.apply(this, arguments);
		var originalTotal=0;
		//the total is a promise itself and will be resolved, before the result is resolved
		Deferred.when(originalResult.total, function(total){
			originalTotal=total;
		});
		
		
		var then=function(result){
			var total=originalTotal;
			//filter out null elements
			var notNull = array.filter(result,function(element){
				return element!=null;
			});
			//wrap a proxy around every object
			array.forEach(notNull,function(item,index,array){
				//this runs when the result returns from the server
				//the caller of createViewStore may allready have references to the result array
				//so we  must not exchange the array as a whole, but just modify elements
				if (item!=null) {
					//just wrap a proxy around every entity
					proxyFactory.createProxy(type,item);
					return;
				}else{
					//this must be the additional empty entry, that was placed on the server
					//in a one-element array.
					//JERSEY in java serializes such an 1-element array not as an array
					//but as an object. to overcome this, we add a null entry at the end on the server, that must be removed here
					if (index==array.length-1) {
						array.splice(-1,1);
						return;
					}
				}
					});
			notNull.total=total;
			return notNull;
        };
        var newResult=Deferred.when(originalResult,then);
        return new dojo.store.util.QueryResults(newResult);
//        if (typeof result.then == "function") {	    	
//		}else{
//			return then(result);
//		};
	};
	return store;
};

lang.setObject("prefabware.plugins.prefabware.store.JsonRestStore", JsonRestStore);

return JsonRestStore;
});
