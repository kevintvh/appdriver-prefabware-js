//a extension for a memory store
define('prefabware/plugins/prefabware/store/ExtensionMemoryStore', [ 
       'dojo',
       'dojo/_base/lang',
       'dojo/_base/array',
       'dojo/store/Memory',
       "dojo/store/Observable",
       'prefabware/plugins/prefabware/store/MemoryReferenceProxy',
       'prefabware/plugins/prefabware/store/MemoryStore',
       'prefabware/plugins/prefabware/store/AsyncStore',
       'prefabware/plugins/prefabware/store/JsonRestStore',
       'prefabware/plugins/prefabware/store/ExtensionStore',
       'dojo/_base/declare' ], function(
		dojo, lang, array) {
	dojo.declare("prefabware.plugins.prefabware.store.ExtensionMemoryStore", [prefabware.plugins.prefabware.store.ExtensionStore], {
		id :null,
		modelPlugin:{inject:'prefabware.model',type:'plugin'}, 
		storePlugin:{inject:'prefabware.store',type:'plugin'}, 
		cacheCreateStore:false,//by default do not cache createStore for MemoryStores
		constructor : function(options) {
		},		
		startup : function() {
		},		
		createStore : function(options) {
			prefabware.lang.assert(options.type!=null);
			var memory = new dojo.store.Memory({data: this.__getData(options.type),idProperty: "pfw_id"});
			memory.__type=options.type;
			var store= prefabware.plugins.prefabware.store.MemoryStore(memory);
			var asyncStore= prefabware.plugins.prefabware.store.AsyncStore(store);
			var observable =  dojo.store.Observable(asyncStore);
			return observable;
			
		},		
		__getData : function(p_type) {
			//TODO move this to the store !
			//returns the data of this store
			//simple case : data is part of the json
			//TODO allow access to json files
			var that=this;
			var data= this.json.data;
			//make them entities
			return array.map(data,function(jsonEntity){
				var type=null;
				//type is not mandatory, if no type is found use the type of the store
				if (jsonEntity.pfw_type==null) {
					type=p_type;
				}else{
					type=that.modelPlugin.findType(jsonEntity.pfw_type);
				}
				var entity=type.create(jsonEntity);
				array.forEach(type.getQualifiedAttributes(),function(qa){
					var attr=qa.attribute;
					var jsonValue=jsonEntity[attr.name];
//					if(attr.isSystem){
//						//allready set, do nothing
//					}else
					if(jsonValue!=null&&attr.type.isEnumeration){
						//its a Enumeration set the value of the according enum constant
		    			entity[attr.name]=attr.type[jsonValue];	
					}else if (jsonValue!=null&&attr.isReference&&!attr.type.isValueType) {
						//its a Reference to another Entity
						var __reference={};						
							__reference.id=jsonValue.pfw_id;
							that.storePlugin.createStore({type:attr.type}).then(function(attstore){
							//the link does not include the id of the reference entity
							//its a special URL
							//http://localhost:8080/plugins-store/city/2/country
							__reference.store=attstore;
							//supply a value that can be used in put
							var proxy=new prefabware.plugins.prefabware.store.MemoryReferenceProxy(attr.type,__reference);
							var __clone=proxy.__clone;
							var refEntity=attr.type.create();//a reference proxy must be an entity also
							lang.mixin(proxy,refEntity); 
							proxy.__clone=__clone;
							entity[attr.name]=proxy;
						});
					} else{
					//a simple value
					entity[attr.name]=attr.convertFrom(jsonValue);
					}	
				});
				return entity;
			});
		},		
		
	});
});
