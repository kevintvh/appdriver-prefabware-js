//a service that provides a JsonRestStore
define('prefabware/plugins/prefabware/store/ExtensionStoreService', [ 
       'dojo', 
       'prefabware/plugins/prefabware/service/ExtensionService',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.store.ExtensionStoreService", [prefabware.plugins.prefabware.service.ExtensionService], {
		__createService : function() {
			var that=this;
			this.inherited(arguments);//creates this.service
			this.service.urlForType=function(type){
				return that.service.url + '/' + type.simpleNameFirstLetterLowerCase();
			};
		},		
	});
});
