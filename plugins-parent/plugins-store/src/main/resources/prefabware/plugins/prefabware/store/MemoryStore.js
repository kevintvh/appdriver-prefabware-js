define(["dojo", 'dojo/_base/lang','dojo/_base/array','prefabware/lang','dojo/store/util/SimpleQueryEngine'
], function(dojo, lang, array,SimpleQueryEngine ){

// module:
//		prefabware/plugins/prefabware/store/MemoryStore
// all methods here are sync. MemoryStores are normaly wrapped inside an AsyncStore
// that makes all methods pseudo async	
//TODO a lot of code is the same as in QueryStore

var MemoryStore = function(/*Store*/ store){
	// wraps and enhances a MemoryStore so that
	// - queries using placeholder '*' as value are understood
	// - a method clear() is available
	var originalQuery = store.query;
	var originalQueryEngine = store.queryEngine;
	var originalPut = store.put;
	var originalGet = store.get;
	store.isMemoryStore=true;//for test and debug

	store.queryEngine=function(oriQuery, options){
		//its important to intercept the store.queryEngine and not 
		// only store.query
		//reason : store.queryEngine.match is called directly by Observable
		//to find out if update listeners must be called or not
		//intercept the call and modify the query, than call the original function
		var query={};
		for (prop in oriQuery)
		  {
		//the SimpleQueryEngine does not understand values like '*' or 'A*' 
		//but automatically calls a method test on every query attribute if it exists	
		//so we convert the placeholder into an according test method here
		var pV=oriQuery[prop];//the value of the property in the query
		if (pV=='*') {
			//create a function test, that allways returns true
			query[prop]={test:function(value){return true;}};
		}  else if (prefabware.lang.endsWith(pV,'*')){
			//create a function test, that returns true when it matches generic
			query[prop]={test:function(value){
				//the property value without the appended star
				var woStar=pV.substring(0, pV.length-1);
				return prefabware.lang.startsWith(value,woStar);}
			};
	  }  else{
			  query[prop]=pV;
		  }
	};
	arguments[0]=query;	
	var result =originalQueryEngine.apply(this, arguments);
	return result;
	},
	
	//memory store does not support partial loading of entities, allways loads with all available data
	//would make no sense, since its an in memory store
	store.query= function(query, options){			
	var result =originalQuery.apply(this, arguments);
	//result is a QueryResult here withs forEach, filter etc. available on the promise
	result.forEach(function(entity){
		entity.__store=store;
	});
	return result ;
	};
	
	store.get = function(id, options){
		//options.properties = array of string with the names of properties to get
		//options.entity = refresh the given entity instead of returning a new instance
		if (options==null) {
			options={};
		}
		var entity=null;
		var gEntity =originalGet.apply(this, arguments);
		if (gEntity!=null) {
			if (options.entity!=null) {
				//use the given entity and refresh it instead returning a new one
				if (options.entity!==gEntity) {
					//only if its not allready the same instance, which its most of the time here
					lang.mixin(options.entity,gEntity);
				}
				entity=options.entity;
			}else{
				entity=gEntity;
			}
			entity.__store=store;
		}
		return entity;
	};			
	store.put = function(object, options){ 
		//returns the given object
		//assigns an id to new objects
		//reason the original dojo/store/Memory : 
		//returns the id of the given object
		//does not assign an id if the object is unsaved (id<0)
		object.__assignId();//assign an id, before put, so it can be indexed
		var id =originalPut.apply(this, arguments);		
		return object;
	};			
	store.clear = function() {			
		// rollback all changes made, else changed items cannot be
		// cleared
		if (this.revert) {
			//not all stores do support this
			this.revert();
		}
		// clear the data of the store
		var items = this.data;
		for ( var index = 0; index < items.length; index++) {
			var item = items[index];
			this.remove(item);					
		}
		return;
	};
	
	return store;
};
lang.setObject("prefabware.plugins.prefabware.store.MemoryStore", MemoryStore);
return MemoryStore;
});
