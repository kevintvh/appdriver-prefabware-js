dojo.provide("prefabware.plugins.prefabware.store.tests.memory.ReferenceTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.tests.memory.ReferenceTest", [ 
{
	name : "query MemoryStore",
	timeout:1000,
	registry : null,
	
	extensionPoint : null,
	extension : null,
	doSetup : function() {
		var that=this;
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.tests.reference',
			fileName : 'referenceTest-registry.json'
		});
		
		this.registry.startup().then(function(registry){
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var withReferenceType=setup.modelPlugin.findType('prefabware.store.tests.reference.WithNTo1Reference');
		setup.storePlugin.matchExtension(
				'prefabware.store.store'
				,{type:withReferenceType}).then(function(extension){
			doh.assertTrue(extension!=null);
			doh.assertEqual('prefabware.plugins.prefabware.store.ExtensionMemoryStore',extension.declaredClass);
			
			setup.storePlugin.createStore({type:withReferenceType}).then(function(store){
				doh.assertTrue(store.isMemoryStore,'store should be a MemoryStore');
				store.query({name:"with ref to first"}).then(function(results){
					doh.assertNotEqual(null,results);
					doh.assertEqual(1,results.length);
					
					doh.assertNotEqual(results.forEach,'QueryResult expected');
					
					var withReference=results[0];
					doh.assertTrue(withReference.__isEntity);
					
					var referenced=withReference.reference_n_1;
					doh.assertTrue(referenced.__isEntity);
					referenced.resolve().then(function(entity){
						doh.assertEqual('first',entity.name);
						doh.assertEqual('first',referenced.name);
						doh.assertTrue(entity.__isEntity);
						doh.assertTrue(referenced.__isResolved);
						testDeferred.callback(true);
					});
					
				});
			});
			
		});
		return testDeferred;
			},
	tearDown : function() {
	}
},
]);