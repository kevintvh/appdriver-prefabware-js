package com.prefabware.js.plugins.store;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.commons.maven.Maven;
import com.prefabware.js.commons.JsCommonsConfig;
import com.prefabware.js.inject.JsInjectConfig;
import com.prefabware.js.model.JsModelConfig;
import com.prefabware.js.plugin.JsPluginConfig;
import com.prefabware.js.plugins.model.JsPluginsModelConfig;
import com.prefabware.js.plugins.resource.JsPluginsResourceConfig;
import com.prefabware.js.plugins.server.JsPluginsServerConfig;
import com.prefabware.js.plugins.service.JsPluginsServiceConfig;
import com.prefabware.web.WebJar;
import com.prefabware.web.ResourceMappings.ResourceMapping;

@Configuration
@Import({		
		JsPluginsModelConfig.class,
		JsPluginsServiceConfig.class,
		JsPluginsServerConfig.class,
		JsPluginsResourceConfig.class,
		JsPluginConfig.class,
		JsInjectConfig.class,
		JsModelConfig.class,
		JsCommonsConfig.class})
public class JsPluginsStoreConfig {
	@Bean
	Maven jsPluginsStoreMaven() {
		return new Maven(this.getClass().getPackage().getName()+".filtered");
	}
	@Bean
	WebJar jsPluginsStoreWebJar(@Qualifier("jsPluginsStoreMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	 
	@Bean()
	ResourceMapping rmJsPluginsStoreWebJar(@Qualifier("jsPluginsStoreMaven") Maven m,@Qualifier("jsPluginsStoreWebJar") WebJar wj) {
		String path = m.getProperty("pfw.static.path");
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
	
}