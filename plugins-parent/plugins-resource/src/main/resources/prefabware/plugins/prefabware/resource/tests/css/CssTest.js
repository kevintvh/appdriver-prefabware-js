// when the test succeds you should lo see the tabs colored in orange
dojo.provide("prefabware.plugins.prefabware.resource.tests.css.CssTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugins.prefabware.resource.tests.css.CssTest", [ 
{
	name : "css test",
	timeout:1000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.resource.tests.css',fileName:'cssTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		var links=dojo.query("head link");
		var before=links.length;
		this.registry.startup().then(function(){
			var resourcePlugin=that.registry.findPlugin('prefabware.resource');
				resourcePlugin.loadCss('prefabware/plugins/prefabware/resource/tests/css/css.css');
				doh.assertEqual(before+1,dojo.query("head link").length);
				testDeferred.callback(true);
			});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);