// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.resource.tests.basic.BasicTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugins.prefabware.resource.tests.basic.BasicTest", [ 
{
	name : "basic test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.resource.tests.basic',fileName:'basicTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var resourcePlugin=that.registry.findPlugin('prefabware.resource');
			doh.assertEqual(resourcePlugin.declaredClass,"prefabware.plugins.prefabware.resource.Plugin");
			resourcePlugin.getLocalization('greeting','label').then(function(label){
				if (prefabware.lang.locale().indexOf('de')!=0) {
				doh.assertEqual("Hallo, Welt!",label);
				}else{
				prefabware.lang.warn("skipping prefabware.plugins.prefabware.resource.tests.basic.BasicTes,cause it only succeeds, if the default locale starts with 'de'");
				}
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);