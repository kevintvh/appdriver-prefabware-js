//a message to the user
define('prefabware/plugins/prefabware/resource/message/Message', [ 
       'dojo',
       'dojo/_base/lang',
       'prefabware/util/OptionsMixin',
       'dojo/_base/declare' ], function(dojo, lang) {
	dojo.declare("prefabware.plugins.prefabware.resource.message.Message", [ prefabware.util.OptionsMixin], {
		id:null,
		icon:null,
		title:null,
		text:null,
		msgParms:null,
		msgType:null,// 'sucess','warning','error'
		constructor : function(options) {
			this.mixinOptions(options);
		},		
		replace : function() {
			this.text= lang.replace(this.text, this.msgParms);
			this.title= lang.replace(this.title, this.msgParms);
			if (this.msgParms.reason!=undefined) {
				this.text=this.text+'<p>'+this.msgParms.reason;
			}
		},		
	});
});
