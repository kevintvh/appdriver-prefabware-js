//to load a dojo i18n resource
// see http://dojotoolkit.org/reference-guide/1.9/dojo/i18n.html
define('prefabware/plugins/prefabware/resource/ExtensionI18nResource', [ 
       'dojo',
       'prefabware/plugin/Extension',
       'prefabware/util/ResourceI18n',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.resource.ExtensionI18nResource", [prefabware.plugin.Extension], {
		namespace:null,//the namespace of the i18n resource
		name:null,//the name of the i18n resource
		locale:null,//the namespace of the i18n resource
		resource:null,//the i18n resource
		constructor : function(options) {
			this.namespace=options.json.namespace;
			this.name=this.matcher.value;// so the name does not be repeated in the json
			this.locale=options.json.locale;//optional
		},	
		startup : function() {
			// async
			var that=this;
			//locale=null menas use the browsers locale
			this.resource = new prefabware.util.ResourceI18n({namespace:that.namespace,name:this.name,locale:this.locale});
			return this.resource.startup();
		},
		getLocalization : function(key) {
			return this.resource.getLocalization(key);
		},
	});
});
