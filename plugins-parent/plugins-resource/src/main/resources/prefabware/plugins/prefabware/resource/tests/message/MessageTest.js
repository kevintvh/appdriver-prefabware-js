// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.prefabware.resource.tests.message.MessageTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.prefabware.resource.tests.message.MessageTest", [ 
{
	name : "message test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.resource.tests.message',fileName:'messageTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var resourcePlugin=that.registry.findPlugin('prefabware.resource');
			doh.assertEqual(resourcePlugin.declaredClass,"prefabware.plugins.prefabware.resource.Plugin");
			resourcePlugin.createMessage('UIM0001',{entity:'client 4711'}).then(function(message){
				if (prefabware.lang.locale().indexOf('de')==0) {
				doh.assertEqual("Konnte client 4711 nicht löschen.",message.text);
				doh.assertEqual("Löschen von client 4711 fehlgeschlagen !",message.title);
				}else{
					prefabware.lang.warn("skipping prefabware.plugins.prefabware.resource.tests.message.MessageTes,cause it only succeeds, if the default locale starts with 'de'");
				}
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);