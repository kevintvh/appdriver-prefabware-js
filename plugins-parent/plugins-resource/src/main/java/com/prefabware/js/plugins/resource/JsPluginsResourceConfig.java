package com.prefabware.js.plugins.resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.commons.maven.Maven;
import com.prefabware.js.commons.JsCommonsConfig;
import com.prefabware.js.inject.JsInjectConfig;
import com.prefabware.js.plugin.JsPluginConfig;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@Configuration
@Import({JsPluginConfig.class,JsInjectConfig.class,JsCommonsConfig.class})
public class JsPluginsResourceConfig {
	@Bean
	Maven jsPluginsResourceMaven() {
		return new Maven(this.getClass().getPackage().getName()+".filtered");
	}
	@Bean
	WebJar jsPluginsResourceWebJar(@Qualifier("jsPluginsResourceMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	
	@Bean
	ResourceMapping rmJsPluginsResourceWebJar(@Qualifier("jsPluginsResourceMaven") Maven m,@Qualifier("jsPluginsResourceWebJar") WebJar wj) {
		String path = m.getProperty("pfw.static.path");
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
}
